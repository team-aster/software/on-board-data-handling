# On Board Data Handling
The OBDH (On Board Data Handling) system contains the code that runs on the main PCB and provides
system state management, communication with the payload and groundstation, an attitude determination
and control system and sensor data acquisition and storage.

## Git repository
### Initialisation

This repository uses [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
Therefore, after the initial clone, the following command needs to be run in order to retrieve the
code in [communication](communication):
```shell
git submodule update --init
```

### Branches

The `main` branch is the default branch which contains the most stable version of the OBDH system.
It contains a tag called `pcb1/latest` which points to the latest commit that will work with version
1 of the PCB.
There are also some other branches:

* `cpu2` contains the code changes needed to adapt the project for the STM32D722VET processor.
* `feature/hardware_tests` contains a work in process version of tests that could be executed on
  the actual hardware to test the software.

## Code
The code of the OBDH can be found in the [`ASTER_STM_Software`](ASTER_STM_Software) folder.
See the [`README`](ASTER_STM_Software/README.md) for more information.

### IDE
The [`ASTER_STM_Software`](ASTER_STM_Software) folder can be loaded as a
[STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) project or as an embedded
[Clion](https://www.jetbrains.com/clion) project.

## Communication
The communication protocol between the OBDH and the ground station is defined in the
[`communication`](communication) folder.

## Documentation

Further documentation can be found in the [`documentation`](documentation) folder.
