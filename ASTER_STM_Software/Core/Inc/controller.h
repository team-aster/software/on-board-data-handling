//
// Created by Sebastian on 28.10.2020.
//

#pragma once

#include <stdint.h>
#include "generated/units.h"
#ifndef bool
typedef uint8_t bool;
#    define true 1
#    define false 0
#endif /* bool */

/**
 * The number of measurements that will be averaged together.
 */
#define CONTROLLER_AVERAGING_WINDOW 1

/** The configuration of a PID controller. */
typedef struct {
    /** The proportional gain of the controller. */
    double p;
    /** The integral gain of the controller. */
    double i;
    /** The derivative gain of the controller. */
    double d;
} pidConfig;

/** A PID controller for an axis. */
typedef struct {
    /** The controller configuration. */
    struct {
        /** Whether the controller should use the motor speed read by the sensors. */
        bool useMotorSpeed;
        /** The P, I and D configuration of the controller. */
        pidConfig pid;
    } config;
    /** The controller state. */
    struct {
        /** The error in the last controller step. */
        double previousError;
        /** The sum of all errors. */
        double errorSum;
        /** The current angular velocity of the motor on the axis this controller is controlling. */
        rpm_t angularVelocity;
        /** The last CONTROLLER_AVERAGING_WINDOW raw errors used for averaging */
        double previousRawErrors[CONTROLLER_AVERAGING_WINDOW];
        /**
         * Index of the next raw error to overwrite.
         * Will be pointing past the array in the beginning,
         * to signify that the values are not set yet.
         */
        uint8_t nexRawErrorIndex;
    } state;
} axisController;

/** An orientation controller for all three axis. */
typedef struct {
    /** The controller for the roll axis. */
    axisController rollController;
    /** The controller for the pitch axis. */
    axisController pitchController;
    /** The controller for the yaw axis. */
    axisController yawController;
} controller_t;

/**
 * Reset a controller.
 *
 * @param controller The controller to reset.
 * @param initialAngularVelocity The current angular velocity of the motor on the axis
 *                               the controller is controlling.
 * @param useMotorSpeed Whether or not to use the motor speed read by the sensors.
 * @param config The configuration of the controller.
 */
extern void resetController(axisController* controller, rpm_t initialAngularVelocity,
    const pidConfig* config, bool useMotorSpeed);

/**
 * Control the rotation on one axis.
 *
 * @param controller The controller to use for controlling.
 * @param error The current error from the target orientation or rotation speed.
 * @param timeDelta The time since the last control step in seconds.
 * @param motorSpeed The current speed of the motor on the axis in radians per second.
 * @return The target angular velocity of the motor that controls the axis.
 */
extern rpm_t controlAxis(axisController* controller, double error, sec_t timeDelta,
    rpm_t motorSpeed);

/**
 * A speed ramp that limits the amount of change to the motor speeds based on the current speed.
 *
 * @param demandedChange The demanded change of the speed.
 * @param currentSpeed The current speed of the motor.
 * @return The allowed speed change of the motor.
 */
extern rpm_t speedRamp(rpm_t demandedChange, rpm_t currentSpeed);
