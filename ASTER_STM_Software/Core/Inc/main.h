/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <cmsis_os2.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/** An error which occurred in the HAL base system. */
typedef enum {
    NO_SYSTEM_ERROR,
    ADC1_INIT_SYSTEM_ERROR,
    ADC2_INIT_SYSTEM_ERROR,
    ADC3_INIT_SYSTEM_ERROR,
    CRC_INIT_SYSTEM_ERROR,
    I2C1_INIT_SYSTEM_ERROR,
    I2C2_INIT_SYSTEM_ERROR,
    I2C3_INIT_SYSTEM_ERROR,
    IWDG_INIT_SYSTEM_ERROR,
    SPI2_INIT_SYSTEM_ERROR,
    SPI4_INIT_SYSTEM_ERROR,
    TIM4_INIT_SYSTEM_ERROR,
    TIM5_INIT_SYSTEM_ERROR,
    UART4_INIT_SYSTEM_ERROR,
    UART7_INIT_SYSTEM_ERROR,
    USART1_INIT_SYSTEM_ERROR,
    USART2_INIT_SYSTEM_ERROR,
    USART3_INIT_SYSTEM_ERROR,
    UART_DMA_INIT_SYSTEM_ERROR,
    ADC_DMA_INIT_SYSTEM_ERROR,
} SystemError;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

extern osThreadId_t defaultTaskHandle;
extern osThreadId_t controlTaskHandle;
extern osThreadId_t recoveryTaskHandle;
extern osThreadId_t adcsTaskHandle;
extern osThreadId_t sensorsTaskHandle;
extern osThreadId_t telecommandTaskHandle;
extern osThreadId_t telemetryTaskHandle;
extern osThreadId_t watchdogTaskHandle;

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

#define SELECT_ERROR_MACRO_(ignored1, ignored2, NAME, ...) NAME

#define ERROR_HANDLER_CALL() System_Error_Handler(__func__, __LINE__)

#define ERROR_HANDLER_DEFINITION(unused) System_Error_Handler(const char* function, int line)

#define Error_Handler(...) \
        SELECT_ERROR_MACRO_(ignored1, ##__VA_ARGS__, ERROR_HANDLER_DEFINITION, ERROR_HANDLER_CALL)()

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/**
 * @warning This function is not thread safe.
 * @return The last system error.
 */
extern SystemError getLastSystemError(void);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define configISR_STACK_SIZE_WORDS 0x0400
#define SDCARD1_SCK_Pin GPIO_PIN_2
#define SDCARD1_SCK_GPIO_Port GPIOE
#define SOE_SIGNAL_Pin GPIO_PIN_3
#define SOE_SIGNAL_GPIO_Port GPIOE
#define SDCARD1_NSS_Pin GPIO_PIN_4
#define SDCARD1_NSS_GPIO_Port GPIOE
#define SDCARD1_MISO_Pin GPIO_PIN_5
#define SDCARD1_MISO_GPIO_Port GPIOE
#define SDCARD1_MOSI_Pin GPIO_PIN_6
#define SDCARD1_MOSI_GPIO_Port GPIOE
#define SODS_SIGNAL_Pin GPIO_PIN_13
#define SODS_SIGNAL_GPIO_Port GPIOC
#define CAM_TRIGGER_Pin GPIO_PIN_14
#define CAM_TRIGGER_GPIO_Port GPIOC
#define LO_SIGNAL_Pin GPIO_PIN_15
#define LO_SIGNAL_GPIO_Port GPIOC
#define GREEN_LED1_Pin GPIO_PIN_0
#define GREEN_LED1_GPIO_Port GPIOH
#define BLUE_LED2_Pin GPIO_PIN_1
#define BLUE_LED2_GPIO_Port GPIOH
#define PITCH_MOTOR_SPEED_Pin GPIO_PIN_0
#define PITCH_MOTOR_SPEED_GPIO_Port GPIOC
#define YAW_MOTOR_SPEED_Pin GPIO_PIN_1
#define YAW_MOTOR_SPEED_GPIO_Port GPIOC
#define ROLL_MOTOR_SPEED_Pin GPIO_PIN_2
#define ROLL_MOTOR_SPEED_GPIO_Port GPIOC
#define ALTIMETER_Pin GPIO_PIN_3
#define ALTIMETER_GPIO_Port GPIOC
#define PITCH_MOTOR_CURRENT_Pin GPIO_PIN_0
#define PITCH_MOTOR_CURRENT_GPIO_Port GPIOA
#define YAW_MOTOR_CURRENT_Pin GPIO_PIN_1
#define YAW_MOTOR_CURRENT_GPIO_Port GPIOA
#define ROLL_MOTOR_CURRENT_Pin GPIO_PIN_2
#define ROLL_MOTOR_CURRENT_GPIO_Port GPIOA
#define THERMAL_BATTERY_Pin GPIO_PIN_3
#define THERMAL_BATTERY_GPIO_Port GPIOA
#define RED_LED5_Pin GPIO_PIN_4
#define RED_LED5_GPIO_Port GPIOA
#define THERMAL_PDU_PCB_Pin GPIO_PIN_5
#define THERMAL_PDU_PCB_GPIO_Port GPIOA
#define THERMAL_MOTOR_CONTROLLER_Pin GPIO_PIN_6
#define THERMAL_MOTOR_CONTROLLER_GPIO_Port GPIOA
#define THERMAL_MOTOR_BRACKET_Pin GPIO_PIN_7
#define THERMAL_MOTOR_BRACKET_GPIO_Port GPIOA
#define THERMAL_RECOVERY_WALL_Pin GPIO_PIN_4
#define THERMAL_RECOVERY_WALL_GPIO_Port GPIOC
#define BATTERY_STATUS_Pin GPIO_PIN_5
#define BATTERY_STATUS_GPIO_Port GPIOC
#define IRIDIUM_CTS_Pin GPIO_PIN_0
#define IRIDIUM_CTS_GPIO_Port GPIOB
#define THERMAL_RETENTION_WALL_Pin GPIO_PIN_1
#define THERMAL_RETENTION_WALL_GPIO_Port GPIOB
#define TESTING_ENABLED_Pin GPIO_PIN_9
#define TESTING_ENABLED_GPIO_Port GPIOE
#define BLUE_LED6_Pin GPIO_PIN_12
#define BLUE_LED6_GPIO_Port GPIOE
#define EXT_POWER_CONNECTED_Pin GPIO_PIN_14
#define EXT_POWER_CONNECTED_GPIO_Port GPIOE
#define IMU1_SCL_Pin GPIO_PIN_10
#define IMU1_SCL_GPIO_Port GPIOB
#define IMU1_SDA_Pin GPIO_PIN_11
#define IMU1_SDA_GPIO_Port GPIOB
#define SDCARD2_NSS_Pin GPIO_PIN_12
#define SDCARD2_NSS_GPIO_Port GPIOB
#define SDCARD2_SCK_Pin GPIO_PIN_13
#define SDCARD2_SCK_GPIO_Port GPIOB
#define SDCARD2_MISO_Pin GPIO_PIN_14
#define SDCARD2_MISO_GPIO_Port GPIOB
#define SDCARD2_MOSI_Pin GPIO_PIN_15
#define SDCARD2_MOSI_GPIO_Port GPIOB
#define RXSM_TX_Pin GPIO_PIN_8
#define RXSM_TX_GPIO_Port GPIOD
#define RXSM_RX_Pin GPIO_PIN_9
#define RXSM_RX_GPIO_Port GPIOD
#define SET_PITCH_MOTOR_SPEED_Pin GPIO_PIN_13
#define SET_PITCH_MOTOR_SPEED_GPIO_Port GPIOD
#define SET_YAW_MOTOR_SPEED_Pin GPIO_PIN_14
#define SET_YAW_MOTOR_SPEED_GPIO_Port GPIOD
#define SET_ROLL_MOTOR_SPEED_Pin GPIO_PIN_15
#define SET_ROLL_MOTOR_SPEED_GPIO_Port GPIOD
#define IMU2_SDA_Pin GPIO_PIN_9
#define IMU2_SDA_GPIO_Port GPIOC
#define IMU2_SCL_Pin GPIO_PIN_8
#define IMU2_SCL_GPIO_Port GPIOA
#define GPS_TX_Pin GPIO_PIN_9
#define GPS_TX_GPIO_Port GPIOA
#define GPS_RX_Pin GPIO_PIN_10
#define GPS_RX_GPIO_Port GPIOA
#define IRIDIUM_RTS_Pin GPIO_PIN_15
#define IRIDIUM_RTS_GPIO_Port GPIOA
#define IRIDIUM_TX_Pin GPIO_PIN_10
#define IRIDIUM_TX_GPIO_Port GPIOC
#define IRIDIUM_RX_Pin GPIO_PIN_11
#define IRIDIUM_RX_GPIO_Port GPIOC
#define PITCH_MOTOR_ENABLE_Pin GPIO_PIN_0
#define PITCH_MOTOR_ENABLE_GPIO_Port GPIOD
#define YAW_MOTOR_ENABLE_Pin GPIO_PIN_1
#define YAW_MOTOR_ENABLE_GPIO_Port GPIOD
#define ROLL_MOTOR_ENABLE_Pin GPIO_PIN_3
#define ROLL_MOTOR_ENABLE_GPIO_Port GPIOD
#define BATTERY_RESET_Pin GPIO_PIN_7
#define BATTERY_RESET_GPIO_Port GPIOD
#define BATTERY_ENABLE_Pin GPIO_PIN_3
#define BATTERY_ENABLE_GPIO_Port GPIOB
#define RECOVERY_TRIGGER_Pin GPIO_PIN_4
#define RECOVERY_TRIGGER_GPIO_Port GPIOB
#define ROLL_MOTOR_DIRECTION_Pin GPIO_PIN_5
#define ROLL_MOTOR_DIRECTION_GPIO_Port GPIOB
#define YAW_MOTOR_DIRECTION_Pin GPIO_PIN_0
#define YAW_MOTOR_DIRECTION_GPIO_Port GPIOE
#define PITCH_MOTOR_DIRECTION_Pin GPIO_PIN_1
#define PITCH_MOTOR_DIRECTION_GPIO_Port GPIOE
void   MX_GPIO_Init(void);
void   MX_DMA_Init(void);
void   MX_ADC1_Init(void);
void   MX_ADC2_Init(void);
void   MX_ADC3_Init(void);
void   MX_CRC_Init(void);
void   MX_I2C1_Init(void);
void   MX_I2C2_Init(void);
void   MX_I2C3_Init(void);
void   MX_IWDG_Init(void);
void   MX_SPI2_Init(void);
void   MX_SPI4_Init(void);
void   MX_TIM4_Init(void);
void   MX_TIM5_Init(void);
void   MX_UART4_Init(void);
void   MX_UART7_Init(void);
void   MX_USART1_UART_Init(void);
void   MX_USART2_UART_Init(void);
void   MX_USART3_UART_Init(void);
/* USER CODE BEGIN Private defines */
#define MALLOCS_INSIDE_ISRs 1
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
