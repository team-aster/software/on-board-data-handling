#pragma once

#include <stdint.h>
#include "generated/units.h"
#include "objects/data.h"


/**
 * The semaphore that can be uses in conjunction with the getBatch_* functions to
 * access GPS values in bulk.
 */
extern osSemaphoreId_t gpsSemaphore;

/**
 * The latitudinal coordinate of the FFU in degrees.
 */
EXPORT_THREAD_SAFE_VAR(deg_t, gpsLatitude);

/**
 * The longitudinal coordinate of the FFU in degrees.
 */
EXPORT_THREAD_SAFE_VAR(deg_t, gpsLongitude);

/**
 * The height of the FFU above the ellipsoid as determined by GPS in meters.
 */
EXPORT_THREAD_SAFE_VAR(meter_t, gpsAltitude);

/**
 * The GPS time received in the last GPS message.
 */
EXPORT_THREAD_SAFE_VAR(ms_t, gpsTime);

/**
 * Initialize the GPS data.
 *
 * @return Whether the initialization was successful or not.
 */
extern bool initGpsData(void);
