#pragma once

#include <stdint.h>
#include "generated/sharedTypes.h"
#include "objects/data.h"


/**
 * A reason of the last device restart.
 */
typedef enum
#ifdef __cplusplus
    : uint8_t
#endif
{
    /**
     * The device restart was triggered byy the hardware watchdog.
     */
    HARDWARE_WATCHDOG_TRIGGERED_RESTART = 0,

    /**
     * The device restarted because the software watchdog detected a freeze in the control thread.
     */
    CONTROL_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the software watchdog detected a freeze in the recovery thread.
     */
    RECOVERY_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the software watchdog detected a freeze in the ADCS thread.
     */
    ADCS_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the software watchdog detected a freeze in the sensors thread.
     */
    SENSORS_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the software watchdog
     * detected a freeze in the telecommand thread.
     */
    TELECOMMAND_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the software watchdog detected a freeze in the telemetry thread.
     */
    TELEMETRY_THREAD_FREEZE_RESTART,

    /**
     * The device restarted because the interrupts were taking too long to complete.
     */
    INTERRUPT_STALL_RESTART,

    /**
     * The device restarted because there was a hard fault.
     */
    HARD_FAULT_RESTART,

    /**
     * The device restarted because there was a memory fault.
     */
    MEMORY_FAULT_RESTART,

    /**
     * The device restarted because there was a bus fault.
     */
    BUS_FAULT_RESTART,

    /**
     * The device restarted because there was a usage fault.
     */
    USAGE_FAULT_RESTART,

    /**
     * The device restarted because of a stack overflow in an unknown thread.
     */
    STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the idle thread.
     */
    IDLE_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the control thread.
     */
    CONTROL_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the recovery thread.
     */
    RECOVERY_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the ADCS thread.
     */
    ADCS_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the sensors thread.
     */
    SENSORS_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the telecommand thread.
     */
    TELECOMMAND_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the telemetry thread.
     */
    TELEMETRY_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because of a stack overflow in the watchdog thread.
     */
    WATCHDOG_THREAD_STACK_OVERFLOW_RESTART,

    /**
     * The device restarted because the FreeRtos scheduler exited.
     */
    SCHEDULER_EXIT_RESTART,

    /**
     * The device restarted because the power to the device was cut off.
     *
     * @note The value of this enum must be 2**(8 * sizeof(POWER_RESTART) - 1).
     *       This is so that the default state has all the bits set.
     *       The reason for doing this is that setting a 0 to a 1 requires an erase of the flash
     *       memory while setting a 1 to a 0 does not. By having the default state as all bits set
     *       an erase can be avoided when setting the value in critical situations.
     *       (e.g. hard fault handler and stack overflow handler)
     */
    POWER_RESTART = 0xFF,
} RestartReason;

/**
 * Whether or not flight mode is enabled.
 */
EXPORT_THREAD_SAFE_VAR(bool, flightMode);

/**
 * The current state of the system state machine.
 */
EXPORT_THREAD_SAFE_VAR(SystemState, systemState);

/**
 * Whether or not the parachute deployment has been initiated.
 */
EXPORT_THREAD_SAFE_VAR(bool, parachuteDeploymentWasStarted);

/**
 * Whether or not the parachute has been deployed.
 */
EXPORT_THREAD_SAFE_VAR(bool, parachuteIsDeployed);

/**
 * Whether or not the system has landed on the ground after deployment.
 */
EXPORT_THREAD_SAFE_VAR(bool, haveLanded);

/** Begin a batch access operation on the flash variables.
 * If this function returns true, it is save to us the getBatch_* and setBatch_* functions
 * of the flash variables. The batch operation must be closed by a call to endFlashBatchAccess.
 * During a batch operation, no other thread can access the flash variables.
 * @see endFlashBatchAccess
 *
 * @return Whether or not a batched access was initiated successfully.
 */
extern bool beginFlashBatchAccess(void);

/**
 * Finish a batch access operation. If any changes were made to the flash variables,
 * they are going to be written to flash after this function returns.
 * @see beginFlashBatchAccess
 */
extern void endFlashBatchAccess(void);

/**
 * Reset the current restart reason.
 *
 * @note This function is not thread safe.
 */
extern void resetRestartReason(void);

/**
 * Set the reason for the last (or current) restart.
 * For this function to work correctly the resetRestartReason function must have been called before.
 * This function is save to be used from fault and stack overflow handlers handlers.
 *
 * @note This function is not thread save and it will only work correctly
 *       if the currently stored restart reason is POWER_RESTART.
 * @param reason The restart reason.
 */
extern void setRestartReason(RestartReason reason);

/**
 * @return The reason for the last system restart.
 */
extern RestartReason getRestartReason(void);

/**
 * Initialize the flash data.
 *
 * @return Whether the initialization was successful or not.
 */
extern bool initDataFlash(void);

/**
 * @return Whether or not the flash data is corrupted.
 */
extern bool isFlashCorrupted(void);

/**
 * @return A live view of the current configuration.
 *         The object will be mutated, if the configuration changes.
 */
extern const Configuration* getConfig(void);

/**
 * Start an update to the configuration. To persist the changes, commitConfigUpdate must be called.
 * @see commitConfigUpdate
 *
 * @return The live configuration object that can be changed or NULL on failure.
 */
extern Configuration* beginConfigUpdate(void);

/**
 * Finish and persist the changes to the configuration.
 * @see beginConfigUpdate
 */
extern void commitConfigUpdate(void);
