#pragma once

#include <stdint.h>
#include "generated/units.h"
#include "objects/data.h"


/**
 * The semaphore that can be uses in conjunction with the getBatch_* functions to
 * access sensor values in bulk.
 */
extern osSemaphoreId_t sensorSemaphore;

/**
 * The temperature of the CPU.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, processorTemperature);

/**
 * The temperature read from the thermal sensor which close to the battery.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, batteryTemperature);

/**
 * The temperature read from the thermal sensor which close to the PDU PCB.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, pduPcbTemperature);

/**
 * The temperature read from the thermal sensor which close to the motor controllers.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, motorControllerTemperature);

/**
 * The temperature read from the thermal sensor which close to the motor bracket.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, motorBracketTemperature);

/**
 * The temperature read from the thermal sensor which close to the retention wall.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, retentionWallTemperature);

/**
 * The temperature read from the thermal sensor which close to the recovery wall.
 */
EXPORT_THREAD_SAFE_VAR(celsius_t, recoveryWallTemperature);

/**
 * The current environmental air pressure in bar.
 */
EXPORT_THREAD_SAFE_VAR(bar_t, pressure);

/**
 * The current output voltage of the battery in volts.
 */
EXPORT_THREAD_SAFE_VAR(volt_t, batteryLevel);

/**
 * The temperature readings of the primary IMU in °C.
 */
EXPORT_THREAD_SAFE_VAR(celsiusInt8_t, primaryImuTemperature);

/**
 * The temperature readings of the secondary IMU in °C.
 */
EXPORT_THREAD_SAFE_VAR(celsiusInt8_t, secondaryImuTemperature);

/**
 * The current accelerometer measurements of the primary IMU in meters per second squared (m/(s^2)).
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, primaryImuAccelerometer);

/**
 * The current magnetometer measurements of the primary IMU in micro tesla.
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, primaryImuMagnetometer);

/**
 * The current accelerometer measurements of the
 * secondary IMU in meters per second squared (m/(s^2)).
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, secondaryImuAccelerometer);

/**
 * The current magnetometer measurements of the secondary IMU in micro tesla.
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, secondaryImuMagnetometer);

/**
 * The current gyroscope measurements of the secondary IMU in radians per seconds.
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, secondaryImuRotation);

/**
 * The current orientation calculated by the IMU2 using sensor fusion.
 */
EXPORT_THREAD_SAFE_VAR(Quaternion, imu2Quaternions);

/**
 * The current drawn by the motors in ampere.
 */
EXPORT_THREAD_SAFE_VAR(Vec3D, motorCurrent);

/**
 * Initialize the sensor data.
 *
 * @return Whether the initialization was successful or not.
 */
extern bool initSensorData(void);
