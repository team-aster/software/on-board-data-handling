#pragma once

#include <stdint.h>
#include "globals.h"
#include "generated/sharedTypes.h"
#include "objects/data.h"
#include "objects/ringBuffer.h"


/**
 * The semaphore that can be uses in conjunction with the getBatch_* functions to
 * access adcs values in bulk.
 */
extern osSemaphoreId_t adcsSemaphore;

/**
 * Whether or not the motors are enabled.
 */
EXPORT_THREAD_SAFE_VAR(Vec3Uint32, motorEnabled);

/**
 * A list of motor speeds demanded by the control loop.
 */
EXPORT_TYPED_RING_BUFFER(Vec3Int32WithTime, demandedMotorSpeeds);

/**
 * The list of the motors speeds in rpm.
 */
EXPORT_TYPED_RING_BUFFER(Vec3FWithTime, motorSpeeds);

/**
 * The list of rotation measurements of the primary IMU in radians per seconds.
 */
EXPORT_TYPED_RING_BUFFER(Vec3FWithTime, rotationMeasurements);

/**
 * Initialize the adcs data.
 *
 * @return Whether the initialization was successful or not.
 */
extern bool initAdcsData(void);
