#pragma once

#include <stdint.h>
#include "globals.h"
#include "objects/data.h"
#include "generated/sharedTypes.h"
#include "objects/communication/iridium.h"


EXPORT_THREAD_SAFE_VAR(uint8_t, system_ejected); // TODO: Either remove or use

EXPORT_THREAD_SAFE_VAR(uint8_t, system_LO_state);

EXPORT_THREAD_SAFE_VAR(uint8_t, system_SOE_state);

EXPORT_THREAD_SAFE_VAR(uint8_t, system_SODS_state);

EXPORT_THREAD_SAFE_VAR(uint8_t, system_parachute_state);

EXPORT_THREAD_SAFE_VAR(uint8_t, system_recovery_state);

/**
 * The current state of the Iridium network communication.
 */
EXPORT_THREAD_SAFE_VAR(IridiumState, systemIridiumState);

/**
 * Initialize the system data.
 *
 * @return Whether the initialization was successful or not.
 */
extern bool initSystemData(void);

/**
 * Get the next telecommand in the queue.
 *
 * @param telecommand Space for the telecommand.
 * @return Whether the telecommand was successfully accessed and removed from the queue.
 */
extern bool getNextTelecommand(TelecommandData* telecommand);

/**
 * Add a telecommand to the queue.
 *
 * @param telecommand The telecommand to add to the queue.
 * @return Whether or not the telecommand was successfully added to the queue.
 */
extern bool enqueueTelecommand(const TelecommandData* telecommand);
