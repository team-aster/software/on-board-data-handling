#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stm32f7xx_hal.h>
#include <globals.h>
#include <generated/sharedTypes.h>


/** The UART interface that is used for GPS communication. */
#define gpsUart huart1
extern UART_HandleTypeDef gpsUart;

#if GPS_ENABLED

/**
 * Initialize the GPS communication.
 *
 * @return Whether the initialization was successful.
 */
extern bool initGps(void);

/**
 * Configure the GPS chip.
 *
 * @return Whether the configuration was successful.
 */
extern bool configureGps(void);

/**
 * @return Whether or not the GPS configuration was completed successfully.
 */
extern bool gpsConfigurationCompleted(void);

/**
 * Called when the new data is available on the Gps connection.
 */
extern void onGpsDataReceived(void);

/**
 * Called when the Gps communication generates an error interrupt.
 */
extern void onGpsError(void);

#endif /* GPS_ENABLED */
