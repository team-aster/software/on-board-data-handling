#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stm32f7xx_hal.h>
#include "generated/sharedTypes.h"


/** The UART interface that is used for RXSM communication. */
#define rxsmUart huart3
extern UART_HandleTypeDef rxsmUart;

/**
 * Initialize the communication via RXSM.
 *
 * @return Whether or not the initialization was successful.
 */
extern bool initRXSM(void);

/**
 * Send data via the RXSM connection.
 * Buffers the data in a queue if sending is not possible at the moment.
 *
 * @param data The data to send via RXSM.
 * @param dataLength The size of the data in bytes.
 * @return Whether or not the data was send or enqueued successfully.
 */
extern bool writeToRXSM(const void* data, size_t dataLength);

/**
 * Called when sending of the current buffer completed on the RXSM connection.
 */
extern void onRXSMSendCompleted(void);

/**
 * Called when the new data is available on the RXSM connection.
 */
extern void onRXSMDataReceived(void);

/**
 * Called when the RXSM communication generates an error interrupt.
 */
extern void onRXSMError(void);
