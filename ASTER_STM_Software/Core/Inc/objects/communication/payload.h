#pragma once

#include <stdint.h>
#include <stddef.h>
#include "generated/sharedTypes.h"


/**
 * Initialize communication with the payload.
 *
 * @return Whether the initialization was successful.
 */
extern bool initPayloadCommunication(void);

/**
 * Send the given data to the payload.
 *
 * @param data The data to send.
 * @param dataLength The size of the data in bytes.
 * @return Whether or not the data was send successfully.
 */
extern bool writeToPayload(const void* data, size_t dataLength);

/**
 * Enable the recording of video by the payload.
 */
extern void enablePayloadVideoRecording(void);

/**
 * Disable the recording of video by the payload.
 */
extern void disablePayloadVideoRecording(void);

/**
 * Initiate a shutdown of the payload.
 */
extern void powerOffPayload(void);
