#pragma once

#include <stm32f7xx_hal.h>
#include "globals.h"
#include "generated/sharedTypes.h"


/** The UART interface the Iridium chip is connected to. */
#define iridiumUart huart4
extern UART_HandleTypeDef iridiumUart;

/** Possible states of the Iridium communication. */
typedef enum {
    /** Iridium communication is unavailable. */
    IRIDIUM_UNAVAILABLE,
    /**
     * Iridium communication is possible, but no connection
     * to the Iridium network is established yet.
     */
    IRIDIUM_NO_SIGNAL,
    /** A connection to the Iridium network has been established. */
    IRIDIUM_CONNECTED
} IridiumState;

#if IRIDIUM_ENABLED

/**
 * Start the initiation of the communication with the Iridium chip.
 * This implicitly enables the communication, when the Iridium chip was found.
 *
 * @return Whether or not the initialization was started successful.
 */
extern bool startIridiumInitialization(void);

/**
 * Enable or disable the Iridium communication.
 *
 * @param enabled Whether or not the communication should be enabled.
 * @return Whether or not the request was send successfully.
 */
extern bool enableIridiumCommunication(bool enabled);

/**
 * Init the Iridium UART communication.
 *
 * @return Whether or not the initialization was successful.
 */
extern bool initIridiumUart(void);

/**
 * Send a message trough the Iridium network.
 *
 * @param message The message to send.
 * @param messageSize The size of the message to send in bytes.
 * @return Whether or not the send was initialized successfully.
 */
extern bool sendIridiumMessage(const void* message, size_t messageSize);

/**
 * Callback when the Iridium chip sends data.
 */
extern void onIridiumInterrupt(void);

/**
 * Called when the Iridium chip generates an error interrupt.
 */
extern void onIridiumError(void);

#endif /* IRIDIUM_ENABLED */
