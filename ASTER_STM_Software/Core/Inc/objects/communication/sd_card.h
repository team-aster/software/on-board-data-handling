#pragma once

#include <stdint.h>


/**
 * Initialize all connected SD-Cards, mount them, creates new files and open them for writing.
 *
 * @return Whether or not at least one SD-Card was initialized successfully.
 */
bool initializeSDCardCommunication(void);

/**
 * Write some data to the log file of all open SD-Cards.
 * The data is not flushed to the log file.
 *
 * @param data The data to write.
 * @param size The size of the data in bytes.
 * @return If the data was written successfully to at least one SD-Card.
 */
extern bool writeToSdCards(const void* data, size_t size);

/**
 * @return Whether or not there is at least one initialized SD-Card.
 */
extern bool haveInitializedSdCards(void);

/**
 * @return Whether the SD-Card subsystem is initialized already.
 */
extern bool isSdCardSubsystemInitialized(void);

/**
 * Close the current log files on all attached SD-Cards and open new log files.
 */
extern void switchToNewLogFile(void);

/**
 * Flush the write cache of all opened log files to the SD-Card.
 */
extern void flushLogFiles(void);

/**
 * Try to reinitialize SD-Cards which are errored.
 */
extern void reinitializeErroredSdCards(void);
