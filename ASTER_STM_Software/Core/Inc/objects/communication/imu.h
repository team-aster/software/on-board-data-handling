/**
 * Interface to the two BNO055 IMUs.
 *
 * The IMUs have a gyroscope, an accelerometer, a magnetometer and temperature sensors.
 * They support a raw and fused mode:
 * - In raw mode, the raw sensor values are available.
 * - In fusion mode, the IMUs will use data from all sensors to automatically calibrate them.
 *   In this mode, an orientation is also available.
 *
 * Coordinate System:
 * - The X axis points to the top of the pcb. (SD-Cards and D-Sub connectors)
 * - The Y axis points to the right of the pcb. (Motor controllers)
 * - The Z axis points out of the front of the pcb. (Processor side)
 */

#pragma once

#include "generated/sharedTypes.h"
#include "globals.h"


#if IMU_ENABLED

/**
 * All connected IMUs.
 */
typedef enum { IMU1 = 1, IMU2 } IMU;


/**
 * Initialize the communication with the IMUs.
 *
 * @return Whether the initialization was successful.
 */
extern bool initImuCommunication(void);

/**
 * Initialize the IMU for measuring.
 *
 * @param imu The IMU to initialize.
 * @param enableFusion Whether to enable fusion mode.
 * @return Whether the initialization was successful.
 */
extern bool initIMU(IMU imu, bool enableFusion);

/**
 * This function checks the calibration, error, and system status of the specified IMU.
 *
 * @param imu The IMU to check.
 * @return false if the status could not be read or it indicates an error state.
 */
extern bool checkImuStatus(IMU imu);

/**
 * Read the rotation speed from the IMU.
 *
 * @param imu The IMU to read the rotation speed from.
 * @param rotation A buffer to write the rotation speed to.
 * @return Whether the speed was read successfully.
 */
extern bool readRotationSpeed(IMU imu, Vec3D* rotation);

/**
 * Read the acceleration from the IMU.
 *
 * @param imu The IMU to read the acceleration from.
 * @param acceleration A buffer to write the acceleration to.
 * @return Whether the acceleration was read successfully.
 */
extern bool readAcceleration(IMU imu, Vec3D* acceleration);

/**
 * Read the magnetic force from the IMU.
 *
 * @param imu The IMU to read the magnetic force from.
 * @param rotation A buffer to write the magnetic force to.
 * @return Whether the magnetic force was read successfully.
 */
extern bool readMagneticForce(IMU imu, Vec3D* magneticForce);

/**
 * Read the orientation from the IMU. Fusion mode must be enable don the IMU.
 *
 * @param imu The IMU to read the orientation from.
 * @param rotation A buffer to write the orientation to.
 * @return Whether the orientation was read successfully.
 */
extern bool readOrientation(IMU imu, Quaternion* orientation);

/**
 * Read the temperature from the IMU.
 *
 * @param imu The IMU to read the temperature from.
 * @param temperature A buffer to write the temperature to.
 * @return Whether the temperature was read successfully.
 */
extern bool readTemperature(IMU imu, int8_t* temperature);

/**
 * Disable the IMU. It will no longer take measurements and use less power.
 *
 * @param imu The IMU to disable.
 * @return Whether the operation succeeded.
 */
extern bool disableImu(IMU imu);

/**
 * Calibrate the sensors of the IMU.
 *
 * @param imu The IMU to calibrate.
 * @return Whether the calibration was successful.
 */
extern bool calibrate(IMU imu);

#endif /* IMU_ENABLED */
