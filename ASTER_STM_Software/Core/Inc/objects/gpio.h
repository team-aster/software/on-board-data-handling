#pragma once

#include <stdint.h>
#include <stm32f7xx_hal.h>
#include "main.h"


/**
 * A pin that can be accessed form the CPU.
 */
typedef struct {
    /**
     * The pin number.
     */
    uint16_t number;

    /**
     * The pin port.
     */
    GPIO_TypeDef* port;
} pin_t;

/**
 * The possible types of actions for a pin.
 */
typedef enum {
    /**
     * The pin will be pulled to ground.
     */
    LOW,

    /**
     * The pin will be pulled to high.
     */
    HIGH,

    /**
     * The pin will be toggles. Low will become high and vice versa.
     */
    TOGGLE,
} PinMode;

/**
 * This pin is HIGH when the board is in testing mode.
 */
#define TESTING_JUMPER_PIN ((pin_t) {TESTING_ENABLED_Pin, TESTING_ENABLED_GPIO_Port})

/**
 * This pin controls whether the roll motor is enabled.
 */
#define ROLL_MOTOR_ENABLE_PIN ((pin_t) {ROLL_MOTOR_ENABLE_Pin, ROLL_MOTOR_ENABLE_GPIO_Port})

/**
 * This pin controls the direction of the roll motor. LOW is forward, HIGH is backwards.
 */
#define ROLL_MOTOR_DIRECTION_PIN \
    ((pin_t) {ROLL_MOTOR_DIRECTION_Pin, ROLL_MOTOR_DIRECTION_GPIO_Port})

/**
 * This pin controls whether the pitch motor is enabled.
 */
#define PITCH_MOTOR_ENABLE_PIN ((pin_t) {PITCH_MOTOR_ENABLE_Pin, PITCH_MOTOR_ENABLE_GPIO_Port})

/**
 * This pin controls the direction of the pitch motor. LOW is forward, HIGH is backwards.
 */
#define PITCH_MOTOR_DIRECTION_PIN \
    ((pin_t) {PITCH_MOTOR_DIRECTION_Pin, PITCH_MOTOR_DIRECTION_GPIO_Port})

/**
 * This pin controls whether the yaw motor is enabled.
 */
#define YAW_MOTOR_ENABLE_PIN ((pin_t) {YAW_MOTOR_ENABLE_Pin, YAW_MOTOR_ENABLE_GPIO_Port})

/**
 * This pin controls the direction of the yaw motor. LOW is forward, HIGH is backwards.
 */
#define YAW_MOTOR_DIRECTION_PIN ((pin_t) {YAW_MOTOR_DIRECTION_Pin, YAW_MOTOR_DIRECTION_GPIO_Port})

/**
 * This pin goes LOW to indicate that the Rocket is launching.
 */
#define LO_SIGNAL_PIN ((pin_t) {LO_SIGNAL_Pin, LO_SIGNAL_GPIO_Port})

/**
 * This pin goes LOW to indicate the "Start Of Experiment" signal.
 */
#define SOE_SIGNAL_PIN ((pin_t) {SOE_SIGNAL_Pin, SOE_SIGNAL_GPIO_Port})

/**
 * This pin goes LOW to indicate the "Start Of Data Storage" signal.
 */
#define SODS_SIGNAL_PIN ((pin_t) {SODS_SIGNAL_Pin, SODS_SIGNAL_GPIO_Port})

/**
 * This pin controls the deployment of the parachutes. A high value deploys the parachute.
 */
#define RECOVERY_CUTTER_TRIGGER_PIN ((pin_t) {RECOVERY_TRIGGER_Pin, RECOVERY_TRIGGER_GPIO_Port})

/**
 * This pin allows to read the Clear To Send signal from the Iridium chip. (Active low)
 */
#define IRIDIUM_CTS_PIN ((pin_t) {IRIDIUM_CTS_Pin, IRIDIUM_CTS_GPIO_Port})

/**
 * This pin sets the Request To Send signal to the Iridium chip. (Active low)
 */
#define IRIDIUM_RTS_PIN ((pin_t) {IRIDIUM_RTS_Pin, IRIDIUM_RTS_GPIO_Port})

/**
 * This pin triggers the start of video recording on the GoPro cameras.
 */
#define GOPRO_TRIGGER_PIN ((pin_t) {CAM_TRIGGER_Pin, CAM_TRIGGER_GPIO_Port})

/**
 * This pin controls the blue LED6 in the front that indicates the control thread is running.
 */
#define STATUS_LED_PIN ((pin_t) {BLUE_LED6_Pin, BLUE_LED6_GPIO_Port})

/**
 * This pin controls the blue LED2 in the back that indicates the control thread is running.
 */
#define STATUS2_LED_PIN ((pin_t) {BLUE_LED2_Pin, BLUE_LED2_GPIO_Port})

/**
 * This pin controls the red error status LED5 in the back.
 */
#define ERROR_INDICATOR_LED_PIN ((pin_t) {RED_LED5_Pin, RED_LED5_GPIO_Port})
/**
 * This pin controls the green LED1 in the back that indicates that the watchdog thread is running.
 */
#define WATCHDOG_STATUS_LED_PIN ((pin_t) {GREEN_LED1_Pin, GREEN_LED1_GPIO_Port})

/**
 * This pin switches the latch to enable power from the battery.
 */
#define BATTERY_ENABLE_PIN ((pin_t) {BATTERY_ENABLE_Pin, BATTERY_ENABLE_GPIO_Port})

/**
 * This pin resets the latch to disable power from the battery.
 */
#define BATTERY_RESET_PIN ((pin_t) {BATTERY_RESET_Pin, BATTERY_RESET_GPIO_Port})

/**
 * This pin is HIGH if the external power is connected.
 */
#define EXT_POWER_CONNECTED_PIN ((pin_t) {EXT_POWER_CONNECTED_Pin, EXT_POWER_CONNECTED_GPIO_Port})

/**
 * The NSS pin (inverted chip select, LOW is selected) for the first Sd-Card.
 */
#define SDCARD1_NSS_PIN ((pin_t) {SDCARD1_NSS_Pin, SDCARD1_NSS_GPIO_Port})

/**
 * The NSS pin (inverted chip select, LOW is selected) for the second Sd-Card.
 */
#define SDCARD2_NSS_PIN ((pin_t) {SDCARD2_NSS_Pin, SDCARD2_NSS_GPIO_Port})

/**
 * Initialize the GPIO system.
 */
extern void initGPIO(void);

/**
 * Read the current mode of a pin.
 *
 * @param pin The pin to read.
 * @return The current state of the pin, either HIGH or LOW.
 */
extern PinMode getPin(pin_t pin);

/**
 * Set the mode of a pin.
 *
 * @param pin The pin to change.
 * @param mode The desired mode of the pin.
 */
extern void setPin(pin_t pin, PinMode mode);
