#pragma once

#include <stdint.h>
#include <cmsis_os2.h>
#include "threads/telemetry.h"


/**
 * Declare a new thread safe variable.
 * This should not be used directly, but it provides additional hooks to run
 * during read and update operations of the variable.
 *
 * @param type: The type of the thread safe variable.
 * @param name: The name of the thread safe variable.
 * @param semaphore: The semaphore that is used to make access to the variable thread safe.
 * @param getter: A getter to retrieve the current value of the variable.
 * @param setter: A setter to update the value of the variable.
 */
#define THREAD_SAFE_VAR_IMP(type, name, semaphore, getter, setter)                               \
    bool get_##name(type* value) {                                                               \
        if (osSemaphoreAcquire(semaphore, 0U) == osOK ||                                         \
            osSemaphoreAcquire(semaphore, 10U) == osOK) {                                        \
            *value = getter;                                                                     \
            osSemaphoreRelease(semaphore);                                                       \
            return true;                                                                         \
        }                                                                                        \
        return false;                                                                            \
    }                                                                                            \
    bool set_##name(const type* value) {                                                         \
        if (osSemaphoreAcquire(semaphore, 0U) == osOK ||                                         \
            osSemaphoreAcquire(semaphore, 10U) == osOK) {                                        \
            setter;                                                                              \
            osSemaphoreRelease(semaphore);                                                       \
            return true;                                                                         \
        }                                                                                        \
        return false;                                                                            \
    }                                                                                            \
    type getBatch_##name() {                                                                     \
        ASSERT(osSemaphoreGetCount(semaphore) == 0, "Called %s but %s was not locked", __func__, \
            #semaphore);                                                                         \
        return getter;                                                                           \
    }                                                                                            \
    void setBatch_##name(const type* value) {                                                    \
        ASSERT(osSemaphoreGetCount(semaphore) == 0, "Called %s but %s was not locked", __func__, \
            #semaphore);                                                                         \
        setter;                                                                                  \
    }


/**
 * Declare a new thread safe variable. This will generate a thread safe setter and getters
 * for accessing the variable, as well as two functions for accessing multiple thread safe
 * variables with an already locked semaphore.
 *
 * @param type: The type of the thread safe variable.
 * @param name: The name of the thread safe variable.
 * @param initValue: The initial value of the thread safe variable.
 * @param semaphore: The semaphore that is used to make access to the variable thread safe.
 */
#define THREAD_SAFE_VAR(type, name, initValue, semaphore) \
    static type name = initValue;                         \
    THREAD_SAFE_VAR_IMP(type, name, semaphore, name, name = *value)

/**
 * Export a thread save variable.
 *
 * @param type: The type of the thread safe variable that is exported.
 * @param name: The name of the thread safe variable that is exported.
 */
#define EXPORT_THREAD_SAFE_VAR(type, name)     \
    extern bool get_##name(type* value);       \
    extern bool set_##name(const type* value); \
    extern type getBatch_##name(void);         \
    extern void setBatch_##name(const type* value)

/**
 * Initialize the data subsystems.
 *
 * @return Whether the subsystems were initialized successfully.
 */
extern bool initData(void);
