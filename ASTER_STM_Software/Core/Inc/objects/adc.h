/**
 * The purpose of adc object is to provide interfaces and
 * initialization of analog to digital converters.
 */
#pragma once

#include <stdint.h>
#include <stm32f7xx_hal.h>
#include "generated/units.h"
#include "generated/sharedTypes.h"

/**
 * An analog to digital converter channel.
 */
typedef enum {
    // The order and value of these enum values must be corresponding to their peripheral and rank.
    // E.g. the first one is the first used channel on ADC1.
    PITCH_MOTOR_CURRENT_ADC = 0,
    YAW_MOTOR_CURRENT_ADC,
    ROLL_MOTOR_CURRENT_ADC,
    PITCH_MOTOR_SPEED_ADC,
    YAW_MOTOR_SPEED_ADC,
    ROLL_MOTOR_SPEED_ADC,
    BATTERY_STATUS_ADC,
    PROCESSOR_TEMPERATURE_ADC,

    BATTERY_TEMPERATURE_ADC,
    PDU_PCB_TEMPERATURE_ADC,
    MOTOR_CONTROLLER_TEMPERATURE_ADC,
    MOTOR_BRACKET_TEMPERATURE_ADC,
    RETENTION_WALL_TEMPERATURE_ADC,
    RECOVERY_WALL_TEMPERATURE_ADC,

    /**
     * The ADC channel of the pressure sensor.
     */
    PRESSURE_SENSOR_ADC,

    /**
     * The last ADC on the ADC1 peripheral.
     */
    LAST_ADC1 = PROCESSOR_TEMPERATURE_ADC,

    /**
     * The last ADC on the ADC2 peripheral.
     */
    LAST_ADC2 = RECOVERY_WALL_TEMPERATURE_ADC,

    /**
     * The last ADC on the ADC2 peripheral.
     */
    LAST_ADC3 = PRESSURE_SENSOR_ADC,

    /**
     * The last ADC.
     */
    LAST_ADC = LAST_ADC3,
} Adc;

/**
 * The maximum value of an ADC measurement, because the measurement mode is 12 bits.
 * @see MX_ADC1_Init, MX_ADC2_Init and MX_ADC3_Init.
 */
#define MAX_ADC_VALUE 4095U

/**
 * The maximum voltage measured by the ADC (corresponding to MAX_ADC_VALUE) in Volts.
 */
#define MAX_ADC_MEASUREMENT 3.3

/**
 * Initialize analog to digital converters.
 *
 * @return Whether the initialization succeeded or not.
 */
extern bool initADC(void);

/**
 * Access the current measured value from the specified ADC.
 * The value will be in the range from 0 to MAX_ADC_VALUE.
 *
 * @param adc The analog to digital converter to query.
 * @return The current measurement from the ADC.
 */
extern uint16_t getAdcValueRaw(Adc adc);

/**
 * Access the current measured value from the specified ADC.
 * The value will be in the range from 0 to MAX_ADC_MEASUREMENT in volt.
 *
 * @param adc The analog to digital converter to query.
 * @return The current measurement from the ADC in volt.
 */
extern volt_t getAdcValue(Adc adc);
