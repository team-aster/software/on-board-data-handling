#pragma once

#include "globals.h"
#include "generated/units.h"
#include "generated/sharedTypes.h"


/**
 * Measure the current pressure from the pressure sensor.
 *
 * @return The current pressure.
 */
extern bar_t readPressure(void);

/**
 * Get the current rotations speeds of the FFU in radians per seconds.
 *
 * @param rotation A buffer to save the current rotations.
 * @return Whether or not the rotations could be read.
 */
extern bool readRotationSpeedFromPrimaryImu(Vec3D* rotation);

#if IMU_ENABLED == 1

/**
 * Initialize the IMUs.
 *
 * @warning This function will block for some time.
 * @return Whether or not at least one initialization succeeded and a primary IMU was selected.
 */
extern bool initIMUs(void);

/**
 * Calibrate the IMUs.
 */
extern void calibrateIMUs(void);

/**
 * Check the status of the IMUs and try to re-initialize currently failed IMUs.
 */
extern void checkIMUs(void);

/**
 * Read and update the relevant measured sensor values form all IMUs.
 */
extern void updateImuData(void);
#endif /* IMU_ENABLED */

/**
 * Read the current of the motors and store them.
 */
extern void updateMotorCurrents(void);

/**
 * Read the current speed of the motor.
 *
 * @param speeds The current speed of the motors in radians per second.
 */
extern void readMotorsSpeed(Vec3D* speeds);

/**
 * Read and update the current voltage level of the battery.
 *
 * @return The voltage level of the battery.
 */
extern volt_t readBatteryLevel(void);

/**
 * Read and update the measured temperature values.
 */
extern void updateTemperatureData(void);
