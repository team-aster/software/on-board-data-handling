#pragma once

#include <stdint.h>

/**
 * Calculate the three byte parity for a Reed Solomon (16, 13) encoding
 * of the data in the given buffer.
 * This allows the recipient of the data to detect 24 bits and correct up to 12 bits of error.
 *
 * @param buffer The buffer containing the 13 bytes data.
 * @param parity A buffer where the 3 bytes parity should be written to.
 */
extern void calculateParity16_13(const void* buffer, uint8_t* parity);

/**
 * Calculate the four byte parity for a Reed Solomon (32, 28) encoding
 * of the data in the given buffer.
 * This allows the recipient of the data to detect 32 bits and correct up to 16 bits of error.
 *
 * @param buffer The buffer containing the 28 bytes data.
 * @param parity A buffer where the 4 bytes parity should be written to.
 */
extern void calculateParity32_28(const void* buffer, uint8_t* parity);
