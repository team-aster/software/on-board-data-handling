#pragma once

#include <stdint.h>
#include <sys/types.h>
#include <stm32f7xx_hal.h>
#include "generated/sharedTypes.h"


/**
 * Initialize the communication subsystems.
 *
 * @return Whether any critical subsystem couldn't be initialized successfully.
 */
extern bool initCommunication(void);
