#pragma once

#include <stdint.h>
#include <stm32f7xx_hal.h>
#include "generated/units.h"
#include "generated/sharedTypes.h"

/**
 * The maximum rotations per minute the motor supports.
 */
#define MOTOR_MAX_RPM 4200

/**
 * The minimum rotations per minute the motor supports.
 */
#define MOTOR_MIN_RPM (-MOTOR_MAX_RPM)

/**
 * The different motors that can be controlled.
 */
typedef enum {
    /**
     * The motor that controls the roll rotation of the FFU.
     */
    MOTOR_ROLL = TIM_CHANNEL_4,

    /**
     * The motor that controls the pitch rotation of the FFU.
     * This controls the vertical axis (axis of parachute) of the FFU.
     */
    MOTOR_PITCH = TIM_CHANNEL_2,

    /**
     * The motor that controls the yaw rotation of the FFU.
     */
    MOTOR_YAW = TIM_CHANNEL_3,
} Motor;

/**
 * Initialize the motors.
 *
 * @return Whether the operation was successful or not.
 */
extern bool initMotors(void);

/**
 * Enable the motors.
 *
 * @return Whether or enabling the motors was successful.
 */
extern bool enableMotors(void);

/**
 * Disable the motors. This does not actively break the motors.
 */
extern void disableMotors(void);

/**
 * Set the target speed of the specific motor.
 *
 * @param motor The motor to change the target speed.
 * @param value The new target speed.
 */
extern void setMotor(Motor motor, pwm_t value);

/**
 * Set the speed of the motors.
 *
 * @param motorRoll The speed of the motor that controls the roll in rpm.
 * @param motorPitch The speed of the motor that controls the pitch in rpm.
 * @param motorYaw The speed of the motor that controls the yaw in rpm.
 */
extern void setMotors(rpm_t motorRoll, rpm_t motorPitch, rpm_t motorYaw);
