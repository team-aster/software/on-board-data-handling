#pragma once
#include "generated/units.h"
#include "generated/sharedTypes.h"

/**
 * @return Whether the function is called inside of an interrupt service handler.
 */
extern bool isInInterrupt(void);

/**
 * @return The time in milliseconds since the system booted.
 */
extern ms_t getTimeSinceBoot(void);

/**
 * Sleep until the specified time.
 *
 * @param wakeupTime The time in milliseconds since boot when the task should be resumed.
 */
extern void sleepUntil(ms_t wakeupTime);
