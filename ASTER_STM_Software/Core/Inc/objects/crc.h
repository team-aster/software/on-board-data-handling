#pragma once

#include <stdint.h>
#include "generated/sharedTypes.h"


/**
 * Initialize the CRC hardware.
 *
 * @return Whether or not the hardware initialization was successful.
 */
extern bool initCRC();

/**
 * Calculate the CRC16 checksum from the block of data.
 *
 * @param data The data to calculate the checksum from.
 * @param size The size of the data block in bytes.
 * @return The checksum of the data block.
 */
extern uint16_t calculateChecksum(const void* data, size_t size);
