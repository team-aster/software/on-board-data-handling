#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stm32f7xx_hal.h>
#include "generated/sharedTypes.h"


/**
 * A ring buffer.
 */
typedef struct {
    /**
     * Reference to the actual memory of the buffer.
     */
    uint8_t* memory;
    /**
     * The size of the actual memory of the buffer.
     */
    size_t size;
    /**
     * The current index into the buffer of the first unread byte.
     */
    size_t readIndex;

    /**
     * The current index into the buffer to the next free byte.
     */
    size_t writeIndex;
} ringBuffer_t;

/**
 * A reference to a memory region in a ring buffer.
 */
typedef struct {
    /**
     * The ring buffer this region belongs to.
     */
    ringBuffer_t* buffer;
    /**
     * The start index of the memory region.
     */
    uint8_t start;
    /**
     * The length of the memory region in bytes.
     */
    uint8_t length;
} rbRegionRef_t;

/**
 * Define and initialize a ring buffer.
 *
 * @param name The name of the ring buffer.
 * @param bufferSize The size of the ring buffer in bytes.
 */
#define DEFINE_RING_BUFFER(name, bufferSize) \
    static uint8_t name##Memory[bufferSize]; \
    static ringBuffer_t name = {             \
        .memory = name##Memory,              \
        .size = (bufferSize),                \
        .readIndex = 0,                      \
        .writeIndex = 0,                     \
    }

/**
 * Define a ring buffer that contains only elements of a certain type.
 * This generates some typed accessor functions for the buffer.
 *
 * @param type The type of the elements in the ring buffer.
 * @param name The name of the ring buffer.
 * @param bufferSize The size of the ring buffer in elements.
 */
#define DEFINE_TYPED_RING_BUFFER(type, name, bufferSize)                                        \
    DEFINE_RING_BUFFER(name, (bufferSize) * sizeof(type));                                      \
    void append_##name(const type* data, size_t dataSize) {                                     \
        rbAppend(&(name), (const void*) data, dataSize * sizeof(type));                         \
    }                                                                                           \
    size_t get_##name(type* data, size_t dataSize) {                                            \
        return rbRead(&(name), data, dataSize * sizeof(type)) / sizeof(type);                   \
    }                                                                                           \
    bool peekFirst_##name(type* data) {                                                         \
        size_t writeIndex = (name).writeIndex;                                                  \
        size_t readIndex = (name).readIndex;                                                    \
        size_t availableBytes = writeIndex >= readIndex ? writeIndex - readIndex :              \
                                                          (name).size - readIndex + writeIndex; \
        if (availableBytes < sizeof(type)) {                                                    \
            return false;                                                                       \
        }                                                                                       \
        rbRegionRef_t region;                                                                   \
        rbGetRegion(&(name), &region, readIndex, sizeof(type));                                 \
        rbMemCpy(&region, data, sizeof(type));                                                  \
        return true;                                                                            \
    }

/**
 * Export a ringbuffer created with DEFINE_TYPED_RING_BUFFER.
 *
 * @param type The type of the elements in the ring buffer.
 * @param name The name of the ring buffer.
 */
#define EXPORT_TYPED_RING_BUFFER(type, name)                      \
    extern void append_##name(const type* data, size_t dataSize); \
    extern size_t get_##name(type* data, size_t dataSize);        \
    extern bool peekFirst_##name(type* data)

/**
 * Check if the part of the contents of the ring buffer starts with the given string.
 *
 * @param region The region to compare against.
 * @param string A string to compare against.
 * @return 1 if region starts with the string, 0 otherwise.
 */
#define RING_BUFFER_STR_STARTSWITH(region, string) rbMemEqual(region, string, strlen(string))

/**
 * Compare a string with a part of the contents of the ring buffer.
 *
 * @param region The region to compare against.
 * @param string A string to compare against.
 * @return 1 if the string and the region match, 0 otherwise.
 */
#define RING_BUFFER_STR_EQUAL(region, string) \
    (strlen(string) == (region)->length && rbMemEqual(region, string, strlen(string)))


/**
 * @param index An index into the ring buffer.
 * @param offset An offset to the index.
 * @return The absolute index from the given start index and the offset.
 */
#define RELATIVE_RING_BUFFER_INDEX(buffer, index, offset)             \
    ((index) + (offset) < 0 ? (buffer)->size - ((index) + (offset)) : \
                              ((index) + (offset)) % (buffer)->size)

/**
 * Initialize a reference to a region in the ring buffer.
 *
 * @param buffer The ring buffer.
 * @param region The region reference to initialize.
 * @param start The start index of the region.
 * @param length The length of the region.
 */
extern void rbGetRegion(ringBuffer_t* buffer, rbRegionRef_t* region, size_t start, size_t length);

/**
 * Check if the memory region in the ring buffer and the given reference are equal.
 *
 * @param region The memory region in the ring buffer to compare against.
 * @param reference The reference to compare against.
 * @param length The length of the reference and memory region.
 * @return Whether the contents of the memory regions match.
 */
extern bool rbMemEqual(const rbRegionRef_t* region, const void* reference, uint8_t length);

/**
 * Copy a memory region from the ring buffer.
 *
 * @param region The region of the ring buffer to copy.
 * @param destination The copy destination. Must be at least as large as the memory region.
 * @param length The amount of bytes to copy.
 */
extern void rbMemCpy(const rbRegionRef_t* region, void* destination, uint8_t length);

/**
 * Read data from the ring buffer into the provided buffer and advance the read index.
 *
 * @param buffer The ring buffer to read from.
 * @param data A buffer to receive the read data from the ring buffer.
 * @param dataSize The maximum size that should be read from the buffer.
 * @return The number of available bytes that can/could be read.
 */
extern size_t rbRead(ringBuffer_t* buffer, const void* data, size_t dataSize);

/**
 * Append the memory to the ring buffer.
 *
 * @param buffer The ring buffer to append to.
 * @param data The data to append to the ring buffer. Can be NULL to just advance the write index.
 * @param dataSize The size if the data buffer to append.
 */
extern void rbAppend(ringBuffer_t* buffer, const void* data, size_t dataSize);

/**
 * Update the write index of the given buffer from the DMA reader.
 *
 * @param buffer The ring buffer whose write index  should be updated.
 * @param reader The reader from which the new write index should be calculated.
 * @return The new write index.
 */
extern size_t updateWriteIndexFromDmaReader(ringBuffer_t* buffer, const DMA_HandleTypeDef* reader);

/**
 * Advance the ring buffers read index by the given offset.
 *
 * @param buffer The buffer whose read index to advance.
 * @param offset The number of bytes to advance the read index by.
 * @return The number of bytes left for reading in the buffer after the advance.
 */
extern size_t rbAdvanceReadIndex(ringBuffer_t* buffer, size_t offset);
