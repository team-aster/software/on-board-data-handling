/**
 * Execution configuration for all the different threads.
 */

#pragma once

#include "generated/units.h"

/*** ADCS ***/

/** The time in milliseconds between the execution of each ADCS thread loop iteration. */
#define THREAD_ADCS_PERIOD ms_t(100)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_ADCS_WATCHDOG_TIMEOUT ms_t(THREAD_ADCS_PERIOD.ms * 2)

/*** Control ***/

/** The time in milliseconds between the execution of each CONTROL thread loop iteration. */
#define THREAD_CONTROL_PERIOD ms_t(250)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_CONTROL_WATCHDOG_TIMEOUT ms_t(THREAD_CONTROL_PERIOD.ms * 2)

/*** Recovery ***/

/** The time in milliseconds between the execution of each RECOVERY thread loop iteration. */
#define THREAD_RECOVERY_PERIOD ms_t(100)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_RECOVERY_WATCHDOG_TIMEOUT ms_t(THREAD_RECOVERY_PERIOD.ms * 2)

/*** Sensors ***/

/** The time in milliseconds between the execution of each SENSORS thread loop iteration. */
#define THREAD_SENSORS_PERIOD ms_t(100)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_SENSORS_WATCHDOG_TIMEOUT ms_t(THREAD_SENSORS_PERIOD.ms * 2)

/*** Telecommand ***/

/** The time in milliseconds between the execution of each TELECOMMAND thread loop iteration. */
#define THREAD_TELECOMMAND_PERIOD ms_t(500)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_TELECOMMAND_WATCHDOG_TIMEOUT ms_t(THREAD_TELECOMMAND_PERIOD.ms * 2)

/*** Telemetry ***/

/** The time in milliseconds between the execution of each TELEMETRY thread loop iteration. */
#define THREAD_TELEMETRY_PERIOD ms_t(250)
/**
 * The maximum time in milliseconds between updates from the thread before triggering the watchdog.
 */
#define THREAD_TELEMETRY_WATCHDOG_TIMEOUT ms_t(THREAD_TELEMETRY_PERIOD.ms * 4)

/*** Watchdog ***/

/** The time in milliseconds between the execution of each WATCHDOG thread loop iteration. */
#define THREAD_WATCHDOG_PERIOD ms_t(100)

/**
 * The maximum time in milliseconds the watchdog thread is allowed to take for one iteration.
 *
 * Because the watchdog is the thread with the highest priority, it should always be able to
 * maintain its period with height precision. The only other reason it would hit this timeout
 * (apart from a bug in FreeRTOS) are the interrupts, which must have a higher priority than
 * all threads. Therefore, if this timeout is hit, it is very likely caused by an interrupt
 * taking unexpectedly long to complete.
 */
#define INTERRUPT_DELAY_TIMEOUT ms_t(200)
