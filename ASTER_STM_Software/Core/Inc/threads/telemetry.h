#pragma once

#include <stdint.h>
#include <stddef.h>
#include "generated/sharedTypes.h"
#include "globals.h"


#ifdef __GNUC__
#    define PRINTF_LIKE(formatIndex, argsIndex) \
        __attribute__((format(printf, formatIndex, argsIndex)))
#else
#    define PRINTF_LIKE(formatIndex, argsIndex)
#endif /* __GNUC__ */

#if defined(NDEBUG) || !defined(DEBUG)
#    define ASSERT(condition, message, ...) ((void) 0)
#else
/**
 * Check that the condition evaluates to true. Otherwise print the message.
 * This function does *not* abort the thread if the condition is false.
 *
 * @param condition The condition to check.
 * @param message The message which is displayed in case the assertion fails.
 * @param ... Additional parameters for the message.
 */
#    define ASSERT(condition, message, ...)                                                     \
        do {                                                                                    \
            if (!(condition)) {                                                                 \
                printCriticalError("ASSERT FAILED: %s:%d: %s", __FILE__, __LINE__, #condition); \
                printCriticalError(message, ##__VA_ARGS__);                                     \
            }                                                                                   \
        } while (0)
#endif

/**
 * A way of transmitting the telemetry.
 */
typedef enum {
    /**
     * The telemetry is transmitted via the RXSM connection.
     */
    RXSM_TELEMETRY = 0b001U,

    /**
     * The telemetry is send to the payload.
     */
    PAYLOAD_TELEMETRY = 0b010U,

    /**
     * The telemetry is written to the SD-Cards.
     */
    SD_CARD_TELEMETRY = 0b100U,
} TelemetryTransmissionChannel;


/**
 * Initialize the printing subsystem. After returning, the print functions can be used.
 *
 * @return Whether or not the initialization was successful.
 */
extern bool initPrintSubsystem(void);

/**
 * Signal to the telemetry thread that a telemetry write can be attempted.
 */
extern void signalTelemetryWriteReady(void);

/**
 * Send the telemetry data via the RXSM connection, to the payload and write it to the SD card.
 * Buffers the data in a queue if sending / writing is not possible at the moment.
 *
 * @param data The telemetry data to send / write.
 * @param dataLength The size of the data in bytes.
 * @return Whether or not the data was send / written or enqueued successfully.
 */
extern bool writeTelemetry(const void* data, size_t dataLength);

/**
 * Run the telemetry thread.
 * Sends telemetry, checks the SD-Cards and handles printing.
 *
 * @param argument Unused.
 */
extern noreturn void StartTelemetryTask(void* argument);

/**
 * Enables or disables a specific telemetry transmission type.
 *
 * @warning This function is not thread save and should only ever be called from the control thread.
 * @param type The transmission type(s) to enable or disable.
 * @param enabled Whether or not the transmission type(s) should be enabled or disabled.
 */
extern void setTelemetryEnabled(TelemetryTransmissionChannel type, bool enabled);

/**
 * Sends the formatted message to the ground station.
 *
 * @param format The format string.
 * @param ... Format values.
 */
extern void print(const char* format, ...) PRINTF_LIKE(1, 2);

/**
 * Sends the formatted debug message to the ground station.
 * The message is only send if the testing pin is set.
 *
 * @param format The format string.
 * @param ... Format values.
 */
extern void printDebug(const char* format, ...) PRINTF_LIKE(1, 2);

/**
 * Writes the formatted error message to the ground station.
 *
 * @param format The format string.
 * @param ... Format values.
 */
extern void printError(const char* format, ...) PRINTF_LIKE(1, 2);

/**
 * Writes the formatted critical error message to the ground station.
 * Also turns on the error indicator LED.
 *
 * @param format The format string.
 * @param ... Format values.
 */
extern void printCriticalError(const char* format, ...) PRINTF_LIKE(1, 2);

/**
 * @note This value does not necessarily include all messages and is also subject to overflows.
 *       This was done deliberately to reduce the runtime overhead of the counter.
 * @return The number of error messages since the last boot.
 */
extern uint8_t getErrorCounter(void);
