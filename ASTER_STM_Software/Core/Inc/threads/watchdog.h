#pragma once

#include <stdint.h>
#include "globals.h"
#include "data/flash.h"


/**
 * Initialize the watchdog subsystem.
 *
 * @return Whether or not the initialization was successful.
 */
extern bool initWatchdog(void);

/**
 * Update the hardware watchdog.
 *
 * @return Whether or not the update was successful.
 */
extern bool updateHardwareWatchdog(void);

    /**
 * Run the watchdog thread.
 *
 * @param argument Unused.
 */
extern noreturn void StartWatchdogTask(void* argument);

/**
 * Refreshes the watchdog timer for the calling thread.
 */
extern void refreshWatchdogTimer(void);

/**
 * Reset the processor.
 *
 * @param reason The reason for the reset.
 */
extern noreturn void internalSoftwareReset(RestartReason reason);

/**
 * Reset the processor.
 *
 * @note The ACKNOWLEDGE_RESET macro must be defined when calling this function.
 *
 * @param reason The reason for the reset.
 */
#define softwareReset(resetReason)                                                                        \
    do {                                                                                       \
        static_assert(ACKNOWLEDGE_RESET, "softwareReset called without acknowledging the CPU " \
                                         "reset (define ACKNOWLEDGE_RESET)");                  \
        internalSoftwareReset(resetReason);                                                               \
    } while (0)

/**
 * @return The value for the hardware watchdog reload register.
 */
extern uint32_t getWatchdogReloadValue(void);
