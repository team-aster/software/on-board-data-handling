#pragma once

#include "globals.h"
#include "generated/units.h"
#include "generated/sharedTypes.h"


/**
 * Set the pwm that is commanded if the ADCS is in MODE_PWM_DEBUG mode.
 * @param value The new pmw that will be send to the motors.
 */
extern void setDebugPWM(pwm_t value);

/**
 * Set the rpm that is commanded if the ADCS is in MODE_RPM_DEBUG mode.
 * @param value The new rpm that will be send to the motors.
 */
extern void setDebugRPM(rpm_t value);

/**
 * Request to change the mode of the ADCS system. This can also be used to disable the ADCS.
 *
 * @warning There is a race condition if this is called while a mode transition is already in
 *          progress or if another mode is requested before the this request was processed.
 * @param newMode The new mode that the ADCS system should be in.
 */
extern void requestAdcsMode(AdcsMode newMode);

/**
 * @return The current mode of the ADCS system.
 */
extern AdcsMode getAdcsMode(void);

/**
 * Run the ADCS thread.
 * This thread is in charge of the motors, running the controllers
 * and gathering rotation measurements.
 *
 * @param argument Unused.
 */
extern noreturn void StartAdcsTask(void* argument);
