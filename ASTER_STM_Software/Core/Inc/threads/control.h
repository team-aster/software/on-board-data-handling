#pragma once

#include "globals.h"
#include "generated/units.h"
#include "generated/sharedTypes.h"

/**
 * Run the control thread, which executes the system state machine.
 *
 * @param argument Unused.
 */
extern noreturn void StartControlTask(void* argument);

/**
 * Initialize the system.
 */
extern void initSystem(void);

/**
 * Request a change of the system state.
 *
 * @warning There is a race condition if this is called while a state transition is already in
 *          progress or if another state is requested before the this request was processed.
 * @param state The new state of the system.
 */
extern void requestStateChange(SystemState state);

/**
 * Enable or disable the GoPros.
 *
 * @param enabled Whether or not the GoPros should be enabled.
 */
extern void enableGoPros(bool enabled);

/**
 * @return The current state of the system state machine.
 */
extern SystemState getCurrentState(void);

/**
 * Set the pre launch timer to the new value.
 *
 * @param timer The new value of the timer in milliseconds since the pre-launch state started.
 */
extern void setPreLaunchTimer(ms_t timer);
