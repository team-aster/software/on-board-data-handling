#pragma once

#include <stdint.h>
#include "globals.h"


extern noreturn void StartSensorsTask(void* argument);

/**
 * Signal the sensor thread to start the IMU calibration.
 */
extern void startImuCalibration(void);
