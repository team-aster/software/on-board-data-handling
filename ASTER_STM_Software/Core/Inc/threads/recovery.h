#pragma once

#include <stdint.h>
#include "globals.h"
#include "generated/sharedTypes.h"


/**
 * Initialize the recovery thread.
 */
extern void initRecovery(void);

/**
 * Run the recovery thread.
 * Handles the recovery and sends the recovery telemetry.
 *
 * @param argument Unused.
 */
extern noreturn void StartRecoveryTask(void* argument);

/**
 * Deploy the parachutes without performing any of the checks if it is save to do so.
 * @warning Only use when these checks have been done before or during debugging.
 */
extern void doDeploymentWithoutChecks(void);

/**
 * Reset the recovery system state to pre-launch conditions.
 *
 * @return Whether the reset was successful.
 */
extern bool resetRecovery(void);

/**
 * Request the recovery system to switch to the new state.
 *
 * @warning This function is not thread save and should only be called from one thread at a time.
 * @param state The new requested state of the recovery system.
 */
extern void requestRecoveryState(RecoveryState state);

/**
 * Send a debug Iridium and recovery telemetry package, when the testing pin is set.
 * @note This function is not thread save.
 */
extern void sendIridiumDebugTelemetry(void);
