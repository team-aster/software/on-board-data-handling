/** A thread that handles incoming telecommands. */

#pragma once

#include <stdint.h>
#include "globals.h"


/**
 * The Telecommand thread checks for new telecommands and handles them.
 */
extern noreturn void StartTelecommandTask(void* argument);
