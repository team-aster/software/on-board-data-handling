/**
 * @brief Global constants and types.
 */
#pragma once

#include <stdint.h>
#include <cmsis_os.h>

#if __STDC_VERSION__ >= 201112L // C11
#    include <stdnoreturn.h>
#else /* No C11 */
#    define noreturn
#endif

#ifndef ARRAY_SIZE
/**
 * Calculate the size of an array at compile time in bytes.
 * This only works for static arrays where the size is known by the compiler!
 *
 * @param array The static array who is measured.
 * @return The size of the array in bytes.
 */
#    define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))
#endif /* ARRAY_SIZE */

#if !defined(__cplusplus) && !defined(static_assert)
/**
 * Assert a condition at compile time.
 *
 * @param condition The condition which must be true.
 * @param message The assertion message.
 */
#    define static_assert(condition, message) ((void) sizeof(char[1 - 2 * !(condition)]))
#endif

/** Configuration */
//------------CONFIGURATION START--------------------
#define SD_CARDS_ENABLED 1
#define SD_CARD_COUNT 2
#define FLASH_ENABLED 1
#define TELEMETRY_ENABLED 1
#define SEND_ALIVE_TIMER 1
#define IMU_ENABLED 1
/**
 * Whether or not to enable communication with the Iridium network.
 */
#define IRIDIUM_ENABLED 1

/**
 * Whether or not to enable receiving GPS packages.
 */
#define GPS_ENABLED 1

/**
 * Whether the idle thread should measure and print the processor usage.
 */
#define MEASURE_PROCESSOR_USAGE 0

/**
 * The number of ticks in a second.
 */
#define SECOND_IN_TICKS configTICK_RATE_HZ

/**
 * @param bitfield: A collection of bits.
 * @param bitmask: A mask for some bits in the bit field.
 * @return Whether or not any of the bits in the mask are set in the field.
 */
#define IS_SET(bitfield, bitmask) ((bitfield) & (bitmask))

/**
 * A three dimensional double vector.
 */
typedef union {
    struct {
        double x, y, z;
    };
    struct {
        double roll, pitch, yaw;
    };
} Vec3D;

/**
 * A three dimensional vector with all components set to zero.
 */
#define VEC3D_ZERO {{0, 0, 0}}

/**
 * A three dimensional uint32_t vector.
 */
typedef union {
    struct {
        uint32_t x, y, z;
    };
    struct {
        uint32_t roll, pitch, yaw;
    };
} Vec3Uint32;
