//
// Created by Sebastian on 28.10.2020.
//

#include <memory.h>
#include <math.h>
#include "controller.h"

/** The rmp level at which the speed ramp should be in effect. */
#define SPEED_RAMP_START rpm_t(1500)

/** The maximum allowed change of the speed within the speed ramp in rpm. */
#define MAX_SPEED_CHANGE rpm_t(200.0)

/** A maximum motor speed in rpm that is always allowed. */
#define SAFE_MOTOR_SPEED_CHANGE rpm_t(20.0)

/** The maximum absolute speed in rpm that the controller is allowed to request from the motors. */
#define MAXIMUM_ABSOLUTE_SPEED rpm_t(4200)

/** A factor to reduce the maximum allowed speed depending on the current motor speed. */
#define MAX_SPEED_CHANGE_FACTOR                             \
    ((MAX_SPEED_CHANGE.rpm - SAFE_MOTOR_SPEED_CHANGE.rpm) / \
        (MAXIMUM_ABSOLUTE_SPEED.rpm - SPEED_RAMP_START.rpm))


/**
 * Execute one step of a PID controller.
 *
 * @param controller The PID controller.
 * @param error The current error from the target value.
 * @param timeDelta The time in seconds since the last controller step.
 * @return The output value of the controller.
 */
static double pidController(axisController* controller, double error, sec_t timeDelta) {
    // Calculate the P-Term
    double pPart = controller->config.pid.p * error;

    // Calculate the I-Term
    controller->state.errorSum += error * timeDelta.sec;
    double iPart = controller->config.pid.i * controller->state.errorSum;

    // Calculate the D-Term
    double dPart =
        controller->config.pid.d * ((error - controller->state.previousError) / timeDelta.sec);
    controller->state.previousError = error;

    // Combine the PID terms
    return pPart + iPart + dPart;
}

void resetController(axisController* controller, rpm_t initialAngularVelocity,
    const pidConfig* config, bool useMotorSpeed) {
    memcpy(&controller->config.pid, config, sizeof(pidConfig));
    controller->config.useMotorSpeed = useMotorSpeed;
    controller->state.previousError = 0;
    controller->state.errorSum = 0;
    controller->state.angularVelocity = initialAngularVelocity;
    controller->state.nexRawErrorIndex = CONTROLLER_AVERAGING_WINDOW;
}

/**
 * Calculate the maximum change in the motor speed that is allowed for the given current speed.
 *
 * @param currentSpeed The current speed of the motor.
 * @return The maximum allowed speed change.
 */
static rpm_t getMaximumAllowedSpeedChange(rpm_t currentSpeed) {
    rpm_t speed = rpm_t(MAX_SPEED_CHANGE.rpm -
                        MAX_SPEED_CHANGE_FACTOR * (fabs(currentSpeed.rpm) - SPEED_RAMP_START.rpm));
    return speed.rpm < SAFE_MOTOR_SPEED_CHANGE.rpm ? SAFE_MOTOR_SPEED_CHANGE : speed;
}

rpm_t speedRamp(rpm_t demandedChange, rpm_t currentSpeed) {
    if ((demandedChange.rpm > 0) != (currentSpeed.rpm > 0) &&
        fabs(currentSpeed.rpm) > SPEED_RAMP_START.rpm) {
        rpm_t absoluteMaximumSpeedChange = getMaximumAllowedSpeedChange(currentSpeed);
        if (fabs(demandedChange.rpm) > absoluteMaximumSpeedChange.rpm) {
            return rpm_t(copysign(absoluteMaximumSpeedChange.rpm, demandedChange.rpm));
        }
    }
    return demandedChange;
}

/**
 * Calculate the averaged error.
 *
 * @param controller The controller to calculate the average for.
 * @param error The current new error that will be part of the average.
 * @return The averaged error.
 */
static double getAveragedError(axisController* controller, double error) {
    uint8_t averageSize;
    if (controller->state.nexRawErrorIndex >= CONTROLLER_AVERAGING_WINDOW) {
        controller->state
            .previousRawErrors[controller->state.nexRawErrorIndex - CONTROLLER_AVERAGING_WINDOW] =
            error;
        averageSize = controller->state.nexRawErrorIndex - CONTROLLER_AVERAGING_WINDOW + 1;
        controller->state.nexRawErrorIndex =
            (controller->state.nexRawErrorIndex + 1) % (CONTROLLER_AVERAGING_WINDOW * 2);
    } else {
        controller->state.previousRawErrors[controller->state.nexRawErrorIndex] = error;
        controller->state.nexRawErrorIndex =
            (controller->state.nexRawErrorIndex + 1) % CONTROLLER_AVERAGING_WINDOW;
        averageSize = CONTROLLER_AVERAGING_WINDOW;
    }
    double averagedError = controller->state.previousRawErrors[0];
    for (int i = 1; i < averageSize; ++i) {
        averagedError += controller->state.previousRawErrors[i];
    }
    averagedError /= averageSize;
    return averagedError;
}

rpm_t controlAxis(axisController* controller, double error, sec_t timeDelta, rpm_t motorSpeed) {
    if (!isnan(error)) {
        double averagedError = getAveragedError(controller, error);
        rpm_t demandedChange =
            rpm_t(pidController(controller, averagedError, timeDelta) * timeDelta.sec);
        rpm_t currentSpeed =
            controller->config.useMotorSpeed ? motorSpeed : controller->state.angularVelocity;
        controller->state.angularVelocity.rpm += speedRamp(demandedChange, currentSpeed).rpm;
    }
    // Limit the maximum absolute speed that is allowed to be requested.
    if (controller->state.angularVelocity.rpm > MAXIMUM_ABSOLUTE_SPEED.rpm) {
        controller->state.angularVelocity = MAXIMUM_ABSOLUTE_SPEED;
    } else if (controller->state.angularVelocity.rpm < -MAXIMUM_ABSOLUTE_SPEED.rpm) {
        controller->state.angularVelocity = rpm_t(-MAXIMUM_ABSOLUTE_SPEED.rpm);
    }
    return rpm_t(-controller->state.angularVelocity.rpm);
}
