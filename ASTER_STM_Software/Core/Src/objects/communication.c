/**
 * The purpose of the COMMUNICATION object is to provide an interface between all different
 * communication ports and other software objects.
 * Its children are objects handling different protocols and ports.
 */

#include <math.h>
#include "threads/telemetry.h"
#include "objects/communication.h"
#include "objects/communication/imu.h"
#include "objects/communication/gps.h"
#include "objects/communication/iridium.h"
#include "objects/communication/payload.h"
#include "objects/communication/sd_card.h"
#include "objects/communication/rxsm.h"
#include "globals.h"
#include "main.h"


bool initCommunication() {
    // Do not use os functions (like osDelay etc) in initialization
    uint8_t success = true;
    if (!initRXSM()) {
        printCriticalError("Unable to initialize RXSM communication");
        success = false;
    }
    MX_I2C1_Init();
    if (getLastSystemError() == I2C1_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize I2C1");
        success = false;
    }

#if IMU_ENABLED
    if (!initImuCommunication()) {
        printCriticalError("Unable to initialize the IMU communication");
        success = false;
    }
#endif /* IMU_ENABLED */
#if SD_CARDS_ENABLED
    initializeSDCardCommunication();
#endif /* SD_CARDS_ENABLED */
    MX_UART7_Init();
    if (getLastSystemError() == UART7_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize UART7");
        success = false;
    }
#if GPS_ENABLED
    if (!initGps()) {
        printCriticalError("Failed to initialize GPS communication");
        success = false;
    }
#endif /* GPS_ENABLED */
    if (!initPayloadCommunication()) {
        printError("Failed to initialize payload communication");
    }
#if IRIDIUM_ENABLED
    if (!initIridiumUart() || !startIridiumInitialization()) {
        printCriticalError("Failed to initialize Iridium communication");
        // Non critical failure
    }
#endif
    return success;
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef* uart, uint16_t size) {
    UNUSED(size);
    if (uart == &iridiumUart) {
#if IRIDIUM_ENABLED
        onIridiumInterrupt();
#endif /* IRIDIUM_ENABLED */
    } else if (uart == &gpsUart) {
#if GPS_ENABLED
        onGpsDataReceived();
#endif /* GPS_ENABLED */
    } else if (uart == &rxsmUart) {
        onRXSMDataReceived();
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart) {
    if (huart == &rxsmUart) {
        onRXSMSendCompleted();
    }
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart) {
    if (huart == &rxsmUart) {
        onRXSMError();
    } else if (huart == &iridiumUart) {
#if IRIDIUM_ENABLED
        onIridiumError();
#endif /* IRIDIUM_ENABLED */
    } else if (huart == &gpsUart) {
#if GPS_ENABLED
        onGpsError();
#endif /* GPS_ENABLED */
    }
}
