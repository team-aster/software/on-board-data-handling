/**
 * The purpose of the GPIO object is to handle all writing and reading of GPIO pins
 * and provide an interface for other objects to access them.
 */

#include "threads/telemetry.h"
#include "objects/gpio.h"
#include "main.h"


void initGPIO(void) {
    MX_GPIO_Init();
}

PinMode getPin(pin_t pin) {
    switch (HAL_GPIO_ReadPin(pin.port, pin.number)) {
    case GPIO_PIN_RESET:
        return LOW;
    case GPIO_PIN_SET:
        return HIGH;
    }
    printCriticalError("HAL_GPIO_ReadPin returned invalid value");
    return LOW;
}

void setPin(pin_t pin, PinMode mode) {
    switch (mode) {
    case LOW:
        HAL_GPIO_WritePin(pin.port, pin.number, GPIO_PIN_RESET);
        break;
    case HIGH:
        HAL_GPIO_WritePin(pin.port, pin.number, GPIO_PIN_SET);
        break;
    case TOGGLE:
        HAL_GPIO_TogglePin(pin.port, pin.number);
        break;
    default:
        printCriticalError("setPin called with invalid mode: %u", (unsigned int) mode);
    }
}
