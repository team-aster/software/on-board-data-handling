#include <math.h>
#include <stdatomic.h>
#include "threads/watchdog.h"
#include "objects/system.h"
#include "objects/sensors.h"
#include "objects/motors.h"
#include "objects/adc.h"
#include "objects/communication/imu.h"
#include "data/sensor.h"
#include "data/adcs.h"


/** Whether or not the fusion mode in IMU1 should be enabled. */
#define IMU1_FUSION_ENABLED false

/** Whether or not the fusion mode in IMU2 should be enabled. */
#define IMU2_FUSION_ENABLED true

/**
 * The power supply voltage for the pressure sensor.
 */
#define PRESSURE_SENSOR_SUPPLY_VOLTAGE 3.3

/**
 * The maximum pressure that the pressure sensor can detect.
 */
#define PRESSURE_SENSOR_MAX_PRESSURE 1.0

/**
 * The minimum pressure that the pressure sensor can detect.
 */
#define PRESSURE_SENSOR_MIN_PRESSURE 0.0

/** A coefficient used in converting the voltage at a temperature sensor into a temperature. */
#define TEMPERATURE_COEFFICIENT 100

/** The voltage that is read if a temperature sensor measures 0°. */
#define TEMPERATURE_ZERO_INPUT volt_t(0.5)

/**
 * A coefficient used in converting the voltage at the
 * processor temperature sensor in into a temperature.
 */
#define PROCESSOR_TEMPERATURE_COEFFICIENT 400

/** The voltage that is read if the processor temperature sensor measures 0°. */
#define PROCESSOR_TEMPERATURE_ZERO_INPUT volt_t(0.6975)

/** A flag that indicates that the IMU1 is working. */
#define IMU1_WORKING 0b00000001U

/** A flag that indicates that the IMU2 is working. */
#define IMU2_WORKING 0b00000010U

/** A coefficient used to convert the read voltage into the actual battery voltage level. */
#define BATTERY_LEVEL_COEFFICIENT (46.1 / 12.1)

/** The current state of the sensors. */
static volatile atomic_uchar sensorState = 0;

/**
 * The primary IMU to use for rotation and orientation measurement.
 */
static IMU primaryIMU = IMU1;

bar_t readPressure(void) {
    // The original formula from the data sheet:
    // Output V = (0.8 * V_supply) / (P_max - P_min) * (pressure – P_min) + 0.10 * V_supply
    bar_t pressure =
        bar_t(((getAdcValue(PRESSURE_SENSOR_ADC).volt - 0.10 * PRESSURE_SENSOR_SUPPLY_VOLTAGE) *
                  (PRESSURE_SENSOR_MAX_PRESSURE - PRESSURE_SENSOR_MIN_PRESSURE) /
                  (0.8 * PRESSURE_SENSOR_SUPPLY_VOLTAGE)) +
              PRESSURE_SENSOR_MIN_PRESSURE);
    if (!set_pressure(&pressure)) {
        printError("Failed to save the pressure");
    }
    return pressure;
}


/**
 * Select the primary IMU based on the current status of the two IMUs.
 */
static void selectPrimaryImu() {
    // Whether the error that both IMUS are unavailable has already been reported.
    static bool reportedBothUnavailableError = false;
    if (primaryIMU == IMU1) {
        if (sensorState & IMU1_WORKING) {
            reportedBothUnavailableError = false;
            return;
        }
        if (sensorState & IMU2_WORKING) {
            print("IMU1 is not available, promoting to IMU2 to primary IMU");
            primaryIMU = IMU2;
            reportedBothUnavailableError = false;
            return;
        }
    } else {
        if (sensorState & IMU2_WORKING) {
            reportedBothUnavailableError = false;
            return;
        }
        if (sensorState & IMU1_WORKING) {
            print("IMU2 is not available, promoting to IMU1 to primary IMU");
            primaryIMU = IMU1;
            reportedBothUnavailableError = false;
            return;
        }
    }
    if (!reportedBothUnavailableError) {
        printCriticalError("Both IMUs are unavailable, no orientation measurement possible");
        reportedBothUnavailableError = true;
    }
}

/**
 * Update a bit in the sensor state.
 *
 * @param stateBit The bit to set.
 * @param value Whether to set or clear the bit.
 */
static __inline__ void setSensorState(uint8_t stateBit, bool value) {
    if (value) {
        sensorState |= stateBit;
    } else {
        sensorState &= ~stateBit;
    }
}

bool initIMUs(void) {
    refreshWatchdogTimer();
    setSensorState(IMU1_WORKING, initIMU(IMU1, IMU1_FUSION_ENABLED));
    refreshWatchdogTimer();
    setSensorState(IMU2_WORKING, initIMU(IMU2, IMU2_FUSION_ENABLED));
    if (IS_SET(sensorState, IMU1_WORKING) || IS_SET(sensorState, IMU2_WORKING)) {
        selectPrimaryImu();
        return true;
    }
    return false;
}

void calibrateIMUs(void) {
#if !IMU1_FUSION_ENABLED
    if (!calibrate(IMU1)) {
        printError("IMU1 calibration failed");
    } else {
        print("IMU1 successfully calibrated");
    }
#endif /* IMU1_FUSION_ENABLED */
#if !IMU2_FUSION_ENABLED
    if (!calibrate(IMU2)) {
        printError("IMU2 calibration failed");
    } else {
        print("IMU2 successfully calibrated");
    }
#endif /* IMU2_FUSION_ENABLED */
}

void checkIMUs(void) {
    if (IS_SET(sensorState, IMU1_WORKING)) {
        setSensorState(IMU1_WORKING, checkImuStatus(IMU1));
    }
    if (IS_SET(sensorState, IMU2_WORKING)) {
        setSensorState(IMU2_WORKING, checkImuStatus(IMU2));
    }
    if (!IS_SET(sensorState, IMU1_WORKING)) {
        setSensorState(IMU1_WORKING, initIMU(IMU1, IMU1_FUSION_ENABLED));
        refreshWatchdogTimer();
    }
    if (!IS_SET(sensorState, IMU2_WORKING)) {
        setSensorState(IMU2_WORKING, initIMU(IMU2, IMU2_FUSION_ENABLED));
        refreshWatchdogTimer();
    }
    selectPrimaryImu();
}

/**
 * @param imu The IMU to check.
 * @return Whether the IMU is marked as working.
 */
static __inline__ bool isImuWorking(IMU imu) {
    return IS_SET(sensorState, 0b1U << (imu - 1));
}

bool readRotationSpeedFromPrimaryImu(Vec3D* rotation) {
    if (!isImuWorking(primaryIMU)) { // Check if primary IMU is working;
        return false;
    }
#if IMU_ENABLED == 1
    if (!readRotationSpeed(primaryIMU, rotation)) {
        return false;
    }
    Vec3FWithTime record = {
        .data =
            {
                .roll = (float) rotation->roll,
                .pitch = (float) rotation->pitch,
                .yaw = (float) rotation->yaw,
            },
    };
    record.time = getTimeSinceBoot();
    append_rotationMeasurements(&record, 1);
    return true;
#else
    return false;
#endif /* IMU_ENABLED */
}

#if IMU_ENABLED == 1
void updateImuData(void) {
    for (IMU imu = IMU1; imu <= IMU2; imu++) {
        if (!isImuWorking(imu)) { // Check if IMU is working;
            continue;
        }
        Vec3D rotation;
        celsiusInt8_t temperature;
        bool haveTemperature = readTemperature(imu, &temperature.celsius);
        if (!haveTemperature) {
            printError("Failed to read the temperature from IMU %d", imu);
        }
        bool haveRotation = false;
        // We don't need to read the rotation of the primary imu,
        // because it will be handled by readRotationSpeedFromPrimaryImu.
        if (imu != primaryIMU && !(haveRotation = readRotationSpeed(imu, &rotation))) {
            printError("Failed to read the rotation from IMU %d", imu);
        }
        Vec3D acceleration;
        bool haveAcceleration = readAcceleration(imu, &acceleration);
        if (!haveAcceleration) {
            printError("Failed to read the acceleration from IMU %d", imu);
        }
        Vec3D magneticForce;
        bool haveMagneticForce = readMagneticForce(imu, &magneticForce);
        if (!haveMagneticForce) {
            printError("Failed to read the magnetic force from IMU %d", imu);
        }
        Quaternion orientation;
        bool haveOrientation = false;
        if ((
#    if IMU1_FUSION_ENABLED
                imu == IMU1 ||
#    endif
#    if IMU2_FUSION_ENABLED
                imu == IMU2 ||
#    endif
                false) &&
            !(haveOrientation = readOrientation(imu, &orientation))) {
            printError("Failed to read the orientation from IMU %d", imu);
        }
        if (osSemaphoreAcquire(sensorSemaphore, 10U) != osOK) {
            printError("Failed to acquire the adcs semaphore to update sensor values for IMU %d",
                imu);
            return;
        }
        if (imu == primaryIMU) {
            if (haveAcceleration) {
                setBatch_primaryImuAccelerometer(&acceleration);
            }
            if (haveMagneticForce) {
                setBatch_primaryImuMagnetometer(&magneticForce);
            }
            if (haveTemperature) {
                setBatch_primaryImuTemperature(&temperature);
            }
        } else {
            if (haveRotation) {
                setBatch_secondaryImuRotation(&rotation);
            }
            if (haveAcceleration) {
                setBatch_secondaryImuAccelerometer(&acceleration);
            }
            if (haveMagneticForce) {
                setBatch_secondaryImuMagnetometer(&magneticForce);
            }
            if (haveTemperature) {
                setBatch_secondaryImuTemperature(&temperature);
            }
        }
        if (haveOrientation) {
            setBatch_imu2Quaternions(&orientation);
        }
        osSemaphoreRelease(sensorSemaphore);
    }
}
#endif /* IMU_ENABLED */

void updateMotorCurrents(void) {
    Vec3D currents = {
        .roll = ((double) getAdcValueRaw(ROLL_MOTOR_CURRENT_ADC) / MAX_ADC_VALUE) * 2.0,
        .pitch = ((double) getAdcValueRaw(PITCH_MOTOR_CURRENT_ADC) / MAX_ADC_VALUE) * 2.0,
        .yaw = ((double) getAdcValueRaw(YAW_MOTOR_CURRENT_ADC) / MAX_ADC_VALUE) * 2.0,
    };
    if (!set_motorCurrent(&currents)) {
        printError("Failed to update the motor currents");
    }
}

void readMotorsSpeed(Vec3D* speed) {
    speed->roll = getAdcValueRaw(ROLL_MOTOR_SPEED_ADC) * ((MOTOR_MAX_RPM * 2.0) / MAX_ADC_VALUE) -
                  MOTOR_MAX_RPM;
    speed->pitch = getAdcValueRaw(PITCH_MOTOR_SPEED_ADC) * ((MOTOR_MAX_RPM * 2.0) / MAX_ADC_VALUE) -
                   MOTOR_MAX_RPM;
    speed->yaw = getAdcValueRaw(YAW_MOTOR_SPEED_ADC) * ((MOTOR_MAX_RPM * 2.0) / MAX_ADC_VALUE) -
                 MOTOR_MAX_RPM;
    Vec3FWithTime record = {
        .data = {.roll = (float) speed->roll,
            .pitch = (float) speed->pitch,
            .yaw = (float) speed->yaw},
    };
    record.time = getTimeSinceBoot();
    append_motorSpeeds(&record, 1);
}

volt_t readBatteryLevel(void) {
    volt_t level = volt_t(getAdcValue(BATTERY_STATUS_ADC).volt * BATTERY_LEVEL_COEFFICIENT);
    set_batteryLevel(&level);
    return level;
}

/**
 * @param value The temperature value read from ADC.
 * @return The temperature value in °C.
 */
static __inline__ celsius_t convertTemperatureSensorValue(volt_t value) {
    return celsius_t((value.volt - TEMPERATURE_ZERO_INPUT.volt) * TEMPERATURE_COEFFICIENT);
}

void updateTemperatureData(void) {
    celsius_t batteryTemperature =
        convertTemperatureSensorValue(getAdcValue(BATTERY_TEMPERATURE_ADC));
    celsius_t pduPcbTemperature =
        convertTemperatureSensorValue(getAdcValue(PDU_PCB_TEMPERATURE_ADC));
    celsius_t motorControllerTemperature =
        convertTemperatureSensorValue(getAdcValue(MOTOR_CONTROLLER_TEMPERATURE_ADC));
    celsius_t motorBracketTemperature =
        convertTemperatureSensorValue(getAdcValue(MOTOR_BRACKET_TEMPERATURE_ADC));
    celsius_t retentionWallTemperature =
        convertTemperatureSensorValue(getAdcValue(RETENTION_WALL_TEMPERATURE_ADC));
    celsius_t recoveryWallTemperature =
        convertTemperatureSensorValue(getAdcValue(RECOVERY_WALL_TEMPERATURE_ADC));
    celsius_t processorTemperature = celsius_t(
        (getAdcValue(PROCESSOR_TEMPERATURE_ADC).volt - PROCESSOR_TEMPERATURE_ZERO_INPUT.volt) *
        PROCESSOR_TEMPERATURE_COEFFICIENT);
    if (osSemaphoreAcquire(sensorSemaphore, 10U) != osOK) {
        printError("Failed to update the temperature values");
        return;
    }
    setBatch_batteryTemperature(&batteryTemperature);
    setBatch_pduPcbTemperature(&pduPcbTemperature);
    setBatch_motorControllerTemperature(&motorControllerTemperature);
    setBatch_motorBracketTemperature(&motorBracketTemperature);
    setBatch_retentionWallTemperature(&retentionWallTemperature);
    setBatch_recoveryWallTemperature(&recoveryWallTemperature);
    setBatch_processorTemperature(&processorTemperature);
    osSemaphoreRelease(sensorSemaphore);
}
