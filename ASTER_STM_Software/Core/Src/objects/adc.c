/**
 * The purpose of adc object is to provide interfaces and
 * initialization of analog to digital converters.
 */

#include "objects/adc.h"
#include "main.h"
#include "globals.h"
#include "threads/telemetry.h"

/** The ADC1 peripheral. */
#define adc1 hadc1
extern ADC_HandleTypeDef adc1;

/** The ADC2 peripheral. */
#define adc2 hadc2
extern ADC_HandleTypeDef adc2;

/** The ADC3 peripheral. */
#define adc3 hadc3
extern ADC_HandleTypeDef adc3;

/** A buffer for ADC measurements which is filled by DMA. */
static uint16_t adcMeasurements[LAST_ADC + 1] = {0};

bool initADC(void) {
    bool success = true;
    MX_ADC1_Init();
    if (getLastSystemError() == ADC1_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize ADC1");
        success = false;
    } else if (getLastSystemError() == ADC_DMA_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to set up DMA for ADC1");
        success = false;
    } else if (HAL_ADC_Start_DMA(&adc1, (uint32_t*) adcMeasurements, LAST_ADC1 + 1) != HAL_OK) {
        printCriticalError("Unable to start the DMA for ADC1");
        success = false;
    } else if (HAL_ADC_GetError(&adc1) != HAL_ADC_ERROR_NONE) {
        printCriticalError("ADC1 indicates an error after initialization: %lu",
            HAL_ADC_GetError(&adc1));
        success = false;
    } else if (HAL_ADC_GetState(&adc1) != HAL_ADC_STATE_REG_BUSY) {
        printCriticalError("ADC1 is not in the expected busy state after initialization: %lu",
            HAL_ADC_GetState(&adc1));
        success = false;
    }
    MX_ADC2_Init();
    if (getLastSystemError() == ADC2_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize ADC2");
        success = false;
    } else if (getLastSystemError() == ADC_DMA_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to set up DMA for ADC2");
        success = false;
    } else if (HAL_ADC_Start_DMA(&adc2, (uint32_t*) &adcMeasurements[LAST_ADC1 + 1],
                   LAST_ADC2 - LAST_ADC1) != HAL_OK) {
        printCriticalError("Unable to start the DMA for ADC2");
        success = false;
    } else if (HAL_ADC_GetError(&adc2) != HAL_ADC_ERROR_NONE) {
        printCriticalError("ADC2 indicates an error after initialization: %lu",
            HAL_ADC_GetError(&adc2));
        success = false;
    } else if (HAL_ADC_GetState(&adc2) != HAL_ADC_STATE_REG_BUSY) {
        printCriticalError("ADC2 is not in the expected busy state after initialization: %lu",
            HAL_ADC_GetState(&adc2));
        success = false;
    }
    MX_ADC3_Init();
    if (getLastSystemError() == ADC3_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize ADC3");
        success = false;
    } else if (getLastSystemError() == ADC_DMA_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to set up DMA for ADC3");
        success = false;
    } else if (HAL_ADC_Start_DMA(&adc3, (uint32_t*) &adcMeasurements[LAST_ADC2 + 1],
                   LAST_ADC3 - LAST_ADC2) != HAL_OK) {
        printCriticalError("Unable to start the DMA for ADC3");
        success = false;
    } else if (HAL_ADC_GetError(&adc3) != HAL_ADC_ERROR_NONE) {
        printCriticalError("ADC3 indicates an error after initialization: %lu",
            HAL_ADC_GetError(&adc3));
        success = false;
    } else if (HAL_ADC_GetState(&adc3) != HAL_ADC_STATE_REG_BUSY) {
        printCriticalError("ADC3 is not in the expected busy state after initialization: %lu",
            HAL_ADC_GetState(&adc3));
        success = false;
    }
    // Disable the interrupts for the ADC direct memory access,
    // we don't want it wasting CPU resources.
    HAL_NVIC_DisableIRQ(DMA2_Stream0_IRQn);
    HAL_NVIC_DisableIRQ(DMA2_Stream1_IRQn);
    HAL_NVIC_DisableIRQ(DMA2_Stream3_IRQn);
    return success;
}

uint16_t getAdcValueRaw(Adc adc) {
    return adcMeasurements[adc];
}

volt_t getAdcValue(Adc adc) {
    return volt_t(getAdcValueRaw(adc) * MAX_ADC_MEASUREMENT / MAX_ADC_VALUE);
}
