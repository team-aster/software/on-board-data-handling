#include <string.h>
#include "objects/ringBuffer.h"
#include "threads/telemetry.h"


void rbGetRegion(ringBuffer_t* buffer, rbRegionRef_t* region, size_t start, size_t length) {
    static_assert(sizeof(buffer->writeIndex) <= sizeof(uint32_t),
        "The write index must be a type that can be read from and written to atomically");
    region->buffer = buffer;
    region->start = start;
    region->length = length;
}

bool rbMemEqual(const rbRegionRef_t* region, const void* reference, uint8_t length) {
    if (region->length < length) {
        return false;
    }
    if (region->start + length < region->buffer->size) {
        return strncmp(&((const char*) region->buffer->memory)[region->start],
                   (const char*) reference, length) == 0;
    }
    uint8_t partLength = region->buffer->size - region->start;
    if (strncmp(&((const char*) region->buffer->memory)[region->start], (const char*) reference,
            partLength) != 0) {
        return false;
    }
    return strncmp((const char*) region->buffer->memory, &((const char*) reference)[partLength],
               length - partLength) == 0;
}

void rbMemCpy(const rbRegionRef_t* region, void* destination, uint8_t length) {
    if (length > region->length) {
        print("rbMemCpy called with length larger than region length");
        length = region->length;
    }
    if (region->start + length < region->buffer->size) {
        memcpy(destination, &((uint8_t*) region->buffer->memory)[region->start], length);
        return;
    }
    uint8_t partLength = region->buffer->size - region->start;
    memcpy(destination, &region->buffer->memory[region->start], partLength);
    memcpy(&((uint8_t*) destination)[partLength], region->buffer->memory, length - partLength);
}

extern size_t rbRead(ringBuffer_t* buffer, const void* data, size_t dataSize) {
    size_t writeIndex = buffer->writeIndex;
    size_t availableBytes = writeIndex >= buffer->readIndex ?
                                writeIndex - buffer->readIndex :
                                buffer->size - buffer->readIndex + writeIndex;
    if (data == NULL || dataSize == 0) {
        return availableBytes;
    }
    if (dataSize > availableBytes) {
        dataSize = availableBytes;
    }
    rbRegionRef_t region;
    rbGetRegion(buffer, &region, buffer->readIndex, dataSize);
    rbMemCpy(&region, (void*) data, (uint8_t) dataSize);
    buffer->readIndex = (buffer->readIndex + dataSize) % buffer->size;
    return dataSize;
}

void rbAppend(ringBuffer_t* buffer, const void* data, size_t dataSize) {
    ASSERT(buffer != NULL, "NULL buffer passed to %s", __FUNCTION__);
    size_t prevWriteIndex = buffer->writeIndex;
    ASSERT(prevWriteIndex < buffer->size,
        "The writeIndex is past the size of the buffer passed to %s", __FUNCTION__);
#if !defined(NDEBUG) || defined(DEBUG)
    size_t readIndex = buffer->readIndex;
    if ((buffer->writeIndex >= readIndex) ?
            (buffer->size - buffer->writeIndex + readIndex <= dataSize) :
            (buffer->writeIndex - readIndex <= dataSize)) {
        printError("rbAppend is overwriting data: writeIndex=%zu, readIndex=%zu, dataSize=%zu",
            buffer->writeIndex, readIndex, dataSize);
    }
#endif /* DEBUG */
    buffer->writeIndex = (buffer->writeIndex + dataSize) % buffer->size;
    if (data == NULL) {
        return;
    }
    const uint8_t* dataBytes = ((const uint8_t*) data);
    while (prevWriteIndex + dataSize > buffer->size) {
        size_t chunkSize = buffer->size - prevWriteIndex;
        memcpy(buffer->memory + prevWriteIndex, dataBytes, chunkSize);
        dataSize -= chunkSize;
        dataBytes += chunkSize;
        prevWriteIndex = 0;
    }
    memcpy(buffer->memory + prevWriteIndex, dataBytes, dataSize);
}

size_t updateWriteIndexFromDmaReader(ringBuffer_t* buffer, const DMA_HandleTypeDef* reader) {
    buffer->writeIndex = buffer->size - __HAL_DMA_GET_COUNTER(reader);
    return buffer->writeIndex;
}

size_t rbAdvanceReadIndex(ringBuffer_t* buffer, size_t offset) {
    size_t writeIndex = buffer->writeIndex;
    size_t availableBytes = writeIndex >= buffer->readIndex ?
                                writeIndex - buffer->readIndex :
                                buffer->size - buffer->readIndex + writeIndex;
    if (availableBytes < offset) {
        printError("rbAdvanceReadIndex called with a higher offset (%zu) than available data (%zu)",
            offset, availableBytes);
    }
    buffer->readIndex = RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex, offset);
    return availableBytes - offset;
}
