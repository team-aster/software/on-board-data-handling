/**
 * The purpose of the MOTORS object is to handle PWM generation for the motor pins
 * and provide an interface for ADCS object to drive the motors.
 */

#include <memory.h>
#include <math.h>
#include "objects/motors.h"
#include "globals.h"
#include "main.h"
#include "objects/gpio.h"
#include "threads/telemetry.h"
#include "data/adcs.h"

/**
 * The minimum pulse with modulation value the motors accept.
 */
#define MOTOR_MIN_PWM 10

/**
 * The maximum pulse with modulation value the motors accept.
 */
#define MOTOR_MAX_PWM 90

/**
 * The Clock that is powering the pulse with modulation signals for the motors.
 */
#define pwmClock htim4
extern TIM_HandleTypeDef pwmClock;


bool initMotors(void) {
    MX_TIM4_Init();
    if (getLastSystemError() == TIM4_INIT_SYSTEM_ERROR) {
        printCriticalError("Failed to initialize the motor pwm clock");
        return false;
    }
    return true;
}

bool enableMotors(void) {
    uint8_t success = true;
    if (TIM_CHANNEL_STATE_GET(&pwmClock, MOTOR_ROLL) != HAL_TIM_CHANNEL_STATE_BUSY &&
        HAL_TIM_PWM_Start(&pwmClock, MOTOR_ROLL) != HAL_OK) {
        printError("Failed to start roll motor pwm generation");
        success = false;
    }
    if (TIM_CHANNEL_STATE_GET(&pwmClock, MOTOR_PITCH) != HAL_TIM_CHANNEL_STATE_BUSY &&
        HAL_TIM_PWM_Start(&pwmClock, MOTOR_PITCH) != HAL_OK) {
        printError("Failed to start pitch motor pwm generation");
        success = false;
    }
    if (TIM_CHANNEL_STATE_GET(&pwmClock, MOTOR_YAW) != HAL_TIM_CHANNEL_STATE_BUSY &&
        HAL_TIM_PWM_Start(&pwmClock, MOTOR_YAW) != HAL_OK) {
        printError("Failed to start yaw motor pwm generation");
        success = false;
    }
    setPin(ROLL_MOTOR_ENABLE_PIN, HIGH);
    setPin(PITCH_MOTOR_ENABLE_PIN, HIGH);
    setPin(YAW_MOTOR_ENABLE_PIN, HIGH);
    Vec3Uint32 enabledVector = {{true, true, true}};
    if (!set_motorEnabled(&enabledVector)) {
        printError("Failed to update motor enable status while enabling");
    }
    return success;
}

void disableMotors(void) {
    setPin(ROLL_MOTOR_ENABLE_PIN, LOW);
    setPin(PITCH_MOTOR_ENABLE_PIN, LOW);
    setPin(YAW_MOTOR_ENABLE_PIN, LOW);
    Vec3Uint32 disabledVector = {{0, 0, 0}};
    if (!set_motorEnabled(&disabledVector)) {
        printError("Failed to update motor enable status while disabling");
    }
    if (HAL_TIM_PWM_Stop(&pwmClock, MOTOR_ROLL) != HAL_OK) {
        printError("Failed to stop roll motor pwm generation");
    }
    if (HAL_TIM_PWM_Stop(&pwmClock, MOTOR_PITCH) != HAL_OK) {
        printError("Failed to stop pitch motor pwm generation");
    }
    if (HAL_TIM_PWM_Stop(&pwmClock, MOTOR_YAW) != HAL_OK) {
        printError("Failed to stop yaw motor pwm generation");
    }
}

void setMotor(Motor motor, pwm_t value) {
    pin_t directionPin;
    switch (motor) {
    case MOTOR_ROLL:
        directionPin = ROLL_MOTOR_DIRECTION_PIN;
        break;
    case MOTOR_PITCH:
        directionPin = PITCH_MOTOR_DIRECTION_PIN;
        break;
    case MOTOR_YAW:
        directionPin = YAW_MOTOR_DIRECTION_PIN;
        break;
    default:
        printCriticalError("Invalid motor given to setMotor");
        return;
    }
    PinMode directionPinValue = HIGH;
    if (value.pwm < 0) {
        value.pwm *= -1;
        directionPinValue = LOW;
    }
    // The yaw motor is oriented towards the negative Y Axis and needs to be flipped.
    if (motor == MOTOR_YAW) {
        directionPinValue = directionPinValue == LOW ? HIGH : LOW;
    }
    setPin(directionPin, directionPinValue);
    uint32_t pulse = (uint32_t) round(pwmClock.Init.Period * value.pwm / 100.0);
    __HAL_TIM_SET_COMPARE(&pwmClock, motor, pulse);
}

/**
 * Convert rounds per minute to a pulse with modulation duty cycle percent.
 *
 * @param value The rpm value.
 * @return The pwm equivalent.
 */
static pwm_t toPWM(rpm_t value) {
    double smallestValue = value.rpm >= 0 ? MOTOR_MIN_PWM : -MOTOR_MIN_PWM;
    return pwm_t(
        smallestValue + ((double) value.rpm / MOTOR_MAX_RPM) * (MOTOR_MAX_PWM - MOTOR_MIN_PWM));
}

void setMotors(rpm_t motorRoll, rpm_t motorPitch, rpm_t motorYaw) {
    rpm_t motorRPMs[] = {motorRoll, motorPitch, motorYaw};
    Motor motors[] = {MOTOR_ROLL, MOTOR_PITCH, MOTOR_YAW};
    for (int i = 0; i < ARRAY_SIZE(motorRPMs); i++) {
        if (motorRPMs[i].rpm > MOTOR_MAX_RPM) {
            printError("Motor %d rpm value out of range: %f", i, motorRPMs[i].rpm);
            motorRPMs[i].rpm = MOTOR_MAX_RPM; // Don't fail, but limit to valid values
        } else if (motorRPMs[i].rpm < MOTOR_MIN_RPM) {
            printError("Motor %d rpm value out of range: %f", i, motorRPMs[i].rpm);
            motorRPMs[i].rpm = MOTOR_MIN_RPM; // Don't fail, but limit to valid values
        }
        setMotor(motors[i], toPWM(motorRPMs[i]));
    }
}
