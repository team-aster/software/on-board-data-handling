#include <cmsis_os2.h>
#include <stm32f7xx_hal.h>
#include "objects/system.h"


#ifndef __ARM_ARCH_6M__
#    define __ARM_ARCH_6M__ 0
#endif
#ifndef __ARM_ARCH_7M__
#    define __ARM_ARCH_7M__ 0
#endif
#ifndef __ARM_ARCH_7EM__
#    define __ARM_ARCH_7EM__ 0
#endif
#ifndef __ARM_ARCH_8M_MAIN__
#    define __ARM_ARCH_8M_MAIN__ 0
#endif
#ifndef __ARM_ARCH_7A__
#    define __ARM_ARCH_7A__ 0
#endif

#if ((__ARM_ARCH_7M__ == 1U) || (__ARM_ARCH_7EM__ == 1U) || (__ARM_ARCH_8M_MAIN__ == 1U))
#    define IS_IRQ_MASKED() ((__get_PRIMASK() != 0U) || (__get_BASEPRI() != 0U))
#elif (__ARM_ARCH_6M__ == 1U)
#    define IS_IRQ_MASKED() (__get_PRIMASK() != 0U)
#elif (__ARM_ARCH_7A__ == 1U)
/* CPSR mask bits */
#    define CPSR_MASKBIT_I 0x80U

#    define IS_IRQ_MASKED() ((__get_CPSR() & CPSR_MASKBIT_I) != 0U)
#else
#    define IS_IRQ_MASKED() (__get_PRIMASK() != 0U)
#endif

#if (__ARM_ARCH_7A__ == 1U)
/* CPSR mode bitmasks */
#    define CPSR_MODE_USER 0x10U
#    define CPSR_MODE_SYSTEM 0x1FU

#    define IS_IRQ_MODE() ((__get_mode() != CPSR_MODE_USER) && (__get_mode() != CPSR_MODE_SYSTEM))
#else
#    define IS_IRQ_MODE() (__get_IPSR() != 0U)
#endif


bool isInInterrupt(void) {
    return IS_IRQ_MODE() || (IS_IRQ_MASKED() && (osKernelGetState() == osKernelRunning));
}

ms_t getTimeSinceBoot(void) {
    // Use the HAL tick as it is more accurate because it is updated in an interrupt with the
    // highest priority. The counter used by FreeRtos is updated in an interrupt handler with
    // the lowest priority and thus drifts when there is any interrupt running.
    return ms_t(HAL_GetTick());
}

void sleepUntil(ms_t wakeupTime) {
    // This will potentially sleep longer, as the FreeRtos timer does not account for time
    // spent in interrupt handlers.
    osDelayUntil(wakeupTime.ms - (HAL_GetTick() - osKernelGetTickCount()));
}
