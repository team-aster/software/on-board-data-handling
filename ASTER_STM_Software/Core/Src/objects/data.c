/**
 * The purpose of the DATA object is to provide an interface
 * for accessing different data needed by threads.
 */

#include "objects/data.h"
#include "data/adcs.h"
#include "data/flash.h"
#include "data/sensor.h"
#include "data/system.h"
#include "data/gps.h"


bool initData(void) {
    if (!initDataFlash() || !initAdcsData() || !initSensorData() || !initSystemData() ||
        !initGpsData()) {
        return false;
    }
    return true;
}
