#include <stm32f7xx_hal.h>
#include "objects/crc.h"
#include "globals.h"
#include "main.h"


/** The CRC hardware peripheral. */
#define crcComputer hcrc
extern CRC_HandleTypeDef crcComputer;

bool initCRC() {
    MX_CRC_Init();
    if (getLastSystemError() == CRC_INIT_SYSTEM_ERROR) {
        crcComputer.Instance = NULL;
        return false;
    }
    return true;
}

uint16_t calculateChecksum(const void* data, size_t size) {
    if (crcComputer.Instance == NULL) {
        return 0;
    }
    return HAL_CRC_Calculate(&crcComputer, (uint32_t*) data, size);
}
