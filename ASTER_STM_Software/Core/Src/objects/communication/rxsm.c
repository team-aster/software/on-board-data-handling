#include <cmsis_os2.h>
#include <memory.h>
#include "objects/communication/rxsm.h"
#include "threads/telemetry.h"
#include "data/system.h"
#include "objects/ringBuffer.h"
#include "objects/crc.h"
#include "globals.h"
#include "main.h"


/** DMA for handling messaged from the RXSM connection.*/
#define rxsmReader hdma_usart3_rx
extern DMA_HandleTypeDef rxsmReader;

/** A semaphore that prevents simultaneous writing from different threads. */
static osSemaphoreId_t semaphoreRXSM = NULL;

/** The ring buffer used for receiving telecommands. */
DEFINE_RING_BUFFER(rxsmReceiveBuffer, sizeof(GroundStationMessage) * 4);

/** A buffer to store the message currently being send, for the DMA. */
static uint8_t rxsmSendBuffer[512];

/** Whether or not the DMA has finished sending the last message and is ready to send another. */
static volatile bool rxsmSendReady = true;


bool initRXSM(void) {
    bool success = true;
    if ((semaphoreRXSM = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
        success = false;
    }
    MX_USART3_UART_Init();
    if (getLastSystemError() == USART3_INIT_SYSTEM_ERROR) {
        rxsmUart.Instance = NULL;
        printCriticalError("Unable to initialize USART3");
        success = false;
    } else if (HAL_UARTEx_ReceiveToIdle_DMA(&rxsmUart, rxsmReceiveBuffer.memory,
                   rxsmReceiveBuffer.size) != HAL_OK) {
        printCriticalError("Unable to set up DMA handler for RXSM");
        success = false;
    }
    return success;
}

bool writeToRXSM(const void* data, size_t dataLength) {
    if (semaphoreRXSM == NULL || rxsmUart.Instance == NULL ||
        (osSemaphoreAcquire(semaphoreRXSM, 0U) != osOK &&
            osSemaphoreAcquire(semaphoreRXSM, 10U) != osOK)) {
        // Wait 10 ticks for the free semaphore
        return false;
    }
    bool success = false;
    if (rxsmSendReady) {
        if (dataLength > ARRAY_SIZE(rxsmSendBuffer)) {
            dataLength = ARRAY_SIZE(rxsmSendBuffer);
        }
        memcpy(rxsmSendBuffer, data, dataLength);
        if (HAL_UART_Transmit_DMA(&rxsmUart, (const uint8_t*) rxsmSendBuffer, dataLength) == HAL_OK) {
            rxsmSendReady = false;
            success = true;
        }
    }
    osSemaphoreRelease(semaphoreRXSM);
    return success;
}

void onRXSMSendCompleted(void) {
    rxsmSendReady = true;
    signalTelemetryWriteReady();
}

void onRXSMDataReceived(void) {
    size_t writeIndex = updateWriteIndexFromDmaReader(&rxsmReceiveBuffer, &rxsmReader);
    size_t readSize = rbRead(&rxsmReceiveBuffer, NULL, 0);
    while (rxsmReceiveBuffer.readIndex != writeIndex && readSize >= sizeof(GroundStationMessage)) {
        if (rxsmReceiveBuffer.memory[rxsmReceiveBuffer.readIndex] != SYNC_BYTE_1) {
            readSize = rbAdvanceReadIndex(&rxsmReceiveBuffer, 1);
            continue;
        }
        if (rxsmReceiveBuffer.memory[RELATIVE_RING_BUFFER_INDEX(&rxsmReceiveBuffer,
                rxsmReceiveBuffer.readIndex, 1)] != SYNC_BYTE_2) {
            readSize = rbAdvanceReadIndex(&rxsmReceiveBuffer, 1);
            continue;
        }
        GroundStationMessage message;
        rbRegionRef_t messageRegion;
        rbGetRegion(&rxsmReceiveBuffer, &messageRegion, rxsmReceiveBuffer.readIndex,
            sizeof(message));
        rbMemCpy(&messageRegion, &message, sizeof(message));
        if (message.checksum !=
            calculateChecksum(&message.telecommand, sizeof(message.telecommand))) {
            printError("Received telecommand with invalid CRC");
            readSize = rbAdvanceReadIndex(&rxsmReceiveBuffer, 2);
            continue;
        } else if (!enqueueTelecommand(&message.telecommand)) {
            printCriticalError("Unable to store received telecommand because the buffer is full");
        }
        readSize = rbAdvanceReadIndex(&rxsmReceiveBuffer, sizeof(message));
    }
}

void onRXSMError(void) {
    // Suppress the error message if the RXSM is not connected.
    if (rxsmUart.ErrorCode != HAL_UART_ERROR_FE && rxsmReceiveBuffer.readIndex != 0) {
        printError("RXSM read error: %lu", (unsigned long) rxsmUart.ErrorCode);
    }
    // Restart the DMA reception
    HAL_UART_DMAStop(&rxsmUart);
    HAL_UARTEx_ReceiveToIdle_DMA(&rxsmUart, rxsmReceiveBuffer.memory, rxsmReceiveBuffer.size);
    // Discard all received buffered data
    updateWriteIndexFromDmaReader(&rxsmReceiveBuffer, &rxsmReader);
}
