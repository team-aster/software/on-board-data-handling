#include <stm32f7xx_hal.h>
#include <cmsis_os2.h>
#include "objects/communication/payload.h"
#include "threads/telemetry.h"
#include "main.h"


/** The UART interface for the payload. */
#define payloadUart huart2
extern UART_HandleTypeDef payloadUart;

/** A semaphore that prevents simultaneous writing from different threads. */
static osSemaphoreId_t semaphorePayload = NULL;


bool initPayloadCommunication(void) {
    if ((semaphorePayload = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
        return false;
    }
    MX_USART2_UART_Init();
    if (getLastSystemError() == USART2_INIT_SYSTEM_ERROR) {
        payloadUart.Instance = NULL;
        printCriticalError("Unable to initialize payload UART2");
        return false;
    }
    return true;
}

bool writeToPayload(const void* data, size_t dataLength) {
    if (semaphorePayload == NULL || payloadUart.Instance == NULL ||
        (osSemaphoreAcquire(semaphorePayload, 0U) != osOK &&
            osSemaphoreAcquire(semaphorePayload, 10U) != osOK)) {
        // Wait 10 ticks for the free semaphore
        return false;
    }
    // If there was no time-out the semaphore was acquired
    HAL_UART_Transmit(&payloadUart, (const uint8_t*) data, dataLength, HAL_MAX_DELAY);
    osSemaphoreRelease(semaphorePayload);
    return true;
}

void enablePayloadVideoRecording(void) {
    // This is a rudimentary way to communicate with the payload.
    // The payload will read the telemetry and listen for this exact string.
    // !!! Don't change this string without adapting the payload. !!!
    print("[payload] Enabling video recording");
}

void disablePayloadVideoRecording(void) {
    // !!! Don't change this string without adapting the payload. !!!
    print("[payload] Disabling video recording");
}

void powerOffPayload(void) {
    // !!! Don't change this string without adapting the payload. !!!
    print("[payload] Shutting down payload");
}
