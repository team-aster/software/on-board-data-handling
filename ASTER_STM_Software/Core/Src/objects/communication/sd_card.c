#include <stdio.h>
#include <string.h>
#include <ff.h>
#include "globals.h"
#include "threads/telemetry.h"
#include "threads/watchdog.h"
#include "printf.h"
#if SD_CARDS_ENABLED == 0
#    include <stm32f7xx_hal.h>
#endif /* SD_CARDS_ENABLED */
#include "objects/communication/sd_card.h"
#include "main.h"

/**
 * Whether or not to debug the SD-Cards.
 */
#define DEBUG_SD_CARDS 0

#if DEBUG_SD_CARDS
#    define PRINT_DEBUG(format, ...) print(format, __VA_ARGS__)
#else
#    define PRINT_DEBUG(format, ...) ((void) 0)
#endif /* DEBUG_ADCS */


/** The maximum number of tries to for opening a new log file. */
#define FILE_OPEN_MAX_TRIES 1000

/** The maximum size for a path buffer. */
#define MAX_PATH_BUFFER_LENGTH 256


/**
 * A connected SD-Card.
 */
typedef struct {
    /** The file system for this SD-Card. */
    FATFS fileSystem;

    /** The current log file for this SD-Card. */
    FIL logFile;

    /** Whether or not this SD-Card was initialized. */
    bool isInitialized;
    
    /** Whether or not the last error during initialization was a mount failure. */
    bool lastErrorWasMountFailure;

    /** A semaphore to allow access form multiple threads. */
    osSemaphoreId_t accessSemaphore;
} SdCard;

/** All connected SD-Cards. */
static SdCard sdCards[SD_CARD_COUNT] = {};


#if SD_CARDS_ENABLED == 1
/**
 * Get the path to the mount point of the given SD-Card.
 *
 * @param buffer A buffer to fill with the mount point path.
 * @param bufferSize The size of the given buffer.
 * @param sdCard The SD-Card to retrieve the mount path for.
 * @return The size of the mount point path.
 */
static size_t getMountPath(char* buffer, size_t bufferSize, SdCard* sdCard) {
    uint8_t sdCardIndex = (uint8_t) (sdCard - sdCards);
    return snprintf(buffer, bufferSize, "%u:/", (unsigned int) sdCardIndex);
}

/**
 * Close the SD-Card.
 * If not called during the initial setup, the caller should have
 * acquired the accessSemaphore of the SD-Card.
 *
 * @param sdCard The SD-Card to close.
 */
static void deInitializeSdCard(SdCard* sdCard) {
    if (!sdCard->isInitialized) {
        return;
    }
    sdCard->isInitialized = false;
    sdCard->lastErrorWasMountFailure = false;
    f_close(&sdCard->logFile);
    char mountPath[32];
    getMountPath(mountPath, ARRAY_SIZE(mountPath), sdCard);
    f_unmount(mountPath);
}

/**
 * Count the number of files in a given directory.
 *
 * @param directoryPath The path to the directory.
 * @param fileCount Space for the number of files found in the directory.
 * @return FR_OK on success or an error otherwise.
 */
static FRESULT getFileCount(const char* directoryPath, size_t* fileCount) {
    DIR directory;
    FILINFO fileInfo;
    *fileCount = 0;
    FRESULT result = f_opendir(&directory, directoryPath);
    if (result != FR_OK) {
        return result;
    }
    while (f_readdir(&directory, &fileInfo) == FR_OK && fileInfo.fname[0] != '\0') {
        if (!(fileInfo.fattrib & AM_DIR)) { // It is not a directory
            (*fileCount)++;
        }
    }
    f_closedir(&directory);
    return FR_OK;
}

/**
 * Open a new log file in the given directory.
 * Modifies the path string so it contains the path to the new log file.
 *
 * @param path The directory in which to open the log file and the path of the new log file
 *             if the function returns successfully.
 * @param dirPathLength The length of the given directory path.
 * @param file Space for opened file.
 * @return FR_OK on success or an error otherwise.
 */
static FRESULT openNewLogFile(char* path, size_t dirPathLength, FIL* file) {
    size_t filesInDirectory = 0;
    FRESULT result = getFileCount(path, &filesInDirectory);
    if (result != FR_OK) {
        printError("openNewLogFile: getFileCount failed: %u", (unsigned int) result);
    }
    do { // Try to find an unused file name
        snprintf(&path[dirPathLength], MAX_PATH_BUFFER_LENGTH - dirPathLength, "log%05d.bin",
            (int) filesInDirectory);
        filesInDirectory++;
    } while ((result = f_open(file, path, FA_WRITE | FA_CREATE_NEW)) == FR_EXIST &&
             filesInDirectory < FILE_OPEN_MAX_TRIES);
    return result;
}

/**
 * Initialize the given SD-Card and open a log file on it.
 *
 * @param sdCard The SD-Card to initialize.
 * @return Whether initialization of the SD-Card succeeded.
 */
static uint8_t initializeSdCard(SdCard* sdCard) {
    sdCard->isInitialized = false;
    char path[MAX_PATH_BUFFER_LENGTH];
    size_t mountPathLength = getMountPath(path, ARRAY_SIZE(path), sdCard);
    FRESULT result = f_mount(&sdCard->fileSystem, path, 1);
    if (result != FR_OK) {
        if (!sdCard->lastErrorWasMountFailure) {
            printError("Failed to mount file system '%s': %u", path, (unsigned int) result);
        }
        sdCard->lastErrorWasMountFailure = true;
        return 0;
    }
    sdCard->lastErrorWasMountFailure = false;

    result = openNewLogFile(path, mountPathLength, &sdCard->logFile);
    if (result != FR_OK) {
        printError("Failed to open new log file: %u", (unsigned int) result);
        path[mountPathLength + 1] = '\0';
        f_unmount(path);
        return 0;
    }
    sdCard->isInitialized = true;
    result = f_sync(&sdCard->logFile); // Sync the file to the SD-card.
    if (result != FR_OK) {
        printError("Failed to flush the empty log file %s: %u", path,
            (unsigned int) result);
        deInitializeSdCard(sdCard);
        return 0;
    }
    printDebug("Opened log file: %s", path);
    return 1;
}
#endif /* SD_CARDS_ENABLED */

bool initializeSDCardCommunication(void) {
    MX_SPI2_Init();
    if (getLastSystemError() == SPI2_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize SPI2 for SD-Card 2");
    }
    MX_SPI4_Init();
    if (getLastSystemError() == SPI4_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize SPI4 for SD-Card 1");
    }
    bool atLeastOneSuccessfulInit = false;
    memset(sdCards, 0, sizeof(sdCards));
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if ((sdCards[sdCardIndex].accessSemaphore = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
            printCriticalError("Failed to initialize access semaphore for SD-Card %u",
                (unsigned int) sdCardIndex);
            continue;
        }
#if SD_CARDS_ENABLED == 1
        if (!initializeSdCard(&sdCards[sdCardIndex])) {
            printError("Failed to initialize SD-Card %u", (unsigned int) sdCardIndex);
        } else
#endif /* SD_CARDS_ENABLED */
        {
            atLeastOneSuccessfulInit = true;
        }
    }
    return atLeastOneSuccessfulInit;
}

bool writeToSdCards(const void* data, size_t size) {
#if SD_CARDS_ENABLED == 0
    UNUSED(data);
    UNUSED(size);
    return false;
#else
    bool haveWriteSuccess = false;
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if (!sdCards[sdCardIndex].isInitialized ||
            (osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 0U) != osOK &&
                osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 10U) != osOK)) {
            continue;
        }
        UINT bytesWritten = 0;
        FRESULT result = f_write(&sdCards[sdCardIndex].logFile, data, size, &bytesWritten);
        if (result != FR_OK) {
            deInitializeSdCard(&sdCards[sdCardIndex]);
        } else {
            haveWriteSuccess = true;
        }
        osSemaphoreRelease(sdCards[sdCardIndex].accessSemaphore);
    }
    return haveWriteSuccess;
#endif /* SD_CARDS_ENABLED */
}

bool haveInitializedSdCards(void) {
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if (sdCards[sdCardIndex].isInitialized) {
            return true;
        }
    }
    return false;
}

bool isSdCardSubsystemInitialized(void) {
#if SD_CARD_COUNT <= 0
    return false;
#else
    return sdCards[0].accessSemaphore != NULL;
#endif /* SD_CARD_COUNT */
}

void switchToNewLogFile(void) {
#if SD_CARDS_ENABLED == 1
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if (osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 0U) != osOK &&
            osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 10U) != osOK) {
            continue;
        }
        if (sdCards[sdCardIndex].isInitialized) {
            f_close(&sdCards[sdCardIndex].logFile);
            char path[MAX_PATH_BUFFER_LENGTH];
            size_t mountPathLength = getMountPath(path, ARRAY_SIZE(path), &sdCards[sdCardIndex]);
            FRESULT result = openNewLogFile(path, mountPathLength, &sdCards[sdCardIndex].logFile);
            if (result != FR_OK) {
                path[mountPathLength + 1] = '\0';
                f_unmount(path);
                sdCards[sdCardIndex].isInitialized = false;
                printError("Failed to open new log file for SD-Card %u",
                    (unsigned int) sdCardIndex);
            } else {
                print("Opened new log file %s", path);
            }
        } else if (!initializeSdCard(&sdCards[sdCardIndex])) {
            printError("Failed to initialize SD-Card %u while switching log files",
                (unsigned int) sdCardIndex);
        }
        osSemaphoreRelease(sdCards[sdCardIndex].accessSemaphore);
    }
#endif /* SD_CARDS_ENABLED */
}

void flushLogFiles(void) {
#if SD_CARDS_ENABLED == 1
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if (!sdCards[sdCardIndex].isInitialized ||
            (osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 0U) != osOK &&
                osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 10U) != osOK)) {
            continue;
        }
        FRESULT result = f_sync(&sdCards[sdCardIndex].logFile);
        if (result != FR_OK) {
            deInitializeSdCard(&sdCards[sdCardIndex]);
            printError("Failed to sync log file on SD-Card %u: %u",
                (unsigned int) sdCardIndex, (unsigned int) result);
        }
        osSemaphoreRelease(sdCards[sdCardIndex].accessSemaphore);
    }
#endif /* SD_CARDS_ENABLED */
}

void reinitializeErroredSdCards(void) {
#if SD_CARDS_ENABLED == 1
    for (uint8_t sdCardIndex = 0; sdCardIndex < SD_CARD_COUNT; sdCardIndex++) {
        if (sdCards[sdCardIndex].isInitialized ||
            (osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 0U) != osOK &&
                osSemaphoreAcquire(sdCards[sdCardIndex].accessSemaphore, 10U) != osOK)) {
            continue;
        }
        PRINT_DEBUG("Reinitializing SD-Card %u", (unsigned int) sdCardIndex);
        if (!initializeSdCard(&sdCards[sdCardIndex])) {
            PRINT_DEBUG("Failed to reinitialize SD-Card %u", (unsigned int) sdCardIndex);
        }
        osSemaphoreRelease(sdCards[sdCardIndex].accessSemaphore);
        refreshWatchdogTimer();
    }
#endif /* SD_CARDS_ENABLED */
}
