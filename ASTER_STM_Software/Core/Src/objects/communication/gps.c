#include <string.h>
#include "objects/communication/gps.h"
#include "threads/telemetry.h"
#include "threads/watchdog.h"
#include "objects/ringBuffer.h"
#include "data/gps.h"
#include "main.h"

/** Whether or not to enable GPS debugging prints. */
#define DEBUG_GPS 0

#if DEBUG_GPS
#    define PRINT_DEBUG_GPS(...) printDebug(__VA_ARGS__)
#    define PRINT_DEBUG_GPS_ERROR(...) printError(__VA_ARGS__)
#else
#    define PRINT_DEBUG_GPS(...) ((void) 0)
#    define PRINT_DEBUG_GPS_ERROR(...) ((void) 0)
#endif /* DEBUG_ADCS */

/** The first byte of a GPS message that is used for synchronisation. */
#define GPS_SYNC_BYTE1 0xB5
/** The second byte of a GPS message that is used for synchronisation. */
#define GPS_SYNC_BYTE2 0x62

/** Apply the dynamic model setting in an UBX-CFG-NAV5 message. */
#define DYNAMIC_MODEL_SETTING 0x1U

/** A dynamic model for airborne conditions with accelerations up to 2g. */
#define DYNAMIC_MODEL_AIRBORNE_2G 7

/** The factor to multiply the raw position data with to get the actual position value. */
#define POSITION_FACTOR 1e-7

/** A meter in millimeters. */
#define METER_IN_MILLIMETER 1000.0

/**
 * Return a NMEA message from compile time constant string parts
 *
 * @param talkerId: The target of the message.
 * @param command: The command to send.
 * @param data: Data for the command.
 * @param checksum: The checksum of the talkerId, command and data.
 * @return The NMEA message.
 */
#define NMEA_MESSAGE(talkerId, command, data, checksum) \
    ("$" talkerId command "," data "*" checksum "\r\n")

/** A NMEA message to switch to the UBX protocol. */
#define GPS_SWITCH_TO_UBX_COMMAND NMEA_MESSAGE("P", "UBX", "41,1,0001,0001,9600,0", "14")

/** A ring buffer to buffer incoming data from the GPS module. */
DEFINE_RING_BUFFER(gpsReceiveBuffer, 512);

/** DMA for handling messaged from the GPS module.*/
#define gpsReader hdma_usart1_rx
extern DMA_HandleTypeDef gpsReader;

/** Acknowledge a message that was send earlier. */
#define GPS_ACKNOWLEDGE ((GpsMessageType) {0x05, 0x01})

/** Reject a message that was send earlier. */
#define GPS_NEGATIVE_ACKNOWLEDGE ((GpsMessageType) {0x05, 0x00})

/** Sets the reporting rate of certain messages. */
#define GPS_MESSAGE_RATE_COMMAND ((GpsMessageType) {0x06, 0x01})

/** Contains information about our current position. */
#define GPS_POSITION_MESSAGE ((GpsMessageType) {0x01, 0x02})

/**
 * Contains information about satellites that are either known to be visible
 * or currently tracked by the receiver.
 */
#define GPS_SATELLITE_STATUS_MESSAGE ((GpsMessageType) {0x01, 0x35})

/** Allows to configure the navigation engine. */
#define GPS_NAVIGATION_CONFIG_MESSAGE ((GpsMessageType) {0x06, 0x24})

/**
 * Compare two message GPS types.
 *
 * @param type1: The first GPS message type.
 * @param type2: The second GPS message type.
 * @return Whether or not the two types are the same.
 */
#define GPS_TYPE_EQUALS(type1, type2) ((type1).id == (type2).id && (type1).clazz == (type2).clazz)

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
/**
 * Converts a little endian value to a value with native byte ordering.
 *
 * @param value The 32 bit value.
 * @return The value in native byte ordering.
 */
#    define LITTLE_ENDIAN_TO_NATIVE_32(value)                                                \
        ((((value) << 24U) | (((value) &0x0000FF00) << 8U) | (((value) &0x00FF0000) >> 8U) | \
            ((value) >> 24U)))
#else
/**
 * Converts a little endian value to a value with native byte ordering.
 *
 * @param value The 32 bit value.
 * @return The value in native byte ordering.
 */
#    define LITTLE_ENDIAN_TO_NATIVE_32(value) value
#endif


/** Identifies a GPS message. */
typedef struct {
    uint8_t clazz; /** The class of the GPS message. */
    uint8_t id; /** The id of the GPS message. */
} GpsMessageType;

/** The header of every GPS message. */
typedef struct __packed {
    /** A synchronization marker marking the beginning of a GPS message. */
    struct {
        uint8_t b1; /** The first synchronization byte. */
        uint8_t b2; /** The second synchronization byte. */
    } sync;
    uint8_t clazz; /** The class of the GPS message. */
    uint8_t id; /** The id of the GPS message. */
    uint16_t length; /** The length of the GPS message. */
} GpsHeader;

/** The checksum of a GPS message. */
typedef struct __packed {
    uint8_t CSA; /** The first byte of the GPS checksum. */
    uint8_t CSB; /** The second byte of the GPS checksum. */
} GpsChecksum;

/** The body of an acknowledge or reject message. */
typedef struct __packed {
    uint8_t clazz; /** The class id of the acknowledged/rejected message. */
    uint8_t id; /** The message id of the acknowledged/rejected message. */
} UbxAckData;

/**
 * Part of the body of an satellite information message
 * containing details about a specific satellite.
 */
typedef struct __packed {
    uint8_t gnssId; /** GNSS identifier. */
    uint8_t id; /** Satellite identifier. */
    uint8_t carrierToNoise; /** Carrier to noise ratio (signal strength) in dBHz. */
    int8_t elevation; /** Elevation in deg (range: +/-90), unknown if out of range. */
    int16_t azimuth; /** Azimuth in deg (range 0-360), unknown if elevation is out of range. */
    int16_t pseudoRangeResidual; /** Pseudo range residual in m (0.1 scaling). */
    uint32_t flags; /** Additional flags. */
} UbxSatelliteInfoDataPart;


/** The body of an satellite information message. */
typedef struct __packed {
    uint32_t iTOW; /** GPS time of week of the navigation epoch in ms. */
    uint8_t version; /** Message version. */
    uint8_t numSatellites; /* Number of satellites. */
    uint8_t reserved1[2];
    UbxSatelliteInfoDataPart satelliteInfos[0]; /** Information about the individual satellites. */
} UbxSatelliteInfoData;

/** The body of a position information message. */
typedef struct __packed {
    uint32_t time; /** GPS time of week of the navigation epoch in ms. */
    int32_t longitude; /** Longitude in deg (1e-7 scaling). */
    int32_t latitude; /** Latitude in deg (1e-7 scaling). */
    int32_t height; /** Height above ellipsoid in mm. */
    int32_t heightAboveMsl; /** Height above mean sea level in mm. */
    uint32_t horizontalAccuracy; /** Horizontal accuracy estimate in mm. */
    uint32_t verticalAccuracy; /** Vertical accuracy estimate in mm. */
} UbxPositionData;

/** Configuration for the navigation engine. */
typedef struct __packed {
    uint16_t parameterMask; /** Parameters bitmask. Only the masked parameters will be applied. */
    uint8_t dynMode; /** Dynamic platform model. */
    uint8_t fixMode; /** Position fixing mode. */
    int32_t fixedAltitude; /** Fixed altitude (mean sea level) for 2D fix mode in cm. */
    uint32_t fixedAltitudeVariance; /** Fixed altitude variance for 2D mode in cm^2. */
    int8_t minElevation; /** Minimum elevation for a GNSS satellite to be used in NAV in deg. */
    uint8_t drLimit; /** Reserved (s). */
    uint16_t positionDop; /** Position DOP mask to use. */
    uint16_t timeDop; /** Time DOP mask to use. */
    uint16_t positionAccuracy; /** Position accuracy mask in m. */
    uint16_t timeAccuracy; /** Time accuracy mask in m. */
    uint8_t staticHoldThreshold; /** Static hold threshold in cm/s. */
    uint8_t dgnssTimeout; /** DGNSS timeout in s. */
    /** Number of satellites required to have C/N0 above cnoThreshold for a fix to be attempted. */
    uint8_t cnoThresholdNumSVs;
    uint8_t cnoThreshold; /** C/N0 threshold in db for deciding whether to attempt a fix. */
    uint8_t reserved1[2];
    /** Static hold distance threshold in m (before quitting static hold). */
    uint16_t statHoldMaxDistance;
    uint8_t utcStandard; /** UTC standard to be used. */
    uint8_t reserved2[5];
} UbxNavigationConfigData;

/** The body of a message rate configuration message. */
typedef struct __packed {
    uint8_t clazz; /** Message class. */
    uint8_t id; /** Message identifier. */
    uint8_t rate; /** Send rate on current port. */
} UbxMessageRateConfigData;

/** The states of GPS receiver communication configurations. */
typedef enum {
    /** The communication with the GPS receiver is not configured. */
    GPS_NOT_CONFIGURED,
    /** The position message rate is currently being configured. */
    GPS_CONFIGURING_POSITION_MSG_RATE,
    /** The satellite availability message rate is currently being configured. */
    GPS_CONFIGURING_SATELLITE_MSG_RATE,
    /** The navigation configuration is currently being configured. */
    GPS_CONFIGURING_NAVIGATION,
    /** The communication with the GPS receiver is configured. */
    GPS_CONFIGURED,
} GpsConfigState;

/** The current GPS receiver communication configuration state. */
static volatile GpsConfigState configState = GPS_NOT_CONFIGURED;


bool initGps(void) {
    MX_USART1_UART_Init();
    if (getLastSystemError() == USART1_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize Gps UART1");
        return false;
    } else if (getLastSystemError() == UART_DMA_INIT_SYSTEM_ERROR) {
        printCriticalError("Unable to initialize the DMA for Gps UART1");
        return false;
    } else if (HAL_UARTEx_ReceiveToIdle_DMA(&gpsUart, gpsReceiveBuffer.memory,
                   gpsReceiveBuffer.size) != HAL_OK) {
        printCriticalError("Unable to set up DMA handler for GPS UART1");
        return false;
    }
    return true;
}

/**
 * Send some bytes to the GPS module.
 *
 * @param data The bytes to send.
 * @param dataLength The number rof bytes to send.
 * @return Whether or not the bytes were send successfully.
 */
static bool writeGPS(const void* data, uint16_t dataLength) {
    return HAL_UART_Transmit(&gpsUart, (const uint8_t*) data, dataLength, HAL_MAX_DELAY) == HAL_OK;
}

/**
 * Calculate the checksum of a GPS message. Supports ring buffers.
 *
 * @param buffer A buffer that the message is stored in. Can be the memory of a ring buffer.
 * @param bufferSize The size of the buffer.
 * @param bufferOffset The offset into the buffer from where to start calculating the checksum.
 * @param msgSize The size of the message part to calculate the checksum for.
 * @param checksum A location to write the calculated checksum to.
 */
static void calculateChecksum(const uint8_t* buffer, size_t bufferSize, size_t bufferOffset,
    size_t msgSize, GpsChecksum* checksum) {
    checksum->CSA = 0;
    checksum->CSB = 0;
    for (size_t i = bufferOffset; i < bufferOffset + msgSize; i++) {
        checksum->CSA = checksum->CSA + buffer[i % bufferSize];
        checksum->CSB = checksum->CSA + checksum->CSB;
    }
}

/**
 * Sens a GPS message to the GPS module.
 *
 * @param type The type of message to send.
 * @param message The data of the message to send.
 * @param messageSize The size of the message data in bytes.
 * @return Whether or not the message was send successfully.
 */
static bool sendGpsMessage(GpsMessageType type, const void* message, uint16_t messageSize) {
    ASSERT(messageSize <= UINT16_MAX - sizeof(GpsHeader) - sizeof(GpsChecksum),
        "Trying to send a GPS message which is too large: %hu", messageSize);
    uint16_t bufferSize = sizeof(GpsHeader) + messageSize + sizeof(GpsChecksum);
    uint8_t buffer[bufferSize];
    GpsHeader* header = (GpsHeader*) buffer;
    header->sync.b1 = GPS_SYNC_BYTE1;
    header->sync.b2 = GPS_SYNC_BYTE2;
    header->clazz = type.clazz;
    header->id = type.id;
    header->length = messageSize;
    memcpy(buffer + sizeof(GpsHeader), message, messageSize);
    calculateChecksum(buffer, bufferSize, sizeof(header->sync),
        messageSize + sizeof(GpsHeader) - sizeof(header->sync),
        (GpsChecksum*) (buffer + sizeof(GpsHeader) + messageSize));
    return writeGPS(buffer, bufferSize);
}

/**
 * Enable the regular periodic sending of a GPS message.
 *
 * @param type The message to enable.
 * @return Whether or not the message was sent successful.
 */
static bool enableGpsMessage(GpsMessageType type) {
    UbxMessageRateConfigData rateConfig = {
        .clazz = type.clazz,
        .id = type.id,
        .rate = 1, // Every message.
    };
    return sendGpsMessage(GPS_MESSAGE_RATE_COMMAND, &rateConfig, sizeof(rateConfig));
}

/**
 * Configure the navigational config of the GPS receiver.
 *
 * @return Whether or not the configuration was send successfully.
 */
static bool configureNavSettings(void) {
    UbxNavigationConfigData msg_NAV5 = {
        .parameterMask = DYNAMIC_MODEL_SETTING,
        .dynMode = DYNAMIC_MODEL_AIRBORNE_2G,
    };
    return sendGpsMessage(GPS_NAVIGATION_CONFIG_MESSAGE, &msg_NAV5, sizeof(msg_NAV5));
}

bool configureGps(void) {
    if (!writeGPS(GPS_SWITCH_TO_UBX_COMMAND, strlen(GPS_SWITCH_TO_UBX_COMMAND))) {
        printError("Failed to send the UBX protocol switch command");
        return false;
    }
    configState = GPS_NOT_CONFIGURED;
    refreshWatchdogTimer();
    osDelay(100); // Wait for the gps receiver to switch
    refreshWatchdogTimer();
    configState = GPS_CONFIGURING_POSITION_MSG_RATE;
    enableGpsMessage(GPS_POSITION_MESSAGE);
    // The rest of the configuration happens asynchronously.
    return true;
}

bool gpsConfigurationCompleted(void) {
    return configState == GPS_CONFIGURED;
}

/**
 * Verify the current message in the GPS receiver buffer with the given length.
 *
 * @param length The length of the payload in the current GPS message.
 * @return Whether the message matches its checksum.
 */
static bool verifyGpsChecksum(uint16_t length) {
    GpsChecksum checksum;
    rbRegionRef_t checksumRegion;
    rbGetRegion(&gpsReceiveBuffer, &checksumRegion,
        RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer, gpsReceiveBuffer.readIndex,
            sizeof(GpsHeader) + length),
        sizeof(GpsChecksum));
    calculateChecksum(gpsReceiveBuffer.memory, gpsReceiveBuffer.size,
        RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer, gpsReceiveBuffer.readIndex,
            offsetof(GpsHeader, clazz)),
        sizeof(GpsHeader) - offsetof(GpsHeader, clazz) + length, &checksum);
    return rbMemEqual(&checksumRegion, &checksum, sizeof(checksum));
}

static void handleGpsMessage(GpsMessageType type, ringBuffer_t* buffer, uint16_t dataLength) {
    if (GPS_TYPE_EQUALS(type, GPS_ACKNOWLEDGE)) {
        if (dataLength != sizeof(UbxAckData)) {
            PRINT_DEBUG_GPS_ERROR("Received acknowledge message with invalid length: %hu",
                dataLength);
            return;
        }
        GpsMessageType originalType = {
            .clazz = buffer->memory[RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxAckData, clazz))],
            .id = buffer->memory[RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxAckData, id))],
        };
        PRINT_DEBUG_GPS("GPS: Received ACK for (%02X, %02X)", originalType.clazz, originalType.id);
        if (GPS_TYPE_EQUALS(originalType, GPS_MESSAGE_RATE_COMMAND)) {
            if (configState == GPS_CONFIGURING_POSITION_MSG_RATE) {
                if (!enableGpsMessage(GPS_SATELLITE_STATUS_MESSAGE)) {
                    printError("Failed to send GPS position message enable command");
                }
                configState = GPS_CONFIGURING_SATELLITE_MSG_RATE;
            } else if (configState == GPS_CONFIGURING_SATELLITE_MSG_RATE) {
                if (!configureNavSettings()) {
                    printError("Failed to send GPS navigation configuration");
                }
                configState = GPS_CONFIGURING_NAVIGATION;
            }
        } else if (GPS_TYPE_EQUALS(originalType, GPS_NAVIGATION_CONFIG_MESSAGE)) {
            configState = GPS_CONFIGURED;
            PRINT_DEBUG_GPS("GPS config complete");
        }
    } else if (GPS_TYPE_EQUALS(type, GPS_NEGATIVE_ACKNOWLEDGE)) {
        if (dataLength != sizeof(UbxAckData)) {
            PRINT_DEBUG_GPS_ERROR("Received reject message with invalid length: %hu", dataLength);
            return;
        }
        GpsMessageType originalType = {
            .clazz = buffer->memory[RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxAckData, clazz))],
            .id = buffer->memory[RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxAckData, id))],
        };
        PRINT_DEBUG_GPS("GPS: Command failed, received NAK for (%02X, %02X)", originalType.clazz,
            originalType.id);
        if (GPS_TYPE_EQUALS(originalType, GPS_MESSAGE_RATE_COMMAND)) {
            if (configState == GPS_CONFIGURING_POSITION_MSG_RATE) {
                if (!enableGpsMessage(GPS_POSITION_MESSAGE)) {
                    printError("Failed to resend GPS position message enable command");
                }
            } else if (configState == GPS_CONFIGURING_SATELLITE_MSG_RATE) {
                if (!enableGpsMessage(GPS_SATELLITE_STATUS_MESSAGE)) {
                    printError("Failed to resend GPS satellite status message enable command");
                }
            }
        } else if (GPS_TYPE_EQUALS(originalType, GPS_NAVIGATION_CONFIG_MESSAGE)) {
            if (!configureNavSettings()) {
                printError("Failed to resend GPS navigation configuration");
            }
        }
    } else if (GPS_TYPE_EQUALS(type, GPS_SATELLITE_STATUS_MESSAGE)) {
        if (dataLength < sizeof(UbxSatelliteInfoData) ||
            (dataLength - sizeof(UbxSatelliteInfoData)) % sizeof(UbxSatelliteInfoDataPart) != 0) {
            PRINT_DEBUG_GPS_ERROR("Received satellite info message with invalid length: %hu",
                dataLength);
            return;
        }
#if DEBUG_GPS
        uint8_t numberOfSatellites = buffer->memory[RELATIVE_RING_BUFFER_INDEX(buffer,
            buffer->readIndex, offsetof(UbxSatelliteInfoData, numSatellites))];
        PRINT_DEBUG_GPS("GPS: Got satellite availability: %u", (unsigned int) numberOfSatellites);
#endif /* DEBUG_GPS */
    } else if (GPS_TYPE_EQUALS(type, GPS_POSITION_MESSAGE)) {
        if (dataLength != sizeof(UbxPositionData)) {
            PRINT_DEBUG_GPS_ERROR("Received position message with invalid length: %hu", dataLength);
            return;
        }
        int32_t longitude, latitude, height;
        rbRegionRef_t region;
        rbGetRegion(buffer, &region,
            RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxPositionData, longitude)),
            sizeof(longitude));
        rbMemCpy(&region, &longitude, sizeof(longitude));
        rbGetRegion(buffer, &region,
            RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxPositionData, latitude)),
            sizeof(latitude));
        rbMemCpy(&region, &latitude, sizeof(latitude));
        rbGetRegion(buffer, &region,
            RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex,
                offsetof(UbxPositionData, height)),
            sizeof(height));
        rbMemCpy(&region, &height, sizeof(height));
        uint32_t time;
        rbGetRegion(buffer, &region,
            RELATIVE_RING_BUFFER_INDEX(buffer, buffer->readIndex, offsetof(UbxPositionData, time)),
            sizeof(time));
        rbMemCpy(&region, &time, sizeof(time));
        deg_t longitudeInDeg = deg_t(LITTLE_ENDIAN_TO_NATIVE_32(longitude) * POSITION_FACTOR);
        deg_t latitudeInDeg = deg_t(LITTLE_ENDIAN_TO_NATIVE_32(latitude) * POSITION_FACTOR);
        meter_t heightInM = meter_t(LITTLE_ENDIAN_TO_NATIVE_32(height) / METER_IN_MILLIMETER);
        ms_t timeMs = ms_t(LITTLE_ENDIAN_TO_NATIVE_32(time));
        PRINT_DEBUG_GPS("GPS: Got data message, lat: %f°, long: %f°, height: %fm, time: %lu",
            latitudeInDeg.deg, longitudeInDeg.deg, heightInM.meter, timeMs.ms);
        if (osSemaphoreAcquire(gpsSemaphore, 0U) == osOK ||
            osSemaphoreAcquire(gpsSemaphore, 10U) == osOK) {
            setBatch_gpsLongitude(&longitudeInDeg);
            setBatch_gpsLatitude(&latitudeInDeg);
            setBatch_gpsAltitude(&heightInM);
            setBatch_gpsTime(&timeMs);
            osSemaphoreRelease(gpsSemaphore);
        } else {
            printError("Failed to lock semaphore to update GPS location");
        }
    } else {
        print("Unhandled UBX message class: %X, id: %X", type.clazz, type.id);
    }
}

void onGpsDataReceived(void) {
    size_t writeIndex = updateWriteIndexFromDmaReader(&gpsReceiveBuffer, &gpsReader);
    size_t readSize = rbRead(&gpsReceiveBuffer, NULL, 0);
    PRINT_DEBUG_GPS("Checking gps (readSize: %d, writeIndex: %d)", (int) readSize,
        (int) writeIndex);
    while (gpsReceiveBuffer.readIndex != writeIndex &&
           readSize > sizeof(GpsHeader) + sizeof(GpsChecksum)) {
        if (gpsReceiveBuffer.memory[gpsReceiveBuffer.readIndex] != GPS_SYNC_BYTE1) {
            readSize = rbAdvanceReadIndex(&gpsReceiveBuffer, 1);
            continue;
        }
        if (gpsReceiveBuffer.memory[RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer,
                gpsReceiveBuffer.readIndex, 1)] != GPS_SYNC_BYTE2) {
            readSize = rbAdvanceReadIndex(&gpsReceiveBuffer, 2);
            continue;
        }
        uint16_t length;
        rbRegionRef_t lengthRegion;
        rbGetRegion(&gpsReceiveBuffer, &lengthRegion,
            RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer, gpsReceiveBuffer.readIndex,
                offsetof(GpsHeader, length)),
            sizeof(length));
        rbMemCpy(&lengthRegion, &length, sizeof(length));
        if (length > gpsReceiveBuffer.size - sizeof(GpsHeader) - sizeof(GpsChecksum)) {
            PRINT_DEBUG_GPS_ERROR("Ignoring GPS message with too large size %hu", length);
            readSize = rbAdvanceReadIndex(&gpsReceiveBuffer, 2);
            continue;
        }
        if (readSize < length + sizeof(GpsHeader) + sizeof(GpsChecksum)) {
            break;
        }
        if (!verifyGpsChecksum(length)) {
            PRINT_DEBUG_GPS_ERROR("Checksum mismatch for GPS message");
            readSize = rbAdvanceReadIndex(&gpsReceiveBuffer, 2);
            continue;
        }
        GpsMessageType messageType = {
            .id = gpsReceiveBuffer.memory[RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer,
                gpsReceiveBuffer.readIndex, offsetof(GpsHeader, id))],
            .clazz = gpsReceiveBuffer.memory[RELATIVE_RING_BUFFER_INDEX(&gpsReceiveBuffer,
                gpsReceiveBuffer.readIndex, offsetof(GpsHeader, clazz))],
        };
        rbAdvanceReadIndex(&gpsReceiveBuffer, sizeof(GpsHeader));
        handleGpsMessage(messageType, &gpsReceiveBuffer, length);
        readSize = rbAdvanceReadIndex(&gpsReceiveBuffer, length + sizeof(GpsChecksum));
    }
}

void onGpsError(void) {
    // Suppress the error message if the GPS module is not connected
    // or has received data before initialization.
    if ((gpsUart.ErrorCode != HAL_UART_ERROR_FE && gpsUart.ErrorCode != HAL_UART_ERROR_ORE) ||
        gpsReceiveBuffer.readIndex != 0) {
        printError("GPS communication error: %lu", (unsigned long) gpsUart.ErrorCode);
    }
    // Restart the DMA reception
    HAL_UART_DMAStop(&gpsUart);
    HAL_UARTEx_ReceiveToIdle_DMA(&gpsUart, gpsReceiveBuffer.memory, gpsReceiveBuffer.size);
    // Discard all received buffered data
    updateWriteIndexFromDmaReader(&gpsReceiveBuffer, &gpsReader);
}
