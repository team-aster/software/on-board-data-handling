#include <math.h>
#include <stm32f7xx_hal.h>
#include <cmsis_os2.h>
#include "threads/telemetry.h"
#include "threads/watchdog.h"
#include "objects/communication/imu.h"
#include "main.h"


#if IMU_ENABLED

#    define DEBUG_IMU 0
#    if DEBUG_IMU
#        define PRINT_DEBUG(...) printDebug(__VA_ARGS__)
#    else
#        define PRINT_DEBUG(...) ((void) 0)
#    endif /* DEBUG_IMU */

#    ifndef __BYTE_ORDER__
#        error "__BYTE_ORDER__" is not defined!
#    endif

/** How many measurements should be done for the IMU calibration. */
#    define CALIBRATION_MEASUREMENTS 10

/** The amount of 10 ms that an IMU should be waited for during initialization. */
#    define MAX_IMU_INIT_WAIT 50

/**
 * The I2C address of both IMUs on their respective busses.
 */
#    define IMU_ADDRESS 0x50U

/**
 * I2C for the first IMU.
 */
#    define imu1I2C hi2c2
extern I2C_HandleTypeDef imu1I2C;

/**
 * I2C for the second IMU.
 */
#    define imu2I2C hi2c3
extern I2C_HandleTypeDef imu2I2C;

/**
 * Output acceleration values and gravity vectors in m/(s^2).
 */
#    define ACCEL_M_PER_S2 0b00000000U

/**
 * Output angular rates in radians per seconds.
 */
#    define ANGULAR_RATE_RPS 0b00000010U

/**
 * Output Euler angles in radians.
 */
#    define EULER_ANGLES_RAD 0b00000100U

/**
 * Output temperature in degrees Celsius.
 */
#    define TEMPERATURE_CELSIUS 0b00000000U

/**
 * Output fusion data with an orientation definition described by the Android operating system.
 */
#    define FUSION_OUTPUT_ANDROID 0b10000000U

/** Set the gyroscope to have a range of 125 degrees per second. */
#    define GYROSCOPE_RANGE_125 0b00000100U

/** Set a 116 Hz refresh rate for the gyroscope. */
#    define GYROSCOPE_BANDWIDTH_116 0b00010000U

/** Enable normal gyroscope operation mode. */
#    define GYROSCOPE_NORMAL_OPERATION 0b00000000U

/** Set the accelerometer to have a range of 2 G. */
#    define ACCELEROMETER_RANGE_2 0b00U

/** Set a 125 Hz refresh rate for the accelerometer. */
#    define ACCELEROMETER_BANDWIDTH_125 0b10000U

/** Enable normal accelerometer operation mode. */
#    define ACCELEROMETER_NORMAL_OPERATION 0b00000000U

/** Set a 30 Hz refresh rate for the magnetometer. */
#    define MAGNETOMETER_BANDWIDTH_30 0b111U

/** Enable normal magnetometer operation mode. */
#    define MAGNETOMETER_NORMAL_OPERATION 0b01000U

/** The bit in the SYS_TRIGGER register that initiates a system reset of the IMU. */
#    define RESET_SYSTEM (1U << 5U)

/** The bit in the SYS_TRIGGER register that initiates a self test of the IMU. */
#    define TRIGGER_SELF_TEST (1U << 0U)

/** The bit in the ST_RESULT register that indicates whether the CPU test succeeded. */
#    define CPU_SELF_TEST_RESULT (1U << 3U)

/** The bit in the ST_RESULT register that indicates whether the gyroscope test succeeded. */
#    define GYROSCOPE_SELF_TEST_RESULT (1U << 2U)

/** The bit in the ST_RESULT register that indicates whether the magnetometer test succeeded. */
#    define MAGNETOMETER_SELF_TEST_RESULT (1U << 1U)

/** The bit in the ST_RESULT register that indicates whether the accelerometer test succeeded. */
#    define ACCELEROMETER_SELF_TEST_RESULT (1U << 0U)

/** An axis of the IMU device coordinate system. */
typedef enum {
    /** The X axis. */
    X_IMU_AXIS = 0b00U,

    /** The Y axis. */
    Y_IMU_AXIS = 0b01U,

    /** The Z axis. */
    Z_IMU_AXIS = 0b10U,
} ImuAxis;

/**
 * Remap the axis of the IMU.
 *
 * @param newXAxis: The new axis that will be mapped to the old X axis.
 * @param newYAxis: The new axis that will be mapped to the old Y axis.
 * @param newZAxis: The new axis that will be mapped to the old Z axis.
 * @see ImuAxis
 */
#    define IMU_AXIS_CONFIG(newXAxis, newYAxis, newZAxis) \
        (((newZAxis) << 4U) | ((newYAxis) << 2U) | (newXAxis))

/** The sign of an axis in the IMU coordinate system. */
typedef enum {
    /** Positive (default) direction of the axis. */
    IMU_POSITIVE_SIGN = 0U,

    /** negative (inverse) direction of the axis. */
    IMU_NEGATIVE_SIGN = 1U,
} ImuAxisSign;

/**
 * Set the sign of an axis in the IMU coordinate system.
 *
 * @param xAxisSign: The new direction sign for the X axis.
 * @param yAxisSign: The new direction sign for the Y axis.
 * @param zAxisSign: The new direction sign for the Z axis.
 * @see ImuAxisSign
 */
#    define IMU_AXIS_SIGN_CONFIG(xAxisSign, yAxisSign, zAxisSign) \
        (((xAxisSign) << 2U) | ((yAxisSign) << 1U) | (zAxisSign))

/** The gyroscope output that represent one radiant per second. */
#    define GYROSCOPE_UNIT_SCALE 900.0

/** The accelerometer output that represent one m/(s^2). */
#    define ACCELEROMETER_UNIT_SCALE 100.0

/** The magnetometer output that represent one micro Tesla. */
#    define MAGNETOMETER_UNIT_SCALE 16.0

/** The orientation output that represent one quaternion element. */
#    define ORIENTATION_UNIT_SCALE 0x4000

/** BNO055 Address A **/
#    define BNO055_ADDRESS_A (0x28 << 1)
/** BNO055 Address B **/
#    define BNO055_ADDRESS_B (0x29 << 1)
/** BNO055 ID **/
#    define BNO055_ID 0xA0U

#    define BNO055_OPR_MODE_MASK 0b00001111U

typedef enum {
    /* Page id register definition */
    BNO055_PAGE_ID_ADDR = 0X07,

    /* PAGE0 REGISTER DEFINITION START*/
    BNO055_CHIP_ID_ADDR = 0x00,
    BNO055_ACCEL_REV_ID_ADDR = 0x01,
    BNO055_MAG_REV_ID_ADDR = 0x02,
    BNO055_GYRO_REV_ID_ADDR = 0x03,
    BNO055_SW_REV_ID_LSB_ADDR = 0x04,
    BNO055_SW_REV_ID_MSB_ADDR = 0x05,
    BNO055_BL_REV_ID_ADDR = 0X06,

    /** Page 0 **/

    /* Accel data register */
    BNO055_ACCEL_DATA_X_LSB_ADDR = 0X08,
    BNO055_ACCEL_DATA_X_MSB_ADDR = 0X09,
    BNO055_ACCEL_DATA_Y_LSB_ADDR = 0X0A,
    BNO055_ACCEL_DATA_Y_MSB_ADDR = 0X0B,
    BNO055_ACCEL_DATA_Z_LSB_ADDR = 0X0C,
    BNO055_ACCEL_DATA_Z_MSB_ADDR = 0X0D,

    /* Mag data register */
    BNO055_MAG_DATA_X_LSB_ADDR = 0X0E,
    BNO055_MAG_DATA_X_MSB_ADDR = 0X0F,
    BNO055_MAG_DATA_Y_LSB_ADDR = 0X10,
    BNO055_MAG_DATA_Y_MSB_ADDR = 0X11,
    BNO055_MAG_DATA_Z_LSB_ADDR = 0X12,
    BNO055_MAG_DATA_Z_MSB_ADDR = 0X13,

    /* Gyro data registers */
    BNO055_GYRO_DATA_X_LSB_ADDR = 0X14,
    BNO055_GYRO_DATA_X_MSB_ADDR = 0X15,
    BNO055_GYRO_DATA_Y_LSB_ADDR = 0X16,
    BNO055_GYRO_DATA_Y_MSB_ADDR = 0X17,
    BNO055_GYRO_DATA_Z_LSB_ADDR = 0X18,
    BNO055_GYRO_DATA_Z_MSB_ADDR = 0X19,

    /* Euler data registers */
    BNO055_EULER_H_LSB_ADDR = 0X1A,
    BNO055_EULER_H_MSB_ADDR = 0X1B,
    BNO055_EULER_R_LSB_ADDR = 0X1C,
    BNO055_EULER_R_MSB_ADDR = 0X1D,
    BNO055_EULER_P_LSB_ADDR = 0X1E,
    BNO055_EULER_P_MSB_ADDR = 0X1F,

    /* Quaternion data registers */
    BNO055_QUATERNION_DATA_W_LSB_ADDR = 0X20,
    BNO055_QUATERNION_DATA_W_MSB_ADDR = 0X21,
    BNO055_QUATERNION_DATA_X_LSB_ADDR = 0X22,
    BNO055_QUATERNION_DATA_X_MSB_ADDR = 0X23,
    BNO055_QUATERNION_DATA_Y_LSB_ADDR = 0X24,
    BNO055_QUATERNION_DATA_Y_MSB_ADDR = 0X25,
    BNO055_QUATERNION_DATA_Z_LSB_ADDR = 0X26,
    BNO055_QUATERNION_DATA_Z_MSB_ADDR = 0X27,

    /* Linear acceleration data registers */
    BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR = 0X28,
    BNO055_LINEAR_ACCEL_DATA_X_MSB_ADDR = 0X29,
    BNO055_LINEAR_ACCEL_DATA_Y_LSB_ADDR = 0X2A,
    BNO055_LINEAR_ACCEL_DATA_Y_MSB_ADDR = 0X2B,
    BNO055_LINEAR_ACCEL_DATA_Z_LSB_ADDR = 0X2C,
    BNO055_LINEAR_ACCEL_DATA_Z_MSB_ADDR = 0X2D,

    /* Gravity data registers */
    BNO055_GRAVITY_DATA_X_LSB_ADDR = 0X2E,
    BNO055_GRAVITY_DATA_X_MSB_ADDR = 0X2F,
    BNO055_GRAVITY_DATA_Y_LSB_ADDR = 0X30,
    BNO055_GRAVITY_DATA_Y_MSB_ADDR = 0X31,
    BNO055_GRAVITY_DATA_Z_LSB_ADDR = 0X32,
    BNO055_GRAVITY_DATA_Z_MSB_ADDR = 0X33,

    /* Temperature data register */
    BNO055_TEMP_ADDR = 0X34,

    /* Status registers */
    BNO055_CALIB_STAT_ADDR = 0X35,
    BNO055_SELF_TEST_RESULT_ADDR = 0X36,
    BNO055_INTR_STAT_ADDR = 0X37,

    BNO055_SYS_CLK_STAT_ADDR = 0X38,
    BNO055_SYS_STAT_ADDR = 0X39,
    BNO055_SYS_ERR_ADDR = 0X3A,

    /* Unit selection register */
    BNO055_UNIT_SEL_ADDR = 0X3B,
    BNO055_DATA_SELECT_ADDR = 0X3C,

    /* Mode registers */
    BNO055_OPR_MODE_ADDR = 0X3D,
    BNO055_PWR_MODE_ADDR = 0X3E,

    BNO055_SYS_TRIGGER_ADDR = 0X3F,
    BNO055_TEMP_SOURCE_ADDR = 0X40,

    /* Axis remap registers */
    BNO055_AXIS_MAP_CONFIG_ADDR = 0X41,
    BNO055_AXIS_MAP_SIGN_ADDR = 0X42,

    /* SIC registers */
    BNO055_SIC_MATRIX_0_LSB_ADDR = 0X43,
    BNO055_SIC_MATRIX_0_MSB_ADDR = 0X44,
    BNO055_SIC_MATRIX_1_LSB_ADDR = 0X45,
    BNO055_SIC_MATRIX_1_MSB_ADDR = 0X46,
    BNO055_SIC_MATRIX_2_LSB_ADDR = 0X47,
    BNO055_SIC_MATRIX_2_MSB_ADDR = 0X48,
    BNO055_SIC_MATRIX_3_LSB_ADDR = 0X49,
    BNO055_SIC_MATRIX_3_MSB_ADDR = 0X4A,
    BNO055_SIC_MATRIX_4_LSB_ADDR = 0X4B,
    BNO055_SIC_MATRIX_4_MSB_ADDR = 0X4C,
    BNO055_SIC_MATRIX_5_LSB_ADDR = 0X4D,
    BNO055_SIC_MATRIX_5_MSB_ADDR = 0X4E,
    BNO055_SIC_MATRIX_6_LSB_ADDR = 0X4F,
    BNO055_SIC_MATRIX_6_MSB_ADDR = 0X50,
    BNO055_SIC_MATRIX_7_LSB_ADDR = 0X51,
    BNO055_SIC_MATRIX_7_MSB_ADDR = 0X52,
    BNO055_SIC_MATRIX_8_LSB_ADDR = 0X53,
    BNO055_SIC_MATRIX_8_MSB_ADDR = 0X54,

    /* Accelerometer Offset registers */
    ACCEL_OFFSET_X_LSB_ADDR = 0X55,
    ACCEL_OFFSET_X_MSB_ADDR = 0X56,
    ACCEL_OFFSET_Y_LSB_ADDR = 0X57,
    ACCEL_OFFSET_Y_MSB_ADDR = 0X58,
    ACCEL_OFFSET_Z_LSB_ADDR = 0X59,
    ACCEL_OFFSET_Z_MSB_ADDR = 0X5A,

    /* Magnetometer Offset registers */
    MAG_OFFSET_X_LSB_ADDR = 0X5B,
    MAG_OFFSET_X_MSB_ADDR = 0X5C,
    MAG_OFFSET_Y_LSB_ADDR = 0X5D,
    MAG_OFFSET_Y_MSB_ADDR = 0X5E,
    MAG_OFFSET_Z_LSB_ADDR = 0X5F,
    MAG_OFFSET_Z_MSB_ADDR = 0X60,

    /* Gyroscope Offset register s*/
    GYRO_OFFSET_X_LSB_ADDR = 0X61,
    GYRO_OFFSET_X_MSB_ADDR = 0X62,
    GYRO_OFFSET_Y_LSB_ADDR = 0X63,
    GYRO_OFFSET_Y_MSB_ADDR = 0X64,
    GYRO_OFFSET_Z_LSB_ADDR = 0X65,
    GYRO_OFFSET_Z_MSB_ADDR = 0X66,

    /* Radius registers */
    ACCEL_RADIUS_LSB_ADDR = 0X67,
    ACCEL_RADIUS_MSB_ADDR = 0X68,
    MAG_RADIUS_LSB_ADDR = 0X69,
    MAG_RADIUS_MSB_ADDR = 0X6A,

    /** Page 1 **/
    ACCELEROMETER_CONFIG_ADDRESS = 0x08,
    MAGNETOMETER_CONFIG_ADDRESS = 0x09,
    GYROSCOPE_CONFIG_1_ADDRESS = 0x0A,
    GYROSCOPE_CONFIG_2_ADDRESS = 0x0B,

} adafruit_bno055_reg_t;

/** BNO055 power settings */
typedef enum {
    POWER_MODE_NORMAL = 0X00,
    POWER_MODE_LOWPOWER = 0X01,
    POWER_MODE_SUSPEND = 0X02
} adafruit_bno055_powermode_t;

/** Operation mode settings **/
typedef enum {
    OPERATION_MODE_CONFIG = 0X00U,
    OPERATION_MODE_ACCONLY = 0X01U,
    OPERATION_MODE_MAGONLY = 0X02U,
    OPERATION_MODE_GYRONLY = 0X03U,
    OPERATION_MODE_ACCMAG = 0X04U,
    OPERATION_MODE_ACCGYRO = 0X05U,
    OPERATION_MODE_MAGGYRO = 0X06U,
    OPERATION_MODE_AMG = 0X07U,
    OPERATION_MODE_IMUPLUS = 0X08U,
    OPERATION_MODE_COMPASS = 0X09U,
    OPERATION_MODE_M4G = 0X0AU,
    OPERATION_MODE_NDOF_FMC_OFF = 0X0BU,
    OPERATION_MODE_NDOF = 0X0CU
} IMUOperationMode;

/** Remap settings **/
typedef enum {
    REMAP_CONFIG_P0 = 0x21,
    REMAP_CONFIG_P1 = 0x24, // default
    REMAP_CONFIG_P2 = 0x24,
    REMAP_CONFIG_P3 = 0x21,
    REMAP_CONFIG_P4 = 0x24,
    REMAP_CONFIG_P5 = 0x21,
    REMAP_CONFIG_P6 = 0x21,
    REMAP_CONFIG_P7 = 0x24
} adafruit_bno055_axis_remap_config_t;

/** Remap Signs **/
typedef enum {
    REMAP_SIGN_P0 = 0x04,
    REMAP_SIGN_P1 = 0x00, // default
    REMAP_SIGN_P2 = 0x06,
    REMAP_SIGN_P3 = 0x02,
    REMAP_SIGN_P4 = 0x03,
    REMAP_SIGN_P5 = 0x01,
    REMAP_SIGN_P6 = 0x07,
    REMAP_SIGN_P7 = 0x05
} adafruit_bno055_axis_remap_sign_t;

/** A structure to represent revisions **/
typedef struct {
    uint8_t accel_rev; /**< acceleration rev */
    uint8_t mag_rev; /**< magnetometer rev */
    uint8_t gyro_rev; /**< gyroscrope rev */
    uint16_t sw_rev; /**< SW rev */
    uint8_t bl_rev; /**< bootloader rev */
} adafruit_bno055_rev_info_t;

/** Vector Mappings **/
typedef enum {
    VECTOR_ACCELEROMETER = BNO055_ACCEL_DATA_X_LSB_ADDR,
    VECTOR_MAGNETOMETER = BNO055_MAG_DATA_X_LSB_ADDR,
    VECTOR_GYROSCOPE = BNO055_GYRO_DATA_X_LSB_ADDR,
    VECTOR_EULER = BNO055_EULER_H_LSB_ADDR,
    VECTOR_LINEARACCEL = BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR,
    VECTOR_GRAVITY = BNO055_GRAVITY_DATA_X_LSB_ADDR
} adafruit_vector_type_t;

typedef enum {
    SYSTEM_IDLE = 0U,
    SYSTEM_ERROR = 1U,
    INITIALIZING_PERIPHERALS = 2U,
    SYSTEM_INITIALIZATION = 3U,
    EXECUTING_SELF_TEST = 4U,
    RUNNING_WITH_SENSOR_FUSION = 5U,
    RUNNING_WITHOUT_SENSOR_FUSION6 = 6U,
    UNKNOWN = 0xFFU,
} ImuSystemStatus;

typedef enum {
    NORMAL_POWER_MODE = 0b00U,
    LOW_POWER_MODE = 0b01U,
    SUSPEND_POWER_MODE = 0b10U,
} ImuPowerMode;

typedef enum {
    NO_ERROR = 0x0U,
    PERIPHERAL_INIT_ERROR = 0x1U,
    SYSTEM_INIT_ERROR = 0x2U,
    SELF_TEST_FAILED = 0x3U,
    REGISTER_MAP_VALUE_OUT_OF_RANGE = 0x4U,
    REGISTER_MAP_ADDRESS_OUT_OF_RANGE = 0x5U,
    REGISTER_MAP_WRITE_ERROR = 0x6U,
    BNO_LOW_POWER_MODE_NOT_AVAILABLE = 0x7U,
    ACCELEROMETER_POWER_MODE_NOT_AVAILABLE = 0x8U,
    FUSION_ALGORITHM_CONFIG_ERROR = 0x9U,
    SENSOR_CONFIG_ERROR = 0xAU,
} ImuErrorState;

/** Storage for the fusion enabled mode of the IMUs. */
static bool imuFusionConfig[] = {
    [IMU1] = false,
    [IMU2] = false,
};

/** A semaphore that mus be acquired for reading and writing to the IMU1. */
static osSemaphoreId_t imu1AccessSemaphore = NULL;

/** A semaphore that mus be acquired for reading and writing to the IMU2. */
static osSemaphoreId_t imu2AccessSemaphore = NULL;


bool initImuCommunication(void) {
    bool success = true;
    if ((imu1AccessSemaphore = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
        printCriticalError("Unable to create access semaphore for IMU1");
        success = false;
    }
    if ((imu2AccessSemaphore = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
        printCriticalError("Unable to create access semaphore for IMU2");
        success = false;
    }
    MX_I2C2_Init();
    if (getLastSystemError() == I2C2_INIT_SYSTEM_ERROR) {
        imu1I2C.Instance = NULL;
        printCriticalError("Unable to initialize I2C2 for IMU1");
        success = false;
    }
    MX_I2C3_Init();
    if (getLastSystemError() == I2C3_INIT_SYSTEM_ERROR) {
        imu2I2C.Instance = NULL;
        printCriticalError("Unable to initialize I2C3 for IMU2");
        success = false;
    }
    return success;
}

/**
 * Write one byte to the IMU.
 *
 * @param imu The IMU to write the byte to.
 * @param dataAddress The address within the IMU to write the byte to.
 * @param data The byte to write.
 * @return Whether or not the write was successful.
 */
static bool writeIMU(IMU imu, uint8_t dataAddress, uint8_t data) {
    uint8_t dataArray[2];
    dataArray[0] = dataAddress;
    dataArray[1] = data;
    I2C_HandleTypeDef* imuHandle = imu == IMU1 ? &imu1I2C : &imu2I2C;
    osSemaphoreId_t accessSemaphore = imu == IMU1 ? imu1AccessSemaphore : imu2AccessSemaphore;
    if (imuHandle->Instance == NULL || accessSemaphore == NULL ||
        osSemaphoreAcquire(accessSemaphore, 10) != osOK) {
        return false;
    }
    bool success = HAL_I2C_Master_Transmit(imuHandle, IMU_ADDRESS, (uint8_t*) dataArray,
                       sizeof(dataArray), HAL_MAX_DELAY) == HAL_OK;
    osSemaphoreRelease(accessSemaphore);
    return success;
}

/**
 * Read data from the IMU.
 *
 * @param imu The IMU to read data from.
 * @param dataAddress The address within the IMU from which to start reading data.
 * @param buffer The buffer to write the data to.
 * @param bufferLength The size of the buffer and how many bytes to read.
 * @return Whether the data was read successfully.
 */
static bool readIMU(IMU imu, uint8_t dataAddress, void* buffer, uint8_t bufferLength) {
    I2C_HandleTypeDef* imuHandle = imu == IMU1 ? &imu1I2C : &imu2I2C;
    osSemaphoreId_t accessSemaphore = imu == IMU1 ? imu1AccessSemaphore : imu2AccessSemaphore;
    if (imuHandle->Instance == NULL || accessSemaphore == NULL ||
        osSemaphoreAcquire(accessSemaphore, 10) != osOK) {
        return false;
    }
    bool success =
        HAL_I2C_Master_Transmit(imuHandle, IMU_ADDRESS, &dataAddress, 1, HAL_MAX_DELAY) == HAL_OK &&
        HAL_I2C_Master_Receive(imuHandle, IMU_ADDRESS, (uint8_t*) buffer, bufferLength,
            HAL_MAX_DELAY) == HAL_OK;
    osSemaphoreRelease(accessSemaphore);
    return success;
}

/**
 * Write the data to the IMU and check the result. Retry one time if the write failed.
 *
 * @param imu The IMU to write the data to.
 * @param dataAddress The address in the IMU to write the data to.
 * @param data The data to write to the IMU
 * @return Whether or not the write was successful.
 */
static bool writeIMUAndCheck(IMU imu, uint8_t dataAddress, uint8_t data) {
    uint8_t response = 0;
    for (int i = 0; i < 2; i++) {
        if (writeIMU(imu, dataAddress, data) &&
            readIMU(imu, dataAddress, &response, sizeof(data)) && response == data) {
            return true;
        }
    }
    return false;
}

/**
 * Set the currently configured operation mode of the IMU.
 *
 * @param imu The IMU to set the operation mode.
 * @param operationMode The operation mode.
 * @return Whether or not the the write succeeded.
 */
static bool setOperationMode(IMU imu, IMUOperationMode operationMode) {
    return writeIMUAndCheck(imu, BNO055_OPR_MODE_ADDR, operationMode);
}

bool initIMU(IMU imu, bool enableFusion) {
    PRINT_DEBUG("Initializing IMU %d", imu);
    uint8_t response = 0;
    // Make sure we have the right device
    if (!readIMU(imu, BNO055_CHIP_ID_ADDR, &response, sizeof(response))) {
        PRINT_DEBUG("IMU %d is not available", imu);
        return false;
    }
    if (response != BNO055_ID) {
        printError("IMU %d: Wrong device identifier: %x", imu, response);
        return false;
    }
    PRINT_DEBUG("IMU Device accepted");
    // Select page 0
    if (!writeIMUAndCheck(imu, BNO055_PAGE_ID_ADDR, 0)) {
        printError("IMU %d: Failed to select page 0", imu);
        return false;
    }
    for (uint8_t i = 0;; ++i) {
        if (!readIMU(imu, BNO055_SYS_STAT_ADDR, &response, sizeof(response))) {
            printError("Failed to read the status from IMU %d", imu);
            return false;
        }
        if (response != INITIALIZING_PERIPHERALS && response != SYSTEM_INITIALIZATION &&
            response != EXECUTING_SELF_TEST) {
            break;
        }
        if (i >= MAX_IMU_INIT_WAIT) {
            printError("Failed to wait for IMU %d startup", imu);
            printDebug("IMU %d state is %02X", imu, response);
            return false;
        }
        osDelay(10);
    }
    // Verify system state
    if (response != SYSTEM_IDLE && response != RUNNING_WITH_SENSOR_FUSION &&
        response != RUNNING_WITHOUT_SENSOR_FUSION6) {
        if (response == SYSTEM_ERROR) {
            uint8_t resetFlag = RESET_SYSTEM;
            if (!readIMU(imu, BNO055_SYS_ERR_ADDR, &response, sizeof(response))) {
                printError("Failed to read the system error from IMU %d", imu);
            } else if (response == SELF_TEST_FAILED) {
                if (!readIMU(imu, BNO055_SELF_TEST_RESULT_ADDR, &response, sizeof(response))) {
                    printError("IMU %d self-test failed and failed to read-self test result", imu);
                } else {
                    printError("IMU %d self-test failed: CPU = %s, GYRO = %s, MAG = %s, ACCEL = %s",
                        imu, IS_SET(response, CPU_SELF_TEST_RESULT) ? "OK" : "FAIL",
                        IS_SET(response, GYROSCOPE_SELF_TEST_RESULT) ? "OK" : "FAIL",
                        IS_SET(response, MAGNETOMETER_SELF_TEST_RESULT) ? "OK" : "FAIL",
                        IS_SET(response, ACCELEROMETER_SELF_TEST_RESULT) ? "OK" : "FAIL");
                }
                resetFlag |= TRIGGER_SELF_TEST;
            } else {
                printError("IMU %d initialization failed: System error=%X", imu, response);
            }
            writeIMU(imu, BNO055_SYS_TRIGGER_ADDR, resetFlag);
            return false;
        }
        printError("Unexpected system state %X for IMU %d after init", response, imu);
    }
    // Enter configuration mode of the IMU
    if (setOperationMode(imu, OPERATION_MODE_CONFIG)) {
        PRINT_DEBUG("IMU %d Configuration Mode Entered", imu);
        PRINT_DEBUG("Mode is: %02X", response);
    } else {
        // If failed to enter abort and continue
        printError("Failed to enter IMU %d Configuration mode\nMode is: %02X", imu, response);
        return false;
    }
    osDelay(100);
    if (!writeIMUAndCheck(imu, BNO055_PWR_MODE_ADDR, POWER_MODE_NORMAL)) {
        printError("IMU %d: Failed to set power mode to normal", imu);
        return false;
    }
    uint8_t units = ACCEL_M_PER_S2 | TEMPERATURE_CELSIUS | ANGULAR_RATE_RPS | EULER_ANGLES_RAD |
                    FUSION_OUTPUT_ANDROID;
    if (!writeIMUAndCheck(imu, BNO055_UNIT_SEL_ADDR, units)) {
        printError("IMU %d: Failed to configure units", imu);
        return false;
    }
    if (imu == IMU1) {
        // Remap {X, Y, Z} to {-Y, Z, -X}
        if (!writeIMUAndCheck(imu, BNO055_AXIS_MAP_CONFIG_ADDR,
                IMU_AXIS_CONFIG(Y_IMU_AXIS, Z_IMU_AXIS, X_IMU_AXIS))) {
            printError("IMU %d: Failed to set the axis configuration", imu);
            return false;
        }
        if (!writeIMUAndCheck(imu, BNO055_AXIS_MAP_SIGN_ADDR,
                IMU_AXIS_SIGN_CONFIG(IMU_NEGATIVE_SIGN, IMU_POSITIVE_SIGN, IMU_NEGATIVE_SIGN))) {
            printError("IMU %d: Failed to set the axis sign configuration", imu);
            return false;
        }
    } else if (imu == IMU2) {
        // Remap {X, Y, Z} to {X, Z, -Y}, e.g rotate 90 degrees clockwise.
        if (!writeIMUAndCheck(imu, BNO055_AXIS_MAP_CONFIG_ADDR,
                IMU_AXIS_CONFIG(X_IMU_AXIS, Z_IMU_AXIS, Y_IMU_AXIS))) {
            printError("IMU %d: Failed to set the axis configuration", imu);
            return false;
        }
        if (!writeIMUAndCheck(imu, BNO055_AXIS_MAP_SIGN_ADDR,
                IMU_AXIS_SIGN_CONFIG(IMU_POSITIVE_SIGN, IMU_POSITIVE_SIGN, IMU_NEGATIVE_SIGN))) {
            printError("IMU %d: Failed to set the axis sign configuration", imu);
            return false;
        }
    }
    IMUOperationMode operationMode;
    if (enableFusion) {
        operationMode = OPERATION_MODE_NDOF;
    } else {
        operationMode = OPERATION_MODE_AMG;
        // Select page 1
        if (!writeIMUAndCheck(imu, BNO055_PAGE_ID_ADDR, 1)) {
            printError("IMU %d: Failed to select page 1", imu);
            return false;
        }
        if (!writeIMUAndCheck(imu, GYROSCOPE_CONFIG_1_ADDRESS,
                GYROSCOPE_RANGE_125 | GYROSCOPE_BANDWIDTH_116)) {
            printError("IMU %d: Failed to configure the gyroscope", imu);
            return false;
        }
        if (!writeIMUAndCheck(imu, GYROSCOPE_CONFIG_2_ADDRESS, GYROSCOPE_NORMAL_OPERATION)) {
            printError("IMU %d: Failed to configure gyroscope operation mode", imu);
            return false;
        }
        if (!writeIMUAndCheck(imu, ACCELEROMETER_CONFIG_ADDRESS,
                ACCELEROMETER_RANGE_2 | ACCELEROMETER_NORMAL_OPERATION |
                    ACCELEROMETER_BANDWIDTH_125)) {
            printError("IMU %d: Failed to configure the accelerometer", imu);
            // The accelerometer is not used for controlling, this is non fatal, don't return here.
        }
        if (!writeIMUAndCheck(imu, MAGNETOMETER_CONFIG_ADDRESS,
                MAGNETOMETER_BANDWIDTH_30 | MAGNETOMETER_NORMAL_OPERATION)) {
            printError("IMU %d: Failed to configure the magnetometer", imu);
            // The magnetometer is not used for controlling, this is non fatal, don't return here.
        }

        // Select page 0
        if (!writeIMUAndCheck(imu, BNO055_PAGE_ID_ADDR, 0)) {
            printError("IMU %d: Failed to select page 0", imu);
            return false;
        }
    }

    // Write operation mode to IMU
    if (setOperationMode(imu, operationMode)) {
        PRINT_DEBUG("IMU %d Final Operational Mode Entered", imu);
        PRINT_DEBUG("Mode is: %02X", response);
    } else {
        // If failed to verify again abort and do nothing
        printError("Failed to enter IMU %d Final Operational Mode", imu);
        return false;
    }
    imuFusionConfig[imu] = enableFusion;
    return true;
}

/**
 * Get the currently configured operation mode of the IMU.
 *
 * @param imu The IMU to query the operation mode.
 * @param operationMode The operation mode output.
 * @return Whether or not the read succeeded.
 */
static bool getIMUOperationMode(IMU imu, IMUOperationMode* operationMode) {
    uint8_t mode = 0;
    if (!readIMU(imu, BNO055_OPR_MODE_ADDR, &mode, sizeof(mode))) {
        return false;
    }
    *operationMode = (IMUOperationMode) (mode & BNO055_OPR_MODE_MASK);
    return true;
}

bool checkImuStatus(IMU imu) {
    int8_t data;
    if (!readIMU(imu, BNO055_SYS_STAT_ADDR, &data, sizeof(data))) {
        printError("Failed to read the system status from IMU %d", imu);
        return false;
    }
    if (data != RUNNING_WITH_SENSOR_FUSION && data != RUNNING_WITHOUT_SENSOR_FUSION6) {
        printError("IMU %d is not in running mode, state is %02X", imu, data);
        return false;
    }
    IMUOperationMode operationMode;
    if (!getIMUOperationMode(imu, &operationMode)) {
        printError("Failed to read the operation mode from IMU %d", imu);
        return false;
    }
    IMUOperationMode expectedOperationMode =
        imuFusionConfig[imu] ? OPERATION_MODE_NDOF : OPERATION_MODE_AMG;
    if (operationMode != expectedOperationMode) {
        printError("IMU %d is not running in the expected operation mode %02X, mode is %02X", imu,
            expectedOperationMode, operationMode);
        return false;
    }
    return true;
}

/**
 * Converts a little endian value to a value with native byte ordering.
 *
 * @param value The 16 bit value.
 * @return The value in native byte ordering.
 */
static __inline__ int16_t littleEndianToNative(int16_t value) {
#    if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
    return (int16_t) ((value << 8U) | (value >> 8U));
#    else
    return value;
#    endif
}

bool readRotationSpeed(IMU imu, Vec3D* rotation) {
    ASSERT(rotation != NULL, "NULL pointer passed to %s", __FUNCTION__);
    int16_t dataBuffer[3];
    // Get Gyroscope data at once
    if (!readIMU(imu, BNO055_GYRO_DATA_X_LSB_ADDR, dataBuffer, sizeof(dataBuffer))) {
        return false;
    }
    rotation->pitch = littleEndianToNative(dataBuffer[0]) / GYROSCOPE_UNIT_SCALE;
    rotation->yaw = littleEndianToNative(dataBuffer[1]) / GYROSCOPE_UNIT_SCALE;
    rotation->roll = littleEndianToNative(dataBuffer[2]) / GYROSCOPE_UNIT_SCALE;
    return true;
}

bool readAcceleration(IMU imu, Vec3D* acceleration) {
    ASSERT(acceleration != NULL, "NULL pointer passed to %s", __FUNCTION__);
    int16_t dataBuffer[3];
    // Get Accelerometer data at once
    if (!readIMU(imu, BNO055_ACCEL_DATA_X_LSB_ADDR, dataBuffer, sizeof(dataBuffer))) {
        return false;
    }
    acceleration->x = littleEndianToNative(dataBuffer[0]) / ACCELEROMETER_UNIT_SCALE;
    acceleration->y = littleEndianToNative(dataBuffer[1]) / ACCELEROMETER_UNIT_SCALE;
    acceleration->z = littleEndianToNative(dataBuffer[2]) / ACCELEROMETER_UNIT_SCALE;
    return true;
}

bool readMagneticForce(IMU imu, Vec3D* magneticForce) {
    ASSERT(magneticForce != NULL, "NULL pointer passed to %s", __FUNCTION__);
    int16_t dataBuffer[3];
    // Get Magnetometer data at once
    if (!readIMU(imu, BNO055_MAG_DATA_X_LSB_ADDR, dataBuffer, sizeof(dataBuffer))) {
        return false;
    }
    magneticForce->x = littleEndianToNative(dataBuffer[0]) / MAGNETOMETER_UNIT_SCALE;
    magneticForce->y = littleEndianToNative(dataBuffer[1]) / MAGNETOMETER_UNIT_SCALE;
    magneticForce->z = littleEndianToNative(dataBuffer[2]) / MAGNETOMETER_UNIT_SCALE;
    return true;
}

bool readOrientation(IMU imu, Quaternion* orientation) {
    if (!imuFusionConfig[imu]) {
        return false;
    }
    int16_t dataBuffer[4];
    // Get Quaternion data at once, if the fusion mode is enabled
    if (!readIMU(imu, BNO055_QUATERNION_DATA_W_LSB_ADDR, dataBuffer, sizeof(dataBuffer))) {
        return false;
    }
    orientation->x = ((double) littleEndianToNative(dataBuffer[1])) / ORIENTATION_UNIT_SCALE;
    orientation->y = ((double) littleEndianToNative(dataBuffer[2])) / ORIENTATION_UNIT_SCALE;
    orientation->z = ((double) littleEndianToNative(dataBuffer[3])) / ORIENTATION_UNIT_SCALE;
    orientation->w = ((double) littleEndianToNative(dataBuffer[0])) / ORIENTATION_UNIT_SCALE;
    return true;
}

bool readTemperature(IMU imu, int8_t* temperature) {
    ASSERT(temperature != NULL, "NULL pointer passed to %s", __FUNCTION__);
    return readIMU(imu, BNO055_TEMP_ADDR, temperature, sizeof(int8_t));
}

bool disableImu(IMU imu) {
    return setOperationMode(imu, OPERATION_MODE_CONFIG) &&
           writeIMUAndCheck(imu, BNO055_PWR_MODE_ADDR, POWER_MODE_SUSPEND);
}

bool calibrate(IMU imu) {
    if (imuFusionConfig[imu]) {
        return true; // The fusion mode is calibrating itself.
    }
    printDebug("Calibration: Measuring current gyroscope output for imu %d", imu);
    int64_t gyroOffsetXSum = 0;
    int64_t gyroOffsetYSum = 0;
    int64_t gyroOffsetZSum = 0;
    for (uint8_t i = 0; i < CALIBRATION_MEASUREMENTS; ++i) {
        int16_t dataBuffer[3];
        if (readIMU(imu, BNO055_GYRO_DATA_X_LSB_ADDR, dataBuffer, sizeof(dataBuffer))) {
            gyroOffsetXSum += littleEndianToNative(dataBuffer[0]);
            gyroOffsetYSum += littleEndianToNative(dataBuffer[1]);
            gyroOffsetZSum += littleEndianToNative(dataBuffer[2]);
        } else {
            printDebug("Calibration: Failed to read the gyroscope data from imu %d", imu);
        }
        osDelay(10);
        refreshWatchdogTimer();
    }
    int16_t gyroOffsetX = (int16_t) round((double) gyroOffsetXSum / CALIBRATION_MEASUREMENTS);
    int16_t gyroOffsetY = (int16_t) round((double) gyroOffsetYSum / CALIBRATION_MEASUREMENTS);
    int16_t gyroOffsetZ = (int16_t) round((double) gyroOffsetZSum / CALIBRATION_MEASUREMENTS);
    if (!setOperationMode(imu, OPERATION_MODE_CONFIG)) {
        printError("Calibration: Failed to set imu %d to config mode", imu);
        return false;
    }
    int16_t previousOffsets[3] = {0, 0, 0};
    if (readIMU(imu, GYRO_OFFSET_X_LSB_ADDR, previousOffsets, sizeof(previousOffsets))) {
        gyroOffsetX = (int16_t) (previousOffsets[0] + gyroOffsetX);
        gyroOffsetY = (int16_t) (previousOffsets[1] + gyroOffsetY);
        gyroOffsetZ = (int16_t) (previousOffsets[2] + gyroOffsetZ);
    } else {
        print("Calibration: Failed to retrieve previous gyroscope offsets from imu %d", imu);
    }
    printDebug("Calibration: Setting gyroscope offset for imu %d: {x: %f, y: %f, z: %f}", imu,
        gyroOffsetX / GYROSCOPE_UNIT_SCALE, gyroOffsetY / GYROSCOPE_UNIT_SCALE,
        gyroOffsetZ / GYROSCOPE_UNIT_SCALE);
    uint8_t success = true;
    if (!writeIMUAndCheck(imu, GYRO_OFFSET_X_LSB_ADDR, gyroOffsetX & 0xFF) ||
        !writeIMUAndCheck(imu, GYRO_OFFSET_X_MSB_ADDR, (gyroOffsetX >> 8U) & 0xFF) ||
        !writeIMUAndCheck(imu, GYRO_OFFSET_Y_LSB_ADDR, gyroOffsetY & 0xFF) ||
        !writeIMUAndCheck(imu, GYRO_OFFSET_Y_MSB_ADDR, (gyroOffsetY >> 8U) & 0xFF) ||
        !writeIMUAndCheck(imu, GYRO_OFFSET_Z_LSB_ADDR, gyroOffsetZ & 0xFF) ||
        !writeIMUAndCheck(imu, GYRO_OFFSET_Z_MSB_ADDR, (gyroOffsetZ >> 8U) & 0xFF)) {
        printError("Calibration: Failed to write the gyroscope calibration offset for imu %d", imu);
        success = false;
    }
    if (!setOperationMode(imu, OPERATION_MODE_AMG)) {
        printError("Calibration: Failed to set imu %d back to normal operation mode", imu);
    }
    return success;
}

#endif /* IMU_ENABLED */
