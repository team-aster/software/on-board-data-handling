#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "objects/communication/iridium.h"
#include "objects/ringBuffer.h"
#include "threads/telemetry.h"
#include "data/system.h"
#include "generated/units.h"


/** Whether or not to enable Iridium debugging prints. */
#define DEBUG_IRIDIUM 0

#if DEBUG_IRIDIUM
#    define PRINT_DEBUG_IRIDIUM(...) printDebug(__VA_ARGS__)
#    define PRINT_DEBUG_IRIDIUM_INFO(...) print(__VA_ARGS__)
#    define PRINT_DEBUG_IRIDIUM_ERROR(...) printError(__VA_ARGS__)
#else
#    define PRINT_DEBUG_IRIDIUM(...) ((void) 0)
#    define PRINT_DEBUG_IRIDIUM_INFO(...) ((void) 0)
#    define PRINT_DEBUG_IRIDIUM_ERROR(...) ((void) 0)
#endif /* DEBUG_ADCS */

/** Command to check the connection to the Iridium chip. */
#define COMMAND_CHIP_CONNECTION_CHECK "AT"
/** Command to request the manufacturer name from the Iridium chip. */
#define COMMAND_GET_MANUFACTURER "AT+CGMI"
/** Command to enable or disable the radio activity on the Iridium chip. */
#define COMMAND_SET_RADIO_ACTIVITY "AT*Rn"
/** Command to enable or disable the signal status reporting on the Iridium chip. */
#define COMMAND_SIGNAL_STATUS "AT+CIER"
/** Command to transfer data to the Iridium chip. */
#define COMMAND_SEND_DATA "AT+SBDWB"
/** Command to send the data in the buffer on the Iridium chip through the network. */
#define COMMAND_TRANSMIT_DATA_BUFFER "AT+SBDIX"

/** The maximum amount in time to wait for a write to the Iridium chip to complete. */
#define IRIDIUM_WRITE_TIMEOUT ms_t(50)

/** Message code for Iridium network signal status updates. */
#define SIGNAL_STATUS_UPDATE "+CIEV:"

#if IRIDIUM_ENABLED


/** A direct memory access reader for data from the Iridium chip. */
#    define iridiumReader hdma_uart4_rx
extern DMA_HandleTypeDef iridiumReader;

/**
 * A circular buffer for incoming data from the Iridium chip.
 *
 * @note 128 is the maximum length for a Iridium message including data.
 * We don't really expect to receive anything other than send confirmations from the chip, which
 * is why the buffer is not large. If that changes, the buffer should be at least 256 bytes large.
 */
DEFINE_RING_BUFFER(iridiumReadBuffer, 128);

/**
 * A buffer for data that should be send over the Iridium network.
 * Only one message is buffered, if a new message arrives while the old one
 * is waiting it will get overwritten.
 *
 * @note The Iridium network charges for every 50 byte package, therefore
 * that is the maximum amount of data that is allowed to be sent at once.
 */
static uint8_t iridiumSendBuffer[50];

/** The length of the current message in the send buffer. */
static uint8_t iridiumSendMessageLength = 0;

/** Indicates if we are waiting for a response to a COMMAND_SEND_DATA command. */
static bool iridiumSendResponsePending = false;

/**
 * Write data to the Iridium transmitter.
 *
 * @param data The data to send.
 * @param dataLength The length of the data in bytes.
 * @return Whether the transmission was successful.
 */
static bool writeIridium(const void* data, uint8_t dataLength) {
    return HAL_UART_Transmit(&iridiumUart, (const uint8_t*) data, dataLength, IRIDIUM_WRITE_TIMEOUT.ms) ==
           HAL_OK;
}

/**
 * Write data to the Iridium transmitter.
 *
 * @param data The data to send.
 * @return Whether the write succeeded.
 */
#    define WRITE_IRIDIUM(data) writeIridium(data "\r", strlen(data "\r"))

/**
 * Write a command and a value to the Iridium transmitter
 *
 * @param command The command to send.
 * @param value The value to send as a string.
 * @return Whether the write succeeded.
 */
#    define WRITE_IRIDIUM_VALUE(command, value) WRITE_IRIDIUM(command "=" value)

/**
 * Print a message with contents from the Iridium response buffer.
 *
 * @param message The message to print.
 * @param dataLines The data line regions from the response.
 * @param numDataLines The number of data lines in the response.
 */
static void printIridiumResponse(const char* message, const rbRegionRef_t* dataLines,
    uint8_t numDataLines) {
#    if DEBUG_IRIDIUM
    if (numDataLines == 0) {
        printDebug("%s: <NO DATA>", message);
        return;
    }
    printDebug("%s:", message);
    char buffer[64];
    for (int i = 0; i < numDataLines; i++) {
        if (dataLines[i].length == 0 || dataLines[i].length > ARRAY_SIZE(buffer)) {
            continue;
        }
        rbMemCpy(&dataLines[i], buffer, dataLines[i].length);
        printDebug("Data: '%.*s'", dataLines[i].length, buffer);
    }
#    else /* DEBUG_IRIDIUM */
    (void) message, (void) dataLines, (void) numDataLines;
#    endif /* DEBUG_IRIDIUM */
}

/**
 * Handle a status update from the Iridium chip.
 *
 * @param dataLines The data lines received from the chip.
 * @param numDataLines The number of data lines.
 * @return The number of remaining data lines that were not part of the status update.
 */
static int16_t handleIridiumUpdate(const rbRegionRef_t* dataLines, uint8_t numDataLines) {
    int16_t remainingDataLines = numDataLines;
    if (RING_BUFFER_STR_STARTSWITH(&dataLines[0], SIGNAL_STATUS_UPDATE)) {
        remainingDataLines--;
        IridiumState newState;
        if (RING_BUFFER_STR_EQUAL(&dataLines[0], SIGNAL_STATUS_UPDATE "1,1")) {
            newState = IRIDIUM_CONNECTED;
            PRINT_DEBUG_IRIDIUM_INFO("Iridium connection established");
        } else if (RING_BUFFER_STR_EQUAL(&dataLines[0], SIGNAL_STATUS_UPDATE "1,0")) {
            newState = IRIDIUM_NO_SIGNAL;
            PRINT_DEBUG_IRIDIUM_INFO("Iridium connection lost");
        } else {
            printIridiumResponse("Received unknown Iridium signal status", dataLines, 1);
            return remainingDataLines;
        }
        if (!set_systemIridiumState(&newState)) {
            printError("Failed to update Iridium signal state");
        }
        if (newState == IRIDIUM_CONNECTED) {}
    } else if (iridiumSendResponsePending && numDataLines >= 2 &&
               RING_BUFFER_STR_EQUAL(&dataLines[1], "OK")) {
        remainingDataLines -= 2;
        iridiumSendResponsePending = false;
        if (!RING_BUFFER_STR_EQUAL(&dataLines[0], "0")) {
            printIridiumResponse("Message transmission to Iridium chip failed", dataLines,
                numDataLines);
        } else {
            PRINT_DEBUG_IRIDIUM("Message transmission to Iridium chip succeeded");
            if (!WRITE_IRIDIUM(COMMAND_TRANSMIT_DATA_BUFFER)) {
                printError("Failed to initiate the Iridium transmission");
            }
        }
    } else {
        if (iridiumSendResponsePending && numDataLines == 1 && dataLines[0].length == 0) {
            return --remainingDataLines; // This is most likely the echo of the send message.
        }
        printIridiumResponse("Received unknown Iridium status", dataLines, numDataLines);
        return -1;
    }
    return remainingDataLines;
}

/**
 * Handle a response from the Iridium chip.
 *
 * @param commandStart The start index in the read buffer of the command to which was responded.
 * @param commandLength The length of the command.
 * @param responseStart The start index in the read buffer of the response data.
 * @param responseLength The length of the response data.
 * @return The number of unused data lines that were not part of the command response.
 */
static int16_t handleIridiumResponse(const rbRegionRef_t* command, const rbRegionRef_t* dataLines,
    uint8_t numDataLines) {
    int16_t remainingDataLines = numDataLines;
    if (RING_BUFFER_STR_EQUAL(command, COMMAND_CHIP_CONNECTION_CHECK)) {
        // Initial communication check result
        if (--remainingDataLines < 0 || !RING_BUFFER_STR_EQUAL(&dataLines[0], "OK")) {
            printIridiumResponse("Iridium chip communication check failed", dataLines,
                numDataLines);
            return -1;
        }
        // Ask for manufacturer for identification
        if (!WRITE_IRIDIUM(COMMAND_GET_MANUFACTURER)) {
            printError("Failed to request Iridium chip manufacturer");
        }
    } else if (RING_BUFFER_STR_EQUAL(command, COMMAND_GET_MANUFACTURER)) {
        // Manufacturer check result
        if ((remainingDataLines -= 2) < 0 || !RING_BUFFER_STR_EQUAL(&dataLines[1], "OK")) {
            printIridiumResponse("Iridium chip identification failed: Failed to get manufacturer",
                dataLines, numDataLines);
            return -1;
        }
        if (!RING_BUFFER_STR_EQUAL(&dataLines[0], "Iridium")) {
            printIridiumResponse("Iridium chip identification failed: Unexpected manufacturer",
                dataLines, numDataLines);
            return remainingDataLines;
        }
        IridiumState newState = IRIDIUM_NO_SIGNAL;
        if (!set_systemIridiumState(&newState)) {
            printError("Unable to set the Iridium systems state to available");
        }
        PRINT_DEBUG_IRIDIUM("Iridium chip found");
        if (!WRITE_IRIDIUM_VALUE(COMMAND_SIGNAL_STATUS, "1,0,1")) {
            printError("Failed to enable signal status reporting");
        }
    } else if (RING_BUFFER_STR_EQUAL(command, COMMAND_TRANSMIT_DATA_BUFFER)) {
        if ((remainingDataLines -= 2) < 0 || !RING_BUFFER_STR_EQUAL(&dataLines[1], "OK")) {
            printIridiumResponse("Failed to initiate Iridium data transmission", dataLines,
                numDataLines);
            return -1;
        }
        printIridiumResponse("Transmit buffer response", dataLines, 1);
    } else if (RING_BUFFER_STR_STARTSWITH(command, COMMAND_SIGNAL_STATUS)) {
        if (--remainingDataLines < 0 || !RING_BUFFER_STR_EQUAL(&dataLines[0], "OK")) {
            printIridiumResponse(
                "Failed to set up signal availability reporting on the Iridium chip", dataLines,
                numDataLines);
            return -1;
        }
    } else if (RING_BUFFER_STR_STARTSWITH(command, COMMAND_SEND_DATA)) {
        if (--remainingDataLines < 0 || !RING_BUFFER_STR_EQUAL(&dataLines[0], "READY")) {
            printIridiumResponse("Failed to send a message via the Iridium chip", dataLines,
                numDataLines);
            return -1;
        }
        if (iridiumSendMessageLength == 0) {
            printError("Ready to transmit Iridium message, but got no message");
            return remainingDataLines;
        }
        PRINT_DEBUG_IRIDIUM("Transferring Iridium message...");
        if (!writeIridium(iridiumSendBuffer, iridiumSendMessageLength)) {
            printError("Failed to transmit Iridium message to the Iridium chip");
            return remainingDataLines;
        }
        uint16_t checksum = 0;
        for (size_t i = 0; i < iridiumSendMessageLength; i++) {
            checksum += iridiumSendBuffer[i];
        }
        iridiumSendMessageLength = 0;
        iridiumSendResponsePending = true;
        uint8_t checksumBuffer[2] = {
            (uint8_t) ((checksum >> 8U) & 0xFFU),
            (uint8_t) (checksum & 0xFFU),
        };
        if (!writeIridium(&checksumBuffer, ARRAY_SIZE(checksumBuffer))) {
            printError("Failed to send message checksum the Iridium chip");
            return remainingDataLines;
        }
    } else {
        printIridiumResponse("Received response for unknown Iridium command", command, 1);
        printIridiumResponse("Data", dataLines, numDataLines);
        return -1;
    }
    return remainingDataLines;
}

bool startIridiumInitialization(void) {
    IridiumState newState = IRIDIUM_UNAVAILABLE;
    if (!set_systemIridiumState(&newState)) {
        printError("Unable to set the Iridium systems state to unavailable");
    }
    if (iridiumUart.Instance == NULL) {
        return false;
    }
    return WRITE_IRIDIUM(COMMAND_CHIP_CONNECTION_CHECK);
}

bool enableIridiumCommunication(bool enabled) {
    IridiumState state = IRIDIUM_UNAVAILABLE;
    if (iridiumUart.Instance != NULL && get_systemIridiumState(&state) &&
        state != IRIDIUM_UNAVAILABLE) {
        if (!(enabled ? WRITE_IRIDIUM_VALUE(COMMAND_SET_RADIO_ACTIVITY, "1") :
                        WRITE_IRIDIUM_VALUE(COMMAND_SET_RADIO_ACTIVITY, "0"))) {
            return false;
        }
        state = IRIDIUM_NO_SIGNAL;
        if (!set_systemIridiumState(&state)) {
            printError("Unable to set the Iridium systems state to no signal");
        }
    }
    return true;
}

bool initIridiumUart(void) {
    iridiumReadBuffer.readIndex = 0;
    MX_UART4_Init();
    if (getLastSystemError() == UART4_INIT_SYSTEM_ERROR) {
        iridiumUart.Instance = NULL;
        printCriticalError("Unable to initialize UART4");
        return false;
    }
    return HAL_UARTEx_ReceiveToIdle_DMA(&iridiumUart, iridiumReadBuffer.memory,
               iridiumReadBuffer.size) == HAL_OK;
}

bool sendIridiumMessage(const void* message, size_t messageSize) {
    if (iridiumUart.Instance == NULL) {
        return false;
    }
    if (messageSize > ARRAY_SIZE(iridiumSendBuffer)) {
        PRINT_DEBUG_IRIDIUM_ERROR("Iridium message to send is too large");
        return false;
    }
    memcpy(iridiumSendBuffer, message, messageSize);
    iridiumSendMessageLength = messageSize;
    char command[16];
    ssize_t length = snprintf(command, ARRAY_SIZE(command), COMMAND_SEND_DATA "=%u\r",
        (unsigned int) messageSize);
    if (length < 0) {
        PRINT_DEBUG_IRIDIUM_ERROR("Failed to send Iridium message: Failed to format command");
        return false;
    }
    return writeIridium(command, length);
}

void onIridiumInterrupt(void) {
    uint8_t writePosition = updateWriteIndexFromDmaReader(&iridiumReadBuffer, &iridiumReader);
    uint8_t readPosition = iridiumReadBuffer.readIndex;
    rbRegionRef_t dataLines[16];
    rbGetRegion(&iridiumReadBuffer, &dataLines[0], readPosition, 0);
    uint8_t numDataLines = 0;
    bool haveCommand = false;
    PRINT_DEBUG_IRIDIUM("Iridium data received: w = %d, r = %d", (int) writePosition,
        (int) readPosition);
    while (writePosition != readPosition) {
        dataLines[numDataLines].length++;
        if (!haveCommand && dataLines[numDataLines].length >= 3 &&
            iridiumReadBuffer.memory[readPosition] == '\n' &&
            iridiumReadBuffer
                    .memory[RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, -1)] ==
                '\r' &&
            iridiumReadBuffer
                    .memory[RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, -2)] ==
                '\r') {
            dataLines[numDataLines].length -= 3;
            if (numDataLines != 0) {
                int16_t unhandledDataLines = numDataLines;
                do {
                    unhandledDataLines = handleIridiumUpdate(
                        &dataLines[numDataLines - unhandledDataLines], unhandledDataLines);
                } while (unhandledDataLines > 0);
                memcpy(&dataLines[0], &dataLines[numDataLines], sizeof(dataLines[0]));
            }
            numDataLines = 1;
            haveCommand = true;
            rbGetRegion(&iridiumReadBuffer, &dataLines[numDataLines],
                RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 1), 0);
        } else if (dataLines[numDataLines].length >= 2 &&
                   iridiumReadBuffer.memory[readPosition] == '\n' &&
                   iridiumReadBuffer.memory[RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer,
                       readPosition, -1)] == '\r') {
            dataLines[numDataLines].length -= 2;
            numDataLines++;
            if (RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 1) != writePosition &&
                RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 2) != writePosition &&
                iridiumReadBuffer
                        .memory[RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 1)] ==
                    '\r' &&
                iridiumReadBuffer
                        .memory[RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 2)] ==
                    '\n') {
                readPosition = RELATIVE_RING_BUFFER_INDEX(&iridiumReadBuffer, readPosition, 3);
                if (numDataLines >= ARRAY_SIZE(dataLines)) {
                    PRINT_DEBUG_IRIDIUM_ERROR(
                        "Warning: Iridium communication contains too many data lines");
                    numDataLines--;
                }
                rbGetRegion(&iridiumReadBuffer, &dataLines[numDataLines], readPosition, 0);
                continue;
            }
            int16_t unhandledDataLines;
            if (haveCommand) {
                unhandledDataLines =
                    handleIridiumResponse(&dataLines[0], &dataLines[1], numDataLines - 1);
            } else {
                unhandledDataLines = handleIridiumUpdate(dataLines, numDataLines);
            }
            while (unhandledDataLines > 0) {
                unhandledDataLines = handleIridiumUpdate(
                    &dataLines[numDataLines - unhandledDataLines], unhandledDataLines);
            }
            haveCommand = false;
            numDataLines = 0;
            dataLines[numDataLines].length = 0;
            iridiumReadBuffer.readIndex = (readPosition + 1) % iridiumReadBuffer.size;
            dataLines[numDataLines].start = iridiumReadBuffer.readIndex;
        }
        readPosition = (readPosition + 1) % iridiumReadBuffer.size;
    }
}

void onIridiumError(void) {
    // Suppress the error message if the RockBlock is not connected
    if (iridiumUart.ErrorCode != HAL_UART_ERROR_FE || iridiumReadBuffer.readIndex != 0) {
        printError("Iridium communication error: %lu", (unsigned long) iridiumUart.ErrorCode);
    }
    // Restart the DMA reception
    HAL_UART_DMAStop(&iridiumUart);
    HAL_UART_Receive_DMA(&iridiumUart, iridiumReadBuffer.memory, iridiumReadBuffer.size);
    // Discard all received buffered data
    updateWriteIndexFromDmaReader(&iridiumReadBuffer, &iridiumReader);
}

#endif /* IRIDIUM_ENABLED */
