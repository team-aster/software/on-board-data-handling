/**
 * This object contains the data read from the sensors excluding ADCS data.
 */

#include "data/sensor.h"
#include "globals.h"


/**
 * A quaternion with all components set to zero.
 */
#define QUATERNION_ZERO {0, 0, 0, 0}


osSemaphoreId_t sensorSemaphore;
THREAD_SAFE_VAR(celsius_t, processorTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, batteryTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, pduPcbTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, motorControllerTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, motorBracketTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, retentionWallTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsius_t, recoveryWallTemperature, celsius_t(0), sensorSemaphore)

THREAD_SAFE_VAR(bar_t, pressure, bar_t(0), sensorSemaphore)

THREAD_SAFE_VAR(volt_t, batteryLevel, volt_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsiusInt8_t, primaryImuTemperature, celsiusInt8_t(0), sensorSemaphore)

THREAD_SAFE_VAR(celsiusInt8_t, secondaryImuTemperature, celsiusInt8_t(0), sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, primaryImuAccelerometer, VEC3D_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, primaryImuMagnetometer, VEC3D_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, secondaryImuAccelerometer, VEC3D_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, secondaryImuMagnetometer, VEC3D_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, secondaryImuRotation, VEC3D_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Quaternion, imu2Quaternions, QUATERNION_ZERO, sensorSemaphore)

THREAD_SAFE_VAR(Vec3D, motorCurrent, VEC3D_ZERO, sensorSemaphore)


bool initSensorData(void) {
    sensorSemaphore = osSemaphoreNew(1U, 1U, NULL);
    return sensorSemaphore != NULL;
}
