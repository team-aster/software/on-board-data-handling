/**
 * This object contains the data relevant to system states and system sensors.
 */
#include <memory.h>
#include "generated/sharedTypes.h"
#include "data/system.h"


/**
 * The number of telecommands that will be buffered.
 */
#define TELECOMMAND_BUFFER_SIZE 10

/**
 * A queue which holds the telecommands send from ground.
 */
static osMessageQueueId_t telecommandQueue = NULL;

static osSemaphoreId_t systemSemaphore = NULL;
THREAD_SAFE_VAR(uint8_t, system_ejected, 0, systemSemaphore)

THREAD_SAFE_VAR(uint8_t, system_LO_state, 0, systemSemaphore)

THREAD_SAFE_VAR(uint8_t, system_SOE_state, 0, systemSemaphore)

THREAD_SAFE_VAR(uint8_t, system_SODS_state, 0, systemSemaphore)

THREAD_SAFE_VAR(uint8_t, system_parachute_state, 0, systemSemaphore)

THREAD_SAFE_VAR(uint8_t, system_recovery_state, 0, systemSemaphore)

THREAD_SAFE_VAR(IridiumState, systemIridiumState, IRIDIUM_UNAVAILABLE, systemSemaphore)


bool initSystemData(void) {
    if ((telecommandQueue = osMessageQueueNew(TELECOMMAND_BUFFER_SIZE, sizeof(TelecommandData),
             NULL)) == NULL) {
        return false;
    }
    systemSemaphore = osSemaphoreNew(1U, 1U, NULL);
    return systemSemaphore != NULL;
}

bool getNextTelecommand(TelecommandData* telecommand) {
    return osMessageQueueGet(telecommandQueue, telecommand, NULL, 10U) == osOK;
}

bool enqueueTelecommand(const TelecommandData* telecommand) {
    return osMessageQueuePut(telecommandQueue, telecommand, 0U, 0U) == osOK;
}
