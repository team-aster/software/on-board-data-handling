#include <string.h>
#include <stm32f7xx_hal.h>
#include "data/flash.h"
#include "objects/system.h"
#include "objects/crc.h"
#include "threads/watchdog.h"
#include "globals.h"


/** The flash sector that is used to store the persistent data. */
#define PERSISTENT_FLASH_SECTOR FLASH_SECTOR_1

/**
 * Force the variable or member to be aligned to the specified alignment.
 *
 * @param alignment: The desired alignment.
 */
#define ALIGN(alignment) __attribute__((aligned(alignment)))

#if FLASH_ENABLED == 1
/** Mark the variable to be stored in the persistent flash sector. */
#    define PERSISTENT const __attribute__((section(".persistent_data")))
#else
#    define PERSISTENT
#endif /* FLASH_ENABLED */

/** A constant that indicates that the persistent data was initialized. */
#define PERSISTENT_DATA_IS_INITIALIZED 0x42U

/**
 * The layout of the data stored in persistent flash memory.
 */
typedef struct {
    struct {
        uint8_t initialized; /** Whether or not the persistent memory was initialized. */
        uint16_t checksum; /** The checksum of the stored data (excluding the header). */
    } header; /** Some meta information about the flash data. */
    RestartReason restartReason; /** The reason for the last restart. */
    bool flightMode; /** Whether or not flight mode is enabled. */
    SystemState systemState; /** The current state of the system state machine. */
    /** Whether or not the parachute deployment has been initiated. */
    bool parachuteDeploymentWasStarted;
    bool parachuteIsDeployed; /** Whether or not the parachute has been deployed. */
    bool haveLanded; /** Whether or not the system has landed on the ground after deployment. */
    Configuration config; /** The configuration of the system. */
} PersistentMemory;

/**
 * Update the values in the persistent flash memory.
 *
 * @param newValues A pointer to the data to write to the persistent memory.
 */
static void writePersistentValues(const PersistentMemory* newValues);

/**
 * Calculate the checksum of the persistent memory.
 *
 * @param memory The persistent memory.
 * @return The checksum.
 */
static uint16_t calculateMemoryChecksum(const PersistentMemory* memory);

/**
 * The setter to update one value from the persistent memory.
 *
 * @param name: The name of the persistent variable to update.
 * @param value: The new value of the persistent variable.
 */
#define FLASH_SETTER(name, value)                                                               \
    do {                                                                                        \
        if (memcmp((const void*) &persistentDataLiveCopy.name, value, sizeof(*(value))) != 0) { \
            PersistentMemory ALIGN(sizeof(uint32_t)) newValues;                                 \
            memcpy(&newValues, (const void*) &persistentDataLiveCopy, sizeof(newValues));       \
            newValues.name = *(value);                                                          \
            newValues.header.checksum = calculateMemoryChecksum(&newValues);                    \
            writePersistentValues(&newValues);                                                  \
        }                                                                                       \
    } while (0)

/**
 * Declare a new thread safe variable that is stored in the persistent flash memory.
 * The variable need be defined in the PersistentMemory struct.
 * This will generate a thread safe setter and getters for accessing the variable.
 *
 * @param type: The type of the thread safe variable.
 * @param name: The name of the thread safe variable.
 * @param semaphore: The semaphore that is used to make access to the variable thread safe.
 */
#define THREAD_SAFE_FLASH_VAR(type, name, semaphore)                        \
    THREAD_SAFE_VAR_IMP(type, name, semaphore, persistentDataLiveCopy.name, \
        FLASH_SETTER(name, value))

/**
 * The actual memory in the persistent flash.
 *
 * The data is placed in the ".persistent_data" section which is defined in the
 * linker script (STM32F732VETX_FLASH.ld) and spans the last section
 * (section 7 which starts at address 0x08060000) of the flash memory.
 */
static PERSISTENT __IO PersistentMemory persistentData;

/** A copy of the persistent data which is kept in RAM for faster access. */
static volatile PersistentMemory persistentDataLiveCopy;

/**
 * A copy of the initial persistent data which is used during batch access.
 * If the initialized field is set to PERSISTENT_DATA_IS_INITIALIZED,
 * a batch operation is in progress.
 */
static PersistentMemory batchAccessInitialValues = {
    .header = {.checksum = 0, .initialized = 0},
};

/**
 * Whether or not the flash that was loaded on startup was corrupted (invalid checksum).
 */
static bool haveCorruptedFlash = false;

// Stored variables
static osSemaphoreId_t flashSemaphore;
THREAD_SAFE_FLASH_VAR(bool, flightMode, flashSemaphore)
THREAD_SAFE_FLASH_VAR(SystemState, systemState, flashSemaphore)
THREAD_SAFE_FLASH_VAR(bool, parachuteDeploymentWasStarted, flashSemaphore)
THREAD_SAFE_FLASH_VAR(bool, parachuteIsDeployed, flashSemaphore)
THREAD_SAFE_FLASH_VAR(bool, haveLanded, flashSemaphore)


#if FLASH_ENABLED == 1
/**
 * Erase the sector of the Flash memory that contains the persistent data.
 *
 * @return Whether or not the erase operation succeeded.
 */
static bool erasePersistentFlash() {
    static FLASH_EraseInitTypeDef eraseOptions = {
        .TypeErase = FLASH_TYPEERASE_SECTORS,
        .VoltageRange = FLASH_VOLTAGE_RANGE_3,
        .Sector = PERSISTENT_FLASH_SECTOR,
        .NbSectors = 1,
    };
    uint32_t faultySector = 0;
    // Erasing the flash causes' software execution to halt and wait for the erase operation
    // to finish. Avoid triggering the hardware watchdog by updating it directly before erasing.
    uint32_t previousInterruptLevel;
    bool inInterrupt = isInInterrupt();
    if (inInterrupt) {
        previousInterruptLevel = taskENTER_CRITICAL_FROM_ISR();
    } else {
        taskENTER_CRITICAL();
    }
    updateHardwareWatchdog();
    HAL_StatusTypeDef eraseResult = HAL_FLASHEx_Erase(&eraseOptions, &faultySector);
    if (inInterrupt) {
        taskEXIT_CRITICAL_FROM_ISR(previousInterruptLevel);
    } else {
        taskEXIT_CRITICAL();
    }
    if (eraseResult != HAL_OK) {
        printCriticalError("Erasing the persistent flash failed for sector %lx: %lu", faultySector,
            HAL_FLASH_GetError());
        return false;
    }
    return true;
}
#endif /* FLASH_ENABLED */

static uint16_t calculateMemoryChecksum(const PersistentMemory* memory) {
    // The header and restart reason are not included in the checksum calculation.
    size_t offset = sizeof(memory->header) + sizeof(memory->restartReason);
    return calculateChecksum(((uint8_t*) memory) + offset, sizeof(PersistentMemory) - offset);
}

/**
 * Check whether the the given pointer is word aligned.
 * @param pointer The pointer to check.
 * @return Whether or not the pointer is word aligned.
 */
static inline uint8_t isWordAligned(const __IO void* pointer) {
    return (uintptr_t) pointer % sizeof(uint32_t) == 0;
}

/**
 * Write the new values to the persistent flash without
 * checking if there is a batch mode currently ongoing.
 *
 * @param newValues The new values to write to the flash.
 * @param oldValues The old values to diff against.
 */
static void writePersistentValuesDirect(const PersistentMemory* newValues,
    const PersistentMemory* oldValues) {
    ASSERT(isWordAligned(newValues), "The new values pointer must be double word aligned");
#if FLASH_ENABLED == 0
    memcpy((void*) &persistentDataLiveCopy, newValues, sizeof(persistentDataLiveCopy));
#else
    bool needsCacheUpdate = oldValues == &persistentDataLiveCopy;
    bool needsErase = false; // Only a change from a 0 into a 1 requires an erase operation
    for (size_t i = 0; i < sizeof(PersistentMemory); i++) {
        uint8_t newByte = ((uint8_t*) newValues)[i];
        if (newByte & (((uint8_t*) oldValues)[i] ^ newByte)) {
            needsErase = true;
            break;
        }
    }
    if (needsCacheUpdate) {
        memcpy((void*) &persistentDataLiveCopy, newValues, sizeof(persistentDataLiveCopy));
    }

    if (HAL_FLASH_Unlock() != HAL_OK) {
        printCriticalError("Failed to save new flash: %lu", HAL_FLASH_GetError());
        return;
    }
    if (needsErase && !erasePersistentFlash()) {
        HAL_FLASH_Lock();
        return;
    }

    size_t offset = 0;
    uint32_t flashAddress = (uint32_t) &persistentData;
    // Don't use double words, it doesn't work. Start with word.
    while (sizeof(persistentData) - offset >= sizeof(uint32_t)) {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,
                (uint32_t) (((uint8_t*) flashAddress) + offset),
                *((uint32_t*) (((uint8_t*) newValues) + offset))) != HAL_OK) {
            printCriticalError("Error while writing flash to %p: %lu",
                (((uint8_t*) flashAddress) + offset), HAL_FLASH_GetError());
            break; // Do not return without locking the flash again.
        }
        offset += sizeof(uint32_t);
    }
    while (sizeof(persistentData) - offset >= sizeof(uint16_t)) {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,
                (uint32_t) (((uint8_t*) flashAddress) + offset),
                *((uint16_t*) (((uint8_t*) newValues) + offset))) != HAL_OK) {
            printCriticalError("Error while writing flash to %p: %lu",
                (((uint8_t*) flashAddress) + offset), HAL_FLASH_GetError());
            break; // Do not return without locking the flash again.
        }
        offset += sizeof(uint16_t);
    }
    while (sizeof(persistentData) > offset) {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
                (uint32_t) (((uint8_t*) flashAddress) + offset),
                *(((uint8_t*) newValues) + offset)) != HAL_OK) {
            printCriticalError("Error while writing flash to %p: %lu",
                (((uint8_t*) flashAddress) + offset), HAL_FLASH_GetError());
            break; // Do not return without locking the flash again.
        }
        offset += sizeof(uint8_t);
    }
    HAL_FLASH_Lock();
#endif /* FLASH_ENABLED */
}

static void writePersistentValues(const PersistentMemory* newValues) {
    if (batchAccessInitialValues.header.initialized == PERSISTENT_DATA_IS_INITIALIZED) {
        memcpy((void*) &persistentDataLiveCopy, newValues, sizeof(persistentDataLiveCopy));
        // In batch operation;
        return;
    }
    writePersistentValuesDirect(newValues, (const PersistentMemory*) &persistentDataLiveCopy);
}

bool beginFlashBatchAccess(void) {
    if (osSemaphoreAcquire(flashSemaphore, 0U) != osOK &&
        osSemaphoreAcquire(flashSemaphore, 10U) != osOK) {
        return false;
    }
    ASSERT(batchAccessInitialValues.header.initialized != PERSISTENT_DATA_IS_INITIALIZED,
        "Called beginFlashBatchAccess without endFlashBatchAccess");
    memcpy(&batchAccessInitialValues, (const void*) &persistentDataLiveCopy,
        sizeof(PersistentMemory));
    return true;
}

void endFlashBatchAccess(void) {
    ASSERT(batchAccessInitialValues.header.initialized == PERSISTENT_DATA_IS_INITIALIZED,
        "Called endFlashBatchAccess without beginFlashBatchAccess");
    bool haveChange = memcmp(&batchAccessInitialValues, (const void*) &persistentDataLiveCopy,
                          sizeof(PersistentMemory)) != 0;
    if (haveChange) {
        writePersistentValuesDirect((const PersistentMemory*) &persistentDataLiveCopy,
            &batchAccessInitialValues);
    }
    batchAccessInitialValues.header.initialized = 0;
    osSemaphoreRelease(flashSemaphore);
}

void resetRestartReason(void) {
    RestartReason value = POWER_RESTART;
    FLASH_SETTER(restartReason, &value);
}

void setRestartReason(RestartReason reason) {
#if FLASH_ENABLED == 0
    (void) reason;
#else
    // The system is in an undefined state, we can't reset the flash, or we might
    // hang forever (even the hardware watchdog didn't restart during testing).
    // Therefore, we need the restart reason to be all 1, so we can set the correct value now.
    // Unfortunately we can't assert that here, because we are unable to print.
    static_assert(__builtin_ctz(sizeof(RestartReason)) == FLASH_TYPEPROGRAM_BYTE ||
                      __builtin_ctz(sizeof(RestartReason)) == FLASH_TYPEPROGRAM_HALFWORD ||
                      __builtin_ctz(sizeof(RestartReason)) == FLASH_TYPEPROGRAM_WORD,
        "The size of RestartReason must match one of the programming types for flashing");

    if (HAL_FLASH_Unlock() == HAL_OK) {
        uint32_t programmingType = __builtin_ctz(sizeof(RestartReason));
        HAL_FLASH_Program(programmingType, (uint32_t) (&persistentData.restartReason), reason);
        HAL_FLASH_Lock();
    }
#endif /* FLASH_ENABLED */
}

RestartReason getRestartReason(void) {
    return persistentDataLiveCopy.restartReason;
}

static bool verifyFlashData(void) {
    return persistentDataLiveCopy.header.checksum ==
           calculateMemoryChecksum((const PersistentMemory*) &persistentDataLiveCopy);
}

bool initDataFlash(void) {
    ASSERT(isWordAligned(&persistentData), "persistentData must be double aligned");
    haveCorruptedFlash = false;
    if ((flashSemaphore = osSemaphoreNew(1U, 1U, NULL)) == NULL) {
        return false;
    }
    memcpy((void*) &persistentDataLiveCopy, (const void*) &persistentData, sizeof(persistentData));
    if (persistentDataLiveCopy.header.initialized == PERSISTENT_DATA_IS_INITIALIZED) {
        if (verifyFlashData()) {
            return true;
        }
        printCriticalError("The checksum of the persistent flash data was invalid");
        haveCorruptedFlash = true;
    }
    PersistentMemory ALIGN(sizeof(uint32_t)) initialValues = {
        .header.initialized = PERSISTENT_DATA_IS_INITIALIZED,
        .restartReason = POWER_RESTART,
        .flightMode = false,
        .systemState = STATE_IDLE,
        .parachuteDeploymentWasStarted = false,
        .parachuteIsDeployed = false,
        .haveLanded = false,
        .config = DEFAULT_CONFIGURATION,
    };
    initialValues.header.checksum = calculateMemoryChecksum(&initialValues);
    writePersistentValues(&initialValues);
    return true;
}

bool isFlashCorrupted(void) {
    return haveCorruptedFlash;
}

const Configuration* getConfig(void) {
    return (const Configuration*) &persistentDataLiveCopy.config;
}

Configuration* beginConfigUpdate(void) {
    if (!beginFlashBatchAccess()) {
        return NULL;
    }
    return (Configuration*) &persistentDataLiveCopy.config;
}

void commitConfigUpdate(void) {
    persistentDataLiveCopy.header.checksum = calculateMemoryChecksum(
        (const PersistentMemory*) &persistentDataLiveCopy);
    endFlashBatchAccess();
}
