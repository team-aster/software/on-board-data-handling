/**
 * This object contains the data read from the GPS module.
 */

#include "data/gps.h"
#include "globals.h"


osSemaphoreId_t gpsSemaphore;

THREAD_SAFE_VAR(deg_t, gpsLatitude, deg_t(0), gpsSemaphore)

THREAD_SAFE_VAR(deg_t, gpsLongitude, deg_t(0), gpsSemaphore)

THREAD_SAFE_VAR(meter_t, gpsAltitude, meter_t(0), gpsSemaphore)

THREAD_SAFE_VAR(ms_t, gpsTime, ms_t(0), gpsSemaphore)

bool initGpsData(void) {
    gpsSemaphore = osSemaphoreNew(1U, 1U, NULL);
    return gpsSemaphore != NULL;
}
