/**
 * This object contains the data relevant to ADCS.
 */

#include "data/adcs.h"


/** A semaphore for accessing threads safe variables that are used in the ADCS. */
osSemaphoreId_t adcsSemaphore;

// Stored variables
THREAD_SAFE_VAR(Vec3Uint32, motorEnabled, VEC3D_ZERO, adcsSemaphore)

DEFINE_TYPED_RING_BUFFER(Vec3Int32WithTime, demandedMotorSpeeds, 8)

DEFINE_TYPED_RING_BUFFER(Vec3FWithTime, motorSpeeds, 8)

DEFINE_TYPED_RING_BUFFER(Vec3FWithTime, rotationMeasurements, 8)


bool initAdcsData(void) {
    adcsSemaphore = osSemaphoreNew(1U, 1U, NULL);
    return adcsSemaphore != NULL;
}
