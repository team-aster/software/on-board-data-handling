/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "threads/watchdog.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
   UNUSED(pcTaskName);

   // Can't call print functions here, as the system state (especially semaphores)
   // are most likely corrupted by the stack overflow at this point.
   RestartReason restartReason = STACK_OVERFLOW_RESTART;
   if (xTask == (TaskHandle_t) defaultTaskHandle) {
       restartReason = IDLE_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) controlTaskHandle) {
       restartReason = CONTROL_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) recoveryTaskHandle) {
       restartReason = RECOVERY_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) adcsTaskHandle) {
       restartReason = ADCS_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) sensorsTaskHandle) {
       restartReason = SENSORS_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) telecommandTaskHandle) {
       restartReason = TELECOMMAND_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) telemetryTaskHandle) {
       restartReason = TELEMETRY_THREAD_STACK_OVERFLOW_RESTART;
   } else if (xTask == (TaskHandle_t) watchdogTaskHandle) {
       restartReason = WATCHDOG_THREAD_STACK_OVERFLOW_RESTART;
   }
#define ACKNOWLEDGE_RESET 1
   softwareReset(restartReason);
#undef ACKNOWLEDGE_RESET
}
/* USER CODE END 4 */

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

