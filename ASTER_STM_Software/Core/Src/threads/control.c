/**
 * The purpose of CONTROL object is to provide a thread for the state machine and for the
 * execution of telecommands. CONTROL object can control all other objects.
 */

#include <stdio.h>
#include "threads/threads.h"
#include "threads/adcs.h"
#include "threads/control.h"
#include "threads/recovery.h"
#include "threads/sensors.h"
#include "threads/telemetry.h"
#include "threads/watchdog.h"

#include "globals.h"
#include "generated/telemetry.h"

#include "data/sensor.h"

#include "objects/communication.h"
#include "objects/system.h"
#include "objects/communication/payload.h"
#include "objects/gpio.h"
#include "objects/motors.h"
#include "objects/adc.h"
#include "objects/crc.h"

/** A flag indicating that the IMU calibration was already commanded. */
#define STATE_IMU_CALIBRATION_DONE 0b00000001U

/** The current state of the system state machine. */
static SystemState currentState = STATE_IDLE;

/** The state requested by other components of the system */
static volatile SystemState requestedState = STATE_IDLE;

/** The tick when the system state was last changed. */
static ms_t lastStateChangeTick = ms_t(0);

/** The next tick when the GoPro signal should be turned low again. */
static ms_t nextGoProLowTick = ms_t(UINT32_MAX);

/** The current state of the pre launch sequence. */
static uint8_t preLaunchState = 0;


/**
 * Enable or disable the battery.
 *
 * @param enabled Whether or not to enable the battery.
 */
static void setBatteryEnabled(bool enabled) {
    if (enabled) {
        setPin(BATTERY_RESET_PIN, LOW);
        setPin(BATTERY_ENABLE_PIN, HIGH);
    } else {
        setPin(BATTERY_ENABLE_PIN, LOW);
        setPin(BATTERY_RESET_PIN, HIGH);
    }
}

/**
 * React to the reason why the device last restarted.
 */
static void handleRestartReason(void) {
    RestartReason restartReason;
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET) {
        restartReason = HARDWARE_WATCHDOG_TRIGGERED_RESTART;
        __HAL_RCC_CLEAR_RESET_FLAGS(); // Clear reset flags
    } else {
        restartReason = getRestartReason();
        if (restartReason == POWER_RESTART) {
            printDebug("Normal restart");
            return;
        }
    }
    resetRestartReason();
    const char* reason;
    switch (restartReason) {
    case HARDWARE_WATCHDOG_TRIGGERED_RESTART:
        reason = "the hardware watchdog was triggered";
        break;
    case CONTROL_THREAD_FREEZE_RESTART:
        reason = "the control thread froze";
        break;
    case RECOVERY_THREAD_FREEZE_RESTART:
        reason = "the recovery thread froze";
        break;
    case ADCS_THREAD_FREEZE_RESTART:
        reason = "the ADCS thread froze";
        break;
    case SENSORS_THREAD_FREEZE_RESTART:
        reason = "the sensors thread froze";
        break;
    case TELECOMMAND_THREAD_FREEZE_RESTART:
        reason = "the telecommand thread froze";
        break;
    case TELEMETRY_THREAD_FREEZE_RESTART:
        reason = "the telemetry thread froze";
        break;
    case INTERRUPT_STALL_RESTART:
        reason = "interrupts took too long to complete";
        break;
    case HARD_FAULT_RESTART:
        reason = "there was a hard fault";
        break;
    case MEMORY_FAULT_RESTART:
        reason = "there was a memory fault";
        break;
    case BUS_FAULT_RESTART:
        reason = "there was a bus fault";
        break;
    case USAGE_FAULT_RESTART:
        reason = "there was a usage fault";
        break;
    case STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in an unknown thread";
        break;
    case IDLE_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the idle thread";
        break;
    case CONTROL_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the control thread";
        break;
    case RECOVERY_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the recovery thread";
        break;
    case ADCS_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the ADCS thread";
        break;
    case SENSORS_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the sensors thread";
        break;
    case TELECOMMAND_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the telecommand thread";
        break;
    case TELEMETRY_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the telemetry thread";
        break;
    case WATCHDOG_THREAD_STACK_OVERFLOW_RESTART:
        reason = "there was a stack overflow in the watchdog thread";
        break;
    case SCHEDULER_EXIT_RESTART:
        reason = "the system scheduler exited unexpectedly";
        break;
    default:
        reason = "of an unknown reason";
    }
    printError("The system was unexpectedly restarted because %s", reason);
}

/**
 * Change the state of the system state machine.
 *
 * @param state The new state.
 */
static void changeState(SystemState state) {
    if (state != currentState) {
        lastStateChangeTick = getTimeSinceBoot();
    }
    bool batteryEnabled = true;
    uint8_t telemetryEnableState = RXSM_TELEMETRY
#if SD_CARDS_ENABLED
                                   | SD_CARD_TELEMETRY
#endif /* SD_CARDS_ENABLED */
                                   | PAYLOAD_TELEMETRY;
    switch (state) {
    case STATE_IDLE:
        print("Changing state to IDLE");
        requestAdcsMode(MODE_OFF);
        break;
    case STATE_PRE_LAUNCH:
        print("Changing state to PRE_LAUNCH");
        preLaunchState = 0;
        requestAdcsMode(MODE_OFF);
        break;
    case STATE_LAUNCH:
        print("Changing state to LAUNCH");
        requestAdcsMode(MODE_OFF);
        break;
    case STATE_EXPERIMENT:
        print("Changing state to EXPERIMENT");
        if (getPin(TESTING_JUMPER_PIN) != HIGH) { // Don't disable RXSM telemetry when testing
            telemetryEnableState &= ~RXSM_TELEMETRY;
        }
        requestAdcsMode(MODE_SPIN_UP); // This should already be the current ADCS mode
        break;
    case STATE_RECOVERY:
        print("Changing state to RECOVERY");
        if (getPin(TESTING_JUMPER_PIN) != HIGH) { // Don't disable RXSM telemetry when testing
            telemetryEnableState &= ~RXSM_TELEMETRY;
        }
        requestAdcsMode(MODE_OFF);
        break;
    case STATE_PAYLOAD_OFF:
        print("Changing state to PAYLOAD_OFF");
        requestAdcsMode(MODE_OFF);
        batteryEnabled = false; // shut battery power down
        break;
    case STATE_TESTING:
        print("Changing state to TESTING");
        break;
    default:
        printCriticalError("Unknown state change requested");
        return;
    }
    setBatteryEnabled(batteryEnabled);
    setTelemetryEnabled(telemetryEnableState, true);
    setTelemetryEnabled(~telemetryEnableState, false);
    if (!set_systemState(&state)) {
        printCriticalError("Failed to save current system state");
    }
    currentState = state;
    requestedState = state;
}

void initSystem(void) {
    // Do not use os functions (like osDelay etc.) in initialization
    if (!initPrintSubsystem()) {
        setPin(ERROR_INDICATOR_LED_PIN, HIGH); // Print is not possible here
    }
    initWatchdog();
    initGPIO();
    if (!initCRC()) {
        printCriticalError("Failed to initialize the CRC system");
    }
    if (!initData()) {
        printCriticalError("Failed to initialize the data system");
    }
    MX_DMA_Init();
    if (!initCommunication()) {
        printCriticalError("Failed to initialize the communication system");
    }
    if (!initADC()) {
        printError("Failed to initialize the ADC system");
    }
    if (!initMotors()) {
        printCriticalError("Failed to initialize the motor system");
    }

    handleRestartReason();
    if (getPin(TESTING_JUMPER_PIN) == HIGH) {
        currentState = STATE_TESTING;
    } else if (isFlashCorrupted()) {
        // TODO: Do a best guess based on the current system state (pins)
        currentState = STATE_IDLE;
    } else if (!get_systemState(&currentState)) {
        printCriticalError("Unable to retrieve previous system state");
        currentState = STATE_IDLE;
    }
    requestedState = currentState;
    initRecovery();
    currentState = requestedState; // Allow the recovery to override the saved state.

    setBatteryEnabled(true);
    switch (currentState) {
    case STATE_EXPERIMENT:
        requestAdcsMode(MODE_STABILITY);
        break;
    case STATE_PAYLOAD_OFF:
        changeState(STATE_IDLE);
        break;
    default:
        break;
    }
}

noreturn void StartControlTask(void* argument) {
    UNUSED(argument);
    const Configuration* config = getConfig();
    uint8_t lastSOEState = HIGH, lastSODSState = HIGH;
#if SEND_ALIVE_TIMER
    uint8_t heartbeatTimer = config->heartbeatPeriod.ms / THREAD_CONTROL_PERIOD.ms;
#endif /* SEND_ALIVE_TIMER */
    while (1) {
        ms_t tick = getTimeSinceBoot();
        if (currentState != requestedState) { // Check if someone requested a new state
            changeState(requestedState);
        }

        switch (currentState) {
        case STATE_PRE_LAUNCH:
            if (!IS_SET(preLaunchState, STATE_IMU_CALIBRATION_DONE) &&
                tick.ms - lastStateChangeTick.ms >= config->preLaunchIMUCalibrationTime.ms) {
                startImuCalibration();
                preLaunchState |= STATE_IMU_CALIBRATION_DONE;
            }
            if (tick.ms - lastStateChangeTick.ms >= config->preLaunchMotorWarmupStartTime.ms) {
                bool isInWarmup = getAdcsMode() == MODE_WARM_UP;
                if (tick.ms - lastStateChangeTick.ms < config->preLaunchMotorWarmupEndTime.ms) {
                    if (!isInWarmup) {
                        requestAdcsMode(MODE_WARM_UP);
                    }
                } else if (isInWarmup) {
                    requestAdcsMode(MODE_OFF);
                }
            }
            if (getPin(LO_SIGNAL_PIN) == LOW) { // Check for the launch signal
                changeState(STATE_LAUNCH);
            }
            break;
        case STATE_LAUNCH:
            if (getPin(SOE_SIGNAL_PIN) == LOW && lastSOEState != LOW) {
                lastSOEState = LOW;
                print("Detected SOE signal");
                enablePayloadVideoRecording();
                enableGoPros(true);
            }
            if (getPin(SODS_SIGNAL_PIN) == LOW && lastSODSState != LOW) {
                lastSODSState = LOW;
                print("Detected reaction wheel spin-up signal");
                requestAdcsMode(MODE_SPIN_UP);
            }
            if (getPin(EXT_POWER_CONNECTED_PIN) == LOW) { // Check for ejection
                bool setFlight = false;
                if (!get_flightMode(&setFlight)) {
                    printError("Unable to retrieve flight state from flash");
                } else if (setFlight) {
                    changeState(STATE_EXPERIMENT);
                }
            }
            break;
        case STATE_EXPERIMENT:
            switch (getAdcsMode()) {
            case MODE_OFF:
                break;
            case MODE_STABILITY:
                // TODO: Check if stable enough to switch to orientation
//                if (false) {
//                    requestAdcsMode(MODE_ORIENTATION);
//                }
            case MODE_ORIENTATION: {
                volt_t batteryLevel;
                if (get_batteryLevel(&batteryLevel) &&
                    batteryLevel.volt < config->minADCSBatteryLevel.volt) {
                    print("Disabling ADCS because of low battery");
                    requestAdcsMode(MODE_OFF);
                }
                break;
            }
            case MODE_WARM_UP:
            case MODE_SPIN_UP:
                if (tick.ms >= lastStateChangeTick.ms + config->ejectADCSStartWaitTime.ms) {
                    requestAdcsMode(MODE_STABILITY);
                }
                break;
            case MODE_PWM_DEBUG:
            case MODE_RPM_DEBUG:
            case MODE_SEQ_DEBUG:
            default:
                requestAdcsMode(MODE_STABILITY);
            }
            break;
        case STATE_IDLE:
        case STATE_RECOVERY:
        case STATE_PAYLOAD_OFF:
        case STATE_TESTING:
            break;
        default:
            printCriticalError("Unknown state specified in task loop: %d", (int) currentState);
            changeState(STATE_IDLE);
        }
        if (nextGoProLowTick.ms <= tick.ms) {
            setPin(GOPRO_TRIGGER_PIN, LOW);
            nextGoProLowTick = ms_t(UINT32_MAX);
        }
#if SEND_ALIVE_TIMER
        if (heartbeatTimer >= config->heartbeatPeriod.ms / THREAD_CONTROL_PERIOD.ms) {
            sendTelemetryHeartbeat(getTimeSinceBoot());
            heartbeatTimer = 0;
        }
        heartbeatTimer++;
#endif /* SEND_ALIVE_TIMER */
        setPin(STATUS_LED_PIN, TOGGLE);
        setPin(STATUS2_LED_PIN, TOGGLE);
        refreshWatchdogTimer();
        sleepUntil(ms_t(tick.ms + THREAD_CONTROL_PERIOD.ms));
    }
}

void requestStateChange(SystemState state) {
    if (requestedState != STATE_PAYLOAD_OFF) {
        requestedState = state;
    }
}

void enableGoPros(bool enabled) {
    const Configuration* config = getConfig();
    nextGoProLowTick =
        ms_t(getTimeSinceBoot().ms + (enabled ? config->GoProOnTime.ms : config->GoProOffTime.ms));
    setPin(GOPRO_TRIGGER_PIN, HIGH);
}

SystemState getCurrentState(void) {
    return currentState;
}

void setPreLaunchTimer(ms_t timer) {
    ms_t currentTick = getTimeSinceBoot();
    if (currentState == STATE_PRE_LAUNCH) {
        if (IS_SET(preLaunchState, STATE_IMU_CALIBRATION_DONE) &&
            timer.ms < getConfig()->preLaunchIMUCalibrationTime.ms) {
            preLaunchState &= ~STATE_IMU_CALIBRATION_DONE; // Reset the calibration state.
        }
        lastStateChangeTick = ms_t(currentTick.ms - timer.ms);
    }
}
