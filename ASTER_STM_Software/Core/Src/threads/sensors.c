/**
 * The purpose of the SENSORS thread is to read data from various secondary sensors
 * and to store the data in the corresponding DATA objects.
 */

#include <math.h>
#include <stdio.h>
#include <stm32f7xx_hal.h>
#include "threads/threads.h"
#include "threads/control.h"
#include "threads/sensors.h"
#include "threads/watchdog.h"
#include "globals.h"
#include "objects/system.h"
#include "objects/sensors.h"


/** A flag that indicates that there is an unhandled calibration request. */
static volatile bool imuCalibrationRequested = false;


void startImuCalibration(void) {
    imuCalibrationRequested = true;
}

noreturn void StartSensorsTask(void* argument) {
    UNUSED(argument);
    const Configuration* config = getConfig();
#if IMU_ENABLED == 1
    ms_t nextImuCheckTime;
    if (!initIMUs()) {
        // Initialization after power-up fails, give the IMUs 300 milliseconds to boot.
        nextImuCheckTime = ms_t(getTimeSinceBoot().ms + 300);
    } else {
        nextImuCheckTime = ms_t(getTimeSinceBoot().ms + config->IMUStatusCheckInterval.ms);
    }
    refreshWatchdogTimer();
#endif
    while (1) {
        ms_t tick = getTimeSinceBoot();
#if IMU_ENABLED == 1
        if (imuCalibrationRequested) {
            calibrateIMUs();
            // Don't check the IMUs immediately after.
            // It takes a bit of time for them to switch out of config mode.
            nextImuCheckTime = ms_t(tick.ms + config->IMUStatusCheckInterval.ms);
            imuCalibrationRequested = false;
        }
        if (tick.ms >= nextImuCheckTime.ms) {
            checkIMUs();
            nextImuCheckTime = ms_t(tick.ms + config->IMUStatusCheckInterval.ms);
        }
#endif /* IMU_ENABLED */
        // Gather data
        updateTemperatureData();
        updateMotorCurrents();
#if IMU_ENABLED == 1
        updateImuData();
#endif
        refreshWatchdogTimer();
        sleepUntil(ms_t(tick.ms + THREAD_SENSORS_PERIOD.ms));
    }
}
