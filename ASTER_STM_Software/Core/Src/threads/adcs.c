/**
 * @brief ADCS Thread
 *
 * The purpose of the ADCS object is to contain all objects and threads
 * handling the ADCS calculations and control and primary IMU gyroscope reading.
 */

#include <math.h>
#include "data/adcs.h"
#include "threads/threads.h"
#include "threads/adcs.h"
#include "threads/watchdog.h"
#include "objects/system.h"
#include "objects/motors.h"
#include "objects/gpio.h"
#include "objects/sensors.h"
#include "controller.h"

/** One second in milliseconds. */
#define SECOND ms_t(1000)

/** Whether or not to enable additional debugging prints for the ADCS system. */
#define DEBUG_ADCS false

#if DEBUG_ADCS
#    define ADCS_PRINT_DEBUG(format, ...) printDebug(format, __VA_ARGS__)
#else
#    define ADCS_PRINT_DEBUG(format, ...) ((void) 0)
#endif /* DEBUG_ADCS */

/** The current control mode of the ADCS system. */
static AdcsMode controlMode = MODE_OFF;

/** The control mode that is requested by other parts of the system. */
static volatile AdcsMode requestedControlMode = MODE_OFF;

/** The current attitude of the FFU in radians. */
static Vec3D ffuAttitude = VEC3D_ZERO;

/** A constant PWM value, used in mode MODE_PWM_DEBUG. The value can be changed via telecommand. */
static pwm_t debugPWM = pwm_t(40);

/** A constant RPM value, used in mode MODE_RPM_DEBUG. The value can be changed via telecommand. */
static rpm_t debugRPM = rpm_t(1500);

/** A sequence of steps. */
typedef struct {
    /** The number of steps in the sequence. */
    size_t numSteps;
    /** The steps of this sequence. */
    const Vec3D* steps;
    /* The start time for the sequence in milliseconds. */
    ms_t startTime;
} sequence_t;

/** A controller for the sequence mode. */
typedef struct {
    /** The sequence that the controller will execute. */
    sequence_t sequence;
    /** The last roll rpm that was commanded. */
    rpm_t lastDemandedRoll;
    /** The last pitch rpm that was commanded. */
    rpm_t lastDemandedPitch;
    /** The last yaw rpm that was commanded. */
    rpm_t lastDemandedYaw;
} sequenceController_t;

/** The motor speeds to for the debug sequence. */
static const Vec3D DEBUG_SEQUENCE[] = {
    // Roll, pitch, yaw
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{950, 950, 950}},
    {{950, 950, 950}},
    {{950, 950, 950}},
    {{950, 950, 950}},
    {{950, 950, 950}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{900, 900, 900}},
    {{900, 900, 900}},
    {{900, 900, 900}},
    {{900, 900, 900}},
    {{900, 900, 900}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{850, 850, 850}},
    {{850, 850, 850}},
    {{850, 850, 850}},
    {{850, 850, 850}},
    {{850, 850, 850}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{800, 800, 800}},
    {{800, 800, 800}},
    {{800, 800, 800}},
    {{800, 800, 800}},
    {{800, 800, 800}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{750, 750, 750}},
    {{750, 750, 750}},
    {{750, 750, 750}},
    {{750, 750, 750}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{1000, 1000, 1000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{2950, 2950, 2950}},
    {{2950, 2950, 2950}},
    {{2950, 2950, 2950}},
    {{2950, 2950, 2950}},
    {{2950, 2950, 2950}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{2900, 2900, 2900}},
    {{2900, 2900, 2900}},
    {{2900, 2900, 2900}},
    {{2900, 2900, 2900}},
    {{2900, 2900, 2900}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{2850, 2850, 2850}},
    {{2850, 2850, 2850}},
    {{2850, 2850, 2850}},
    {{2850, 2850, 2850}},
    {{2850, 2850, 2850}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{2800, 2800, 2800}},
    {{2800, 2800, 2800}},
    {{2800, 2800, 2800}},
    {{2800, 2800, 2800}},
    {{2800, 2800, 2800}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{3000, 3000, 3000}},
    {{2900, 2900, 2900}},
    {{2800, 2800, 2800}},
    {{2700, 2700, 2700}},
    {{2600, 2600, 2600}},
    {{2500, 2500, 2500}},
    {{2400, 2400, 2400}},
    {{2300, 2300, 2300}},
    {{2200, 2200, 2200}},
    {{2100, 2100, 2100}},
    {{2000, 2000, 2000}},
    {{1900, 1900, 1900}},
    {{1800, 1800, 1800}},
    {{1700, 1700, 1700}},
    {{1600, 1600, 1600}},
    {{1500, 1500, 1500}},
    {{1400, 1400, 1400}},
    {{1300, 1300, 1300}},
    {{1200, 1200, 1200}},
    {{1100, 1100, 1100}},
    {{1000, 1000, 1000}},
    {{500, 500, 500}},
    {{500, 500, 500}},
    {{500, 500, 500}},
    {{500, 500, 500}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
    {{-950, -950, -950}},
    {{-950, -950, -950}},
    {{-950, -950, -950}},
    {{-950, -950, -950}},
    {{-950, -950, -950}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-900, -900, -900}},
    {{-900, -900, -900}},
    {{-900, -900, -900}},
    {{-900, -900, -900}},
    {{-900, -900, -900}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-850, -850, -850}},
    {{-850, -850, -850}},
    {{-850, -850, -850}},
    {{-850, -850, -850}},
    {{-850, -850, -850}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-800, -800, -800}},
    {{-800, -800, -800}},
    {{-800, -800, -800}},
    {{-800, -800, -800}},
    {{-800, -800, -800}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-750, -750, -750}},
    {{-750, -750, -750}},
    {{-750, -750, -750}},
    {{-750, -750, -750}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-1000, -1000, -1000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-2950, -2950, -2950}},
    {{-2950, -2950, -2950}},
    {{-2950, -2950, -2950}},
    {{-2950, -2950, -2950}},
    {{-2950, -2950, -2950}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-2900, -2900, -2900}},
    {{-2900, -2900, -2900}},
    {{-2900, -2900, -2900}},
    {{-2900, -2900, -2900}},
    {{-2900, -2900, -2900}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-2850, -2850, -2850}},
    {{-2850, -2850, -2850}},
    {{-2850, -2850, -2850}},
    {{-2850, -2850, -2850}},
    {{-2850, -2850, -2850}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-2800, -2800, -2800}},
    {{-2800, -2800, -2800}},
    {{-2800, -2800, -2800}},
    {{-2800, -2800, -2800}},
    {{-2800, -2800, -2800}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-3000, -3000, -3000}},
    {{-2900, -2900, -2900}},
    {{-2800, -2800, -2800}},
    {{-2700, -2700, -2700}},
    {{-2600, -2600, -2600}},
    {{-2500, -2500, -2500}},
    {{-2400, -2400, -2400}},
    {{-2300, -2300, -2300}},
    {{-2200, -2200, -2200}},
    {{-2100, -2100, -2100}},
    {{-2000, -2000, -2000}},
    {{-1900, -1900, -1900}},
    {{-1800, -1800, -1800}},
    {{-1700, -1700, -1700}},
    {{-1600, -1600, -1600}},
    {{-1500, -1500, -1500}},
    {{-1400, -1400, -1400}},
    {{-1300, -1300, -1300}},
    {{-1200, -1200, -1200}},
    {{-1100, -1100, -1100}},
    {{-1000, -1000, -1000}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
    {{-500, -500, -500}},
};

/** The controller of the debug sequence. */
static sequenceController_t debugSequenceController = {
    .sequence =
        {
            .numSteps = ARRAY_SIZE(DEBUG_SEQUENCE),
            .steps = DEBUG_SEQUENCE,
        },
};

/** The motor speeds to for the motor spin up sequence. */
static const Vec3D SPIN_UP_SEQUENCE[] = { // TODO: Define a good spinup sequence
    // Roll, pitch, yaw
    {{100, 0, 0}},
    {{100, 100, 0}},
    {{100, 100, 100}},
};

/** The controller of the motor spin up sequence. */
static sequenceController_t spinUpController = {
    .sequence =
        {
            .numSteps = ARRAY_SIZE(SPIN_UP_SEQUENCE),
            .steps = SPIN_UP_SEQUENCE,
        },
};

/** The motor speeds to for the motor warm up sequence. */
static const Vec3D WARM_UP_SEQUENCE[] = { // TODO: Define a good warmup sequence
    // Roll, pitch, yaw
    {{100, 0, 0}},
    {{100, 100, 0}},
    {{100, 100, 100}},
};

/** The controller of the motor warm up sequence. */
static sequenceController_t warmUpController = {
    .sequence =
        {
            .numSteps = ARRAY_SIZE(WARM_UP_SEQUENCE),
            .steps = WARM_UP_SEQUENCE,
        },
};

/** The steps of the attitude maneuver. */
static Vec3D attitudeManeuverSteps[] = { // TODO: Define an attitude maneuver
    {{0, 0, 0}},
};

/** The maneuver that the attitude controller should perform. */
static sequence_t attitudeManeuver = {
    .numSteps = ARRAY_SIZE(attitudeManeuverSteps),
    .steps = attitudeManeuverSteps,
};

/**
 * Set the demanded speed of the motors.
 *
 * @param rollWheelSpeed The demanded speed of the motor which controls the roll angle.
 * @param pitchWheelSpeed The demanded speed of the motor which controls the pitch angle.
 * @param yawWheelSpeed The demanded speed of the motor which controls the yaw angle.
 */
static void setDemandedMotorSpeed(rpm_t rollWheelSpeed, rpm_t pitchWheelSpeed,
    rpm_t yawWheelSpeed) {
    setMotors(rollWheelSpeed, pitchWheelSpeed, yawWheelSpeed);
    Vec3Int32WithTime speeds = {
        .data =
            {
                .roll = (int32_t) rollWheelSpeed.rpm,
                .pitch = (int32_t) pitchWheelSpeed.rpm,
                .yaw = (int32_t) yawWheelSpeed.rpm,
            },
    };
    speeds.time = getTimeSinceBoot();
    append_demandedMotorSpeeds(&speeds, 1);
}

/**
 * Execute a tick of a sequence controller.
 *
 * @param config The experiment configuration.
 * @param controller The controller to execute.
 * @param currentMotorsSpeed The current speed of the motors.
 * @param looping Whether the sequence should be looping.
 */
static void executeSequenceController(const Configuration* config, sequenceController_t* controller,
    const Vec3D* currentMotorsSpeed, bool looping) {
    uint16_t currentTimeSec =
        floor((getTimeSinceBoot().ms - controller->sequence.startTime.ms) / (double) SECOND.ms);
    size_t index = currentTimeSec;
    if (looping) {
        index %= controller->sequence.numSteps;
    } else if (currentTimeSec >= controller->sequence.numSteps) {
        index = controller->sequence.numSteps - 1;
    }

    if (!config->useMotorSpeedsForController) {
        Vec3D currentSpeed = {};
        currentSpeed.roll = (double) controller->lastDemandedRoll.rpm;
        currentSpeed.pitch = (double) controller->lastDemandedPitch.rpm;
        currentSpeed.yaw = (double) controller->lastDemandedYaw.rpm;
        currentMotorsSpeed = &currentSpeed;
    }

    rpm_t demandedRollChange =
        rpm_t(controller->sequence.steps[index].roll - controller->lastDemandedRoll.rpm);
    controller->lastDemandedRoll.rpm +=
        speedRamp(demandedRollChange, rpm_t(currentMotorsSpeed->roll)).rpm;
    rpm_t demandedPitchChange =
        rpm_t(controller->sequence.steps[index].pitch - controller->lastDemandedPitch.rpm);
    controller->lastDemandedPitch.rpm +=
        speedRamp(demandedPitchChange, rpm_t(currentMotorsSpeed->pitch)).rpm;
    rpm_t demandedYawChange =
        rpm_t(controller->sequence.steps[index].yaw - controller->lastDemandedYaw.rpm);
    controller->lastDemandedYaw.rpm +=
        speedRamp(demandedYawChange, rpm_t(currentMotorsSpeed->yaw)).rpm;

    ADCS_PRINT_DEBUG("Mode: %02X, RPM: (%f, %f, %f)", controlMode, controller->lastDemandedRoll.rpm,
        controller->lastDemandedPitch.rpm, controller->lastDemandedYaw.rpm);
    setDemandedMotorSpeed(controller->lastDemandedRoll, controller->lastDemandedPitch,
        controller->lastDemandedYaw);
}

/**
 * Initialize an ADCS mode.
 *
 * @param newMode The new mode to initialize.
 */
static void initializeAdcsMode(AdcsMode newMode) {
    switch (newMode) {
    case MODE_ORIENTATION:
        print("Enabling orientation controller");
        attitudeManeuver.startTime = getTimeSinceBoot();
        ffuAttitude = (Vec3D) VEC3D_ZERO;
        enableMotors();
        break;
    case MODE_STABILITY:
        print("Enabling stability controller");
        enableMotors();
    case MODE_SPIN_UP:
        print("Enabling motor spin up mode");
        spinUpController.sequence.startTime = getTimeSinceBoot();
        enableMotors();
        break;
    case MODE_WARM_UP:
        print("Enabling motor warm up mode");
        warmUpController.sequence.startTime = getTimeSinceBoot();
        enableMotors();
        break;
    case MODE_SEQ_DEBUG:
        debugSequenceController.sequence.startTime = getTimeSinceBoot();
    case MODE_RPM_DEBUG:
    case MODE_PWM_DEBUG:
        enableMotors();
        break;
    case MODE_OFF:
        print("Disabling ADCS");
        disableMotors();
        break;
    default:
        printCriticalError("Invalid mode in initializeAdcsMode: %d", newMode);
        return;
    }
    printDebug("Switching ADCS mode, current: %d, new: %d", controlMode, newMode);
    requestedControlMode = controlMode = newMode;
}

void requestAdcsMode(AdcsMode newMode) {
    if (newMode == controlMode) {
        return;
    }
    switch (newMode) {
    case MODE_RPM_DEBUG:
    case MODE_PWM_DEBUG:
    case MODE_SEQ_DEBUG:
        if (getPin(TESTING_JUMPER_PIN) != HIGH) {
            printError("Unable to set Adcs mode: Got debug mode but testing pin is not set");
            return;
        }
    case MODE_SPIN_UP:
    case MODE_WARM_UP:
    case MODE_STABILITY:
    case MODE_ORIENTATION:
    case MODE_OFF:
        requestedControlMode = newMode;
        break;
    default:
        printCriticalError("Invalid mode: %d", newMode);
    }
}

AdcsMode getAdcsMode(void) {
    return controlMode;
}

void setDebugPWM(pwm_t value) {
    debugPWM = value;
}
void setDebugRPM(rpm_t value) {
    debugRPM = value;
}

/**
 * Update the FFU attitude based on the current rotation measurements.
 *
 * @param rotation The current measured rotation in rpm.
 * @param timeDelta The time since the last attitude update in seconds.
 */
static void updateAttitude(const Vec3D* rotation, sec_t timeDelta) {
    ffuAttitude.pitch += rotation->pitch * timeDelta.sec;
    ffuAttitude.yaw += rotation->yaw * timeDelta.sec;
    ffuAttitude.roll += rotation->roll * timeDelta.sec;
    if (ffuAttitude.pitch < 0) {
        ffuAttitude.pitch += M_TWOPI;
    } else {
        ffuAttitude.pitch = fmod(ffuAttitude.pitch, M_TWOPI);
    }
    if (ffuAttitude.yaw < 0) {
        ffuAttitude.yaw += M_TWOPI;
    } else {
        ffuAttitude.yaw = fmod(ffuAttitude.yaw, M_TWOPI);
    }
    if (ffuAttitude.roll < 0) {
        ffuAttitude.roll += M_TWOPI;
    } else {
        ffuAttitude.roll = fmod(ffuAttitude.roll, M_TWOPI);
    }
}

/**
 * Execute one step of the given attitude controller.
 *
 * @param controller The controller to execute.
 * @param target The target that the controller should reach
 *               (can be either orientation or rotation speed).
 * @param currentValue The current value that the controller controls.
 * *                   (can be either orientation or rotation speed).
 * @param motorSpeed The current speed of the motors.
 * @param deltaTime The time in milliseconds since the last step.
 */
static void attitudeControl(controller_t* controller, const Vec3D* target,
    const Vec3D* currentValue, const Vec3D* motorSpeed, ms_t deltaTime) {
    sec_t timeDeltaSec = sec_t(deltaTime.ms / (double) SECOND.ms);
#if DEBUG_ADCS
    if (isnan(currentValue->roll) || isnan(currentValue->pitch) || isnan(currentValue->yaw)) {
        printError("Error: NaN in currentValue detected!");
    }
#endif /* DEBUG_ADCS */
    // TODO: The simulation treats the result as rps, but here it is rpm!!!
    rpm_t rollWheelRpm = controlAxis(&controller->rollController, target->roll - currentValue->roll,
        timeDeltaSec, rpm_t(motorSpeed->roll));
    rpm_t pitchWheelRpm = controlAxis(&controller->pitchController,
        target->pitch - currentValue->pitch, timeDeltaSec, rpm_t(motorSpeed->pitch));
    rpm_t yawWheelRpm = controlAxis(&controller->yawController, target->yaw - currentValue->yaw,
        timeDeltaSec, rpm_t(motorSpeed->yaw));
    setDemandedMotorSpeed(rollWheelRpm, pitchWheelRpm, yawWheelRpm);
    ADCS_PRINT_DEBUG(
        "Controller: input {roll: %f, pitch: %f, yaw: %f}, output: {roll: %f, pitch: %f, yaw: %f}",
        currentValue->roll, currentValue->pitch, currentValue->yaw, rollWheelRpm.rpm,
        pitchWheelRpm.rpm, yawWheelRpm.rpm);

    /** Check if thread time is within limits. Otherwise write a debug message with warning.*/
    if (deltaTime.ms < THREAD_ADCS_PERIOD.ms * 0.9 || deltaTime.ms > THREAD_ADCS_PERIOD.ms * 1.1) {
        printDebug("Warning: ADCS time step is %u ms", (unsigned int) deltaTime.ms);
    }
}

/**
 * @return The current targeted attitude of the attitude controller.
 */
static const Vec3D* getCurrentAttitudeTarget(void) {
    uint16_t currentTimeSec =
        floor((getTimeSinceBoot().ms - attitudeManeuver.startTime.ms) / (double) SECOND.ms);
    const Vec3D* target = &attitudeManeuver.steps[currentTimeSec % attitudeManeuver.numSteps];
    return target;
}

noreturn void StartAdcsTask(void* argument) {
    UNUSED(argument);
    ms_t lastTime = ms_t(0);
    Vec3D zeroVector = {{0, 0, 0}};
    controller_t detumbleController;
    const Configuration* config = getConfig();
    pidConfig detumbleConfig = {
        .p = config->detumbleControllerP,
        .i = config->detumbleControllerI,
        .d = config->detumbleControllerD,
    };
    controller_t orientationController;
    pidConfig orientationConfig = {
        .p = config->orientationControllerP,
        .i = config->orientationControllerI,
        .d = config->orientationControllerD,
    };
    resetController(&detumbleController.rollController, rpm_t(0), &detumbleConfig,
        config->useMotorSpeedsForController);
    resetController(&detumbleController.pitchController, rpm_t(0), &detumbleConfig,
        config->useMotorSpeedsForController);
    resetController(&detumbleController.yawController, rpm_t(0), &detumbleConfig,
        config->useMotorSpeedsForController);
    resetController(&orientationController.rollController, rpm_t(0), &orientationConfig,
        config->useMotorSpeedsForController);
    resetController(&orientationController.pitchController, rpm_t(0), &orientationConfig,
        config->useMotorSpeedsForController);
    resetController(&orientationController.yawController, rpm_t(0), &orientationConfig,
        config->useMotorSpeedsForController);
    initializeAdcsMode(requestedControlMode);

    while (1) {
        ms_t tick = getTimeSinceBoot();
        /* Calculate Delta Time */
        ms_t deltaTime = ms_t(tick.ms - lastTime.ms);
        lastTime = tick;

        Vec3D motorSpeed;
        readMotorsSpeed(&motorSpeed);
        Vec3D angularVelocity;
        bool haveRotationMeasurements = readRotationSpeedFromPrimaryImu(&angularVelocity);

        if (requestedControlMode != controlMode) {
            initializeAdcsMode(requestedControlMode);
        }

        /* Control Mode States */
        /* Remember to also add mode in checkMode() */
        switch (controlMode) {
        case MODE_OFF:
            break;
        case MODE_SPIN_UP:
            executeSequenceController(config, &spinUpController, &motorSpeed, false);
            break;
        case MODE_WARM_UP:
            executeSequenceController(config, &warmUpController, &motorSpeed, false);
            break;
        case MODE_STABILITY:
            if (!haveRotationMeasurements) {
                printCriticalError("Failed to read the current rotation speed");
            } else {
                attitudeControl(&detumbleController, &zeroVector, &angularVelocity, &motorSpeed,
                    deltaTime);
            }
            break;
        case MODE_ORIENTATION:
            if (!haveRotationMeasurements) {
                printCriticalError("Failed to read the current rotation speed");
            } else {
                updateAttitude(&angularVelocity, sec_t(deltaTime.ms / (double) SECOND.ms));
                const Vec3D* attitudeTarget = getCurrentAttitudeTarget();
                attitudeControl(&orientationController, attitudeTarget, &ffuAttitude, &motorSpeed,
                    deltaTime);
            }
            break;
        case MODE_RPM_DEBUG:
            setDemandedMotorSpeed(debugRPM, debugRPM, debugRPM);
            ADCS_PRINT_DEBUG("Mode: %02X, RPM: %f", controlMode, debugRPM.rpm);
            break;
        case MODE_SEQ_DEBUG:
            executeSequenceController(config, &debugSequenceController, &motorSpeed, true);
            break;
        case MODE_PWM_DEBUG:
            setMotor(MOTOR_ROLL, debugPWM);
            setMotor(MOTOR_PITCH, debugPWM);
            setMotor(MOTOR_YAW, debugPWM);
            ADCS_PRINT_DEBUG("Mode: %02X (MODE_PWM_DEBUG), PWM: %f", controlMode, debugPWM.pwm);
            break;
        default: /* Unknown mode error */
            printCriticalError("Unknown ADCS mode: %02X", controlMode);
            initializeAdcsMode(MODE_OFF);
        }
        ADCS_PRINT_DEBUG("delta time: %lu ms", deltaTime.ms);

        refreshWatchdogTimer();
        sleepUntil(ms_t(tick.ms + THREAD_ADCS_PERIOD.ms));
    }
}
