/**
 * The purpose of the WATCHDOG object is to provide the thread for watchdog timers,
 * providing relevant error handling and to update the hardware watchdog.
 */

#include <cmsis_os.h>
#include <FreeRTOS.h>
#include "main.h"
#include "globals.h"
#include "threads/threads.h"
#include "threads/watchdog.h"
#include "objects/system.h"
#include "objects/gpio.h"


/** The timer instance that is used to measure the clock */
#define measurementTimer htim5
extern TIM_HandleTypeDef measurementTimer;

/** The hardware watchdog instance */
#define hardwareWatchdog hiwdg
extern IWDG_HandleTypeDef hardwareWatchdog;

/** The time in milliseconds after which the hardware watchdog should reset the board. */
#define HW_WATCHDOG_TRIGGER_TIME_MS 2000
/** @see HW_WATCHDOG_TRIGGER_TIME_MS */
#define HW_WATCHDOG_TRIGGER_TIME ms_t(HW_WATCHDOG_TRIGGER_TIME_MS)

/** The amount of wait loop iterations to wait for the reload value measurement. */
#define RELOAD_VALUE_MEASUREMENT_TIMEOUT 6000

/**
 * The time to wait for the last flash operation to complete when restarting.
 * The value is taken from the datasheet of the CPU (Table 53, t_ERASE16KB, x32)
 * as 265 ms plus a little padding.
 */
#define FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART_MS 270
/** @see FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART_MS */
#define FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART ms_t(FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART_MS)

/**
 * The reload value for the hardware watchdog configuration.
 * This needs to be computed at runtime because it depends on the speed of the CPU clock.
 * @see initializeWatchdogReloadValue
 */
static volatile uint32_t watchdogReloadValue = 0;

/** The tick value when the ADCS thread last called the refreshWatchdogTimer. */
static ms_t adcsLastWatchdogTick = ms_t(0);

/** The tick value when the control thread last called the refreshWatchdogTimer. */
static ms_t controlLastWatchdogTick = ms_t(0);

/** The tick value when the recovery thread last called the refreshWatchdogTimer. */
static ms_t recoveryLastWatchdogTick = ms_t(0);

/** The tick value when the sensors thread last called the refreshWatchdogTimer. */
static ms_t sensorsLastWatchdogTick = ms_t(0);

/** The tick value when the telecommand thread last called the refreshWatchdogTimer. */
static ms_t telecommandLastWatchdogTick = ms_t(0);

/** The tick value when the telemetry thread last called the refreshWatchdogTimer. */
static ms_t telemetryLastWatchdogTick = ms_t(0);

/** Initialize the watchdogReloadValue constant. The value depends on the speed of the LSI clock. */
static void initializeWatchdogReloadValue(void) {
    MX_TIM5_Init();
    if (getLastSystemError() == TIM5_INIT_SYSTEM_ERROR) {
        printCriticalError("Failed to initialize the timer to measure the watchdog reload value");
        return;
    }
    // Reset the flags
    measurementTimer.Instance->SR = 0;
    // Start the TIM Input Capture measurement in interrupt mode
    if (HAL_TIM_IC_Start_IT(&measurementTimer, TIM_CHANNEL_4) != HAL_OK) {
        printCriticalError("Failed to initialize measurement of the watchdog reload value");
        return;
    }

    // Make sure interrupts are enabled
    taskENABLE_INTERRUPTS();

    // Wait until the TIM5 get 2 LSI edges (see HAL_TIM_IC_CaptureCallback)
    uint32_t waitCounter = 0;
    while (!watchdogReloadValue) {
        if (waitCounter++ > RELOAD_VALUE_MEASUREMENT_TIMEOUT) {
            printCriticalError("Timeout during measurement of the watchdog reload value");
            break;
        }
    }

    // De-initialize the measurement timer peripheral registers to their default reset values
    HAL_TIM_IC_DeInit(&measurementTimer);
}

bool initWatchdog(void) {
    // Do not use os functions (like osDelay etc) in initialization
    initializeWatchdogReloadValue();
    MX_IWDG_Init();
    if (getLastSystemError() == IWDG_INIT_SYSTEM_ERROR) {
        printCriticalError("Failed to initialize the watchdog");
        return false;
    }
    return true;
}

bool updateHardwareWatchdog(void) {
    return HAL_IWDG_Refresh(&hardwareWatchdog) == HAL_OK;
}

noreturn void StartWatchdogTask(void* argument) {
    UNUSED(argument);
    adcsLastWatchdogTick = getTimeSinceBoot();
    controlLastWatchdogTick = getTimeSinceBoot();
    recoveryLastWatchdogTick = getTimeSinceBoot();
    sensorsLastWatchdogTick = getTimeSinceBoot();
    telecommandLastWatchdogTick = getTimeSinceBoot();
    telemetryLastWatchdogTick = getTimeSinceBoot();
    ms_t lastTick = getTimeSinceBoot();
    uint8_t count = 0;
    while (1) {
        ms_t tick = getTimeSinceBoot();
        if (lastTick.ms + INTERRUPT_DELAY_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(INTERRUPT_STALL_RESTART);
        }
        if (adcsLastWatchdogTick.ms + THREAD_ADCS_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(ADCS_THREAD_FREEZE_RESTART);
        }
        if (controlLastWatchdogTick.ms + THREAD_CONTROL_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(CONTROL_THREAD_FREEZE_RESTART);
        }
        if (recoveryLastWatchdogTick.ms + THREAD_RECOVERY_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(RECOVERY_THREAD_FREEZE_RESTART);
        }
        if (sensorsLastWatchdogTick.ms + THREAD_SENSORS_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(SENSORS_THREAD_FREEZE_RESTART);
        }
        if (telecommandLastWatchdogTick.ms + THREAD_TELECOMMAND_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(TELECOMMAND_THREAD_FREEZE_RESTART);
        }
        if (telemetryLastWatchdogTick.ms + THREAD_TELEMETRY_WATCHDOG_TIMEOUT.ms < tick.ms) {
            internalSoftwareReset(TELEMETRY_THREAD_FREEZE_RESTART);
        }

        if (count++ >= 5) {
            setPin(WATCHDOG_STATUS_LED_PIN, TOGGLE);
            count = 0;
        }
        if (!updateHardwareWatchdog()) {
            printCriticalError("Failed to refresh hardware watchdog");
        }
        sleepUntil(ms_t(tick.ms + THREAD_WATCHDOG_PERIOD.ms));
        lastTick = tick;
    }
}

void refreshWatchdogTimer(void) {
    osThreadId_t threadId = osThreadGetId();
    if (threadId == adcsTaskHandle) {
        adcsLastWatchdogTick = getTimeSinceBoot();
    } else if (threadId == controlTaskHandle) {
        controlLastWatchdogTick = getTimeSinceBoot();
    } else if (threadId == recoveryTaskHandle) {
        recoveryLastWatchdogTick = getTimeSinceBoot();
    } else if (threadId == sensorsTaskHandle) {
        sensorsLastWatchdogTick = getTimeSinceBoot();
    } else if (threadId == telecommandTaskHandle) {
        telecommandLastWatchdogTick = getTimeSinceBoot();
    } else if (threadId == telemetryTaskHandle) {
        telemetryLastWatchdogTick = getTimeSinceBoot();
    } else {
        printError("refreshWatchdogTimer called from unknown thread %p", threadId);
    }
}

void noreturn internalSoftwareReset(RestartReason reason) {
    setRestartReason(reason);
    // taskENTER_CRITICAL_FROM_ISR disables normal interrupts, but kees the sys-tick
    // timer enabled, which is important for FLASH_WaitForLastOperation.
    uint32_t previousInterruptLevel;
    bool inInterrupt = isInInterrupt();
    if (inInterrupt) {
        previousInterruptLevel = taskENTER_CRITICAL_FROM_ISR();
    } else {
        taskENTER_CRITICAL();
    }
    // Update the hardware watchdog to avoid triggering it during the wait for the flash.
    HAL_IWDG_Refresh(&hardwareWatchdog);
    // The compiler is not able to get the value from the unit type, hence the raw values here.
    static_assert(FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART_MS < HW_WATCHDOG_TRIGGER_TIME_MS,
        "The timeout to wait for the last flash operation must be less than the trigger time "
        "of the hardware watchdog, otherwise it will potentially trigger during the flash "
        "operation, which can crash the chip.");
    FLASH_WaitForLastOperation(FLASH_OPERATION_WAIT_TIMEOUT_ON_RESTART.ms);
    HAL_NVIC_SystemReset(); // This function does not return
    if (inInterrupt) {
        taskEXIT_CRITICAL_FROM_ISR(previousInterruptLevel);
    } else {
        taskEXIT_CRITICAL();
    }
    while (1) {} // Guarantee this function does not return
}

/**
 * Input Capture callback to measure the watchdogReloadValue.
 *
 * @param timer The timer used for measuring.
 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef* timer) {
    static uint32_t measurementIndex = 0;
    static uint16_t measurements[] = {0, 0};
    if (timer != &measurementTimer) {
        return;
    }
    /* Get the Input Capture value */
    measurements[measurementIndex++] = HAL_TIM_ReadCapturedValue(timer, TIM_CHANNEL_4);
    if (measurementIndex >= 2) {
        measurementIndex = 0;
        /* Compute the period length */
        uint16_t uwPeriodValue = (uint16_t) (0xFFFF - measurements[0] + measurements[1] + 1);
        /* Compute the LSI frequency, depending on measurementTimer input clock frequency */
        uint32_t latency = 0;
        RCC_ClkInitTypeDef clockConfig = {0};
        uint32_t frequency = HAL_RCC_GetPCLK1Freq();
        HAL_RCC_GetClockConfig(&clockConfig, &latency);
        if (clockConfig.APB1CLKDivider != RCC_HCLK_DIV1) {
            frequency *= 2;
        }
        /*
         * Configure a (HW_WATCHDOG_TRIGGER_TIME / 1000) second hardware watchdog timeout:
         * LsiFreq = (frequency / uwPeriodValue) * 8
         * Reload = (HW_WATCHDOG_TRIGGER_TIME / 1000) / hardware watchdog counter clock period
         *        = (HW_WATCHDOG_TRIGGER_TIME / 1000) / (32 / LsiFreq)
         *        = LsiFreq / (32 / (HW_WATCHDOG_TRIGGER_TIME / 1000))
         */
        watchdogReloadValue =
            (frequency / uwPeriodValue) / (32000 / (HW_WATCHDOG_TRIGGER_TIME.ms * 8));
    }
}

uint32_t getWatchdogReloadValue(void) {
    if (!watchdogReloadValue) {
        printCriticalError("Watchdog reload value is requested before it was initialized");
        return LSI_VALUE / (32000 / HW_WATCHDOG_TRIGGER_TIME.ms);
    }
    return watchdogReloadValue;
}
