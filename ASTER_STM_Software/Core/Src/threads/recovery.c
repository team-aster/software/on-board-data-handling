/**
 * The purpose of the RECOVERY object is to initialize the Recovery thread and communication
 * with Iridium, GPS and parachute deployment. Recovery thread is continuously monitoring
 * the altitude readings from the GPS and has the privilege of changing the system state
 * in CONTROL object to "recovery state". It handles the data packets sent to Iridium module.
 *
 * Recovery object should be self-sufficient to provide safe recovery of the FFU.
 */

#include <cmsis_os.h>
#include <math.h>
#include "threads/threads.h"
#include "threads/recovery.h"
#include "globals.h"
#include "objects/system.h"
#include "objects/gpio.h"
#include "objects/sensors.h"
#include "objects/communication/gps.h"
#include "objects/communication/iridium.h"
#include "objects/communication/payload.h"
#include "objects/motors.h"
#include "objects/reedSolomon.h"
#include "data/sensor.h"
#include "data/adcs.h"
#include "data/gps.h"
#include "generated/telemetry.h"
#include "data/flash.h"
#include "threads/watchdog.h"
#include "threads/control.h"


/** The id of the payload message in the Iridium telemetry. */
#define PAYLOAD_MESSAGE_ID 0b11001010U

/** The id of the platform message in the Iridium telemetry. */
#define PLATFORM_MESSAGE_ID 0b00110101U

/** The factor by which the latitude and longitude are multiplied by before sending via Iridium. */
#define POSITION_FACTOR 1e7

/** The maximum expected acceleration of the FFU in (m/(s^2)). */
#define MAX_ACCELERATION 10

/** The maximum expected rotation of the FFU in rad/s. */
#define MAX_ROTATION 5000

/** The maximum expected FFU voltage in volt. */
#define MAX_FFU_VOLTAGE 5

/** The amount of retries for a failed parachute deployment in testing mode. */
#define TESTING_PARACHUTE_DEPLOY_RETRIES 3

/** A payload Iridium telemetry message. */
typedef struct __packed {
    uint8_t messageId; /** Payload message ID. */
    /**
     * Acceleration on the X-axis measured by the primary IMU
     * in meters per second squared (0x7fff/10 scaling).
     */
    int16_t xAcceleration;
    /**
     * Acceleration on the Y-axis measured by the primary IMU
     * in meters per second squared (0x7fff/10 scaling).
     */
    int16_t yAcceleration;
    /**
     * Acceleration on the Z-axis measured by the primary IMU
     * in meters per second squared (0x7fff/10 scaling).
     */
    int16_t zAcceleration;
    /**
     * The speed of the wheel that controls the roll axis
     * in rotations per minute (0x7fff/4200 scaling).
     */
    int16_t rollMotorSpeed;
    /**
     * The speed of the wheel that controls the pitch axis
     * in rotations per minute (0x7fff/4200 scaling).
     */
    int16_t pitchMotorSpeed;
    /**
     * The speed of the wheel that controls the yaw axis
     * in rotations per minute (0x7fff/4200 scaling).
     */
    int16_t yawMotorSpeed;
    uint8_t parity[3]; /** Parity bits for the payload telemetry using RS(16, 13). */
} PayloadIridiumTelemetry;

/** A platform Iridium telemetry message. */
typedef struct __packed {
    uint8_t messageId; /** Platform Message ID. */
    uint32_t gnssTime; /** GPS time of week of the navigation epoch in milliseconds. */
    int32_t latitude; /** Latitude in degrees (1e-7 scaling). */
    int32_t longitude; /** Longitude in degrees (1e-7 scaling). */
    int32_t altitude; /** Height above the ellipsoid in millimeters. */
    int16_t ffuVoltage; /** The voltage of the FFU in Volt (0x7fff/5 scaling). */
    /**
     * The roll rotation of the FFU measured by the primary IMU
     * in radians per second (0x7fff/5000 scaling).
     */
    int16_t rollRotation;
    /**
     * The pitch rotation of the FFU measured by the primary IMU
     * in radians per second (0x7fff/5000 scaling).
     */
    int16_t pitchRotation;
    /**
     * The yaw rotation of the FFU measured by the primary IMU
     * in radians per second (0x7fff/5000 scaling).
     */
    int16_t yawRotation;
    uint8_t systemState; /** The FFU system state. */
    uint8_t recoveryState; /** The state of the recovery subsystem. */
    uint8_t errorCounter; /** The number of error messages generated since system start. */
    uint8_t parity[4]; /** Parity bits for the platform telemetry using RS(32, 28). */
} PlatformIridiumTelemetry;

/** The Iridium telemetry message. */
typedef struct __packed {
    PayloadIridiumTelemetry payload; /** Payload related data. */
    uint8_t stopBits; /** Stop bits, used to separate the two telemetry components. */
    PlatformIridiumTelemetry platform; /** Platform related data. */
} ExperimentIridiumTelemetry;


/** Encapsulates the status of the recovery system. */
typedef struct {
    /** The current state of the recovery system. */
    RecoveryState currentRecoveryState;
    /** The requested state of the recovery system. */
    RecoveryState requestedRecoveryState;
    /**
     * The amount of time that has passed in which the senor readings stayed
     * within the threshold required for parachute deployment.
     */
    ms_t consecutiveParachuteDeploySensorDuration;
    /** The amount of time in which the pressure was stable within the required threshold. */
    ms_t landedStablePressureDuration;
    /** The amount of time in which the attitude was stable within the required threshold. */
    ms_t landedStableAttitudeDuration;
    /** The time when the the system was ejected from the rocket. */
    ms_t ejectionTime;
    /** The time when the the system has landed on the ground. */
    ms_t landedTime;
} RecoveryStatus;

/** The state of the recovery system. */
static RecoveryStatus status;


/**
 * Encode a double value into a linearly scaled signed 16 bit integer.
 * The double value is assumed to be symmetric (maximum = abs(minimum)).
 * In case of an overflow the value is saturated.
 *
 * @param value The double value to convert.
 * @param absMax The absolut of the maximum value.
 * @return The encoded value.
 */
static int16_t encodeDouble(double value, double absMax) {
    if (value >= absMax) {
        return INT16_MAX;
    }
    if (value <= -absMax) {
        return INT16_MIN;
    }
    return (int16_t) round(value * INT16_MAX / absMax);
}

/**
 * Send the recovery telemetry.
 *
 * @param ffuVoltage The current voltage of the battery.
 */
static void sendTelemetry(volt_t ffuVoltage) {
    ms_t time = getTimeSinceBoot();
    Vec3D acceleration = {};
    deg_t latitude = deg_t(0), longitude = deg_t(0);
    meter_t gpsAltitude = meter_t(0);
    ms_t gpsTime = ms_t(0);
    if (!get_primaryImuAccelerometer(&acceleration)) {
        printError("Failed to acquire sensor semaphore for recovery telemetry");
    }
    if (osSemaphoreAcquire(gpsSemaphore, 10) != osOK) {
        printError("Failed to acquire GPS semaphore for recovery telemetry");
    } else {
        latitude = getBatch_gpsLatitude();
        longitude = getBatch_gpsLongitude();
        gpsAltitude = getBatch_gpsAltitude();
        gpsTime = getBatch_gpsTime();
        osSemaphoreRelease(gpsSemaphore);
    }
    Vec3FWithTime motorSpeed = {};
    peekFirst_motorSpeeds(&motorSpeed);
    Vec3FWithTime rotation = {};
    peekFirst_rotationMeasurements(&rotation);
    SystemState systemState = getCurrentState();
    uint8_t errorCounter = getErrorCounter();
    ExperimentIridiumTelemetry telemetry = {
        .payload =
            {
                .messageId = PAYLOAD_MESSAGE_ID,
                .xAcceleration = encodeDouble(acceleration.x, MAX_ACCELERATION),
                .yAcceleration = encodeDouble(acceleration.y, MAX_ACCELERATION),
                .zAcceleration = encodeDouble(acceleration.z, MAX_ACCELERATION),
                .rollMotorSpeed = encodeDouble(motorSpeed.data.roll, MOTOR_MAX_RPM),
                .pitchMotorSpeed = encodeDouble(motorSpeed.data.pitch, MOTOR_MAX_RPM),
                .yawMotorSpeed = encodeDouble(motorSpeed.data.yaw, MOTOR_MAX_RPM),
                .parity = {0},
            },
        .stopBits = 0,
        .platform =
            {
                .messageId = PLATFORM_MESSAGE_ID,
                .gnssTime = gpsTime.ms,
                .latitude = (int32_t) (latitude.deg * POSITION_FACTOR),
                .longitude = (int32_t) (longitude.deg * POSITION_FACTOR),
                .altitude = (int32_t) (gpsAltitude.meter * 1000.0),
                .ffuVoltage = encodeDouble(ffuVoltage.volt, MAX_FFU_VOLTAGE),
                .rollRotation = encodeDouble(rotation.data.roll, MAX_ROTATION),
                .pitchRotation = encodeDouble(rotation.data.pitch, MAX_ROTATION),
                .yawRotation = encodeDouble(rotation.data.yaw, MAX_ROTATION),
                .systemState = systemState,
                .recoveryState = status.currentRecoveryState,
                .errorCounter = errorCounter,
                .parity = {0},
            },
    };
    static_assert(sizeof(telemetry.payload) - sizeof(telemetry.payload.parity) == 13,
        "The payload data must be 13 bytes big.");
    static_assert(sizeof(telemetry.payload.parity) == 3,
        "The payload parity data must be 3 bytes big.");
    calculateParity16_13(&telemetry.payload, telemetry.payload.parity);
    static_assert(sizeof(telemetry.platform) - sizeof(telemetry.platform.parity) == 28,
        "The platform data must be 28 bytes big.");
    static_assert(sizeof(telemetry.platform.parity) == 4,
        "The platform parity data must be 4 bytes big.");
    calculateParity32_28(&telemetry.platform, telemetry.platform.parity);
    static_assert(sizeof(telemetry) < 50U, "The Iridium telemetry must be less then 50 bytes");
    if (!sendIridiumMessage(&telemetry, sizeof(telemetry))) {
        printError("Failed to send Iridium message");
    }
    sendTelemetryDataRecovery(time, gpsTime, longitude, latitude, gpsAltitude,
        status.currentRecoveryState);
}

/**
 * Deploy the parachute, if all conditions are fulfilled.
 *
 * @return Whether or not the parachute was deployed.
 */
static bool deployParachute(void) {
    bool isInFlight = false;
    if (!get_flightMode(&isInFlight)) {
        printError("Parachute pyro conditions failed: Failed to retrieve flight mode from flash");
        return false;
    }
    if (!isInFlight || getPin(EXT_POWER_CONNECTED_PIN) != LOW ||
        getPin(TESTING_JUMPER_PIN) != LOW) {
        printError("Parachute pyro conditions unfulfilled... not firing");
        return false;
    }
    printDebug("Parachute pyro conditions fulfilled");
    doDeploymentWithoutChecks();
    return true;
}

void doDeploymentWithoutChecks(void) {
    print("Firing pyro...");
    bool started = true;
    if (!set_parachuteDeploymentWasStarted(&started)) {
        printError("Failed to set parachute deployment started state in flash");
    }

    setPin(RECOVERY_CUTTER_TRIGGER_PIN, HIGH);
    for (int waitingTime = 1000; waitingTime > 0; waitingTime -= (int) THREAD_RECOVERY_PERIOD.ms) {
        osDelay(THREAD_RECOVERY_PERIOD.ms);
        refreshWatchdogTimer();
    }
    setPin(RECOVERY_CUTTER_TRIGGER_PIN, LOW);

    // TODO checks to see if vertical speed is decreasing???

    bool parachuteOpened = true;
    if (!set_parachuteIsDeployed(&parachuteOpened)) {
        printError("Failed to set parachute deployment state in flash");
    }
    print("Pyro fire complete");
}

/**
 * Transition to a recovery state.
 *
 * @param state The desired state of the recovery system.
 */
static void setRecoveryState(RecoveryState state) {
    switch (state) {
    default:
    case RECOVERY_START:
        break;
    case RECOVERY_FLIGHT:
        status.consecutiveParachuteDeploySensorDuration = ms_t(0);
        status.ejectionTime = getTimeSinceBoot();
        break;
    case RECOVERY_COMMANDED:
        requestStateChange(STATE_RECOVERY);
        break;
    case RECOVERY_DESCENDING:
        requestStateChange(STATE_RECOVERY);
        status.landedStablePressureDuration = ms_t(0);
        status.landedStableAttitudeDuration = ms_t(0);
        break;
    case RECOVERY_LANDED:
        requestStateChange(STATE_RECOVERY);
        status.landedTime = getTimeSinceBoot();
        disablePayloadVideoRecording();
        break;
    case RECOVERY_STAYING_ALIVE:
        requestStateChange(STATE_RECOVERY);
        powerOffPayload();
        break;
    }
    status.currentRecoveryState = state;
    status.requestedRecoveryState = state;
}

bool resetRecovery(void) {
    uint8_t result = true;
    if (!beginFlashBatchAccess()) {
        printError("Failed to start flash batch access");
        result = false;
    } else {
        bool resetValue = false;
        setBatch_parachuteDeploymentWasStarted(&resetValue);
        setBatch_parachuteIsDeployed(&resetValue);
        setBatch_haveLanded(&resetValue);
        endFlashBatchAccess();
    }
    status.requestedRecoveryState = RECOVERY_START;
    return result;
}

void initRecovery(void) {
    // Do not use os functions (like osDelay etc.) in initialization
    status.currentRecoveryState = RECOVERY_START;
    status.requestedRecoveryState = RECOVERY_START;
    status.consecutiveParachuteDeploySensorDuration = ms_t(0);
    status.landedStablePressureDuration = ms_t(0);
    status.landedStableAttitudeDuration = ms_t(0);
    status.ejectionTime = ms_t(0);
    status.landedTime = ms_t(0);

    setRecoveryState(RECOVERY_START);
    if (getPin(EXT_POWER_CONNECTED_PIN) == LOW) {
        setRecoveryState(RECOVERY_FLIGHT);
    }
    bool parachuteDeploymentWasStarted = false;
    bool parachuteIsDeployed = false;
    bool haveLanded = false;
    if (!beginFlashBatchAccess()) {
        printError("Failed to start flash batch access");
    } else {
        parachuteDeploymentWasStarted = getBatch_parachuteDeploymentWasStarted();
        parachuteIsDeployed = getBatch_parachuteIsDeployed();
        haveLanded = getBatch_haveLanded();
        endFlashBatchAccess();
    }
    if (parachuteDeploymentWasStarted) {
        setRecoveryState(RECOVERY_COMMANDED);
    }
    if (parachuteIsDeployed) {
        setRecoveryState(RECOVERY_DESCENDING);
    }
    if (haveLanded) {
        setRecoveryState(RECOVERY_LANDED);
    }
    if (status.currentRecoveryState == RECOVERY_COMMANDED) {
        // TODO check vertical speed?, immediately try to fire again?, check altitude?
    }
}

void requestRecoveryState(RecoveryState state) {
    status.requestedRecoveryState = state;
}

/**
 * Determine if the pressure sensor is working.
 *
 * @param pressure The current pressure read by the pressure sensor.
 * @param config The experiment configuration.
 * @param wasWorking Whether the pressure sensor was considered working before.
 *                   This will get updated, if the pressure sensor is determined as not working.
 * @return Whether the pressure sensor is considered working.
 */
static bool isPressureSensorWorking(const bar_t* pressure, const Configuration* config,
    bool* wasWorking) {
    if (*wasWorking) {
        if (pressure->bar >= config->altimeterMinPossibleValue.bar &&
            pressure->bar <= config->altimeterMaxPossibleValue.bar) {
            return true;
        }
        print("Warning: Pressure sensor is no longer considered working: "
              "Current pressure is out of limits: %f bar",
            pressure->bar);
        *wasWorking = false;
    }
    return false;
}

noreturn void StartRecoveryTask(void* argument) {
    UNUSED(argument);
    ms_t gpsInitTimer = ms_t(0);
    bar_t lastPressure = bar_t(0);
    meter_t gpsAltitude = meter_t(0);
    ms_t lastTelemetryTime = ms_t(0);
    const Configuration* config = getConfig();
    bool pressureSensorIsWorking = true;
    uint8_t parachuteDeployRetryCounter = 0;
    while (true) {
        ms_t tick = getTimeSinceBoot();
#if GPS_ENABLED
        if (gpsInitTimer.ms <= tick.ms) {
            if (gpsConfigurationCompleted()) {
                gpsInitTimer = ms_t(UINT32_MAX); // Stop trying to configure the GPS
            } else {
                if (!configureGps()) {
                    printError("GPS configuration failed");
                }
                gpsInitTimer = ms_t(tick.ms + config->GPSFailedInitWaitTime.ms);
            }
        }
#endif /* GPS_ENABLED */
        bar_t currentPressure = readPressure();
        volt_t ffuVoltage = readBatteryLevel();
        if (status.requestedRecoveryState != status.currentRecoveryState) {
            setRecoveryState(status.requestedRecoveryState);
        }
        bool isExternalPowerConnected = getPin(EXT_POWER_CONNECTED_PIN) == HIGH;
        switch (status.currentRecoveryState) {
        default: // In case the state gets corrupted somehow
            setRecoveryState(RECOVERY_START);
        case RECOVERY_START:
            if (!isExternalPowerConnected) {
                setRecoveryState(RECOVERY_FLIGHT);
            }
            break;
        case RECOVERY_FLIGHT:
            if (isExternalPowerConnected) {
                setRecoveryState(RECOVERY_START);
                break;
            }
            get_gpsAltitude(&gpsAltitude);
            if (isPressureSensorWorking(&currentPressure, config, &pressureSensorIsWorking)) {
                if (currentPressure.bar >= config->parachuteTriggerPressure.bar) {
                    // Make sure that not a one time anomalous reading
                    // and actually decreasing height
                    status.consecutiveParachuteDeploySensorDuration.ms += THREAD_RECOVERY_PERIOD.ms;
                    if (status.consecutiveParachuteDeploySensorDuration.ms >=
                        config->requiredConsecutiveParachuteDeploySensorTime.ms) {
                        print("Triggering Parachute deployment because of pressure");
                        setRecoveryState(RECOVERY_COMMANDED);
                    }
                } else {
                    status.consecutiveParachuteDeploySensorDuration = ms_t(0);
                }
            } else if (gpsAltitude.meter != 0) { // 0 = no Data, we don't land at sea level
                // Use the GPS values only when the altimeter isn't working
                // and GPS data was received
                if (gpsAltitude.meter < config->parachuteTriggerAltitude.meter) {
                    status.consecutiveParachuteDeploySensorDuration.ms += THREAD_RECOVERY_PERIOD.ms;
                    if (++status.consecutiveParachuteDeploySensorDuration.ms >=
                        config->requiredConsecutiveParachuteDeploySensorTime.ms) {
                        print("Triggering Parachute deployment because of GPS attitude");
                        setRecoveryState(RECOVERY_COMMANDED);
                    }
                } else {
                    status.consecutiveParachuteDeploySensorDuration = ms_t(0);
                }
            }

            // Ejection timer trigger
            if (tick.ms - status.ejectionTime.ms > config->parachuteDeployTriggerTime.ms) {
                print("Triggering Parachute deployment because the recovery timer expired");
                setRecoveryState(RECOVERY_COMMANDED);
            } else if (status.currentRecoveryState != RECOVERY_COMMANDED) {
                break;
            } // Fall through if we decided to trigger parachute deployment
        case RECOVERY_COMMANDED: {
            if (isExternalPowerConnected) {
                setRecoveryState(RECOVERY_START);
                break;
            }
            requestStateChange(STATE_RECOVERY);
            if (tick.ms >= lastTelemetryTime.ms + config->recoveryTelemetrySendPeriod.ms) {
                sendTelemetry(ffuVoltage);
                lastTelemetryTime = tick;
            }
            bool parachuteOpened;
            if (get_parachuteIsDeployed(&parachuteOpened) && parachuteOpened) {
                setRecoveryState(RECOVERY_DESCENDING);
            } else {
                if (!deployParachute()) {
                    if (parachuteDeployRetryCounter++ >= TESTING_PARACHUTE_DEPLOY_RETRIES) {
                        parachuteDeployRetryCounter = 0;
                        if (getPin(TESTING_JUMPER_PIN) == HIGH) {
                            print("Switching to DESCENDING state because testing mode is active");
                            setRecoveryState(RECOVERY_DESCENDING);
                        }
                    }
                } else {
                    setRecoveryState(RECOVERY_DESCENDING);
                }
            }
            break;
        }
        case RECOVERY_DESCENDING: {
            if (isExternalPowerConnected) {
                setRecoveryState(RECOVERY_START);
                break;
            }
            requestStateChange(STATE_RECOVERY);
            if (tick.ms >= lastTelemetryTime.ms + config->recoveryTelemetrySendPeriod.ms) {
                sendTelemetry(ffuVoltage);
                lastTelemetryTime = tick;
            }
            meter_t newGpsAltitude;
            bool haveLanded = false;
            get_gpsAltitude(&newGpsAltitude);
            if (isPressureSensorWorking(&currentPressure, config, &pressureSensorIsWorking)) {
                if (currentPressure.bar <= config->landedPressureMin.bar ||
                    fabsf(currentPressure.bar - lastPressure.bar) >
                        config->landedPressureMaxChange.bar) {
                    status.landedStablePressureDuration = ms_t(0);
                } else {
                    status.landedStablePressureDuration.ms += THREAD_RECOVERY_PERIOD.ms;
                    if (status.landedStablePressureDuration.ms >=
                        config->stablePressureLandedDuration.ms) {
                        haveLanded = true;
                        print("Landed state triggered by pressure");
                    }
                }
            } else if (newGpsAltitude.meter != 0) {
                if (newGpsAltitude.meter > config->landedMaxAttitude.meter ||
                    fabs(newGpsAltitude.meter - gpsAltitude.meter) >
                        config->landedMaxAttitudeChange.meter) {
                    status.landedStableAttitudeDuration = ms_t(0);
                } else {
                    status.landedStableAttitudeDuration.ms += THREAD_RECOVERY_PERIOD.ms;
                    if (status.landedStableAttitudeDuration.ms >=
                        config->stableAttitudeLandedDuration.ms) {
                        haveLanded = true;
                        print("Landed state triggered by GPS attitude");
                    }
                }
            }
            if (haveLanded) {
                if (!set_haveLanded(&haveLanded)) {
                    printError("Failed to set landed state in flash");
                }
                setRecoveryState(RECOVERY_LANDED);
            }
            gpsAltitude = newGpsAltitude;
            break;
        }
        case RECOVERY_LANDED:
            if (isExternalPowerConnected) {
                setRecoveryState(RECOVERY_START);
                break;
            }
            requestStateChange(STATE_RECOVERY);
            if (tick.ms >= lastTelemetryTime.ms + config->recoveryTelemetryLandedSendPeriod.ms) {
                sendTelemetry(ffuVoltage);
                lastTelemetryTime = tick;
            }
            if (tick.ms > status.landedTime.ms + config->stayAliveTriggerTime.ms) {
                setRecoveryState(RECOVERY_STAYING_ALIVE);
            }
            break;
        case RECOVERY_STAYING_ALIVE:
            if (isExternalPowerConnected) {
                setRecoveryState(RECOVERY_START);
                break;
            }
            requestStateChange(STATE_RECOVERY);
            // TODO wat do?

            if (tick.ms >=
                lastTelemetryTime.ms + config->recoveryTelemetryStayingAliveSendPeriod.ms) {
                sendTelemetry(ffuVoltage);
                lastTelemetryTime = tick;
            }
            break;
        }
        lastPressure = currentPressure;

        refreshWatchdogTimer();
        sleepUntil(ms_t(tick.ms + THREAD_RECOVERY_PERIOD.ms));
    }
}

void sendIridiumDebugTelemetry(void) {
    if (getPin(TESTING_JUMPER_PIN) == HIGH) {
        sendTelemetry(readBatteryLevel());
    }
}
