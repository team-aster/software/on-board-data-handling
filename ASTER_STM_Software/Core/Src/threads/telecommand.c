/**
 * A thread that handles incoming telecommands.
 */

#include "generated/sharedTypes.h"
#include "data/system.h"
#include "data/flash.h"
#include "globals.h"
#include "objects/system.h"
#include "objects/communication/payload.h"
#include "objects/communication/sd_card.h"
#include "objects/motors.h"
#include "objects/communication/imu.h"
#include "threads/threads.h"
#include "threads/telecommand.h"
#include "threads/adcs.h"
#include "threads/recovery.h"
#include "threads/watchdog.h"
#include "threads/control.h"
#include "generated/telecommands.h"


void StartTelecommandTask(void* argument) {
    UNUSED(argument);
    TelecommandData currentCommand;
    while (1) {
        ms_t tick = getTimeSinceBoot();
        // Go through the loop until no more unread telecommands are available or an error occurred.
        while (getNextTelecommand(&currentCommand)) {
            if (!handleTelecommand(&currentCommand)) {
                printError("Unknown command");
            }
        }
        refreshWatchdogTimer();
        sleepUntil(ms_t(tick.ms + THREAD_TELECOMMAND_PERIOD.ms));
    }
}

void handleCommandSetLastState(SystemState state) {
    if (state < SYSTEM_STATE_FIRST || state > SYSTEM_STATE_LAST) {
        printError("Got invalid system state: %u", (unsigned int) state);
        return;
    }
    if (!set_systemState(&state)) {
        printError("Failed to update system state");
    }
}

SystemState handleCommandGetLastState(void) {
    SystemState state = STATE_IDLE;
    if (!get_systemState(&state)) {
        printError("Failed to retrieve current system state");
    }
    return state;
}

void handleCommandNewLogFile(void) {
    switchToNewLogFile();
}

void handleCommandResetChip(uint32_t verificationCode) {
    if (verificationCode == RESET_CHIP_VERIFICATION_CODE) {
        print("Resetting");
#define ACKNOWLEDGE_RESET 1
        softwareReset(POWER_RESTART);
#undef ACKNOWLEDGE_RESET
    } else {
        printError("Invalid reset code");
    }
}

void handleCommandSetCurrentState(SystemState state) {
    if (state < SYSTEM_STATE_FIRST || state > SYSTEM_STATE_LAST) {
        printError("Got invalid system state: %u", (unsigned int) state);
        return;
    }
    requestStateChange(state);
}

SystemState handleCommandGetCurrentState(void) {
    return getCurrentState();
}

void handleCommandSetFlight(uint32_t verificationCode, bool enable) {
    if (verificationCode != SET_FLIGHT_VERIFICATION_CODE) {
        printError("Received flight mode with invalid verification code");
        return;
    }
    if (set_flightMode(&enable)) {
        print("Successfully %s flight mode", enable ? "enabled" : "disabled");
    } else {
        printError("Failed to change flight mode");
    }
}

bool handleCommandGetFlight(void) {
    bool flightMode = false;
    if (!get_flightMode(&flightMode)) {
        printError("Failed to retrieve the current flight mode");
    }
    return flightMode;
}

void handleCommandSetPreLaunchTimer(ms_t timer) {
    setPreLaunchTimer(timer);
}

void handleCommandSetVideoEnabled(bool enabled) {
    enableGoPros(enabled);
}

void handleCommandResetRecovery(void) {
    if (!resetRecovery()) {
        printError("Failed to reset the recovery thread");
    }
}

void handleCommandMotorEnabled(bool enabled) {
    if (enabled) {
        setMotors(rpm_t(0), rpm_t(0), rpm_t(0));
        if (!enableMotors()) {
            printError("Enabling the motors failed");
        }
    } else {
        disableMotors();
    }
}

void handleCommandMotorWarmup(bool enabled) {
    requestAdcsMode(enabled ? MODE_WARM_UP : MODE_OFF);
}

void handleCommandSetPwm(pwm_t value) {
    setDebugPWM(value);
}

void handleCommandSetRpm(rpm_t value) {
    setDebugRPM(value);
}

void handleCommandSetAdcsMode(AdcsMode mode) {
    if (mode < ADCS_MODE_FIRST || mode > ADCS_MODE_LAST) {
        printError("Got invalid ADCS mode: %u", (unsigned int) mode);
        return;
    }
    requestAdcsMode(mode);
}

void handleCommandDeployParachute(void) {
    doDeploymentWithoutChecks();
}

void handleCommandSendIridiumMessage(void) {
#if IRIDIUM_ENABLED
    sendIridiumDebugTelemetry();
#else
    printError("Iridium support is disabled");
#endif /* IRIDIUM_ENABLED */
}

bool handleCommandCalibrateImu(uint8_t imu) {
    IMU imuDevice = imu;
    if (imuDevice > IMU2) {
        printError("Invalid IMU given: %u", (unsigned) imu);
        return false;
    }
    return calibrate(imuDevice);
}

void handleCommandSetPayloadVideo(bool enabled) {
    if (enabled) {
        enablePayloadVideoRecording();
    } else {
        disablePayloadVideoRecording();
    }
}

void handleCommandSetRecoveryState(RecoveryState state) {
    if (state < RECOVERY_STATE_FIRST || state > RECOVERY_STATE_LAST) {
        printError("Got invalid recovery state: %u", (unsigned int) state);
        return;
    }
    requestRecoveryState(state);
}
