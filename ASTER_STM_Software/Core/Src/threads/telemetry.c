/**
 * The purpose of the TELEMETRY object is to provide the threads to package the data into
 * data packets and send them to RXSM, payload and/or debug.
 */

#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include "threads/threads.h"
#include "threads/telemetry.h"
#include "globals.h"
#include "objects/system.h"
#include "objects/communication/rxsm.h"
#include "objects/communication/payload.h"
#include "objects/communication/sd_card.h"
#include "threads/watchdog.h"
#include "objects/gpio.h"
#include "threads/control.h"
#include "data/adcs.h"
#include "data/sensor.h"
#include "generated/telemetry.h"
#include "printf.h"


/** Number of message that can be buffered. */
#define MESSAGE_QUEUE_LENGTH 8

/**
 * The minimum number of required free buffer spaces
 * for a failed message to get put back in the queue.
 */
#define MESSAGE_REQUEUE_THRESHOLD 3

/** The amount of tries to delete the first message from the queue to add a new message. */
#define MAX_MESSAGE_ENQUEUE_TRIES 3

/** The maximum size of one telemetry message in bytes. */
#define MAX_TELEMETRY_SIZE 300

/** Indicates that a telemetry was send via all the intended transmission channels. */
#define SEND_COMPLETE 0U

/** A flag that lets the  telemetry thread know that a telemetry send can be attempted. */
#define TELEMETRY_WRITE_READY_FLAG 0x00000001b

/** All possible telemetry states. */
#define TELEMETRY_ENABLE_STATE_MASK (RXSM_TELEMETRY | PAYLOAD_TELEMETRY | SD_CARD_TELEMETRY)

/** Which telemetry transmission channels are enabled on startup. */
#define INITIAL_TELEMETRY_ENABLE_STATE (RXSM_TELEMETRY | PAYLOAD_TELEMETRY | SD_CARD_TELEMETRY)

/** A queue for messages which can't be send immediately. */
static osMessageQueueId_t messageQueue = NULL;

/** An entry in the buffered message queue. */
typedef struct {
    /** Flags that indicate where the message needs to be send. */
    uint8_t sendChannels;
    /** The size in bytes of the message. */
    size_t size;
    /** The actual message. */
    uint8_t data[MAX_TELEMETRY_SIZE];
} MessageQueueEntry;

/** The current enable state of the telemetry transmission channels. */
static volatile uint8_t telemetryEnableState = INITIAL_TELEMETRY_ENABLE_STATE;

/** The amount of error messages send since boot. */
static volatile uint8_t errorCounter = 0;


/**
 * Send the telemetry packages of the current system state.
 */
static void sendTelemetry() {
    if (osSemaphoreAcquire(sensorSemaphore, 10U) != osOK) {
        printError("Failed to send DATA 1-3 telemetry: Could not acquire sensorSemaphore");
    } else {
        ms_t time = getTimeSinceBoot();

        // Get all temperature values
        celsius_t processorTemperature = getBatch_processorTemperature();
        celsius_t batteryTemperature = getBatch_batteryTemperature();
        celsius_t pduPcbTemperature = getBatch_pduPcbTemperature();
        celsius_t motorControllerTemperature = getBatch_motorControllerTemperature();
        celsius_t motorBracketTemperature = getBatch_motorBracketTemperature();
        celsius_t retentionWallTemperature = getBatch_retentionWallTemperature();
        celsius_t recoveryWallTemperature = getBatch_recoveryWallTemperature();
        bar_t pressure = getBatch_pressure();

        Vec3D motorCurrentsPrecise = getBatch_motorCurrent();
        Vec3F motorCurrents = {
            .roll = (float) motorCurrentsPrecise.roll,
            .pitch = (float) motorCurrentsPrecise.pitch,
            .yaw = (float) motorCurrentsPrecise.yaw,
        };
        // Get IMU data
        Vec3D primaryImuAcceleration = getBatch_primaryImuAccelerometer();
        Vec3D primaryImuMagneticForce = getBatch_primaryImuMagnetometer();
        celsiusInt8_t primaryImuTemperature = getBatch_primaryImuTemperature();
        Vec3D secondaryImuAcceleration = getBatch_secondaryImuAccelerometer();
        Vec3D secondaryImuMagneticForce = getBatch_secondaryImuMagnetometer();
        celsiusInt8_t secondaryImuTemperature = getBatch_secondaryImuTemperature();
        Vec3D secondaryImuRotationPrecise = getBatch_secondaryImuRotation();
        Vec3F secondaryImuRotation = {
            .roll = (float) secondaryImuRotationPrecise.roll,
            .pitch = (float) secondaryImuRotationPrecise.pitch,
            .yaw = (float) secondaryImuRotationPrecise.yaw,
        };
        Quaternion orientation = getBatch_imu2Quaternions();
        voltFloat_t batteryLevel = voltFloat_t((float) getBatch_batteryLevel().volt);
        osSemaphoreRelease(sensorSemaphore);

        // Compose messages and send the telemetry data
        sendTelemetryData1(time, processorTemperature, batteryTemperature, pduPcbTemperature,
            motorControllerTemperature, motorBracketTemperature, retentionWallTemperature,
            recoveryWallTemperature, primaryImuTemperature, secondaryImuTemperature, &motorCurrents,
            &secondaryImuRotation);
        sendTelemetryData2(time, mps2_t((float) primaryImuAcceleration.x),
            mps2_t((float) primaryImuAcceleration.y), mps2_t((float) primaryImuAcceleration.z),
            microTesla_t((float) primaryImuMagneticForce.x),
            microTesla_t((float) primaryImuMagneticForce.y),
            microTesla_t((float) primaryImuMagneticForce.z),
            mps2_t((float) secondaryImuAcceleration.x), mps2_t((float) secondaryImuAcceleration.y),
            mps2_t((float) secondaryImuAcceleration.z),
            microTesla_t((float) secondaryImuMagneticForce.x),
            microTesla_t((float) secondaryImuMagneticForce.y),
            microTesla_t((float) secondaryImuMagneticForce.z), pressure, getCurrentState());
        sendTelemetryData3(time, &orientation, batteryLevel);
    }

    size_t numRotationMeasurements = get_rotationMeasurements(NULL, 0);
    if (numRotationMeasurements > 0) {
        numRotationMeasurements =
            numRotationMeasurements > UINT_MAX ? UINT_MAX : numRotationMeasurements;
        Vec3FWithTime rotationMeasurements[numRotationMeasurements];
        get_rotationMeasurements(rotationMeasurements, numRotationMeasurements);
        sendTelemetryDataRotationSpeeds(numRotationMeasurements, rotationMeasurements);
    }

    size_t numMotorSpeeds = get_motorSpeeds(NULL, 0);
    if (numMotorSpeeds > 0) {
        numMotorSpeeds = numMotorSpeeds > UINT_MAX ? UINT_MAX : numMotorSpeeds;
        Vec3FWithTime motorSpeeds[numMotorSpeeds];
        get_motorSpeeds(motorSpeeds, numMotorSpeeds);
        sendTelemetryDataMotorSpeeds(numMotorSpeeds, motorSpeeds);
    }

    size_t numDemandedMotorSpeeds = get_demandedMotorSpeeds(NULL, 0);
    if (numDemandedMotorSpeeds > 0) {
        numDemandedMotorSpeeds =
            numDemandedMotorSpeeds > UINT_MAX ? UINT_MAX : numDemandedMotorSpeeds;
        Vec3Int32WithTime demandedMotorSpeeds[numDemandedMotorSpeeds];
        get_demandedMotorSpeeds(demandedMotorSpeeds, numDemandedMotorSpeeds);
        sendTelemetryDataSetPoints(numDemandedMotorSpeeds, demandedMotorSpeeds);
    }
}

/**
 * Try to write a telemetry package via the given channels.
 *
 * @param data The telemetry data to write.
 * @param dataLength The size of the data in bytes.
 * @param sendChannels The channels to write the data to.
 * @return For which channels the write failed.
 */
static uint8_t tryWriteTelemetry(const void* data, size_t dataLength, uint8_t sendChannels) {
    uint8_t failedSendFlags = SEND_COMPLETE;
    if (IS_SET(sendChannels, RXSM_TELEMETRY) && !writeToRXSM(data, dataLength)) {
        failedSendFlags |= RXSM_TELEMETRY;
    }
    if (IS_SET(sendChannels, SD_CARD_TELEMETRY) && !writeToSdCards(data, dataLength) &&
        (haveInitializedSdCards() || !isSdCardSubsystemInitialized())) {
        failedSendFlags |= SD_CARD_TELEMETRY;
    }
    if (IS_SET(sendChannels, PAYLOAD_TELEMETRY) && !writeToPayload(data, dataLength)) {
        failedSendFlags |= PAYLOAD_TELEMETRY;
    }
    return failedSendFlags;
}

void signalTelemetryWriteReady(void) {
    osThreadFlagsSet(telemetryTaskHandle, TELEMETRY_WRITE_READY_FLAG);
}

/**
 * Try to resend the oldest queued message.
 */
static void resendOldestMessageFromMessageQueue(void) {
    MessageQueueEntry message;
    if (osMessageQueueGet(messageQueue, &message, NULL, 0U) != osOK) {
        return;
    }
    uint8_t sendResult =
        tryWriteTelemetry(&message.data, message.size, message.sendChannels & telemetryEnableState);
    if (sendResult != SEND_COMPLETE &&
        osMessageQueueGetSpace(messageQueue) > MESSAGE_REQUEUE_THRESHOLD) {
        message.sendChannels = sendResult;
        osMessageQueuePut(messageQueue, &message, 0U, 0U);
    }
}

/**
 * Check the message queue for unsent messages and send them over
 * the channels which they have not been send yet.
 * Re-queues the message if if fails to send and there is enough space.
 */
static void printFromMessageQueue(void) {
    uint32_t initialMessageCount = osMessageQueueGetCount(messageQueue);
    while (initialMessageCount-- > 0) {
        resendOldestMessageFromMessageQueue();
    }
}

bool writeTelemetry(const void* data, size_t dataLength) {
    uint8_t sendFlags = tryWriteTelemetry(data, dataLength, telemetryEnableState);
    if (sendFlags != SEND_COMPLETE) {
        if (dataLength > MAX_TELEMETRY_SIZE) {
            return false;
        }
        MessageQueueEntry entry = {.sendChannels = sendFlags, .size = dataLength};
        memcpy(entry.data, data, dataLength);
        uint8_t try = 0;
        while (osMessageQueuePut(messageQueue, &entry, 0U, 0U) != osOK) {
            if (try++ > MAX_MESSAGE_ENQUEUE_TRIES) {
                break;
            }
            // New messages are always considered more important.
            // Remove the oldest message to make space.
            MessageQueueEntry removedEntry;
            osMessageQueueGet(messageQueue, &removedEntry, NULL, 0U);
        }
    }
    return true;
}

bool initPrintSubsystem(void) {
    errorCounter = 0;
    telemetryEnableState = INITIAL_TELEMETRY_ENABLE_STATE;
#if !SD_CARDS_ENABLED
    setTelemetryEnabled(SD_CARD_TELEMETRY, false);
#endif /* !SD_CARDS_ENABLED */
    /* Initializing message queue for blocked telemetry writes (such as from ISR) */
    messageQueue = osMessageQueueNew(MESSAGE_QUEUE_LENGTH, sizeof(MessageQueueEntry), NULL);
    return messageQueue != NULL;
}

noreturn void StartTelemetryTask(void* argument) {
    UNUSED(argument);
    const Configuration* config = getConfig();
    ms_t nextSdCardCheck = ms_t(0);
    // Delay in order to get some sensor readings before starting telemetry writes
    osDelay(config->initialTelemetryDelay.ms);
    refreshWatchdogTimer();
    while (1) {
        ms_t tick = getTimeSinceBoot();
        if (tick.ms >= nextSdCardCheck.ms) {
            reinitializeErroredSdCards();
            nextSdCardCheck = ms_t(tick.ms + config->SDCardCheckInterval.ms);
        }
#if TELEMETRY_ENABLED == 1
        sendTelemetry();
#endif
        flushLogFiles();
        printFromMessageQueue();

        refreshWatchdogTimer();
        osThreadFlagsClear(TELEMETRY_WRITE_READY_FLAG);
        int32_t waitTime;
        // Sleep until a new period starts. Wake up in between, if the queued telemetry can be sent.
        while ((waitTime = (int32_t) (((int64_t) tick.ms + THREAD_TELEMETRY_PERIOD.ms) -
                                      getTimeSinceBoot().ms)) > 0 &&
               osThreadFlagsWait(waitTime, osFlagsWaitAny, TELEMETRY_WRITE_READY_FLAG) >= osOK) {
            resendOldestMessageFromMessageQueue();
        }
    }
}

void setTelemetryEnabled(TelemetryTransmissionChannel type, bool enabled) {
    type &= TELEMETRY_ENABLE_STATE_MASK;
    if (enabled) {
        telemetryEnableState |= type;
    } else {
        telemetryEnableState &= ~type;
    }
}

/**
 * Send a message as telemetry.
 *
 * @param type The message type.
 * @param format The message format.
 * @param args Arguments for creating the message.
 */
static void sendMessage(MessageType type, const char* format, va_list args) {
    char buffer[MAX_TELEMETRY_SIZE - 32];
    size_t length = vsnprintf(buffer, ARRAY_SIZE(buffer), format, args);
    sendTelemetryMessage(getTimeSinceBoot(), type, length, buffer);
}

void print(const char* format, ...) {
    va_list args;
    va_start(args, format);
    sendMessage(INFO_MESSAGE, format, args);
    va_end(args);
}

void printDebug(const char* format, ...) {
    if (getPin(TESTING_JUMPER_PIN) != HIGH) {
        return;
    }
    va_list args;
    va_start(args, format);
    sendMessage(DEBUG_MESSAGE, format, args);
    va_end(args);
}

void printError(const char* format, ...) {
    errorCounter += 1;
    va_list args;
    va_start(args, format);
    sendMessage(ERROR_MESSAGE, format, args);
    va_end(args);
}

void printCriticalError(const char* format, ...) {
    setPin(ERROR_INDICATOR_LED_PIN, HIGH);
    errorCounter += 1;
    va_list args;
    va_start(args, format);
    sendMessage(CRITICAL_ERROR_MESSAGE, format, args);
    va_end(args);
}

uint8_t getErrorCounter(void) {
    return errorCounter;
}
