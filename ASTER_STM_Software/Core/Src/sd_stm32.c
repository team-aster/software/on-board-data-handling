/*-----------------------------------------------------------------------*/
/* MMC/SDC (in SPI mode) control module  (C)ChaN, 2007                   */
/*-----------------------------------------------------------------------*/
/* Only rcvr_spi(), xmit_spi(), disk_timerproc() and some macros         */
/* are platform dependent.                                               */
/*-----------------------------------------------------------------------*/
#define TRUE  1
#define FALSE 0
#define bool BYTE

#include "stm32f7xx_hal.h"

#include "diskio.h"
#include "objects/gpio.h"
#include <stdint.h>
#include "ff.h"

#include "sd_stm32.h"

extern SPI_HandleTypeDef hspi2; //SD card 2
extern SPI_HandleTypeDef hspi4; //SD card 1

/* Definitions for MMC/SDC command */
#define CMD0    (0x40+0)    /* GO_IDLE_STATE */
#define CMD1    (0x40+1)    /* SEND_OP_COND */
#define CMD8    (0x40+8)    /* SEND_IF_COND */
#define CMD9    (0x40+9)    /* SEND_CSD */
#define CMD10    (0x40+10)    /* SEND_CID */
#define CMD12    (0x40+12)    /* STOP_TRANSMISSION */
#define CMD16    (0x40+16)    /* SET_BLOCKLEN */
#define CMD17    (0x40+17)    /* READ_SINGLE_BLOCK */
#define CMD18    (0x40+18)    /* READ_MULTIPLE_BLOCK */
#define CMD23    (0x40+23)    /* SET_BLOCK_COUNT */
#define CMD24    (0x40+24)    /* WRITE_BLOCK */
#define CMD25    (0x40+25)    /* WRITE_MULTIPLE_BLOCK */
#define CMD41    (0x40+41)    /* SEND_OP_COND (ACMD) */
#define CMD55    (0x40+55)    /* APP_CMD */
#define CMD58    (0x40+58)    /* READ_OCR */

/*--------------------------------------------------------------------------

 Module Private Functions

 ---------------------------------------------------------------------------*/

static volatile DSTATUS Stat[2] = {STA_NOINIT,STA_NOINIT}; /* Disk status */

static volatile BYTE Timer1, Timer1b, Timer2, Timer2b, Timer3; /* 100Hz decrement timer */

static BYTE CardType[2]; /* b0:MMC, b1:SDC, b2:Block addressing */

static BYTE PowerFlag[2] = {0}; /* indicates if "power" is on */

static
void SELECT(BYTE drv)
{
	switch(drv){
		case 0:
            setPin(SDCARD1_NSS_PIN, LOW);
			break;
		case 1:
            setPin(SDCARD2_NSS_PIN, LOW);
			break;
	}

}

static
void DESELECT(BYTE drv)
{
	switch(drv){
		case 0:
            setPin(SDCARD1_NSS_PIN, HIGH);
			break;
		case 1:
            setPin(SDCARD2_NSS_PIN, HIGH);
			break;
		}
}

static
void xmit_spi(BYTE Data, BYTE drv)
{

	switch(drv){
		case 0:
            while (HAL_SPI_GetState(&hspi4) != HAL_SPI_STATE_READY){}
		    HAL_SPI_Transmit(&hspi4, &Data, 1, 5000);
			break;
		case 1:
			while (HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY);
			HAL_SPI_Transmit(&hspi2, &Data, 1, 5000);
			break;
		default:

			break;
	}
}

static BYTE rcvr_spi(BYTE drv)
{
	unsigned char Dummy, Data;
	Dummy = 0xFF;
	Data = 0;
	switch(drv){
		case 0:
		    while ((HAL_SPI_GetState(&hspi4) != HAL_SPI_STATE_READY));
		    HAL_SPI_TransmitReceive(&hspi4, &Dummy, &Data, 1, 5000);
			break;
		case 1:
		    while ((HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY));
		    HAL_SPI_TransmitReceive(&hspi2, &Dummy, &Data, 1, 5000);

			break;
		default:

			break;
	}

	return Data;
}

static
void rcvr_spi_m(BYTE *dst, BYTE drv) {
	*dst = rcvr_spi(drv);
}

/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------z------------------------------------------*/

static BYTE wait_ready(BYTE drv) {
	BYTE res;

	switch(drv){
		case 0:
		    Timer2 = 10;
		    rcvr_spi(drv);
		    do
		        res = rcvr_spi(drv);
		    while ((res != 0xFF) && Timer2);
			break;
		case 1:
		    Timer2b = 10;
		    rcvr_spi(drv);
		    do
		        res = rcvr_spi(drv);
		    while ((res != 0xFF) && Timer2b);

			break;
		default:

			break;
	}

	return res;
}

/*-----------------------------------------------------------------------*/
/* Power Control  (Platform dependent)                                   */
/*-----------------------------------------------------------------------*/
/* When the target system does not support socket power control, there   */
/* is nothing to do in these functions and chk_power always returns 1.   */

static
void power_on(BYTE drv) {
	unsigned char i, cmd_arg[6];
	unsigned int Count = 0x1FFF;

	DESELECT(drv);

	for (i = 0; i < 10; i++)
		xmit_spi(0xFF,drv);
	
	SELECT(drv);
	cmd_arg[0] = (CMD0 | 0x40);
	cmd_arg[1] = 0;
	cmd_arg[2] = 0;
	cmd_arg[3] = 0;
	cmd_arg[4] = 0;
	cmd_arg[5] = 0x95;

	for (i = 0; i < 6; i++)
		xmit_spi(cmd_arg[i],drv);

	while ((rcvr_spi(drv) != 0x01) && Count)
		Count--;

	DESELECT(drv);
	xmit_spi(0XFF,drv);

	PowerFlag[drv] = 1;
}

static
void power_off(BYTE drv) {
	PowerFlag[drv] = 0;
}

static
int chk_power(BYTE drv) /* Socket power state: 0=off, 1=on */
{
	return PowerFlag[drv];
}

/*-----------------------------------------------------------------------*/
/* Receive a data packet from MMC                                        */
/*-----------------------------------------------------------------------*/

static bool rcvr_datablock(BYTE *buff, /* Data buffer to store received data */
UINT btr, /* Byte count (must be even number) */
BYTE drv	// Drive number
) {
	BYTE token;

	switch(drv){
		case 0:
		    Timer1 = 10;
		    do { /* Wait for data packet in timeout of 100ms */
		        token = rcvr_spi(drv);
		    } while ((token == 0xFF) && Timer1);
		    if (token != 0xFE){
		        return FALSE; /* If not valid data token, return with error */
		    }
			break;
		case 1:
		    Timer1b = 10;
		    do { /* Wait for data packet in timeout of 100ms */
		        token = rcvr_spi(drv);
		    } while ((token == 0xFF) && Timer1b);
		    if (token != 0xFE){
		        return FALSE; /* If not valid data token, return with error */
		    }
			break;
		default:
			return FALSE;
			break;
	}

	do { /* Receive the data block into buffer */
		rcvr_spi_m(buff++, drv);
		rcvr_spi_m(buff++, drv);
	} while (btr -= 2);
	rcvr_spi(drv); /* Discard CRC */
	rcvr_spi(drv);

	return TRUE; /* Return with success */
}

/*-----------------------------------------------------------------------*/
/* Send a data packet to MMC                                             */
/*-----------------------------------------------------------------------*/

#if _READONLY == 0
static bool xmit_datablock(const BYTE *buff, /* 512 byte data block to be transmitted */
BYTE token, /* Data/Stop token */
BYTE drv	//drive number
) {
	BYTE resp, wc;
	uint32_t i = 0;

	if (wait_ready(drv) != 0xFF)
		return FALSE;

	xmit_spi(token,drv); /* Xmit data token */
	if (token != 0xFD) { /* Is data token */
		wc = 0;
		do { /* Xmit the 512 byte data block to MMC */
			xmit_spi(*buff++,drv);
			xmit_spi(*buff++,drv);
		} while (--wc);

		rcvr_spi(drv);
		rcvr_spi(drv);

		while (i <= 64) {
			resp = rcvr_spi(drv); /* Reveive data response */
			if ((resp & 0x1F) == 0x05) /* If not accepted, return with error */
				break;
			i++;
		}
		while (rcvr_spi(drv) == 0)
			;
	}
	if ((resp & 0x1F) == 0x05)
		return TRUE;
	else
		return FALSE;
}
#endif /* _READONLY */

/*-----------------------------------------------------------------------*/
/* Send a command packet to MMC                                          */
/*-----------------------------------------------------------------------*/

static BYTE send_cmd(BYTE cmd, /* Command byte */
DWORD arg, /* Argument */
BYTE drv	// drive number
) {
	BYTE n, res;

	if (wait_ready(drv) != 0xFF)
		return 0xFF;

	/* Send command packet */
	xmit_spi(cmd,drv); /* Command */
	xmit_spi((BYTE) (arg >> 24),drv); /* Argument[31..24] */
	xmit_spi((BYTE) (arg >> 16),drv); /* Argument[23..16] */
	xmit_spi((BYTE) (arg >> 8),drv); /* Argument[15..8] */
	xmit_spi((BYTE) arg,drv); /* Argument[7..0] */
	n = 0;
	if (cmd == CMD0)
		n = 0x95; /* CRC for CMD0(0) */
	if (cmd == CMD8)
		n = 0x87; /* CRC for CMD8(0x1AA) */
	xmit_spi(n,drv);

	/* Receive command response */
	if (cmd == CMD12)
		rcvr_spi(drv); /* Skip a stuff byte when stop reading */
	n = 10; /* Wait for a valid response in timeout of 10 attempts */
	do
		res = rcvr_spi(drv);
	while ((res & 0x80) && --n);

	return res; /* Return with the response value */
}

/*--------------------------------------------------------------------------

 Public Functions

 ---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE drv /* Physical drive nmuber (0) */
) {
	BYTE n, ty, ocr[4];

//	if (drv)
//		return STA_NOINIT; /* Supports only single drive */
//	if (Stat & STA_NODISK)
//		return Stat; /* No card in the socket */

	power_on(drv); /* Force socket power on */
	//send_initial_clock_train();

	SELECT(drv); /* CS = L */
	ty = 0;
	if (send_cmd(CMD0, 0, drv) == 1) { /* Enter Idle state */
		Timer1 = 100; /* Initialization timeout of 1000 msec */
		if (send_cmd(CMD8, 0x1AA, drv) == 1) { /* SDC Ver2+ */
			for (n = 0; n < 4; n++)
				ocr[n] = rcvr_spi(drv);
			if (ocr[2] == 0x01 && ocr[3] == 0xAA) { /* The card can work at vdd range of 2.7-3.6V */
				do {
					if (send_cmd(CMD55, 0, drv) <= 1
							&& send_cmd(CMD41, 1UL << 30, drv) == 0)
						break; /* ACMD41 with HCS bit */
				} while (Timer1);
				if (Timer1 && send_cmd(CMD58, 0, drv) == 0) { /* Check CCS bit */
					for (n = 0; n < 4; n++)
						ocr[n] = rcvr_spi(drv);
					ty = (ocr[0] & 0x40) ? 6 : 2;
				}
			}
		} else { /* SDC Ver1 or MMC */
			ty = (send_cmd(CMD55, 0, drv) <= 1 && send_cmd(CMD41, 0, drv) <= 1) ? 2 : 1; /* SDC : MMC */
			do {
				if (ty == 2) {
					if (send_cmd(CMD55, 0, drv) <= 1 && send_cmd(CMD41, 0, drv) == 0)
						break; /* ACMD41 */
				} else {
					if (send_cmd(CMD1, 0, drv) == 0)
						break; /* CMD1 */
				}
			} while (Timer1);
			if (!Timer1 || send_cmd(CMD16, 512, drv) != 0) /* Select R/W block length */
				ty = 0;
		}
	}
	CardType[drv] = ty;
	DESELECT(drv); /* CS = H */
	rcvr_spi(drv); /* Idle (Release DO) */

	if (ty) /* Initialization succeded */
		Stat[drv] &= ~STA_NOINIT; /* Clear STA_NOINIT */
	else
		/* Initialization failed */
		power_off(drv);

	return Stat[drv];
}

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE drv /* Physical drive nmuber (0) */
) {
	if (drv>=2)
		return STA_NOINIT; /* Supports only single drive */
	return Stat[drv];
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv, BYTE* buff, DWORD sector, UINT count) {
	if (pdrv>=2 || !count)
		return RES_PARERR;
	if (Stat[pdrv] & STA_NOINIT)
		return RES_NOTRDY;

	if (!(CardType[pdrv] & 4))
		sector *= 512; /* Convert to byte address if needed */

	SELECT(pdrv); /* CS = L */

	if (count == 1) { /* Single block read */
		if ((send_cmd(CMD17, sector, pdrv) == 0) /* READ_SINGLE_BLOCK */
		&& rcvr_datablock(buff, 512, pdrv))
			count = 0;
	} else { /* Multiple block read */
		if (send_cmd(CMD18, sector, pdrv) == 0) { /* READ_MULTIPLE_BLOCK */
			do {
				if (!rcvr_datablock(buff, 512, pdrv))
					break;
				buff += 512;
			} while (--count);
			send_cmd(CMD12, 0, pdrv); /* STOP_TRANSMISSION */
		}
	}

	DESELECT(pdrv); /* CS = H */
	rcvr_spi(pdrv); /* Idle (Release DO) */

	return count ? RES_ERROR : RES_OK;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _READONLY == 0
DRESULT disk_write(BYTE pdrv, const BYTE* buff, DWORD sector, UINT count) {
	if (pdrv>=2 || !count)
		return RES_PARERR;
	if (Stat[pdrv] & STA_NOINIT)
		return RES_NOTRDY;
	if (Stat[pdrv] & STA_PROTECT)
		return RES_WRPRT;

	if (!(CardType[pdrv] & 4))
		sector *= 512; /* Convert to byte address if needed */

	SELECT(pdrv); /* CS = L */

	if (count == 1) { /* Single block write */
		if ((send_cmd(CMD24, sector, pdrv) == 0) /* WRITE_BLOCK */
		&& xmit_datablock(buff, 0xFE, pdrv))
			count = 0;
	} else { /* Multiple block write */
		if (CardType[pdrv] & 2) {
			send_cmd(CMD55, 0, pdrv);
			send_cmd(CMD23, count, pdrv); /* ACMD23 */
		}
		if (send_cmd(CMD25, sector, pdrv) == 0) { /* WRITE_MULTIPLE_BLOCK */
			do {
				if (!xmit_datablock(buff, 0xFC, pdrv))
					break;
				buff += 512;
			} while (--count);
			if (!xmit_datablock(0, 0xFD, pdrv)) /* STOP_TRAN token */
				count = 1;
		}
	}

	DESELECT(pdrv); /* CS = H */
	rcvr_spi(pdrv); /* Idle (Release DO) */

	return count ? RES_ERROR : RES_OK;
}
#endif /* _READONLY */

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl(BYTE drv, /* Physical drive nmuber (0) */
BYTE ctrl, /* Control code */
void *buff /* Buffer to send/receive control data */
) {
	DRESULT res;
	BYTE n, csd[16], *ptr = buff;
	WORD csize;

	if (drv>=2)
		return RES_PARERR;

	res = RES_ERROR;

	if (ctrl == CTRL_POWER) {
		switch (*ptr) {
		case 0: /* Sub control code == 0 (POWER_OFF) */
			if (chk_power(drv))
				power_off(drv); /* Power off */
			res = RES_OK;
			break;
		case 1: /* Sub control code == 1 (POWER_ON) */
			power_on(drv); /* Power on */
			res = RES_OK;
			break;
		case 2: /* Sub control code == 2 (POWER_GET) */
			*(ptr + 1) = (BYTE) chk_power(drv);
			res = RES_OK;
			break;
		default:
			res = RES_PARERR;
		}
	} else {
		if (Stat[drv] & STA_NOINIT)
			return RES_NOTRDY;

		SELECT(drv); /* CS = L */

		switch (ctrl) {
		case GET_SECTOR_COUNT: /* Get number of sectors on the disk (DWORD) */
			if ((send_cmd(CMD9, 0, drv) == 0) && rcvr_datablock(csd, 16, drv)) {
				if ((csd[0] >> 6) == 1) { /* SDC ver 2.00 */
					csize = csd[9] + ((WORD) csd[8] << 8) + 1;
					*(DWORD*) buff = (DWORD) csize << 10;
				} else { /* MMC or SDC ver 1.XX */
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7)
							+ ((csd[9] & 3) << 1) + 2;
					csize = (csd[8] >> 6) + ((WORD) csd[7] << 2)
							+ ((WORD) (csd[6] & 3) << 10) + 1;
					*(DWORD*) buff = (DWORD) csize << (n - 9);
				}
				res = RES_OK;
			}
			break;

		case GET_SECTOR_SIZE: /* Get sectors on the disk (WORD) */
			*(WORD*) buff = 512;
			res = RES_OK;
			break;

		case CTRL_SYNC: /* Make sure that data has been written */
			if (wait_ready(drv) == 0xFF)
				res = RES_OK;
			break;

		case MMC_GET_CSD: /* Receive CSD as a data block (16 bytes) */
			if (send_cmd(CMD9, 0, drv) == 0 /* READ_CSD */
			&& rcvr_datablock(ptr, 16, drv))
				res = RES_OK;
			break;

		case MMC_GET_CID: /* Receive CID as a data block (16 bytes) */
			if (send_cmd(CMD10, 0, drv) == 0 /* READ_CID */
			&& rcvr_datablock(ptr, 16, drv))
				res = RES_OK;
			break;

		case MMC_GET_OCR: /* Receive OCR as an R3 resp (4 bytes) */
			if (send_cmd(CMD58, 0, drv) == 0) { /* READ_OCR */
				for (n = 0; n < 4; n++)
					*ptr++ = rcvr_spi(drv);
				res = RES_OK;
			}

//        case MMC_GET_TYPE :    /* Get card type flags (1 byte) */
//            *ptr = CardType;
//            res = RES_OK;
//            break;

		default:
			res = RES_PARERR;
		}

		DESELECT(drv); /* CS = H */
		rcvr_spi(drv); /* Idle (Release DO) */
	}

	return res;
}

/*-----------------------------------------------------------------------*/
/* Device Timer Interrupt Procedure  (Platform dependent)                */
/*-----------------------------------------------------------------------*/
/* This function must be called in period of 10ms                        */

void disk_timerproc(void) {
//    BYTE n, s;
	BYTE n;

	n = Timer1; /* 100Hz decrement timer */
	if (n)
		Timer1 = --n;
	n = Timer1b;
	if (n)
		Timer1b = --n;
	n = Timer2;
	if (n)
		Timer2 = --n;
	n = Timer3;
	if (n)
		Timer3 = --n;
	n = Timer2b;
	if (n)
		Timer2b = --n;

}

volatile unsigned short int sdcard_timer;

void inline sdcard_systick_timerproc(void) {
	++sdcard_timer;
	if (sdcard_timer >= 10) {
		sdcard_timer = 0;
		disk_timerproc();
	}
}

/*---------------------------------------------------------*/
/* User Provided Timer Function for FatFs module           */
/*---------------------------------------------------------*/
/* This is a real time clock service to be called from     */
/* FatFs module. Any valid time must be returned even if   */
/* the system does not support a real time clock.          */

DWORD get_fattime(void) {

	return ((2021UL - 1980) << 25) // Year = 2021
	| (3UL << 21) // Month = March
			| (12UL << 16) // Day = 12
			| (13U << 11) // Hour = 11
			| (37U << 5) // Min = 38
			| (0U >> 1) // Sec = 0
	;

}

