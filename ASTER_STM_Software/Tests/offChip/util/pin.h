#pragma once

#include <map>
#include <vector>
#include <set>
#include <string>

extern "C" {
#include "objects/gpio.h"
}


class PinMockManager;

typedef enum { UNDEFINED_STATE, HIGH_STATE, LOW_STATE } PinState;
typedef enum { UNDEFINED_MODE, INPUT_MODE, OUTPUT_MODE } PinIoMode;

class PinMock {
    friend PinMockManager;

private:
    explicit PinMock(pin_t pin) noexcept;

public:
    [[nodiscard]] PinState getState() const;
    [[nodiscard]] PinIoMode getIoMode() const;
    void setState(PinState newState);
    [[nodiscard]] std::string name() const;
    const pin_t pin;

private:
    PinState state = UNDEFINED_STATE;
    PinIoMode mode = UNDEFINED_MODE;
    static std::map<GPIO_TypeDef*, std::map<uint16_t, PinMock*>> pinRegistry;
};

class PinMockManager {
public:
    static PinMock* getPin(pin_t pin);
    static std::vector<PinMock*> allPins();
    static void initPin(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_Init);
    static void enablePortClock(GPIO_TypeDef* port);
    static bool isPortClockEnabled(GPIO_TypeDef* port);

    static PinMock testingJumper;
    static PinMock rollMotorEnable;
    static PinMock rollMotorDirection;
    static PinMock pitchMotorEnable;
    static PinMock pitchMotorDirection;
    static PinMock yawMotorEnable;
    static PinMock yawMotorDirection;
    static PinMock launchSignal;
    static PinMock startOfExperimentSignal;
    static PinMock startOfDataStorageSignal;
    static PinMock recoveryCutterTrigger;
    static PinMock iridiumClearToSend;
    static PinMock iridiumReadyToSend;
    static PinMock goProTrigger;
    static PinMock statusLed;
    static PinMock watchdogStatusLed;
    static PinMock errorLed;
    static PinMock status2Led;
    static PinMock batteryEnable;
    static PinMock batteryReset;
    static PinMock externalPowerConnected;
    static PinMock sdCard1Nss;
    static PinMock sdCard2Nss;
    static PinMock iridiumRx;
    static PinMock iridiumTx;

private:
    static std::set<GPIO_TypeDef*> enabledPortClocks;
};
