#include "pin.h"
#include <stdexcept>
#include <cmath>
#include <sstream>


extern "C" {

char getGPIOPortName(GPIO_TypeDef* port) {
    switch ((uintptr_t) port) {
    case GPIOA_BASE:
        return 'A';
    case GPIOB_BASE:
        return 'B';
    case GPIOC_BASE:
        return 'C';
    case GPIOD_BASE:
        return 'D';
    case GPIOE_BASE:
        return 'E';
    case GPIOF_BASE:
        return 'F';
    case GPIOG_BASE:
        return 'G';
    case GPIOH_BASE:
        return 'H';
    case GPIOI_BASE:
        return 'I';
    default:
        return '?';
    }
}

unsigned int getGPIOPinNumber(uint16_t pin) {
    return (int) log2(pin & -pin);
}

void HAL_GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_Init) {
    PinMockManager::initPin(GPIOx, GPIO_Init);
}

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState) {
    for (uint16_t pinNumber = GPIO_PIN_0; pinNumber <= GPIO_PIN_All && pinNumber != 0;
         pinNumber <<= 1U) {
        if (GPIO_Pin & pinNumber) {
            PinMock* pin = PinMockManager::getPin({.number = pinNumber, .port = GPIOx});
            pin->setState(PinState == GPIO_PIN_RESET ? LOW_STATE : HIGH_STATE);
        }
    }
}

GPIO_PinState HAL_GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
    PinState state = PinMockManager::getPin({.number = GPIO_Pin, .port = GPIOx})->getState();
    switch (state) {
    case UNDEFINED_STATE:
        throw std::runtime_error(dynamic_cast<std::stringstream&&>(
            std::stringstream() << "Read from uninitialized pin " << getGPIOPortName(GPIOx)
                                << getGPIOPinNumber(GPIO_Pin))
                                     .str());
    case HIGH_STATE:
        return GPIO_PIN_SET;
    case LOW_STATE:
        return GPIO_PIN_RESET;
    }
    throw std::runtime_error("Invalid state");
}

void HAL_GPIO_TogglePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
    GPIO_PinState state = HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, state == GPIO_PIN_SET ? GPIO_PIN_RESET : GPIO_PIN_SET);
}

void HAL_ENABLE_PORT_CLOCK(GPIO_TypeDef* port) {
    PinMockManager::enablePortClock(port);
}
}

std::map<GPIO_TypeDef*, std::map<uint16_t, PinMock*>> PinMock::pinRegistry = {};

PinMock::PinMock(pin_t pin) noexcept : pin(pin) {
    pinRegistry[pin.port][pin.number] = this;
}

PinState PinMock::getState() const {
    return this->state;
}

PinIoMode PinMock::getIoMode() const {
    return this->mode;
}

void PinMock::setState(PinState newState) {
    this->state = newState;
}

std::string PinMock::name() const {
    std::stringstream nameBuffer;
    nameBuffer << getGPIOPortName(this->pin.port) << getGPIOPinNumber(this->pin.number);
    return nameBuffer.str();
}

PinMock* PinMockManager::getPin(pin_t pin) {
    try {
        return PinMock::pinRegistry.at(pin.port).at(pin.number);
    } catch (std::out_of_range& ignored) {
        throw std::runtime_error(
            dynamic_cast<std::stringstream&&>(
                std::stringstream()
                << "Accessing pin without mock, add a mock to the PinMockManager for pin "
                << getGPIOPortName(pin.port) << getGPIOPinNumber(pin.number))
                .str());
    }
}

std::vector<PinMock*> PinMockManager::allPins() {
    std::vector<PinMock*> pins = {};
    for (const auto& portPins : PinMock::pinRegistry) {
        for (const auto& pin : portPins.second) {
            pins.push_back(pin.second);
        }
    }
    return pins;
}

void PinMockManager::enablePortClock(GPIO_TypeDef* port) {
    enabledPortClocks.emplace(port);
}

bool PinMockManager::isPortClockEnabled(GPIO_TypeDef* port) {
    return enabledPortClocks.find(port) != enabledPortClocks.end();
}

void PinMockManager::initPin(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_Init) {
    for (uint16_t pinNumber = GPIO_PIN_0; pinNumber <= GPIO_PIN_All && pinNumber != 0;
         pinNumber <<= 1U) {
        if (GPIO_Init->Pin & pinNumber) {
            PinMock* pin = PinMockManager::getPin({.number = pinNumber, .port = GPIOx});
            switch (GPIO_Init->Mode) {
            case GPIO_MODE_INPUT:
                pin->mode = INPUT_MODE;
                break;
            case GPIO_MODE_OUTPUT_PP:
            case GPIO_MODE_OUTPUT_OD:
                pin->mode = OUTPUT_MODE;
                break;
            }
        }
    }
}


std::set<GPIO_TypeDef*> PinMockManager::enabledPortClocks = {};

PinMock PinMockManager::testingJumper(TESTING_JUMPER_PIN);
PinMock PinMockManager::rollMotorEnable(ROLL_MOTOR_ENABLE_PIN);
PinMock PinMockManager::rollMotorDirection(ROLL_MOTOR_DIRECTION_PIN);
PinMock PinMockManager::pitchMotorEnable(PITCH_MOTOR_ENABLE_PIN);
PinMock PinMockManager::pitchMotorDirection(PITCH_MOTOR_DIRECTION_PIN);
PinMock PinMockManager::yawMotorEnable(YAW_MOTOR_ENABLE_PIN);
PinMock PinMockManager::yawMotorDirection(YAW_MOTOR_DIRECTION_PIN);
PinMock PinMockManager::launchSignal(LO_SIGNAL_PIN);
PinMock PinMockManager::startOfExperimentSignal(SOE_SIGNAL_PIN);
PinMock PinMockManager::startOfDataStorageSignal(SODS_SIGNAL_PIN);
PinMock PinMockManager::recoveryCutterTrigger(RECOVERY_CUTTER_TRIGGER_PIN);
PinMock PinMockManager::iridiumClearToSend(IRIDIUM_CTS_PIN);
PinMock PinMockManager::iridiumReadyToSend(IRIDIUM_RTS_PIN);
PinMock PinMockManager::goProTrigger(GOPRO_TRIGGER_PIN);
PinMock PinMockManager::statusLed(STATUS_LED_PIN);
PinMock PinMockManager::watchdogStatusLed(WATCHDOG_STATUS_LED_PIN);
PinMock PinMockManager::errorLed(ERROR_INDICATOR_LED_PIN);
PinMock PinMockManager::status2Led(STATUS2_LED_PIN);
PinMock PinMockManager::batteryEnable(BATTERY_ENABLE_PIN);
PinMock PinMockManager::batteryReset(BATTERY_RESET_PIN);
PinMock PinMockManager::externalPowerConnected(EXT_POWER_CONNECTED_PIN);
PinMock PinMockManager::sdCard1Nss(SDCARD1_NSS_PIN);
PinMock PinMockManager::sdCard2Nss(SDCARD2_NSS_PIN);
PinMock PinMockManager::iridiumRx({IRIDIUM_RX_Pin, IRIDIUM_RX_GPIO_Port});
PinMock PinMockManager::iridiumTx({IRIDIUM_TX_Pin, IRIDIUM_TX_GPIO_Port});
