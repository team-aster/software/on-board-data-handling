#pragma once

#include "stubs/overwrites.h"
#include <stm32f7xx_hal.h>
#include <map>
#include <vector>
#include <string>


enum ReceiveMethod {
    None, Interrupt, DMA
};

class UartMockManager;

class UartMock {
    friend UartMockManager;
public:
    ~UartMock();
    HAL_StatusTypeDef transmit(const void* data, size_t size);
    void setErrorState(bool enabled);
    void receive(const void* data, size_t size);
    void receive(const std::string& data);
    void popTransmittedData(std::vector<uint8_t> &buffer);
    std::string popTransmittedData();
    [[nodiscard]] bool isInitialized() const;
    
    [[nodiscard]] ReceiveMethod getReceiveMethod() const;

    [[nodiscard]] size_t getReceiveBufferSize() const;

    [[nodiscard]] bool hasInterruptEnabled(uint32_t interrupt) const;
    
    HAL_StatusTypeDef initializeWithFailure(UART_HandleTypeDef* configuration);
    void initialize(UART_HandleTypeDef* configuration);

    void deInitialize();

    HAL_StatusTypeDef setReceiveMethod(ReceiveMethod method, void* buffer = nullptr, size_t size = 0);

private:
    explicit UartMock(USART_TypeDef* uart) noexcept;
    
    USART_TypeDef* uart;
    UART_HandleTypeDef* config = nullptr;
    bool errorState = false;
    std::vector<uint8_t> transmitBuffer = {};
    ReceiveMethod receiveMethod = ReceiveMethod::None;
    void* receiveBuffer = nullptr;
    size_t receiveBufferSize = 0;
};

class UartMockManager {
    friend UartMock;
public:
    static UartMock* getUart(USART_TypeDef* uart);
    
    static UartMock iridium;
private:
    static std::map<USART_TypeDef*, UartMock*> mocks;
};
