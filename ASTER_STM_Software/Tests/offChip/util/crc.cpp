#include <algorithm>
#include "crc.h"

extern "C" {
#define crcComputer hcrc
extern CRC_HandleTypeDef crcComputer;

HAL_StatusTypeDef HAL_CRC_Init(CRC_HandleTypeDef* crc) {
    return Crc::instance.initialize(crc) ? HAL_OK : HAL_ERROR;
}


uint32_t HAL_CRC_Calculate(CRC_HandleTypeDef* crc, uint32_t buffer[], uint32_t bufferLength) {
    return Crc::instance.doCalculate(crc, buffer, bufferLength);
}
}

Crc Crc::instance(&crcComputer);

Crc::Crc(CRC_HandleTypeDef* crc) noexcept : crc(crc) {
}

void Crc::initialize(size_t newCrcLength, size_t newInputDataAlignment, size_t newInputInversion,
                     size_t newOutputInversion, uint32_t newInitialValue) {
    this->crc->Instance = CRC;
    crcLength = newCrcLength;
    inputDataAlignment = newInputDataAlignment;
    inputInversion = newInputInversion;
    outputInversion = newOutputInversion;
    initialValue = newInitialValue;
    failureMode = false;
    initialized = true;
}

bool Crc::initialize(CRC_HandleTypeDef* handle) {
    if (failureMode) {
        return false;
    }
    if (handle != crc) {
        EXPECT_TRUE(handle == crc)
                    << "The Crc peripheral was initialized with an unexpected crc handle";
        return false;
    }
    switch (handle->Init.CRCLength) {
    case CRC_POLYLENGTH_32B:
        crcLength = 32;
        break;
    case CRC_POLYLENGTH_16B:
        crcLength = 16;
        break;
    case CRC_POLYLENGTH_8B:
        crcLength = 8;
        break;
    case CRC_POLYLENGTH_7B:
        crcLength = 7;
        break;
    default:
        EXPECT_EQ(handle->Init.CRCLength, CRC_POLYLENGTH_32B)
                    << "The Crc peripheral was initialized with an invalid CRCLength";
        return false;
    }
    switch (hcrc.InputDataFormat) {
    case CRC_INPUTDATA_FORMAT_BYTES:
        inputDataAlignment = 1;
        break;
    case CRC_INPUTDATA_FORMAT_HALFWORDS:
        inputDataAlignment = 2;
        break;
    case CRC_INPUTDATA_FORMAT_WORDS:
        inputDataAlignment = 4;
        break;
    default:
        EXPECT_EQ(handle->InputDataFormat, CRC_INPUTDATA_FORMAT_BYTES)
                    << "The Crc peripheral was initialized with an invalid InputDataFormat";
        return false;
    }
    switch (hcrc.Init.InputDataInversionMode) {
    case CRC_INPUTDATA_INVERSION_NONE:
        inputInversion = 0;
        break;
    case CRC_INPUTDATA_INVERSION_BYTE:
        inputInversion = 1;
        break;
    case CRC_INPUTDATA_INVERSION_HALFWORD:
        inputInversion = 2;
        break;
    case CRC_INPUTDATA_INVERSION_WORD:
        inputInversion = 4;
        break;
    default:
        EXPECT_EQ(handle->Init.InputDataInversionMode, CRC_INPUTDATA_INVERSION_NONE)
                    << "The Crc peripheral was initialized with an invalid InputDataInversionMode";
        return false;
    }
    switch (hcrc.Init.OutputDataInversionMode) {
    case CRC_OUTPUTDATA_INVERSION_DISABLE:
        outputInversion = false;
        break;
    case CRC_OUTPUTDATA_INVERSION_ENABLE:
        outputInversion = true;
        break;
    default:
        EXPECT_EQ(handle->Init.OutputDataInversionMode, CRC_OUTPUTDATA_INVERSION_DISABLE)
                    << "The Crc peripheral was initialized with an invalid OutputDataInversionMode";
        return false;
    }
    initialValue = handle->Init.InitValue;
    initialPolynomial = handle->Init.GeneratingPolynomial;
    initialized = true;
    return true;
}

void Crc::deInitialize() {
    crc->Instance = nullptr;
    initialized = false;
}

bool Crc::isInitialized() const {
    return initialized;
}

size_t Crc::getCrcLength() const {
    return crcLength;
}

size_t Crc::getInputDataAlignment() const {
    return inputDataAlignment;
}

size_t Crc::getInputInversion() const {
    return inputInversion;
}

bool Crc::getOutputInversion() const {
    return outputInversion;
}

uint32_t Crc::getInitialValue() const {
    return initialValue;
}

uint32_t Crc::getInitialPolynomial() const {
    return initialPolynomial;
}

void Crc::setFailureMode(bool enabled) {
    failureMode = enabled;
}
CallResolver<uint32_t, const void*, size_t>::ExpectedCall Crc::expectChecksumCall(uint32_t checksum,
                                                                                  const void* bufferArgument, size_t bufferSizeArgument) {
    return calculateChecksumResolver.mock(checksum, bufferArgument, bufferSizeArgument);
}
uint32_t Crc::doCalculate(CRC_HandleTypeDef* crcInstance, uint32_t* buffer, uint32_t bufferLength) {
    if (crcInstance != crc) {
        EXPECT_TRUE(crcInstance != crc) << "HAL_CRC_Calculate was called with an unexpected crc";
        return 0;
    }
    EXPECT_TRUE(initialized) << "HAL_CRC_Calculate was called on an uninitialized crc";
    return calculateChecksumResolver.resolve(buffer, bufferLength);
}
