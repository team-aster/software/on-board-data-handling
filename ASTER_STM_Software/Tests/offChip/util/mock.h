#pragma once

#include <deque>
#include <gtest/gtest.h>
using namespace testing;

template<typename R = void*, typename A1 = void*, typename A2 = void*>
class CallResolver {
public:
    class ExpectedCall {
        friend CallResolver;

    public:
        ~ExpectedCall() {
            EXPECT_TRUE(wasChecked) << "ExpectedCall result was not checked";
            for (auto it = resolver->expectedCalls.begin(); it != resolver->expectedCalls.end();) {
                if (*it == this) {
                    it = resolver->expectedCalls.erase(it);
                } else {
                    it++;
                }
            }
        }

        [[nodiscard]] bool wasCalled() {
            wasChecked = true;
            return hasBeenCalled;
        };

    private:
        R doCall(A1 argument1 = nullptr, A2 argument2 = nullptr) {
            hasBeenCalled = true;
            EXPECT_EQ(arg1, argument1) << "Expected argument 1 for this call to match";
            EXPECT_EQ(arg2, argument2) << "Expected argument 2 for this call to match";
            return result;
        }

        explicit ExpectedCall(CallResolver* resolver, R result = nullptr, A1 arg1 = nullptr,
            A2 arg2 = nullptr)
            : resolver(resolver), result(result), arg1(arg1), arg2(arg2) {
            resolver->expectedCalls.push_back(this);
        };
        CallResolver* resolver;
        bool wasChecked = false;
        bool hasBeenCalled = false;
        R result;
        A1 arg1;
        A2 arg2;
    };

    ExpectedCall mock(R result = nullptr, A1 arg1 = nullptr, A2 arg2 = nullptr) {
        return ExpectedCall(this, result, arg1, arg2);
    }

    R resolve(A1 argument1 = nullptr, A2 argument2 = nullptr) {
        if (expectedCalls.empty()) {
            throw std::runtime_error(
                "Call to CallResolver::resolve without registered ExpectedCall");
        }
        return expectedCalls.front()->doCall(argument1, argument2);
    }

private:
    std::deque<ExpectedCall*> expectedCalls;
};
