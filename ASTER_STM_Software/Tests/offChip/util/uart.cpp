#include <stdexcept>
#include <cstring>
#include <gtest/gtest.h>
#include "uart.h"

extern "C" {
#pragma weak HAL_UARTEx_RxEventCallback

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef* uart, uint16_t size) {
    UNUSED(uart);
    UNUSED(size);
}

USART_TypeDef UART_MEMORY[1] = {0};

HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef* uart, const uint8_t* data, uint16_t size,
    [[maybe_unused]] uint32_t timeout) {
    EXPECT_TRUE(uart != nullptr) << "HAL_UART_Transmit was called with a NULL uart";
    EXPECT_TRUE(data != nullptr || size == 0) << "HAL_UART_Transmit was called with a NULL data";
    UartMock* mock = UartMockManager::getUart(uart->Instance);
    if (mock == nullptr || (data == nullptr && size != 0)) {
        return HAL_ERROR;
    }
    return mock->transmit(data, size);
}

HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef* uart) {
    EXPECT_TRUE(uart != nullptr) << "HAL_UART_Init was called with a NULL uart";
    UartMock* mock = UartMockManager::getUart(uart->Instance);
    if (mock == nullptr) {
        return HAL_ERROR;
    }
    HAL_StatusTypeDef result = mock->initializeWithFailure(uart);
    if (result == HAL_OK) {
        HAL_UART_MspInit(uart);
    }
    return result;
}

HAL_StatusTypeDef HAL_UART_Receive_DMA(UART_HandleTypeDef* uart, uint8_t* data, uint16_t size) {
    EXPECT_TRUE(uart != nullptr) << "HAL_UART_Receive_DMA was called with a NULL uart";
    UartMock* mock = UartMockManager::getUart(uart->Instance);
    if (mock == nullptr) {
        return HAL_ERROR;
    }
    return mock->setReceiveMethod(ReceiveMethod::DMA, data, size);
}

HAL_StatusTypeDef HAL_UARTEx_ReceiveToIdle_DMA(UART_HandleTypeDef* uart, uint8_t* data,
    uint16_t size) {
    EXPECT_TRUE(uart != nullptr) << "HAL_UARTEx_ReceiveToIdle_DMA was called with a NULL uart";
    UartMock* mock = UartMockManager::getUart(uart->Instance);
    if (mock == nullptr) {
        return HAL_ERROR;
    }
    __HAL_UART_ENABLE_IT(uart, UART_IT_IDLE); // Enable idle line interrupt
    return mock->setReceiveMethod(ReceiveMethod::DMA, data, size);
}

HAL_StatusTypeDef HAL_UART_DMAStop(UART_HandleTypeDef* uart) {
    EXPECT_TRUE(uart != nullptr) << "HAL_UART_DMAStop was called with a NULL uart";
    UartMock* mock = UartMockManager::getUart(uart->Instance);
    if (mock == nullptr) {
        return HAL_ERROR;
    }
    EXPECT_EQ(mock->getReceiveMethod(), ReceiveMethod::DMA)
        << "HAL_UART_DMAStop was called on an uart that was not configured for DMA";
    if (mock->getReceiveMethod() != ReceiveMethod::DMA) {
        return HAL_ERROR;
    }
    return mock->setReceiveMethod(ReceiveMethod::None);
}

HAL_StatusTypeDef HAL_DMA_Init(DMA_HandleTypeDef* dma) {
    EXPECT_TRUE(dma != nullptr) << "HAL_DMA_Init was called with a NULL dma";
    if (dma == nullptr) {
        return HAL_ERROR;
    }
    dma->Instance = new DMA_Stream_TypeDef;
    return dma->Instance != nullptr ? HAL_OK : HAL_ERROR;
}
}

UartMock::UartMock(USART_TypeDef* uart) noexcept : uart(uart) {
    UartMockManager::mocks[uart] = this;
}

UartMock::~UartMock() {
    UartMockManager::mocks.erase(this->uart);
}

HAL_StatusTypeDef UartMock::transmit(const void* data, size_t size) {
    if (this->errorState) {
        return HAL_ERROR;
    }
    const size_t oldSize = this->transmitBuffer.size();
    this->transmitBuffer.resize(oldSize + size);
    std::memcpy(&this->transmitBuffer[oldSize], data, size);
    return HAL_OK;
}

void UartMock::setErrorState(bool enabled) {
    this->errorState = enabled;
}

void UartMock::receive(const void* data, size_t size) {
    if (this->config == nullptr) {
        return;
    }
    if (this->getReceiveMethod() == ReceiveMethod::DMA) {
        size_t spaceLeft = this->config->hdmarx->Instance->NDTR;
        if (size <= spaceLeft) {
            std::memcpy(&((uint8_t*) this->receiveBuffer)[this->receiveBufferSize - spaceLeft],
                data, size);
            this->config->hdmarx->Instance->NDTR -= size;
        } else {
            std::memcpy(&((uint8_t*) this->receiveBuffer)[this->receiveBufferSize - spaceLeft],
                data, spaceLeft);
            std::memcpy(this->receiveBuffer, &((uint8_t*) data)[spaceLeft], size - spaceLeft);
            this->config->hdmarx->Instance->NDTR = this->receiveBufferSize - (size - spaceLeft);
        }
    }
    if (this->hasInterruptEnabled(UART_IT_IDLE)) {
        HAL_UARTEx_RxEventCallback(this->config, size);
    } // Only idle interrupts is implemented for now
}

void UartMock::popTransmittedData(std::vector<uint8_t>& buffer) {
    buffer = this->transmitBuffer;
    this->transmitBuffer.clear();
}

std::string UartMock::popTransmittedData() {
    std::vector<uint8_t> buffer;
    this->popTransmittedData(buffer);
    return std::string(buffer.begin(), buffer.end());
}

void UartMock::receive(const std::string& data) {
    this->receive(data.c_str(), data.size());
}

bool UartMock::isInitialized() const {
    return this->config != nullptr;
}

ReceiveMethod UartMock::getReceiveMethod() const {
    return this->receiveMethod;
}

size_t UartMock::getReceiveBufferSize() const {
    return this->receiveBufferSize;
}

bool UartMock::hasInterruptEnabled(uint32_t interrupt) const {
    if (this->config == nullptr) {
        return false;
    }
    return __HAL_UART_GET_IT_SOURCE(this->config, interrupt) == SET;
}

HAL_StatusTypeDef UartMock::initializeWithFailure(UART_HandleTypeDef* configuration) {
    if (errorState) {
        return HAL_ERROR;
    }
    if (configuration->Instance != uart) {
        EXPECT_EQ(uart, configuration->Instance) << "Tried to initialize UartMock with wrong uart";
        return HAL_ERROR;
    }
    initialize(configuration);
    return HAL_OK;
}

void UartMock::initialize(UART_HandleTypeDef* configuration) {
    configuration->Instance = uart;
    this->config = configuration;
    this->config->Instance->ISR = 0;
    this->config->Instance->ICR = 0;
    errorState = false;
    this->transmitBuffer.clear();
}

void UartMock::deInitialize() {
    if (this->config != nullptr) {
        this->config->Instance = nullptr;
        this->config = nullptr;
    }
    this->errorState = false;
    this->transmitBuffer.clear();
}

HAL_StatusTypeDef UartMock::setReceiveMethod(ReceiveMethod method, void* buffer, size_t size) {
    if (this->receiveMethod != ReceiveMethod::None && method != this->receiveMethod) {
        EXPECT_EQ(this->receiveMethod, ReceiveMethod::None)
            << "Uart configured for two receive methods";
        return HAL_ERROR;
    }
    if (method == ReceiveMethod::DMA) {
        if (buffer == nullptr) {
            EXPECT_TRUE(buffer != nullptr) << "Received a null DMA buffer";
            return HAL_ERROR;
        }
        if (this->config == nullptr) {
            EXPECT_TRUE(this->config != nullptr) << "Enable DMA on uninitialized uart";
            return HAL_ERROR;
        }
        if (this->config->hdmarx == nullptr) {
            EXPECT_TRUE(this->config->hdmarx != nullptr)
                << "Enable DMA without configuring DMA reader";
            return HAL_ERROR;
        }
        this->config->hdmarx->Instance->NDTR = size;
    }
    this->receiveMethod = method;
    this->receiveBuffer = buffer;
    this->receiveBufferSize = size;
    return HAL_OK;
}

UartMock* UartMockManager::getUart(USART_TypeDef* uart) {
    try {
        return mocks.at(uart);
    } catch (std::out_of_range&) {
        for (auto& mock : mocks) {
            if (mock.second->config != nullptr && mock.second->config->Instance == uart) {
                return mock.second;
            }
        }
        throw std::runtime_error("Accessing uart without mock, add a mock to the UartMockManager");
    }
}

std::map<USART_TypeDef*, UartMock*> UartMockManager::mocks = {};

UartMock UartMockManager::iridium(UART4);
