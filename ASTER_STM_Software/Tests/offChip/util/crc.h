#pragma once
#include "overwrites.h"
extern "C" {
#include <stm32f7xx_hal.h>
}
#include "util/mock.h"


class Crc {
public:
    static Crc instance;
    void initialize(size_t newCrcLength = 16, size_t newInputDataAlignment = 8, size_t newInputInversion = 0,
        size_t newOutputInversion = 0, uint32_t newInitialValue = 7);
    bool initialize(CRC_HandleTypeDef* handle);

    void deInitialize();

    [[nodiscard]] bool isInitialized() const;

    [[nodiscard]] size_t getCrcLength() const;

    [[nodiscard]] size_t getInputDataAlignment() const;

    [[nodiscard]] size_t getInputInversion() const;

    [[nodiscard]] bool getOutputInversion() const;

    [[nodiscard]] uint32_t getInitialValue() const;

    void setFailureMode(bool enabled);

    CallResolver<uint32_t, const void*, size_t>::ExpectedCall expectChecksumCall(uint32_t checksum,
        const void* bufferArgument, size_t bufferSizeArgument);

    uint32_t doCalculate(CRC_HandleTypeDef* crcInstance, uint32_t buffer[], uint32_t bufferLength);

    uint32_t getInitialPolynomial() const;

private:
    explicit Crc(CRC_HandleTypeDef* crc) noexcept;
    CRC_HandleTypeDef* crc;
    CallResolver<uint32_t, const void*, size_t> calculateChecksumResolver;
    bool initialized = false;
    bool failureMode = false;
    size_t crcLength = 16;
    size_t inputDataAlignment = 8;
    size_t inputInversion = 0;
    bool outputInversion = false;
    uint32_t initialValue = 7;
    uint32_t initialPolynomial = 0;
};
