#pragma once

#include <stm32f7xx_hal.h>
// Include the unit header ahead of time to ensure including it in a C++ context.
#include "generated/units.h"

#ifdef __cplusplus
extern "C" {
#endif
extern USART_TypeDef UART_MEMORY[];
extern void HAL_ENABLE_PORT_CLOCK(GPIO_TypeDef* port);
#ifdef __cplusplus
}
#endif

#undef UART4
#define UART4 (&UART_MEMORY[0])

#define main test_main

#undef __HAL_RCC_UART4_CLK_ENABLE
#define __HAL_RCC_UART4_CLK_ENABLE() ((void) 0)
#undef __HAL_RCC_UART7_CLK_ENABLE
#define __HAL_RCC_UART7_CLK_ENABLE() ((void) 0)
#undef __HAL_RCC_USART1_CLK_ENABLE
#define __HAL_RCC_USART1_CLK_ENABLE() ((void) 0)
#undef __HAL_RCC_USART2_CLK_ENABLE
#define __HAL_RCC_USART2_CLK_ENABLE() ((void) 0)
#undef __HAL_RCC_USART3_CLK_ENABLE
#define __HAL_RCC_USART3_CLK_ENABLE() ((void) 0)

#undef __HAL_RCC_GPIOA_CLK_ENABLE
#define __HAL_RCC_GPIOA_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOA)
#undef __HAL_RCC_GPIOB_CLK_ENABLE
#define __HAL_RCC_GPIOB_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOB)
#undef __HAL_RCC_GPIOC_CLK_ENABLE
#define __HAL_RCC_GPIOC_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOC)
#undef __HAL_RCC_GPIOD_CLK_ENABLE
#define __HAL_RCC_GPIOD_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOD)
#undef __HAL_RCC_GPIOE_CLK_ENABLE
#define __HAL_RCC_GPIOE_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOE)
#undef __HAL_RCC_GPIOF_CLK_ENABLE
#define __HAL_RCC_GPIOF_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOF)
#undef __HAL_RCC_GPIOG_CLK_ENABLE
#define __HAL_RCC_GPIOG_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOG)
#undef __HAL_RCC_GPIOH_CLK_ENABLE
#define __HAL_RCC_GPIOH_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOH)
#undef __HAL_RCC_GPIOI_CLK_ENABLE
#define __HAL_RCC_GPIOI_CLK_ENABLE() HAL_ENABLE_PORT_CLOCK(GPIOI)
