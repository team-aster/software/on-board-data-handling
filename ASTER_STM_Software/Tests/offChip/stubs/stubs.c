#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <ff.h>
#ifdef LPTR
#    undef LPTR
#endif
#ifdef ERROR
#    undef ERROR
#endif

#include <stm32f7xx_hal.h>
#include "main.h"
#include "globals.h"
#include "data/system.h"
#include "overwrites.h"


struct _reent * _REENT = {0}; // NOLINT(bugprone-reserved-identifier)

void print(const char* format, ...) {
    va_list argList;
    va_start(argList, format);
    vprintf(format, argList);
    va_end(argList);
    printf("\n");
}

void printCriticalError(const char* format, ...) {
    printf("[CRITICAL] ");
    va_list argList;
    va_start(argList, format);
    vprintf(format, argList);
    va_end(argList);
    printf("\n");
}

void printError(const char* format, ...) {
    printf("[ERROR] ");
    va_list argList;
    va_start(argList, format);
    vprintf(format, argList);
    va_end(argList);
    printf("\n");
}

void printDebug(const char* format, ...) {
    printf("[DEBUG] ");
    va_list argList;
    va_start(argList, format);
    vprintf(format, argList);
    va_end(argList);
    printf("\n");
}

void internalSoftwareReset(void) {

}

void HAL_NVIC_SetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority) {
    (void) IRQn; (void) PreemptPriority; (void) SubPriority;
}

void HAL_NVIC_EnableIRQ(IRQn_Type IRQn) {
    (void) IRQn;
}

void HAL_UART_IRQHandler(UART_HandleTypeDef* uart) {
    (void) uart;
}

void HAL_DMA_IRQHandler(DMA_HandleTypeDef* dma) {
    (void) dma;
}

uint32_t getFlightTimer(){
    return 0;
}

uint32_t osKernelGetTickCount (void) {
    return 0;
}

void refreshWatchdogTimer() {
}

osStatus_t osDelayUntil (uint32_t ticks) {
    return osOK;
}

osStatus_t osDelay (uint32_t ticks) {
    return osOK;
}

void set_motorEnabled(const Vec3Uint32* value) {

}

bool set_systemIridiumState(const IridiumState* value) {
    return false;
}

bool get_systemIridiumState(IridiumState* value) {
    return false;
}

HAL_StatusTypeDef HAL_Init(void) {
    return HAL_OK;
}

osStatus_t osKernelInitialize (void) {
    return osOK;
}

void initSystem(void) {
}

osThreadId_t osThreadNew (osThreadFunc_t func, void *argument, const osThreadAttr_t *attr) {
    return NULL;
}

noreturn void StartControlTask(void* argument) {
    while (1) {}
}

noreturn void StartRecoveryTask(void* argument) {
    while (1) {}
}

noreturn void StartAdcsTask(void* argument) {
    while (1) {}
}

noreturn void StartSensorsTask(void* argument) {
    while (1) {}
}

noreturn void StartTelecommandTask(void* argument) {
    while (1) {}
}

noreturn void StartTelemetryTask(void* argument) {
    while (1) {}
}

noreturn void StartWatchdogTask(void* argument) {
    while (1) {}
}

osStatus_t osKernelStart (void) {
    return osOK;
}

HAL_StatusTypeDef HAL_RCC_OscConfig(RCC_OscInitTypeDef  *RCC_OscInitStruct){
    return HAL_OK;
}

HAL_StatusTypeDef HAL_PWREx_EnableOverDrive(void) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_RCCEx_PeriphCLKConfig(RCC_PeriphCLKInitTypeDef  *PeriphClkInit) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_ADC_Init(ADC_HandleTypeDef* hadc) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_ADC_ConfigChannel(ADC_HandleTypeDef* hadc, ADC_ChannelConfTypeDef* sConfig) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_RCC_ClockConfig(RCC_ClkInitTypeDef *RCC_ClkInitStruct, uint32_t FLatency) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *hi2c) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_I2CEx_ConfigAnalogFilter(I2C_HandleTypeDef *hi2c, uint32_t AnalogFilter) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_I2CEx_ConfigDigitalFilter(I2C_HandleTypeDef *hi2c, uint32_t DigitalFilter) {
    return HAL_OK;
}

uint32_t getWatchdogReloadValue(void) {
    return 0;
}

HAL_StatusTypeDef HAL_IWDG_Init(IWDG_HandleTypeDef *hiwdg) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIM_Base_Init(TIM_HandleTypeDef *htim) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIM_ConfigClockSource(TIM_HandleTypeDef *htim, TIM_ClockConfigTypeDef *sClockSourceConfig) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIM_PWM_Init(TIM_HandleTypeDef *htim) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIMEx_MasterConfigSynchronization(TIM_HandleTypeDef *htim,
                                                        TIM_MasterConfigTypeDef *sMasterConfig) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIM_PWM_ConfigChannel(TIM_HandleTypeDef *htim, TIM_OC_InitTypeDef *sConfig, uint32_t Channel) {
    return HAL_OK;
}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim) {

}

HAL_StatusTypeDef HAL_TIM_IC_Init(TIM_HandleTypeDef *htim) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIM_IC_ConfigChannel(TIM_HandleTypeDef *htim, TIM_IC_InitTypeDef *sConfig, uint32_t Channel) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_TIMEx_RemapConfig(TIM_HandleTypeDef *htim, uint32_t Remap) {
    return HAL_OK;
}

void HAL_IncTick(void) {

}

osMessageQueueId_t osMessageQueueNew (uint32_t msg_count, uint32_t msg_size, const osMessageQueueAttr_t *attr) {
    return 0;
}

bool initRXSM(void) {
    return true;
}

bool initImuCommunication(void) {
    return true;
}

bool initPayloadCommunication(void) {
    return true;
}

osStatus_t osMessageQueueGet (osMessageQueueId_t mq_id, void *msg_ptr, uint8_t *msg_prio, uint32_t timeout) {
    return osError;
}

bool writeToRXSM(const void* data, size_t dataLength) {
    return true;
}

osStatus_t osMessageQueuePut (osMessageQueueId_t mq_id, const void *msg_ptr, uint8_t msg_prio, uint32_t timeout) {
    return osError;
}

HAL_StatusTypeDef HAL_SPI_Transmit(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout) {
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout) {
    return HAL_ERROR;
}

bool initializeSDCardCommunication(void) {
    return true;
}

void onRXSMSendCompleted(void) {

}

void onGpsDataReceived(void) {

}

void onGpsError(void) {

}

bool initGps(void) {
    return true;
}

HAL_SPI_StateTypeDef HAL_SPI_GetState(SPI_HandleTypeDef *hspi) {
    return HAL_SPI_STATE_READY;
}

void onRXSMDataReceived(void) {

}

void onRXSMError(void) {

}

// TODO: Temp workaround
extern DMA_HandleTypeDef hdma_uart4_rx;
extern DMA_HandleTypeDef hdma_usart1_rx;

void HAL_UART_MspInit(UART_HandleTypeDef* huart) {
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(huart->Instance==UART4)
    {
        /* USER CODE BEGIN UART4_MspInit 0 */
        
        /* USER CODE END UART4_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_UART4_CLK_ENABLE();
        
        __HAL_RCC_GPIOC_CLK_ENABLE();
        /**UART4 GPIO Configuration
        PC10     ------> UART4_TX
        PC11     ------> UART4_RX
        */
        GPIO_InitStruct.Pin = IRIDIUM_TX_Pin|IRIDIUM_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
        
        /* UART4 DMA Init */
        /* UART4_RX Init */
        hdma_uart4_rx.Instance = DMA1_Stream2;
        hdma_uart4_rx.Init.Channel = DMA_CHANNEL_4;
        hdma_uart4_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_uart4_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_uart4_rx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_uart4_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_uart4_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_uart4_rx.Init.Mode = DMA_CIRCULAR;
        hdma_uart4_rx.Init.Priority = DMA_PRIORITY_LOW;
        hdma_uart4_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
        if (HAL_DMA_Init(&hdma_uart4_rx) != HAL_OK)
        {
            Error_Handler();
        }
        
        __HAL_LINKDMA(huart,hdmarx,hdma_uart4_rx);
        
        /* UART4 interrupt Init */
        HAL_NVIC_SetPriority(UART4_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(UART4_IRQn);
        /* USER CODE BEGIN UART4_MspInit 1 */
        
        /* USER CODE END UART4_MspInit 1 */
    }
    else if(huart->Instance==UART7)
    {
        /* USER CODE BEGIN UART7_MspInit 0 */
        
        /* USER CODE END UART7_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_UART7_CLK_ENABLE();
        
        __HAL_RCC_GPIOE_CLK_ENABLE();
        /**UART7 GPIO Configuration
        PE7     ------> UART7_RX
        PE8     ------> UART7_TX
        */
        GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF8_UART7;
        HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
        
        /* USER CODE BEGIN UART7_MspInit 1 */
        
        /* USER CODE END UART7_MspInit 1 */
    }
    else if(huart->Instance==USART1)
    {
        /* USER CODE BEGIN USART1_MspInit 0 */
        
        /* USER CODE END USART1_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_USART1_CLK_ENABLE();
        
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART1 GPIO Configuration
        PA9     ------> USART1_TX
        PA10     ------> USART1_RX
        */
        GPIO_InitStruct.Pin = GPS_TX_Pin|GPS_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        
        /* USART1 DMA Init */
        /* USART1_RX Init */
        hdma_usart1_rx.Instance = DMA2_Stream2;
        hdma_usart1_rx.Init.Channel = DMA_CHANNEL_4;
        hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_usart1_rx.Init.Mode = DMA_CIRCULAR;
        hdma_usart1_rx.Init.Priority = DMA_PRIORITY_LOW;
        hdma_usart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
        if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK)
        {
            Error_Handler();
        }
        
        __HAL_LINKDMA(huart,hdmarx,hdma_usart1_rx);
        
        /* USART1 interrupt Init */
        HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
        /* USER CODE BEGIN USART1_MspInit 1 */
        
        /* USER CODE END USART1_MspInit 1 */
    }
    else if(huart->Instance==USART2)
    {
        /* USER CODE BEGIN USART2_MspInit 0 */
        
        /* USER CODE END USART2_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();
        
        __HAL_RCC_GPIOD_CLK_ENABLE();
        /**USART2 GPIO Configuration
        PD5     ------> USART2_TX
        PD6     ------> USART2_RX
        */
        GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
        HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
        
        /* USER CODE BEGIN USART2_MspInit 1 */
        
        /* USER CODE END USART2_MspInit 1 */
    }
    else if(huart->Instance==USART3)
    {
        /* USER CODE BEGIN USART3_MspInit 0 */
        
        /* USER CODE END USART3_MspInit 0 */
        /* Peripheral clock enable */
        __HAL_RCC_USART3_CLK_ENABLE();
        
        __HAL_RCC_GPIOD_CLK_ENABLE();
        /**USART3 GPIO Configuration
        PD8     ------> USART3_TX
        PD9     ------> USART3_RX
        */
        GPIO_InitStruct.Pin = RXSM_TX_Pin|RXSM_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
        HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
        
        /* USART3 interrupt Init */
        HAL_NVIC_SetPriority(USART3_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(USART3_IRQn);
        /* USER CODE BEGIN USART3_MspInit 1 */
        
        /* USER CODE END USART3_MspInit 1 */
    }
}
