#pragma once

struct _reent {  // NOLINT(bugprone-reserved-identifier)
    int dummy;
};

extern struct _reent * _REENT;

