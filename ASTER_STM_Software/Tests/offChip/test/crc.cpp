#include "stubs/overwrites.h"
extern "C" {
#include "objects/crc.c"
}

#include <gtest/gtest.h>
#include "util/crc.h"
using namespace testing;


TEST(CRCTest, initialization) {
    Crc::instance.deInitialize();
    EXPECT_TRUE(initCRC()) << "Expected initCRC to successfully initialize the CRC peripheral";
    EXPECT_TRUE(Crc::instance.isInitialized()) << "Expected initCRC to initialize the CRC";
    EXPECT_EQ(Crc::instance.getCrcLength(), 16)
        << "Expected initCRC to configure the CRC for 16 bit length";
    EXPECT_EQ(Crc::instance.getInputDataAlignment(), 1)
        << "Expected initCRC to configure the CRC input format to bytes";
    EXPECT_EQ(Crc::instance.getInputInversion(), 0)
        << "Expected initCRC to configure the CRC input inversion to None";
    EXPECT_FALSE(Crc::instance.getOutputInversion())
        << "Expected initCRC to configure the CRC output inversion to disabled";
    EXPECT_EQ(Crc::instance.getInitialValue(), 0)
        << "Expected initCRC to configure no initial crc value";
    EXPECT_EQ(Crc::instance.getInitialPolynomial(), 34833)
        << "Expected initCRC to configure the initial polynomial";

    Crc::instance.deInitialize();
    Crc::instance.setFailureMode(true);
    EXPECT_FALSE(initCRC())
        << "Expected initCRC to propagate initialization errors of the CRC peripheral";
}

TEST(CRCTest, calculateChecksum) {
    Crc::instance.initialize(16);
    char buffer[] = "Hello World";
    uint16_t checksum = 1234;
    auto callResolver = Crc::instance.expectChecksumCall(checksum, buffer, ARRAY_SIZE(buffer));
    EXPECT_EQ(calculateChecksum(buffer, ARRAY_SIZE(buffer)), checksum)
        << "Expected calculateChecksum to return the correct checksum "
           "if the crc peripheral is not initialized";
    EXPECT_TRUE(callResolver.wasCalled())
        << "Expected calculateChecksum to delegate the checksum creation to the crc peripheral";
    Crc::instance.deInitialize();
    EXPECT_EQ(calculateChecksum(buffer, ARRAY_SIZE(buffer)), 0)
        << "Expected calculateChecksum to return a 0 checksum "
           "if the crc peripheral is not initialized";
}
