#include "stubs/overwrites.h"
extern "C" {
#include "objects/ringBuffer.c"
}

#include <gtest/gtest.h>

using namespace testing;


TEST(RingBufferTest, initialization) {
    DEFINE_RING_BUFFER(testBuffer, 4);
    ASSERT_EQ(rbRead(&testBuffer, nullptr, 0), 0)
        << "Expected a newly created ring buffer to be empty";
}

TEST(RingBufferTest, write) {
    DEFINE_RING_BUFFER(testBuffer, 4);
    const char* testData1 = "123";
    const char* testData2 = "456";
    const char* testData3 = "0123456789";
    char compareBuffer[5];
    memset(compareBuffer, '\0', ARRAY_SIZE(compareBuffer));

    rbAppend(&testBuffer, testData1, strlen(testData1));
    memcpy(compareBuffer, testBufferMemory, 3);
    ASSERT_STREQ(compareBuffer, testData1)
        << "Expected rbAppend to correctly write the 3 bytes at the beginning of the buffer";

    rbAppend(&testBuffer, testData2, strlen(testData2));
    memcpy(compareBuffer, testBufferMemory, 4);
    ASSERT_STREQ(compareBuffer, "5634")
        << "Expected rbAppend to correctly append the 3 bytes at to the end of the previous write "
           "and correctly wrap around";

    rbAppend(&testBuffer, testData3, strlen(testData3));
    memcpy(compareBuffer, testBufferMemory, 4);
    ASSERT_STREQ(compareBuffer, "6789")
        << "Expected rbAppend to correctly append a string which is "
           "longer than the buffer and correctly wrap around";
}

TEST(RingBufferTest, read) {
    DEFINE_RING_BUFFER(testBuffer, 4);
    char readBuffer[5];

    rbAppend(&testBuffer, "12", 2);
    ASSERT_EQ(rbRead(&testBuffer, nullptr, 0), 2)
        << "Expected rbRead to return the correct number of bytes available in the buffer";
    rbAppend(&testBuffer, "3", 1);
    ASSERT_EQ(rbRead(&testBuffer, nullptr, 0), 3)
        << "Expected rbRead to return the correct number of bytes available in the buffer after "
           "appending an additional byte";
    memset(readBuffer, '\0', ARRAY_SIZE(readBuffer));
    ASSERT_EQ(rbRead(&testBuffer, readBuffer, 2), 2)
        << "Expected rbRead to read only the number of bytes specified even if there are more "
           "available";
    ASSERT_STREQ(readBuffer, "12") << "Expected rbRead to correctly read bytes from the buffer";
    memset(readBuffer, '\0', ARRAY_SIZE(readBuffer));
    ASSERT_EQ(rbRead(&testBuffer, readBuffer, 4), 1)
        << "Expected rbRead to read only the number of available bytes even if the caller wants "
           "more";
    ASSERT_STREQ(readBuffer, "3") << "Expected rbRead to correctly read bytes from the buffer";
    memset(readBuffer, '\0', ARRAY_SIZE(readBuffer));

    rbAppend(&testBuffer, "456", 3);
    ASSERT_EQ(rbRead(&testBuffer, nullptr, 0), 3)
        << "Expected rbRead to return the correct number of bytes available in the buffer even if "
           "the buffer area wraps around";
    ASSERT_EQ(rbRead(&testBuffer, readBuffer, 4), 3)
        << "Expected rbRead to read only the number of available bytes even if the buffer area "
           "wraps around";
    ASSERT_STREQ(readBuffer, "456") << "Expected rbRead to correctly read bytes from the buffer "
                                       "even if the buffer area wraps around";
}
