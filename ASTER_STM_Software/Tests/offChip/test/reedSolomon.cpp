#include "stubs/overwrites.h"
extern "C" {
#include "objects/reedSolomon.c"
}

#include <gtest/gtest.h>
#include "globals.h"

using namespace testing;

typedef struct {
    const char* input;
    const char* output;
} testCase_t;

static std::string formatParity(const char* parity, size_t length) {
    std::stringstream stream;
    stream << std::setfill('0') << std::setw(2) << std::right << std::hex << (parity[0] & 0xFF);
    for (size_t i = 1; i < length; i++) {
        stream << ' ' << std::setfill('0') << std::setw(2) << std::right << std::hex
               << (parity[i] & 0xFF);
    }
    return stream.str();
}

TEST(ReedSolomonTest, encode_16_13) {
    const testCase_t testCases[] = {
        {.input = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c", .output = "\xc0\x61\x4b"},
        {.input = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", .output = "\x00\x00\x00"},
        {.input = "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff", .output = "\x5c\x79\xd7"},
        {.input = "\x98\x19\x70\x22\x0b\x49\x23\x96\xfa\xdb\x1b\xa8\x0e", .output = "\x29\xf2\xe9"},
        {.input = "\xe5\x8a\x95\x6c\x92\xa1\x06\x9c\xf5\x93\xec\x85\xfe", .output = "\x49\x31\xf9"},
        {.input = "\xd4\xe1\xec\xc8\x75\xce\xc6\xbe\x7b\x9a\x1a\xdf\x29", .output = "\xf8\x56\x4e"},
        {.input = "\x7b\x72\x1f\x5e\xf8\x41\x9b\x3c\x16\x26\xfa\x42\xd2", .output = "\x36\x1e\x73"},
        {.input = "\x16\x1b\x8f\xa4\x94\x48\xb6\xfa\xb0\xa7\x70\x1a\x09", .output = "\x1c\xb3\xba"},
        {.input = "\x0b\x22\xda\xb8\xe0\x06\x19\x0c\x39\x14\x37\xe5\xca", .output = "\x5b\x4d\xad"},
    };
    char parity[3] = {};
    for (auto testCase : testCases) {
        memset(parity, 0, ARRAY_SIZE(parity));
        calculateParity16_13(testCase.input, (uint8_t*) parity);
        const std::string expectedParity = formatParity(testCase.output, ARRAY_SIZE(parity));
        const std::string actualParity = formatParity(parity, ARRAY_SIZE(parity));
        EXPECT_EQ(expectedParity, actualParity)
            << "Expected calculateParity16_13 to calculate the correct parity";
    }
}

TEST(ReedSolomonTest, encode_32_28) {
    const testCase_t testCases[] = {
        {.input = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13"
                  "\x14\x15\x16\x17\x18\x19\x1a\x1b",
            .output = "\x57\x20\x2f\xac"},
        {.input = "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
                  "\x00\x00\x00\x00\x00\x00\x00\x00",
            .output = "\x00\x00\x00\x00"},
        {.input = "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff"
                  "\xff\xff\xff\xff\xff\xff\xff\xff",
            .output = "\xab\x5c\xe4\x0b"},
        {.input = "\x22\x03\x3e\x69\x2f\x40\x35\xe3\xef\x10\xd0\x5b\x07\x39\x1a\xe3\x70\x4f\x3a\x25"
                  "\x80\xa1\x37\x82\x87\x3d\x21\x1e",
            .output = "\xf9\x0a\x63\xad"},
        {.input = "\xe6\x91\x60\x86\xdb\x94\x90\xbf\x0a\xb6\x37\x26\xa2\x73\xc0\xcd\xdb\x7c\x67\x44"
                  "\x23\x42\x08\xe2\x77\x5d\x70\xb2",
            .output = "\x46\xb0\x2f\xe3"},
        {.input = "\x95\x0a\x62\x04\x6c\x98\x15\xc9\x77\xd4\xbf\x5e\x9c\xf3\x23\x29\x75\x47\xdb\x26"
                  "\x23\x19\x78\x04\xcb\xf6\x84\xf2",
            .output = "\x8a\x39\x8d\x55"},
        {.input = "\xe1\xc7\x9f\x97\x17\x49\x1a\xe6\x2c\x23\xd9\x86\xd4\xc5\x3d\x77\x2e\x94\x8b\x43"
                  "\x94\x4d\xe7\x36\xfa\x65\x9d\xff",
            .output = "\x64\x97\x38\x23"},
        {.input = "\xe3\x05\x05\xcc\x13\xbf\xdb\xd1\x9a\x36\x8a\x57\x2c\x0f\x18\x3e\x34\x8b\xfc\xf2"
                  "\xa1\xcc\x30\x26\x99\xa0\x21\xf0",
            .output = "\x6c\x69\x1b\x95"},
        {.input = "\xeb\xb6\xbb\x88\xff\xbc\xdc\x92\x1b\x7e\x89\xd9\x5d\x0c\x80\x15\x71\x6e\x38\xb0"
                  "\x94\x74\xf8\x87\x62\x3b\xe8\xbf",
            .output = "\xce\x08\xe9\xa7"},
        {.input = "\xf1\xd6\x6e\x9e\x73\x4c\x01\x09\xa5\xa5\xbd\x23\x39\xf0\xdd\x05\xdb\xde\x63\x41"
                  "\x44\x63\x1c\x0c\xb8\xf0\x1b\x8d",
            .output = "\x44\xa6\x52\x33"},
        {.input = "\x8d\xc9\xcd\xa7\x8f\x0c\x2d\x88\x82\x72\x1d\x26\xf6\x09\x77\xb9\x82\x47\x13\x38"
                  "\x00\x84\xa2\xbd\xaa\x1c\x79\x88",
            .output = "\x0d\x7b\x1d\xaa"},
    };
    char parity[4];
    for (auto testCase : testCases) {
        memset(parity, 0, ARRAY_SIZE(parity));
        calculateParity32_28(testCase.input, (uint8_t*) parity);
        const std::string expectedParity = formatParity(testCase.output, ARRAY_SIZE(parity));
        const std::string actualParity = formatParity(parity, ARRAY_SIZE(parity));
        EXPECT_EQ(expectedParity, actualParity)
            << "Expected calculateParity32_28 to calculate the correct parity";
    }
}
