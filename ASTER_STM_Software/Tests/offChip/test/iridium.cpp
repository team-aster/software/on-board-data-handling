#include "stubs/overwrites.h"
#include "util/uart.h"

extern "C" {
#include "objects/communication/iridium.c"
}

#include <gtest/gtest.h>

using namespace testing;

TEST(IridiumTest, initialization) {
    UartMockManager::iridium.deInitialize();
    ASSERT_TRUE(initIridiumUart()) << "Expected the Iridium uart initialization to succeed";
    EXPECT_TRUE(UartMockManager::iridium.isInitialized())
        << "Expected Iridium communication to initialize on the UART4 port";
    EXPECT_EQ(UartMockManager::iridium.getReceiveMethod(), ReceiveMethod::DMA)
        << "Expected Iridium communication to use direct memory access for receiving";
    EXPECT_GE(UartMockManager::iridium.getReceiveBufferSize(), 128)
        << "Expected Iridium communication buffer to be larger or equal to the maximum length of "
           "an iridium message";
    EXPECT_TRUE(UartMockManager::iridium.hasInterruptEnabled(UART_IT_IDLE))
        << "Expected Iridium communication to enable the idle line interrupt";

    EXPECT_TRUE(startIridiumInitialization()) << "Expected the Iridium initialization to succeed";
    ASSERT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "AT\r")
        << "Expected startIridiumInitialization to send a communication check request";
    UartMockManager::iridium.receive("AT\r\r\nERROR\r\n");
    EXPECT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "")
        << "Expected no communication on a failed communication check response";
    UartMockManager::iridium.receive("AT\r\r\nOK\r\n");
    ASSERT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "AT+CGMI\r")
        << "Expected a manufacturer request on a communication check response";
    UartMockManager::iridium.receive("AT+CGMI\r\r\nWrong\r\n\r\nOK\r\n");
    EXPECT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "")
        << "Expected no communication if the manufacturer is wrong";
    UartMockManager::iridium.receive("AT+CGMI\r\r\nIridium\r\n\r\nOK\r\n");
    ASSERT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "AT+CIER=1,0,1\r")
        << "Expected a request to enable signal status reporting on a manufacturer check response";
}

TEST(IridiumTest, initializationFailure) {
    UartMockManager::iridium.deInitialize();
    UartMockManager::iridium.setErrorState(true);
    EXPECT_FALSE(startIridiumInitialization())
        << "Expected startIridiumInitialization to propagate write errors";

    UartMockManager::iridium.deInitialize();
    EXPECT_FALSE(startIridiumInitialization())
        << "Expected startIridiumInitialization to error if the Iridium chip is not initialized";
}

TEST(IridiumTest, iridiumMessageTransmission) {
    UartMockManager::iridium.initialize(&iridiumUart);
    ASSERT_FALSE(sendIridiumMessage("hello", 51))
        << "Expected sendIridiumMessage to reject messages larger than 50 bytes";

    const char* transmissionResponses[] = {"\r\n1\r\n\r\nOK\r\n", "\r\n0\r\n\r\nERROR\r\n",
        "\r\n0\r\n\r\nOK\r\n"};
    for (size_t i = 0; i < ARRAY_SIZE(transmissionResponses); i++) {
        ASSERT_TRUE(sendIridiumMessage("hello", 5))
            << "Expected sendIridiumMessage to successfully initiate a transmission";
        ASSERT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "AT+SBDWB=5\r")
            << "Expected sendIridiumMessage to send a data transmission initialization request";
        UartMockManager::iridium.receive("AT+SBDWB=5\r\r\nFAIL\r\n");
        EXPECT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "")
            << "Expected no transmission of data if the request failed";
        UartMockManager::iridium.receive("AT+SBDWB=5\r\r\nREADY\r\n");
        EXPECT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "hello\x02\x14")
            << "Expected the transmission of the specified data with the correct checksum";
        UartMockManager::iridium.receive(transmissionResponses[i]);
        if (i < 2) {
            EXPECT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "")
                << "Expected no communication on a failed transmission";
        }
    }
    ASSERT_STREQ(UartMockManager::iridium.popTransmittedData().c_str(), "AT+SBDIX\r")
        << "Expected a send buffer command after a successful message transmission";
}

TEST(IridiumTest, iridiumMessageTransmissionWithoutIridiumChip) {
    UartMockManager::iridium.initialize(&iridiumUart);
    UartMockManager::iridium.setErrorState(true);
    EXPECT_FALSE(sendIridiumMessage("hello", 5))
        << "Expected sendIridiumMessage to propagate write errors";

    UartMockManager::iridium.deInitialize();
    EXPECT_FALSE(sendIridiumMessage("hello", 5))
        << "Expected sendIridiumMessage to error if the Iridium chip is not initialized";
}
