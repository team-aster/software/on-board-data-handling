#include "stubs/overwrites.h"
extern "C" {
#include "objects/gpio.c"
}

#include <gtest/gtest.h>
#include "util/pin.h"
using namespace testing;


TEST(GPIOTest, initialization) {
    std::vector<PinMock*> allPins = PinMockManager::allPins();
    for (PinMock* pinMock : allPins) {
        pinMock->setState(UNDEFINED_STATE);
    }
    initGPIO();
    for (PinMock* pinMock : allPins) {
        if (pinMock == &PinMockManager::iridiumRx || pinMock == &PinMockManager::iridiumTx ||
            pinMock == &PinMockManager::iridiumClearToSend ||
            pinMock == &PinMockManager::iridiumReadyToSend) {
            continue;
        }
        EXPECT_TRUE(PinMockManager::isPortClockEnabled(pinMock->pin.port))
            << "Expected initGPIO to enable the port clock for pin " << pinMock->name();
        PinIoMode ioMode = pinMock->getIoMode();
        EXPECT_NE(UNDEFINED_MODE, ioMode)
            << "Expected initGPIO to initialize the IO mode of pin " << pinMock->name();
        if (ioMode == OUTPUT_MODE) {
            EXPECT_NE(UNDEFINED_STATE, pinMock->getState())
                << "Expected initGPIO to initialize the output state of pin " << pinMock->name();
        }
    }
}

TEST(GPIOTest, getPin) {
    for (PinMock* pinMock : PinMockManager::allPins()) {
        pinMock->setState(HIGH_STATE);
        EXPECT_EQ(HIGH, getPin(pinMock->pin))
            << "Expected getPin to return HIGH for pin " << pinMock->name() << " which is high";
        pinMock->setState(LOW_STATE);
        EXPECT_EQ(LOW, getPin(pinMock->pin))
            << "Expected getPin to return LOW for pin " << pinMock->name() << " which is low";
    }
}

TEST(GPIOTest, setPin) {
    for (PinMock* pinMock : PinMockManager::allPins()) {
        pinMock->setState(LOW_STATE);
        setPin(pinMock->pin, HIGH);
        EXPECT_EQ(HIGH_STATE, pinMock->getState())
            << "Expected setPin to be able to set low pin " << pinMock->name() << " to high";
        setPin(pinMock->pin, LOW);
        EXPECT_EQ(LOW_STATE, pinMock->getState())
            << "Expected setPin to be able to set high pin " << pinMock->name() << " to low";
        setPin(pinMock->pin, TOGGLE);
        EXPECT_EQ(HIGH_STATE, pinMock->getState())
            << "Expected setPin to be able to toggle low pin " << pinMock->name() << " to high";
        setPin(pinMock->pin, TOGGLE);
        EXPECT_EQ(LOW_STATE, pinMock->getState())
            << "Expected setPin to be able to toggle high pin " << pinMock->name() << " to low";
    }
}